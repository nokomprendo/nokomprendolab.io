
-- https://wiki.haskell.org/Arrow_tutorial

{-# LANGUAGE Arrows #-}
{-# LANGUAGE ImportQualifiedPost #-}

import Control.Arrow
import Control.Category qualified as Cat

-------------------------------------------------------------------------------
-- type
-------------------------------------------------------------------------------

newtype Func b c = Func { runFunc :: b -> c }

-------------------------------------------------------------------------------
-- Category
-------------------------------------------------------------------------------

instance Cat.Category Func where

  id :: Func b b
  id = Func id

  (.) :: Func c d -> Func b c -> Func b d
  g . f = Func (runFunc g . runFunc f)
  -- Func q . Func p = Func (q . p)

-------------------------------------------------------------------------------
-- Arrow
-------------------------------------------------------------------------------

instance Arrow Func where

  arr :: (b -> c) -> Func b c
  arr = Func

  first :: Func b c -> Func (b, d) (c, d)
  first f = Func $ \(x, y) -> (runFunc f x, y)
  -- first (Func p) = Func $ \(x, y) -> (p x, y)

-------------------------------------------------------------------------------
-- exemples d'utilisation
-------------------------------------------------------------------------------

fPlus1 :: Func Int Int
fPlus1 = arr (+1)

fMul2 :: Func Int Int
fMul2 = arr (*2)

-- composition
fMyProc1 :: Func Int Int
fMyProc1 = fPlus1 >>> fMul2

-- idem fMyProc3 mais avec des opérateurs
fMyProc2 :: Func Int Int
fMyProc2 = fPlus1 >>> fMul2 >>> (arr (*3) &&& arr id) >>> arr (uncurry (+))

fMyProc3 :: Func Int Int
fMyProc3 = proc x -> do
  y <- fPlus1 -< x
  z <- fMul2 -< y
  u1 <- arr (*3) -< z
  returnA -< u1 + z

-------------------------------------------------------------------------------
-- application
-------------------------------------------------------------------------------

main :: IO ()
main = do
  putStrLn "Func"
  print $ runFunc fPlus1 41
  print $ runFunc fMul2 21
  print $ runFunc fMyProc1 1
  print $ runFunc fMyProc2 1
  print $ runFunc fMyProc3 1

