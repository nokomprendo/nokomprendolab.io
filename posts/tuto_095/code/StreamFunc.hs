
-- https://www.cse.chalmers.se/~rjmh/afp-arrows.pdf

{-# LANGUAGE ImportQualifiedPost #-}

import Control.Arrow
import Control.Category qualified as Cat
import Data.Char

-------------------------------------------------------------------------------
-- type
-------------------------------------------------------------------------------

newtype StreamFunc b c = StreamFunc { runStreamFunc :: [b] -> [c] }

-------------------------------------------------------------------------------
-- Category
-------------------------------------------------------------------------------

instance Cat.Category StreamFunc where

  id :: StreamFunc b b
  id = StreamFunc id

  (.) :: StreamFunc c d -> StreamFunc b c -> StreamFunc b d
  StreamFunc q . StreamFunc p = StreamFunc (q . p)

-------------------------------------------------------------------------------
-- Arrow
-------------------------------------------------------------------------------

instance Arrow StreamFunc where

  arr :: (b -> c) -> StreamFunc b c
  arr p = StreamFunc (map p)

  first :: StreamFunc b c -> StreamFunc (b, d) (c, d)
  first (StreamFunc p) = StreamFunc $ \b ->
    let (xs, ys) = unzip b
    in zip (p xs) ys

-------------------------------------------------------------------------------
-- exemples d'utilisation
-------------------------------------------------------------------------------

sfMul2 :: StreamFunc Int Int
sfMul2 = arr (*2)

sfC2I :: StreamFunc Char Int
sfC2I = arr digitToInt

-- composition
sfMyProc1 :: StreamFunc Char Int
sfMyProc1 = sfC2I >>> sfMul2

-- composition + entrée initiale
sfMyProc2 :: StreamFunc Char (Int, Char)
sfMyProc2 = sfC2I &&& arr id >>> first sfMul2

sfMyProc2' :: StreamFunc Char (Int, Char)
sfMyProc2' = arr (\x -> (x,x)) >>> first (sfC2I >>> sfMul2)

-- conversion + insérer 21 dans le flux
sfMyProc3 :: StreamFunc Char Int
sfMyProc3 = sfC2I >>> StreamFunc (21:)

-------------------------------------------------------------------------------
-- application
-------------------------------------------------------------------------------

main :: IO ()
main = do
  putStrLn "StreamFunc"
  print $ runStreamFunc sfMyProc1 "123"
  print $ runStreamFunc sfMyProc2 "123"
  print $ runStreamFunc sfMyProc2' "123"
  print $ runStreamFunc sfMyProc3 "123"


