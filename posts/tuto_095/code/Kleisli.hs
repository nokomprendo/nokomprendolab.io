
-- https://wiki.haskell.org/Arrow_tutorial

{-# LANGUAGE Arrows #-}

import Control.Arrow

{-
import Control.Arrow hiding (Kleisli(..))
import Control.Category qualified as Cat

-------------------------------------------------------------------------------
-- type
-------------------------------------------------------------------------------

newtype Kleisli m b c = Kleisli { runKleisli :: b -> m c }

instance Monad m => Cat.Category (Kleisli m) where

  id :: Monad m => Kleisli m b b
  id = Kleisli return

  (.) :: Monad m => Kleisli m c d -> Kleisli m b c -> Kleisli m b d
  Kleisli q . Kleisli p = Kleisli $ \b -> do
      c <- p b 
      d <- q c
      return d
  -- ou: Kleisli q . Kleisli p = Kleisli (\b -> p b >>= q)

instance Monad m => Arrow (Kleisli m) where

  arr :: Monad m => (b -> c) -> Kleisli m b c
  arr p = Kleisli (return . p)

  first :: Monad m => Kleisli m b c -> Kleisli m (b, d) (c, d)
  first (Kleisli p) = Kleisli $ \(b, d) -> do
      c <- p b 
      return (c, d)
  -- ou: first (Kleisli p) = Kleisli (\(b, d) -> p b >>= \c -> return (c, d))

-}

-------------------------------------------------------------------------------
-- io
-------------------------------------------------------------------------------

mProcIO1 :: IO Int
mProcIO1 = do
  putStrLn "hello mProcIO1"
  return 42

kProcIO1 :: Kleisli IO () Int
kProcIO1 = Kleisli $ \() -> do
  putStrLn "hello kProcIO1"
  return 42

kProcIO2 :: Kleisli IO (Int, Int) Int
kProcIO2 = Kleisli $ \(x, y) -> do
  print x
  print y
  return (x+y)

-------------------------------------------------------------------------------
-- maybe
-------------------------------------------------------------------------------

kMul2 :: Kleisli Maybe Double Double
kMul2 = arr (*2)

kSqrt :: Kleisli Maybe Double Double
kSqrt = Kleisli (\x -> if x >= 0 then Just (sqrt x) else Nothing)

-- composition avec les opérateurs
kProcMaybe1 :: Kleisli Maybe Double Double
kProcMaybe1 = kSqrt >>> kMul2

-- composition avec la notation do
kProcMaybe2 :: Kleisli Maybe Double Double
kProcMaybe2 = proc x0 -> do
  x1 <- kSqrt -< x0
  kMul2 -< x1

-- idem kProcMaybe2 mais avec la notation do des monades
kProcMaybe3 :: Kleisli Maybe Double Double
kProcMaybe3 = Kleisli $ \x0 -> do
  x1 <- runKleisli kSqrt x0
  runKleisli kMul2 x1

-------------------------------------------------------------------------------
-- application
-------------------------------------------------------------------------------

main :: IO ()
main = do

  putStrLn "Kleisli"

  _ <- mProcIO1

  _ <- runKleisli kProcIO1 ()
  _ <- runKleisli kProcIO2 (20, 22)

  print $ runKleisli kMul2 21

  print $ runKleisli kProcMaybe1 4 
  print $ runKleisli kProcMaybe1 (-4) 

  print $ runKleisli kProcMaybe2 4 
  print $ runKleisli kProcMaybe2 (-4) 

  print $ runKleisli kProcMaybe3 4 
  print $ runKleisli kProcMaybe3 (-4) 

