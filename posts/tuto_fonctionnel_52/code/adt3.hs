data Figure 
    = Rectangle Double Double

aire :: Figure -> Double
aire (Rectangle l h) = l * h

main :: IO ()
main = do
    let rect = Rectangle 21 2
    print $ aire rect

