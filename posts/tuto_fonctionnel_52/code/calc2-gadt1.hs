{-# LANGUAGE GADTs #-}

data Expr a where
    ValI :: Int -> Expr Int
    ValB :: Bool -> Expr Bool
    Add :: Expr Int -> Expr Int -> Expr Int
    Mul :: Expr Int -> Expr Int -> Expr Int
    Equals :: Eq a => Expr a -> Expr a -> Expr Bool

eval :: Expr a -> a
eval (ValI x) = x
eval (ValB x) = x
eval (Add e1 e2) = eval e1 + eval e2
eval (Mul e1 e2) = eval e1 * eval e2
eval (Equals e1 e2) = eval e1 == eval e2

main :: IO ()
main = do
    print $ eval $ ValI 21
    print $ eval $ Mul (ValI 2)
                       (Add (ValI 10) (ValI 11))
    print $ eval $ Equals (ValI 42)
                          (Mul (ValI 2) (ValI 21))
    print $ eval $ Equals (ValB True)
                          (ValB False)
    -- print $ eval $ Equals (ValB True) (Mul (ValI 2) (ValI 21))    -- compile error

