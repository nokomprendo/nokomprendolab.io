data Figure 
    = Rectangle
        { _largeur :: Double
        , _hauteur :: Double
        }

aire :: Figure -> Double
aire r = _largeur r * _hauteur r

main :: IO ()
main = do
    let rect = Rectangle 21 2
    print $ aire rect

