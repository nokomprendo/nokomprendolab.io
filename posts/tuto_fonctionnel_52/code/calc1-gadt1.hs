{-# LANGUAGE GADTs #-}

data Expr where
    Val :: Int -> Expr
    Add :: Expr -> Expr ->  Expr
    Mul :: Expr -> Expr ->  Expr

eval :: Expr -> Int
eval (Val x) = x
eval (Add e1 e2) = eval e1 + eval e2
eval (Mul e1 e2) = eval e1 * eval e2

main :: IO ()
main = do
    print $ eval $ Val 21
    print $ eval $ Mul (Val 2)
                       (Add (Val 10) (Val 11))

