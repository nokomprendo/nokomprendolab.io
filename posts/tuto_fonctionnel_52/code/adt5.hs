data Figure a
    = Rectangle a a
    | Disque a

aire :: Floating a => Figure a -> a
aire (Rectangle l h) = l * h
aire (Disque r) = pi * r**2

main :: IO ()
main = do
    let rect = Rectangle 21 (2::Float)
        disq = Disque (1::Double)
    print $ aire rect
    print $ aire disq

