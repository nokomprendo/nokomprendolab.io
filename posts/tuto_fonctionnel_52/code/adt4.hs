data Figure 
    = Rectangle Double Double
    | Disque Double

aire :: Figure -> Double
aire (Rectangle l h) = l * h
aire (Disque r) = pi * r**2

main :: IO ()
main = do
    let rect = Rectangle 21 2
        disq = Disque 1
    print $ aire rect
    print $ aire disq

