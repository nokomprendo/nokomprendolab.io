data Couleur 
    = Rouge 
    | Vert 
    | Bleu

toHtml :: Couleur -> String
toHtml Rouge = "#ff0000"
toHtml Vert = "#00ff00"
toHtml Bleu = "#0000ff"

main :: IO ()
main = do
    putStrLn $ toHtml Rouge
    putStrLn $ toHtml Vert

