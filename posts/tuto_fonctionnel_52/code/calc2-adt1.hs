data Expr
    = ValI Int
    | ValB Bool
    | Add Expr Expr
    | Mul Expr Expr
    | Equals Expr Expr
    deriving (Eq, Show)

eval :: Expr -> Expr
eval (Add e1 e2) = 
    case (eval e1, eval e2) of
        (ValI x1, ValI x2) -> ValI (x1 + x2)
        _ -> error "Add failed"
eval (Mul e1 e2) = 
    case (eval e1, eval e2) of
        (ValI x1, ValI x2) -> ValI (x1 * x2)
        _ -> error "Mul failed"
eval (Equals e1 e2) = ValB (eval e1 == eval e2)
eval x = x

main :: IO ()
main = do
    print $ eval $ ValI 21
    print $ eval $ Mul (ValI 2)
                       (Add (ValI 10) (ValI 11))
    print $ eval $ Equals (ValI 42)
                          (Mul (ValI 2) (ValI 21))
    print $ eval $ Equals (ValB True)
                          (ValB False)
    print $ eval $ Equals (ValB True) (Mul (ValI 2) (ValI 21))  -- erreur à l'exécution

