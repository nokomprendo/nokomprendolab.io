{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# OPTIONS_GHC -fno-warn-unused-do-bind #-}

import Reflex.GI.Gtk
import qualified GI.Gtk as Gtk
import qualified GI.Gio as Gio

mkGui :: (MonadReflexGtk t m) => Gtk.Application -> m (Gtk.Entry, Gtk.Label)
mkGui app = runGtk $ do

  win <- Gtk.applicationWindowNew app
  Gtk.windowSetTitle win "echo-reflex1"
  Gtk.windowSetDefaultSize win 200 100
  Gtk.onWidgetDestroy win Gtk.mainQuit

  vbox <- Gtk.boxNew Gtk.OrientationVertical 0
  Gtk.containerAdd win vbox

  inputEntry <- Gtk.entryNew
  Gtk.boxPackStart vbox inputEntry False False 2

  outputLabel <- Gtk.labelNew Nothing
  Gtk.boxPackStart vbox outputLabel True True 2

  button <- Gtk.buttonNewWithLabel "Quit"
  Gtk.onButtonClicked button (Gio.applicationQuit app)
  Gtk.boxPackStart vbox button False False 2

  Gio.onApplicationActivate app $ Gtk.widgetShowAll win
  return (inputEntry, outputLabel)

mkReactiveCode :: (MonadReflexGtk t m) => Gtk.Application -> m ()
mkReactiveCode app = do
  (inputEntry, outputLabel) <- mkGui app
  txtEvent <- dynamicOnSignal "" inputEntry #changed 
                (\fire -> Gtk.entryGetText inputEntry >>= fire)
  sink outputLabel [ #label :== txtEvent ]
  return ()

main :: IO ()
main = do
  Just app <- Gtk.applicationNew (Just "org.example.echo-reflex1") []
  runReflexGtk app Nothing $ mkReactiveCode app
  return ()

-- https://hackage.haskell.org/package/reflex-gi-gtk
-- https://reflex-frp.org/tutorial
-- https://examples.reflex-frp.org/

