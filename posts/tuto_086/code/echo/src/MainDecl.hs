{-# LANGUAGE OverloadedLabels  #-}
{-# LANGUAGE OverloadedLists   #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-unused-do-bind #-}

import Data.Text (Text)
import GI.Gtk.Declarative
import GI.Gtk.Declarative.App.Simple
import qualified GI.Gtk as Gtk

newtype State = State { _msg :: Text }

data Event = ChangeText Text | Closed

handleView :: State -> AppView Gtk.Window Event
handleView state =
  let params = [ #title := "echo-decl"
               , #defaultWidth := 200
               , #defaultHeight := 100
               , on #deleteEvent (const (True, Closed))
               ]
      ui = container Gtk.Box [#orientation := Gtk.OrientationVertical]
             [ BoxChild defaultBoxChildProperties { padding = 5 } $ widget
                Gtk.Entry [ onM #changed (fmap ChangeText . Gtk.entryGetText) ]
             , BoxChild defaultBoxChildProperties { padding = 5 } $ widget
                Gtk.Label [ #label := _msg state ]
             , BoxChild defaultBoxChildProperties { padding = 5 } $ widget
                Gtk.Button [ #label := "Quit", on #clicked Closed ]
             ]
  in bin Gtk.Window params ui

handleUpdate :: State -> Event -> Transition State Event
handleUpdate _ (ChangeText txt) = Transition (State txt) (return Nothing)
handleUpdate _ Closed = Exit

main :: IO ()
main = do
  let app = App { view = handleView
                , update = handleUpdate
                , inputs = []
                , initialState = State ""
                }
  run app
  return ()

