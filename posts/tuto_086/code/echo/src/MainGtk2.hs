{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# OPTIONS_GHC -fno-warn-unused-do-bind #-}

import qualified GI.Gio as Gio
import qualified GI.Gtk as Gtk
import Data.GI.Base

activateApp :: Gtk.Application -> IO ()
activateApp app = do

  win <- new Gtk.ApplicationWindow 
    [ #application := app
    , #title := "echo-gtk2"
    , #defaultWidth := 200
    , #defaultHeight := 100
    ]

  vbox <- new Gtk.Box [ #orientation := Gtk.OrientationVertical ]
  #add win vbox

  entry <- new Gtk.Entry []
  #packStart vbox entry False False 2

  label <- new Gtk.Label []
  #packStart vbox label True True 2

  on entry #changed (#getText entry >>= #setText label)

  button <- new Gtk.Button [ #label := "Quit" ]
  on button #clicked (Gio.applicationQuit app)
  #packStart vbox button False False 2

  #showAll win

main :: IO ()
main = do
  app <- new Gtk.Application [ #applicationId := "org.examples.echo-gtk2" ]
  on app #activate $ activateApp app
  Gio.applicationRun app Nothing
  return ()

-- https://hackage.haskell.org/package/gi-gtk-3.0.38

