{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# OPTIONS_GHC -fno-warn-unused-do-bind #-}

import Reflex.GI.Gtk
import GI.Gtk (new, on, AttrOp(..))
import qualified GI.Gtk as Gtk
import qualified GI.Gio as Gio

mkGui :: (MonadReflexGtk t m) => Gtk.Application -> m (Gtk.Entry, Gtk.Label)
mkGui app = runGtk $ do

  win <- new Gtk.ApplicationWindow 
    [ #application := app
    , #title := "echo-reflex2"
    , #defaultWidth := 200
    , #defaultHeight := 100
    ]

  vbox <- new Gtk.Box [ #orientation := Gtk.OrientationVertical ]
  #add win vbox

  inputEntry <- new Gtk.Entry []
  #packStart vbox inputEntry False False 2

  outputLabel <- new Gtk.Label []
  #packStart vbox outputLabel True True 2

  button <- new Gtk.Button [ #label := "Quit" ]
  on button #clicked (Gio.applicationQuit app)
  #packStart vbox button False False 2

  on app #activate $ Gtk.widgetShowAll win
  return (inputEntry, outputLabel)

mkReactiveCode :: (MonadReflexGtk t m) => Gtk.Application -> m ()
mkReactiveCode app = do
  (inputEntry, outputLabel) <- mkGui app
  txtEvent <- dynamicOnSignal "" inputEntry #changed 
                (\fire -> #getText inputEntry >>= fire)
  sink outputLabel [ #label :== txtEvent ]
  return ()

main :: IO ()
main = do
  Just app <- Gtk.applicationNew (Just "org.example.echo-reflex2") []
  runReflexGtk app Nothing $ mkReactiveCode app
  return ()

-- https://hackage.haskell.org/package/reflex-gi-gtk
-- https://reflex-frp.org/tutorial
-- https://examples.reflex-frp.org/
-- https://github.com/reflex-frp/reflex-examples/blob/master/frontend/src/Frontend/Examples/FileReader/Main.hs

