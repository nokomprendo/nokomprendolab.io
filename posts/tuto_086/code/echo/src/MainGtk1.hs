{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# OPTIONS_GHC -fno-warn-unused-do-bind #-}

import qualified GI.Gio as Gio
import qualified GI.Gtk as Gtk

activateApp :: Gtk.Application -> IO ()
activateApp app = do

  win <- Gtk.applicationWindowNew app
  Gtk.windowSetTitle win "echo-gtk1"
  Gtk.windowSetDefaultSize win 200 100
  Gtk.onWidgetDestroy win Gtk.mainQuit

  vbox <- Gtk.boxNew Gtk.OrientationVertical 0
  Gtk.containerAdd win vbox

  entry <- Gtk.entryNew
  Gtk.boxPackStart vbox entry False False 2

  label <- Gtk.labelNew Nothing
  Gtk.boxPackStart vbox label True True 2

  Gtk.onEditableChanged entry 
    (Gtk.entryGetText entry >>= Gtk.labelSetText label)

  button <- Gtk.buttonNewWithLabel "Quit"
  Gtk.onButtonClicked button (Gio.applicationQuit app)
  Gtk.boxPackStart vbox button False False 2

  Gtk.widgetShowAll win

main :: IO ()
main = do
  Just app <- Gtk.applicationNew (Just "org.examples.echo-gtk1") []
  Gio.onApplicationActivate app (activateApp app)
  Gio.applicationRun app Nothing
  return ()

