{-# LANGUAGE OverloadedStrings #-}

import Data.Text (Text)
import Monomer

newtype AppModel = AppModel 
  { _echoTxt :: Text
  } deriving (Eq)

data AppEvent
  = EventClosed
  | EventChanged Text

buildUI
  :: WidgetEnv AppModel AppEvent
  -> AppModel
  -> WidgetNode AppModel AppEvent
buildUI _wenv model = 
  vstack 
    [ textFieldV (_echoTxt model) EventChanged
    , spacer
    , label (_echoTxt model) `styleBasic` [ textCenter ]
    , spacer
    , button "Quit" EventClosed
    ] `styleBasic` [ padding 2 ]

handleEvent
  :: WidgetEnv AppModel AppEvent
  -> WidgetNode AppModel AppEvent
  -> AppModel
  -> AppEvent
  -> [AppEventResponse AppModel AppEvent]
handleEvent _wenv _node model evt = case evt of
  EventClosed -> [ exitApplication ]
  EventChanged t -> [ Model (model { _echoTxt = t }) ]

main :: IO ()
main = 
  let model = AppModel ""
      config = 
        [ appWindowTitle "echo-monomer"
        , appWindowState (MainWindowNormal (200, 110))
        , appFontDef "Regular" "./assets/fonts/Roboto-Regular.ttf"
        , appTheme darkTheme
        , appInitEvent (EventChanged "")
        ]
  in startApp model handleEvent buildUI config

