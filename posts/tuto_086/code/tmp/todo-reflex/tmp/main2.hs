{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}

import Control.Monad (void)
import GI.Gtk hiding (main)
import Reflex.GI.Gtk
import System.Environment
import System.Exit

import Data.Text as T

main :: IO ()
main = do
  argv <- (:) <$> getProgName <*> getArgs
  Just gtkApplication <- applicationNew (Just "org.example.todo-reflex") []
  rc <- runReflexGtk gtkApplication (Just argv) $ myReactiveCode gtkApplication
  case rc of
    0 -> exitSuccess
    n -> exitWith $ ExitFailure $ fromIntegral n


myReactiveCode :: (MonadReflexGtk t m) => Application -> m ()
myReactiveCode gtkApplication = do
  window <- runGtk $ applicationWindowNew gtkApplication
  box <- runGtk $ boxNew OrientationVertical 0
  containerAdd window box
  input1 <- runGtk entryNew
  output <- runGtk $ labelNew Nothing
  runGtk $ boxPackStart box input1 False False 0
  runGtk $ boxPackStart box output False False 0
  text1 <- dynamicOnSignal "" input1 #changed $ \fire -> do
    x <- entryGetText input1 
    fire (x <> T.reverse x)
  sink output [#label :== text1]
  void $ on gtkApplication #activate $ widgetShowAll window

-- https://hackage.haskell.org/package/reflex-gi-gtk
-- https://reflex-frp.org/tutorial
-- https://examples.reflex-frp.org/

