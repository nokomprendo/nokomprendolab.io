{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# OPTIONS_GHC -fno-warn-unused-do-bind #-}

-- import Data.Text as T

import GI.Gtk hiding (main)
import Reflex.GI.Gtk
import qualified GI.Gtk as Gtk
import qualified GI.Gio as Gio

main :: IO ()
main = do
  Just app <- applicationNew (Just "org.example.todo-reflex") []
  runReflexGtk app Nothing $ myReactiveCode app
  return ()

myGui :: (MonadReflexGtk t m) => Application -> m (Entry, Label)
myGui app = runGtk $ do

  win <- new Gtk.ApplicationWindow 
    [ #application := app
    , #title := "todo-reflex"
    , #defaultWidth := 400
    , #defaultHeight := 300
    ]

  vbox <- new Gtk.Box [ #orientation := Gtk.OrientationVertical ]
  #add win vbox
 
  hbox <- new Gtk.Box [ #orientation := Gtk.OrientationHorizontal ]
  #packStart vbox hbox False False 2

  -- todo
  output <- labelNew Nothing
  #packEnd vbox output True True 2

  scrolled <- new Gtk.ScrolledWindow []
  #packEnd vbox scrolled True True 2
  vboxTasks <- new Gtk.Box [ #orientation := Gtk.OrientationVertical ]
  #add scrolled vboxTasks

  entry <- new Gtk.Entry []
  #packStart hbox entry True True 2
  button1 <- new Gtk.Button [ #label := "Add task" ]

  -- runGtk $ on button1 #clicked $ do


  {-
  on button1 #clicked $ do
    t <- get entry #text
    unless (T.null t) $ do
      i <- atomically $ do
              m <- readTVar var
              let i = _mId m
              writeTVar var $ m { _mId = i+1 }
              return i
      b <- new Gtk.Button [ #label := showt i <> ". " <> t
                          , #xalign := 0
                          ]
      #add vboxTasks b
      set entry [ #text := "" ]
      on b #clicked (#remove vboxTasks b)
      #showAll vboxTasks
  -}

  #packStart hbox button1 False False 2

  button2 <- new Gtk.Button [ #label := "Quit" ]

  on button2 #clicked (Gio.applicationQuit app)
  #packStart hbox button2 False False 5

  on app #activate $ widgetShowAll win
  return (entry, output)


myReactiveCode :: (MonadReflexGtk t m) => Application -> m ()
myReactiveCode app = do
  (entry, output) <- myGui app
  text1 <- dynamicOnSignal "" entry #changed (\fire -> entryGetText entry >>= fire)
  sink output [ #label :== text1 ]
  return ()

-- https://hackage.haskell.org/package/reflex-gi-gtk
-- https://reflex-frp.org/tutorial
-- https://examples.reflex-frp.org/
-- https://github.com/reflex-frp/reflex-examples/blob/master/frontend/src/Frontend/Examples/BasicToDo/Main.hs

