let

  config = {
    allowBroken = true;
    packageOverrides = pkgs: {
      haskell = pkgs.haskell // {
        packages = pkgs.haskell.packages // {
          ghc = pkgs.haskellPackages.override {
            overrides = self: super: with pkgs.haskell.lib; {
              reflex-gi-gtk = dontCheck (dontHaddock (doJailbreak super.reflex-gi-gtk));
            };
          };
        };
      };
    };
  };

  # channel = fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz";
  channel = <nixpkgs>;

  pkgs = import channel { inherit config; };

  drv = pkgs.haskell.packages.ghc.callCabal2nix "todo-reflex" ./. {};

in if pkgs.lib.inNixShell then drv.env else drv

