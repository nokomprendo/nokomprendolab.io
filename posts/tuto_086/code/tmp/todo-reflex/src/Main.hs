{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# OPTIONS_GHC -fno-warn-unused-do-bind #-}

import Reflex.GI.Gtk
import GI.Gtk (new, on, AttrOp(..))
import qualified GI.Gtk as Gtk
import qualified GI.Gio as Gio

main :: IO ()
main = do
  Just app <- Gtk.applicationNew (Just "org.example.todo-reflex") []
  runReflexGtk app Nothing $ myReactiveCode app
  return ()

myGui :: (MonadReflexGtk t m) => Gtk.Application -> m (Gtk.Entry, Gtk.Label)
myGui app = runGtk $ do

  win <- new Gtk.ApplicationWindow 
    [ #application := app
    , #title := "todo-reflex"
    , #defaultWidth := 400
    , #defaultHeight := 300
    ]

  vbox <- new Gtk.Box [ #orientation := Gtk.OrientationVertical ]
  #add win vbox
 
  hbox <- new Gtk.Box [ #orientation := Gtk.OrientationHorizontal ]
  #packStart vbox hbox False False 2

  outputLabel <- new Gtk.Label [ #xalign := 0 ]
  #packStart vbox outputLabel False False 2

  scrolled <- new Gtk.ScrolledWindow []
  #packStart vbox scrolled True True 2
  vboxTasks <- new Gtk.Box [ #orientation := Gtk.OrientationVertical ]
  #add scrolled vboxTasks

  inputEntry <- new Gtk.Entry []
  #packStart hbox inputEntry True True 2
  button1 <- new Gtk.Button [ #label := "Add task" ]
  -- on button1 #clicked $ do


  {-
  on button1 #clicked $ do
    t <- get inputEntry #text
    unless (T.null t) $ do
      i <- atomically $ do
              m <- readTVar var
              let i = _mId m
              writeTVar var $ m { _mId = i+1 }
              return i
      b <- new Gtk.Button [ #label := showt i <> ". " <> t
                          , #xalign := 0
                          ]
      #add vboxTasks b
      set inputEntry [ #text := "" ]
      on b #clicked (#remove vboxTasks b)
      #showAll vboxTasks
  -}

  #packStart hbox button1 False False 2

  button2 <- new Gtk.Button [ #label := "Quit" ]

  on button2 #clicked (Gio.applicationQuit app)
  #packStart hbox button2 False False 2

  on app #activate $ Gtk.widgetShowAll win
  return (inputEntry, outputLabel)


myReactiveCode :: (MonadReflexGtk t m) => Gtk.Application -> m ()
myReactiveCode app = do
  (inputEntry, outputLabel) <- myGui app
  txtEvent <- dynamicOnSignal "" inputEntry #changed 
                (\fire -> Gtk.entryGetText inputEntry >>= fire)
  sink outputLabel [ #label :== txtEvent ]
  return ()

-- https://hackage.haskell.org/package/reflex-gi-gtk
-- https://reflex-frp.org/tutorial
-- https://examples.reflex-frp.org/
-- https://github.com/reflex-frp/reflex-examples/blob/master/frontend/src/Frontend/Examples/BasicToDo/Main.hs


