{-# LANGUAGE OverloadedStrings #-}

import Data.List (intersperse)
import Data.Text as T (Text, null)
import Monomer
import TextShow

data AppTask = AppTask
  { _tId :: Int
  , _tDesc :: Text
  } deriving (Eq)

data AppModel = AppModel 
  { _mId :: Int
  , _mTasks :: [AppTask]
  , _mDesc :: Text
  } deriving (Eq)

data AppEvent
  = EventInit
  | EventQuit
  | EventDelTask Int
  | EventAddTask
  | EventEntry Text

buildUI
  :: WidgetEnv AppModel AppEvent
  -> AppModel
  -> WidgetNode AppModel AppEvent
buildUI _wenv model = 
  let mkButton (AppTask i d) = button d (EventDelTask i) `styleBasic` [textLeft]
  in vstack 
    [ hstack 
      [ textFieldV (_mDesc model) EventEntry
      , spacer
      , button "Add task" EventAddTask
      , spacer
      , button "Quit" EventQuit
      ]
      , spacer
      , scroll $ vstack $ intersperse spacer (map mkButton (_mTasks model))
    ] `styleBasic` [padding 10 ]

handleEvent
  :: WidgetEnv AppModel AppEvent
  -> WidgetNode AppModel AppEvent
  -> AppModel
  -> AppEvent
  -> [AppEventResponse AppModel AppEvent]
handleEvent _wenv _node model evt = case evt of
  EventInit -> []
  EventQuit -> [exitApplication]
  EventEntry t -> [ Model (model { _mDesc = t }) ]
  EventDelTask i -> 
    let mtasks = filter ((/=i) . _tId) (_mTasks model)
    in [ Model (model { _mTasks = mtasks }) ]
  EventAddTask -> 
    let i = _mId model
        d = _mDesc model
        mtasks = _mTasks model ++ [AppTask i (showt i <> ". " <> d)]
    in if T.null d 
        then [ Model model ]
        else [ Model (AppModel (i+1) mtasks "") ]

main :: IO ()
main = 
  let model = AppModel 1 [] ""
      config = 
        [ appWindowTitle "todo-monomer"
        , appTheme darkTheme
        , appFontDef "Regular" "./assets/fonts/Roboto-Regular.ttf"
        , appInitEvent EventInit
        ]
  in startApp model handleEvent buildUI config

-- (lens)

