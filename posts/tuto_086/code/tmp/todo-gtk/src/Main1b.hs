{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# OPTIONS_GHC -fno-warn-unused-do-bind #-}

import Control.Concurrent.STM
import Control.Monad (unless)
import Data.Text as T
import TextShow

import qualified GI.Gio as Gio
import qualified GI.Gtk as Gtk

newtype AppModel = AppModel 
  { _nextId :: Int
  } deriving (Eq)

activateApp :: Gtk.Application -> IO ()
activateApp app = do
  -- main window
  win <- Gtk.applicationWindowNew app
  Gtk.windowSetTitle win "todo-gtk"
  Gtk.windowSetDefaultSize win 400 300
  Gtk.onWidgetDestroy win Gtk.mainQuit

  -- vbox inside window
  vbox <- Gtk.boxNew Gtk.OrientationVertical 0
  Gtk.containerAdd win vbox

  -- hbox inside vbox
  hbox <- Gtk.boxNew Gtk.OrientationHorizontal 0
  Gtk.boxPackStart vbox hbox False False 2

  -- scrolledWindow inside vbox
  let adjust = Nothing :: Maybe Gtk.Adjustment
  scrolled <- Gtk.scrolledWindowNew adjust adjust
  Gtk.boxPackStart vbox scrolled True True 2
  vboxTasks <- Gtk.boxNew Gtk.OrientationVertical 0
  Gtk.containerAdd scrolled vboxTasks

  entry <- Gtk.entryNew
  Gtk.boxPackStart hbox entry True True 2

  button1 <- Gtk.buttonNewWithLabel "Add task"
  Gtk.onButtonClicked button1 (mkTaskButton entry vboxTasks)
  Gtk.boxPackStart hbox button1 False False 2

  button2 <- Gtk.buttonNewWithLabel "Quit"
  Gtk.onButtonClicked button2 (Gio.applicationQuit app)
  Gtk.boxPackStart hbox button2 False False 2

  Gtk.widgetShowAll win

mkTaskButton :: Gtk.Entry -> Gtk.Box -> IO ()
mkTaskButton entry vboxTasks = do
    t <- Gtk.getEntryText entry
    unless (T.null t) $ do
      b <- Gtk.buttonNewWithLabel t -- (showt i <> ". " <> t)
      Gtk.setButtonXalign b 0
      Gtk.containerAdd vboxTasks b
      Gtk.setEntryText entry ""
      Gtk.onButtonClicked b (Gtk.containerRemove vboxTasks b)
      Gtk.widgetShowAll vboxTasks

main :: IO ()
main = do
  modelVar <- newTVarIO (AppModel 1)
  Just app <- Gtk.applicationNew (Just "org.examples.todo-gtk") []
  Gio.onApplicationActivate app (activateApp app)
  Gio.applicationRun app Nothing
  return ()


