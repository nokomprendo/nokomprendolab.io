{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# OPTIONS_GHC -fno-warn-unused-do-bind #-}

import Control.Concurrent.STM
import Control.Monad (unless)
import Data.Text as T
import TextShow (showt)

import qualified GI.Gio as Gio
import qualified GI.Gtk as Gtk
import Data.GI.Base

data AppTask = AppTask
  { _tId :: Int
  , _tDesc :: Text
  } deriving (Eq)

data AppModel = AppModel 
  { _mId :: Int
  , _mTasks :: [AppTask]
  , _mDesc :: Text
  } deriving (Eq)


activateApp :: Gtk.Application -> TVar AppModel -> IO ()
activateApp app var = do
  win <- new Gtk.ApplicationWindow [ #application := app
                                   , #title := "todo-gtk"
                                   , #defaultWidth := 400
                                   , #defaultHeight := 300
                                   ]

  vbox <- new Gtk.Box [ #orientation := Gtk.OrientationVertical ]
  #add win vbox
  
  hbox <- new Gtk.Box [ #orientation := Gtk.OrientationHorizontal ]
  #packStart vbox hbox False False 2

  scrolled <- new Gtk.ScrolledWindow []
  #packEnd vbox scrolled True True 2
  vboxTasks <- new Gtk.Box [ #orientation := Gtk.OrientationVertical ]
  #add scrolled vboxTasks

  entry <- new Gtk.Entry []
  #packStart hbox entry True True 2
  button1 <- new Gtk.Button [ #label := "Add task" ]
  on button1 #clicked $ do
    t <- get entry #text
    unless (T.null t) $ do
      i <- atomically $ do
              m <- readTVar var
              let i = _mId m
              writeTVar var $ m { _mId = i+1 }
              return i
      b <- new Gtk.Button [ #label := showt i <> ". " <> t
                          , #xalign := 0
                          ]
      #add vboxTasks b
      set entry [ #text := "" ]
      on b #clicked (#remove vboxTasks b)
      #showAll vboxTasks

  #packStart hbox button1 False False 2

  button2 <- new Gtk.Button [ #label := "Quit" ]
  on button2 #clicked (Gio.applicationQuit app)
  #packStart hbox button2 False False 5

  #showAll win

main :: IO ()
main = do
  modelVar <- newTVarIO (AppModel 1 [] "")
  app <- new Gtk.Application [ #applicationId := "org.examples.todo-gtk" ]
  on app #activate $ activateApp app modelVar
  Gio.applicationRun app Nothing
  return ()

-- https://hackage.haskell.org/package/gi-gtk-3.0.38

