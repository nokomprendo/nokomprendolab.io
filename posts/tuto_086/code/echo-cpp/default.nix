with import <nixpkgs> {};
stdenv.mkDerivation {
    name = "echo";
    src = ./.;
    buildInputs = [
        cmake
        pkgconfig
        gtkmm3
    ];
}
