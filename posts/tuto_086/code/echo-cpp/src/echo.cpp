#include <gtkmm.h>

void initWin(Gtk::Window & win) {

  // initialise la fenêtre principale
  win.set_title("echo");
  win.set_default_size(200, 100);

  // construit la pile de widgets
  auto vbox = Gtk::make_managed<Gtk::Box>(Gtk::ORIENTATION_VERTICAL);
  win.add(*vbox);

  // ajoute l'entrée texte dans la pile de widgets
  auto entry = Gtk::make_managed<Gtk::Entry>();
  vbox->pack_start(*entry);

  // ajoute le label
  auto label = Gtk::make_managed<Gtk::Label>();
  vbox->pack_start(*label);

  // connecte une fonction de rappel pour l'événement "editable changed"
  auto handleChanged = [label, entry]() { label->set_text(entry->get_text()); };
  entry->signal_changed().connect( handleChanged );

  // ajoute le bouton
  auto button = Gtk::make_managed<Gtk::Button>("Quit");
  vbox->pack_start(*button);
  // connecte une fonction de rappel pour l'événement "button clicked"
  button->signal_clicked().connect( sigc::mem_fun(win, &Gtk::Window::hide) );

  // active l'affichage de tous les widgets de la fenêtre
  win.show_all();
}

int main(int argc, char ** argv) {
  auto app = Gtk::Application::create(argc, argv, "org.examples.echo");
  Gtk::Window win;
  initWin(win);
  return app->run(win);
}

