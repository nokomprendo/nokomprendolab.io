{-# LANGUAGE OverloadedStrings #-}

import Data.Text              qualified as T
import Data.Text.IO           qualified as T
import Data.Text.Lazy         qualified as L
import Data.Text.Lazy.IO      qualified as L
import Fmt
import System.Environment

main :: IO ()
main = do
  args <- getArgs
  case args of
    [filename] -> do
      txts <- L.lines <$> L.readFile filename
      case txts of
        (x:_) -> do
          T.putStrLn $ "line 1 of " +| length txts |+ ":"
          T.putStrLn $ L.toStrict x
        [] -> T.putStrLn $ T.pack filename <> " is empty"
    _ -> T.putStrLn "usage: <filename>"

