{-# LANGUAGE OverloadedStrings #-}

import Data.ByteString.Char8  qualified as C
import Data.Text.Encoding     qualified as E
import Data.Text.Lazy         qualified as L
import Data.Text.Lazy.IO      qualified as L
import Fmt
import System.Environment

main :: IO ()
main = do
  args <- getArgs
  case args of
    [filename] -> do
      txts <- L.lines <$> L.readFile filename
      case txts of
        (x:_) -> do
          C.putStrLn $ "line 1 of " +| length txts |+ ":"
          C.putStrLn $ E.encodeUtf8 $ L.toStrict x
        [] -> C.putStrLn $ C.pack filename <> " is empty"
    _ -> C.putStrLn "usage: <filename>"

