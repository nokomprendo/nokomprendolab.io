{-# LANGUAGE OverloadedStrings #-}

import Data.Text              qualified as T
import Data.Text.IO           qualified as T
import TextShow
import System.Environment

main :: IO ()
main = do
  args <- getArgs
  case args of
    [filename] -> do
      txts <- T.lines <$> T.readFile filename
      case txts of
        (x:_) -> do
          T.putStrLn $ "line 1 of " <> showt (length txts) <> ":"
          T.putStrLn x
        [] -> T.putStrLn $ T.pack filename <> " is empty"
    _ -> T.putStrLn "usage: <filename>"

