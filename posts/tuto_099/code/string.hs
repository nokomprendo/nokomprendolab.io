import System.Environment

main :: IO ()
main = do
  args <- getArgs
  case args of
    [filename] -> do
      txts <- lines <$> readFile filename
      case txts of
        (x:_) -> do
          putStrLn $ "line 1 of " ++ show (length txts) ++ ":"
          putStrLn x
        [] -> putStrLn $ filename ++ " is empty"
    _ -> putStrLn "usage: <filename>"

