{pkgs}:

pkgs.vim_configurable.customize {

  name = "vim";

  vimrcConfig.customRC = ''
    set nocompatible

    set t_Co=256
    set gfn=Monospace\ 13
    syntax on
    colorscheme molokai
    let g:molokai_original = 1
    let g:rehash256 = 1

    set number
    set cursorline
    set colorcolumn=80
    set ruler         " show the cursor position all the time
    set showcmd       " display incomplete command

    " airline
    set laststatus=2
  '';

  vimrcConfig.packages.myVimPackage = with pkgs.vimPlugins; {
    start = [ 
      airline 
      molokai
      youcompleteme 
    ];
  };

}

