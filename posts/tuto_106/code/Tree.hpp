#pragma once

#include "Dict.hpp"

#include <memory>

template <typename K, typename V>
class Tree : public Dict<K, V> {

  private:

    struct Node {
      K _key;
      V _value;
      std::unique_ptr<Node> _left;
      std::unique_ptr<Node> _right;
    };

    std::unique_ptr<Node> _data;
  
  public:

    Dict<K, V> & insertDict(const K & k, const V & v) override {
      std::unique_ptr<Node> * pnode = &_data;
      while (*pnode) {
        if ((*pnode)->_key < k)
          pnode = &((*pnode)->_left);
        else if ((*pnode)->_key > k)
          pnode = &((*pnode)->_right);
        else {
          (*pnode)->_value = v;
          return *this;
        }
      }
      *pnode = std::make_unique<Node>(Node {k, v, {}, {}});
      return *this;
    }

    std::optional<V> lookupDict(const K & k) const override {
      const std::unique_ptr<Node> * pnode = &_data;
      while (*pnode) {
        if ((*pnode)->_key < k)
          pnode = &((*pnode)->_left);
        else if ((*pnode)->_key > k)
          pnode = &((*pnode)->_right);
        else
          return {(*pnode)->_value};
      }
      return {};
    }

};


