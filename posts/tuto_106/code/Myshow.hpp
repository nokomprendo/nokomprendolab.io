#pragma once

#include <string>

template <typename T>
std::string myshow(const T & t);

template<>
std::string myshow<bool>(const bool & t) {
  return t ? "true" : "false";
}

template<>
std::string myshow<int>(const int & t) {
  return std::to_string(t);
}

