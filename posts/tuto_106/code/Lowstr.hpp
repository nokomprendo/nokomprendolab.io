#pragma once

#include "Myshow.hpp"

#include <algorithm>
#include <iostream>


/*

struct Lowstr : public std::string {

  explicit Lowstr(const std::string & str) : std::string(str) {
    std::transform(begin(), end(), begin(), tolower);
  }

};

template<>
std::string myshow<Lowstr>(const Lowstr & t) {
  return t;
}

*/

struct Lowstr {

  std::string _data;

  explicit Lowstr(const std::string & str) : _data(str) {
    std::transform(_data.begin(), _data.end(), _data.begin(), tolower);
  }

};

bool operator==(const Lowstr & str1, const Lowstr & str2) {
  return str1._data == str2._data;
}

bool operator>(const Lowstr & str1, const Lowstr & str2) {
  return str1._data > str2._data;
}

bool operator<(const Lowstr & str1, const Lowstr & str2) {
  return str1._data < str2._data;
}

template<>
std::string myshow<Lowstr>(const Lowstr & t) {
  return t._data;
}

