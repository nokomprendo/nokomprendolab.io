
#include "List.hpp"
#include "Lowstr.hpp"
#include "Myshow.hpp"
#include "Tree.hpp"

#include <iostream>
#include <vector>

template <typename K, typename V>
void elems(const std::vector<K> & vect, const Dict<K, V> & dict) {
  for (auto k : vect) {
    auto opt_v = dict.lookupDict(k);
    auto str_v = bool(opt_v) ? myshow(*opt_v) : "not found";
    auto str_k = myshow(k);
    std::cout << str_k << " -> " << str_v << std::endl;
  }
}

int main() {

  Tree<int, bool> treeIntBool;
  treeIntBool
    .insertDict(2, true)
    .insertDict(1, false)
    .insertDict(5, true)
    .insertDict(3, false);
  elems({3, 4}, treeIntBool);

  List<int, bool> listIntBool;
  listIntBool
    .insertDict(2, true)
    .insertDict(1, false)
    .insertDict(5, true)
    .insertDict(3, false);
  elems({3, 4}, listIntBool);

  Tree<Lowstr, int> treeLowstrInt;
  treeLowstrInt
    .insertDict(Lowstr("fooBar"), 42)
    .insertDict(Lowstr("FOO"), 13)
    .insertDict(Lowstr("bAR"), 37);
  elems({Lowstr("fOO"), Lowstr("Blou")}, treeLowstrInt);

  List<Lowstr, int> listLowstrInt;
  listLowstrInt
    .insertDict(Lowstr("fooBar"), 42)
    .insertDict(Lowstr("FOO"), 13)
    .insertDict(Lowstr("bAR"), 37);
  elems({Lowstr("fOO"), Lowstr("Blou")}, listLowstrInt);

  return 0;
}

