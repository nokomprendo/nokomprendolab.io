#pragma once

#include "Dict.hpp"

#include <algorithm>
#include <list>

template <typename K, typename V>
class List : public Dict<K, V> {

  private:
    using elem_t = std::pair<K, V>;
    std::list<elem_t> _data;
  
  public:

    Dict<K, V> & insertDict(const K & k, const V & v) override {
      _data.push_front({k, v});
      return *this;
    }

    std::optional<V> lookupDict(const K & k) const override {
      const auto f = [&k](const elem_t & e) { return e.first == k; };
      const auto iter = std::find_if(_data.begin(), _data.end(), f);
      if (iter == _data.end()) 
        return {};
      else 
        return {iter->second};
    }

};

