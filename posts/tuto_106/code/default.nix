{ pkgs ? import <nixpkgs> {} }:

with pkgs; stdenv.mkDerivation {
    name = "dict";
    src = ./.;
    nativeBuildInputs = [ cmake cppcheck ];
}

