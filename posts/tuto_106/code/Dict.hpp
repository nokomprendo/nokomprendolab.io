#pragma once

#include <optional>

template <typename K, typename V>
class Dict {
  public:
    virtual Dict<K, V> & insertDict(const K & k, const V & v) = 0;
    virtual std::optional<V> lookupDict(const K & k) const = 0;
};

