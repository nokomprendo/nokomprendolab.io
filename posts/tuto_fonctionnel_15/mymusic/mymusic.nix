{ pkgs ? import <nixpkgs> {} } :

with pkgs;
stdenv.mkDerivation {
  name = "mymusic";
  src = ./.;
  installPhase = ''
    mkdir -p $out
    cp -r data/* $out
  '';
}

