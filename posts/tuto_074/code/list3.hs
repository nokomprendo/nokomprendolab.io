
data List a
    = Nil 
    | Cons a (List a)
    deriving (Show)

instance Functor List where
    fmap = _

main :: IO ()
main = do
    let l1 = Cons 1 $ Cons 2 Nil :: List Int
    print $ (*2) <$> l1


