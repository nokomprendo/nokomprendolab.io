
f1 :: Int -> Int
f1 x = 1 + x

-- f2 :: Int -> Int
f2 x = 1 + f1 x

-- f3 :: Num a => a -> a
f3 x = 1 + x

main :: IO ()
main = do
    print $ f1 1
    print $ f2 1
    -- print $ f3 1
    print $ f3 (1::Int)
    print $ f3 (1::Double)

