
data List a
    = Nil 
    | Cons a (List a)
    deriving (Show)

instance Functor List where
    fmap _ Nil = Nil
    fmap f (Cons x xs) = Cons (f x) (fmap f xs)

incList :: (Functor f, Num b) => f b -> f b
incList xs = (+1) <$> xs

main :: IO ()
main = do
    let l1 = Cons 1 $ Cons 2 Nil :: List Int
    print $ (*2) <$> l1
    print $ incList l1

