#include <iostream>
#include <typeinfo>
#include <vector>

int main() {

    auto x1 = 13.37;
    auto x2 = 42;
    auto p = &x2;

    std::cout << "x1 " << typeid(x1).name() << std::endl;
    std::cout << "x2 " << typeid(x2).name() << std::endl;
    std::cout << "p  " << typeid(p).name() << std::endl;

    std::vector<int> v {13, 37};
    auto it1 = v.begin();
    std::vector<int>::iterator it2 = v.begin();

    std::cout << "v   " << typeid(v).name() << std::endl;
    std::cout << "it1 " << typeid(it1).name() << std::endl;
    std::cout << "it2 " << typeid(it2).name() << std::endl;

    return 0;
}

