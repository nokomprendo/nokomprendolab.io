
mul2 :: Num a => a -> a
mul2 x = _
-- mul2 x = 2 * x

allPositive :: [Int] -> Bool
-- allPositive = foldr _ True
allPositive = foldr (\x acc -> x>=0 && acc) True

main :: IO ()
main = do
    print $ mul2 (1::Int)
    print $ allPositive [13, 37, 42]

