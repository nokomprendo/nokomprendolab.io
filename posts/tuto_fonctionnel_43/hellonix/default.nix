with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "hellonix";
  src = ./.;
  buildInputs = [
    cmake
    gtkmm3
    pkgconfig
  ];
}

