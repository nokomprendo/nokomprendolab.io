---
title: Architectures de code en Haskell (3/6), Système d'effets
description: Dans l'article précédent, on a vu comment organiser un code Haskell en utilisant des fonctionnalités basiques, notamment des types d'enregistrement. On va maintenant voir comment réécrire ce code en utilisant un système d'effets, ici la bibliothèque Polysemy. Ce genre d'outils peut utiliser des fonctionnalités assez évoluées d'Haskell mais la complexité est normalement gérée dans la bibliothèque de telle sorte que son utilisation reste relativement simple.
---

Voir aussi : [video youtube](https://youtu.be/kQFKj3Ke9sU) -
[code source](https://gitlab.com/nokomprendo/harchi)


Dans l'[article précédent](tuto_110/2024-05-24-fr-README.html), on a vu comment
organiser un code Haskell en utilisant des fonctionnalités basiques, notamment
des types d'enregistrement. 

On va maintenant voir comment réécrire ce code en utilisant un système
d'effets, ici la bibliothèque
[Polysemy](https://hackage.haskell.org/package/polysemy). Ce genre d'outils
peut utiliser des fonctionnalités assez évoluées d'Haskell mais la complexité
est normalement gérée dans la bibliothèque de telle sorte que son utilisation
reste relativement simple.

Pour rappel, le projet de code est un serveur d'API JSON qui fournit des
informations sur des personnes stockées dans une base Sqlite et génère des
messages de log. Le projet contient aussi des tests automatisés et une
application de test.  Enfin, le code est découpé en différentes parties pour
réduire le couplage de code (domaine métier, effets, interpréteurs,
applications). 


# Implémentation avec Polysemy

## Messages de log

Avec Polysemy, on définit un effet via un type de données. Par exemple, pour
notre logger :

```haskell
-- Harchi.Eff.Effects.Logger

data Logger m a where
  LogMsg :: String -> Logger m ()

makeSem ''Logger
```

Le type `Logger` est un type algébrique généralisé (GADT). La valeur `LogMsg`
contient une fonction qui génère un message de log. La fonction `makeSem` crée
des fonctions à partir des valeurs du GADT (ici, on aura donc une fonction
`logMsg`).

On peut ensuite écrire un interpréteur pour cet effet, par exemple pour
afficher les messages de log dans la sortie standard :

```haskell
-- Harchi.Eff.Interpreters.LoggerStdout

runLoggerStdout :: 
  Member (Embed IO) r => 
  String -> Sem (Logger ': r) a -> Sem r a
runLoggerStdout prefix = interpret \case
  LogMsg str -> liftIO $ putStrLn $ "[" <> prefix <> "] " <> str
```

En gros, on utilise la fonction `interpret` de Polysemy et on implémente notre
interpréteur pour chaque valeur du GADT (ici `LogMsg`). 

Au niveau de la signature de `runLoggerStdout`, la monade `Sem` permet de
définir la liste des effets utilisables. Autrement dit, notre fonction consomme
et interprète l'effet `Logger` d'une liste. Enfin, la contrainte de type permet
de spécifier que `IO` appartient à la liste d'effets, ce qui nous permet
d'appeler la fonction `putStrLn` (de type `String -> IO ()`).


## Base de données

De mème, on définit un effet pour accéder à une base de données :

```haskell
-- Harchi.Eff.Effects.Db

data Db m a where
  GetPersons :: Db m [Person]
  GetPersonFromId :: Int -> Db m [Person]

makeSem ''Db
```

Ici, `makeSem` génère les fonctions `getPersons` et `getPersonFromId`, qu'on
pourra donc appeler dans un `Sem` contenant l'effet `Db`.

On écrit ensuite un interpréteur, par exemple qui implémente l'effet pour notre
base de données Sqlite :

```haskell
-- Harchi.Eff.Interpreters.DbSql

runDbSql :: 
  Members '[Logger, Embed IO] r => 
  FilePath -> Sem (Db ': r) a -> Sem r a
runDbSql sqlFile = interpret \case

  GetPersons -> do
    logMsg "Interpreters DbSql runDbSql GetPersons"
    liftIO $ withConnection sqlFile $ \conn -> 
      query_ conn 
        "SELECT personId, personName, personCountry \
        \FROM persons"

  GetPersonFromId i -> do
    logMsg $ "Interpreters DbSql runDbSql GetPersonFromId " <> show i
    liftIO $ withConnection sqlFile $ \conn -> 
      query conn 
        "SELECT personId, personName, personCountry \
        \FROM persons \
        \WHERE personId = (?)" (Only i)
```

Comme précédemment, on définit l'interpréteur pour chaque valeur du GADT. On
notera, dans la contrainte de types, que `IO` et `Logger` doivent être dans la
liste d'effets, car on utilise `withConnection` (pour accéder à la base de
données) et `logMsg` (pour générer des messages de log).


## Application de test

On écrit ensuite notre application de test, qui génère quelques messages de log
et accède à la base de données : 

```haskell
-- Harchi.Eff.Applications.Test

testApp :: Members '[Db, Logger] r => Sem r [Person]
testApp = do
  logMsg "Applications Test testApp getPerson 2"
  res1 <- getPersonFromId 2 
  logMsg "Applications Test testApp getPersons"
  res2 <- getPersons 
  return (res1 ++ res2)
```

Il s'agit donc d'une série d'actions dans la monade `Sem`, qui construit et
retourne une liste de personnes, en utilisant les effets `Db` et `Logger`.

Pour exécuter ce type d'applications, on appelle des interpréteurs implémentant
les effets spécifiés ainsi que la fonction `runM` de Polysemy :

```haskell
-- Harchi.Eff.Applications.Test

runApp :: Sem '[Db, Logger, Embed IO] a -> IO a
runApp app = runM $ logger $ db app
  where
    logger = runLoggerStdout "Eff" 
    db = runDbSql "persons.db" 
```

Exemple d'exécution :

```sh
ghci> import Harchi.Eff.Applications.Test 

ghci> runApp testApp 
[Eff] Applications Test testApp getPerson 2
[Eff] Interpreters DbSql runDbSql GetPersonFromId 2
[Eff] Applications Test testApp getPersons
[Eff] Interpreters DbSql runDbSql GetPersons
[Person {personId = 2, personName = "Alan Turing", personCountry = "GB"}, ...]
```


## Tests automatisés

Pour tester automatiquement notre application `testApp`, notamment les messages
de log générés, on peut définir un nouvel interpréteur de `Logger` qui stocke
en mémoire les messages plutôt que de les afficher. 

Dans l'article précédent, on avait utilisé une monade `WriterT [String] IO`
pour avoir un contexte permettant d'implémenter ces tests. Polysemy propose
directement une implémentation de `Writer` sous forme d'effet. Il suffit donc
d'écrire un interpréteur de `Logger` utilisant l'effet `Writer` ainsi qu'une
fonction d'exécution qui appelle les bons interpréteurs :

```haskell
-- Harchi.Eff.Applications.TestSpec

runLoggerWriter :: Member (Writer [String]) r => Sem (Logger ': r) a -> Sem r a
runLoggerWriter = interpret \case
  LogMsg str -> tell [str]

runTestApp :: 
  Sem '[Db, Logger, Writer [String], Embed IO] [Person] ->
  IO ([String], [Person])
runTestApp = runM . runWriter . runLoggerWriter . runDbSql sqlFile
```

On peut ensuite écrire un test automatisé, qui récupère et vérifie le résultat
et les logs de `testApp` avec ces interpréteurs :

```haskell
-- Harchi.Eff.Applications.TestSpec

spec :: Spec
spec = do

  describe "Applications Test" $ do

    it "logs + db" $ do
      (logs, ps) <- runTestApp testApp 
      ps `shouldBe` (person2 : persons)
      logs `shouldBe` 
        [ "Applications Test testApp getPerson 2"
        , "Interpreters DbSql runDbSql GetPersonFromId 2"
        , "Applications Test testApp getPersons"
        , "Interpreters DbSql runDbSql GetPersons"]
```


## Application serveur

Le système d'effets proposé par Polysemy s'intègre facilement avec la
bibliothèque Servant, notamment via des transformateurs de monades (ceci sera
détaillé dans l'article suivant).

Dans les grandes lignes, pour implémenter notre application serveur, on définit
 l'API via un type `Routes` et une application `serverRecord` qui
l'implémente (en utilisant la monade `Sem` et les effets `Db` et `Logger`) :

```haskell
-- Harchi.Eff.Applications.Api

data Routes route = Routes
  ...

serverRecord :: Members '[Db, Logger] r => Routes (AsServerT (Sem r))
serverRecord = Routes
  { _home = do
      logMsg "Applications Api serverRecord /"
      pure "hello"
  , _persons = do
      logMsg "Applications Api serverRecord /persons"
      getPersons
  , _person = \i -> do
      logMsg $ "Applications Api serverRecord /person/" <> show i 
      getPersonFromId i
  }
```

Pour exécuter `serverRecord`, il suffit essentiellement de spécifier les
interpréteurs à utiliser :

```haskell
-- Harchi.Eff.Applications.Server

runServer :: Int -> String -> FilePath -> IO ()
runServer port prefix sqlFile = do

  putStrLn $ "listening on port " ++ show port ++ "..." 

  let runApp :: Sem '[Db, Logger, Embed IO] a -> Handler a
      runApp = liftToHandler . runM . runLoggerStdout prefix . runDbSql sqlFile

  run port $ genericServeT runApp serverRecord
```


# Conclusion

Les systèmes d'effets sont assez populaires en Haskell (et dans la
programmation fonctionnelle à typage statique, en général). En effet, ils sont
généralement simples et pratiques à utiliser. En interne, ces outils reprennent
des architectures (free monades, ReaderT, etc) qu'on peut utiliser directement
pour organiser un code en Haskell. Les articles suivants  détaillent quelques
unes de ces architectures.

Voir aussi :

- [polysemy: Higher-order, low-boilerplate free monads](https://hackage.haskell.org/package/polysemy)
- [Warp as an effect in Polysemy](https://thma.github.io/posts/2022-07-04-polysemy-and-warp.html)
- [Implementing Clean Architecture with Haskell and Polysemy](https://github.com/thma/PolysemyCleanArchitecture)


