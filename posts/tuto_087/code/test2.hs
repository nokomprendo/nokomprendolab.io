import Data.Monoid

main :: IO ()
main = do

  let s = Sum 3 <> Sum 4 :: Sum Int
  print s
  print $ getSum s
  print $ foldr (<>) (Sum 0) [Sum 13, Sum 37]

  let p = Product 3 <> Product 4 :: Product Int
  print p
  print $ getProduct p

