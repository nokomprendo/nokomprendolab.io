-- Haskell in depth

data Turn = TNone | TLeft | TRight | TAround
  deriving (Show)

instance Semigroup Turn where
  TNone <> y = y
  TLeft <> TLeft = TAround
  TLeft <> TRight = TNone
  TLeft <> TAround = TRight
  TRight <> TRight = TAround
  TRight <> TAround = TLeft
  TAround <> TAround = TNone
  x <> y = y <> x

instance Monoid Turn where
  mempty = TNone

main :: IO ()
main = do
  print $ TLeft <> TAround <> TNone
  print $ mconcat [TLeft, TAround, TNone]

