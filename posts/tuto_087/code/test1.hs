data MyList a = Nil | Cons a (MyList a)
  deriving (Show)

instance Semigroup (MyList a) where
  Nil <> ys = ys
  Cons x xs <> ys = Cons x (xs <> ys)

instance Monoid (MyList a) where
  mempty = Nil

main :: IO ()
main = do
  let l1 = Cons 13 (Cons 37 Nil) :: MyList Int
  let l2 = Cons 42 Nil :: MyList Int
  print l1
  print l2
  print $ l1 <> l2
  print $ mconcat [l1, l2, l1] 

