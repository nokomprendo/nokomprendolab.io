{-# Language OverloadedStrings #-}

import Data.Text

myIntercalate :: Monoid a => a -> [a] -> a
myIntercalate _sep [] = mempty
myIntercalate _sep [x] = x
myIntercalate sep (x:xs) = x <> sep <> myIntercalate sep xs

main :: IO ()
main = do
  print $ myIntercalate " - " ["foo", "bar", "baz" :: Text]
  print $ myIntercalate " - " ["foo", "bar", "baz" :: String]

