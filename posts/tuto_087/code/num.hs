
import Data.Monoid
import Data.Foldable

main :: IO ()
main = do

  print $ Sum 2 <> Sum 3
  print $ fold [Sum 2, Sum 3]
  print $ foldMap Sum [2, 3]

  print $ Product 2 <> Product 3
  print $ fold [Product 2, Product 3]
  print $ foldMap Product [2, 3]

