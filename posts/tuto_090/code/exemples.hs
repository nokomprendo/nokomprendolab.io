import System.Environment

import Control.Applicative 
import Control.Monad.State
import Data.Functor.Identity
import Data.Monoid

positiveState :: Int -> State Int ()
positiveState x = modify (+ max 0 x)

positiveSum :: Int -> Const (Sum Int) ()
positiveSum = Const . Sum . max 0

mul2State :: Int -> State () Int
mul2State = pure . (*2)

mul2Identity :: Int -> Identity Int
mul2Identity = Identity . (*2)


main :: IO ()
main = do
  getEnv "HOME" >>= print
  traverse getEnv ["HOME", "USER"] >>= print 

  print $ runState (traverse positiveState [1, -2, 3, -4]) 0
  print $ traverse positiveSum [1, -2, 3, -4] 

  print $ runState (traverse mul2State [1, -2, 3, -4]) ()
  print $ traverse mul2Identity [1, -2, 3, -4] 
  

