
import System.Environment

main :: IO ()
main = do
  getEnv "HOME" >>= print
  (sequenceA $ fmap getEnv ["HOME", "USER"]) >>= print 
  traverse getEnv ["HOME", "USER"] >>= print 

