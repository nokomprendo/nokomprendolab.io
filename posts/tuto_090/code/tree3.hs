{-# LANGUAGE DeriveTraversable #-}

import Data.Traversable

data Tree a 
  = Leaf
  | Node (Tree a) a (Tree a)
  deriving (Foldable, Functor, Show, Traversable)

ifPositive :: (Ord a, Num a) => a -> Maybe a
ifPositive x = if x < 0 then Nothing else Just x

main :: IO ()
main = do
  let t1 = Node Leaf 13 (Node Leaf 42 Leaf) :: Tree Int
      t2 = Node Leaf (-13) (Node Leaf 42 Leaf) :: Tree Int

  print t1
  print t2

  print $ traverse ifPositive t1
  print $ traverse ifPositive t2

  msg <- for t2 $ \x -> do
    putStr "toto: "
    print x
    return (if x>=0 then x else -x)

  print msg

