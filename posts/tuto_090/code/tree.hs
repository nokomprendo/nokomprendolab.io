{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE DeriveTraversable #-}

import Control.Applicative 
import Control.Monad.State
import Data.Functor.Identity
import Data.Monoid

data Tree a 
  = Leaf
  | Node (Tree a) a (Tree a)
  deriving (Show)

-- posSumAndMul2 :: Int -> State Int Int

main :: IO ()
main = do
  let t1 = Node (Node Leaf 20 Leaf) (-42) (Node Leaf 22 Leaf) :: Tree Int
  print t1

  -- print $ traverse (Identity . (*2)) t1
  -- print $ traverse (Const . Sum . max 0) t1

