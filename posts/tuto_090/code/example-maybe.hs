
ifPositive :: Int -> Maybe Int
ifPositive x = if x >= 0 then Just x else Nothing

main :: IO ()
main = do
  let l1 = [1, 2, 3, 4]
      l2 = [1, -2, 3, -4]

  print $ fmap ifPositive l1
  print $ fmap ifPositive l2

  print $ sequenceA $ fmap ifPositive l1
  print $ sequenceA $ fmap ifPositive l2

  print $ traverse ifPositive l1
  print $ traverse ifPositive l2

  print $ sequence $ fmap ifPositive l1
  print $ sequence $ fmap ifPositive l2

  print $ mapM ifPositive l1
  print $ mapM ifPositive l2

