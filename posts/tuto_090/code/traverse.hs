{-# LANGUAGE DeriveTraversable #-}

import Data.Monoid (Sum(..))

-- import Control.Applicative (Const(..), getConst)
-- import Data.Functor.Identity (Identity(..), runIdentity)

-------------------------------------------------------------------------------
-- fmap
-------------------------------------------------------------------------------

myFmap :: (Traversable t) => (a -> b) -> t a -> t b
myFmap f = runIdentity . traverse (Identity . f)

newtype Identity a = Identity { runIdentity :: a }

instance Functor Identity where
    fmap f (Identity x) = Identity (f x)

instance Applicative Identity where
    pure = Identity
    Identity f <*> Identity x = Identity (f x)

-------------------------------------------------------------------------------
-- foldMap
-------------------------------------------------------------------------------

myFoldMap :: (Traversable t, Monoid m) => (a -> m) -> t a -> m
myFoldMap f = getConst . traverse (Const . f)

newtype Const a b = Const { getConst :: a }

instance Functor (Const a) where
    fmap _ (Const x) = Const x

instance Monoid a => Applicative (Const a) where
    pure _ = Const mempty
    Const x <*> Const y = Const (x `mappend` y)

-------------------------------------------------------------------------------
-- test
-------------------------------------------------------------------------------

data Tree a 
  = Leaf
  | Node (Tree a) a (Tree a)
  deriving (Foldable, Functor, Show, Traversable)

main :: IO ()
main = do
  let t2 = Node Leaf (-13) (Node Leaf 42 Leaf) :: Tree Int
  print t2
  print $ myFmap (*2) t2
  print $ myFoldMap Sum t2

  print $ myFmap (*2) [1, -2, 3, -4]
  print $ myFoldMap Sum [1, -2, 3, -4]

