
import Control.Applicative 
import Control.Monad.State
import Data.Functor.Identity
import Data.Monoid

positiveSum :: Int -> Const (Sum Int) ()
positiveSum = Const . Sum . max 0

positiveState :: Int -> State Int ()
positiveState x = modify (+ max 0 x)

mul2State :: Int -> State () Int
mul2State = pure . (*2)

mul2Identity :: Int -> Identity Int
mul2Identity = Identity . (*2)

main :: IO ()
main = do
  let l1 = [1, 2, 3, 4]
      l2 = [1, -2, 3, -4]

  putStrLn "\nfold"
  print $ foldl (\acc x -> if x>=0 then acc+x else acc) 0 l2

  print $ traverse positiveSum l1
  print $ traverse positiveSum l2
  print $ getSum $ getConst $ traverse positiveSum l2

  print $ runState (traverse positiveState l1) 0
  print $ runState (traverse positiveState l2) 0
  print $ execState (traverse positiveState l2) 0

  putStrLn "\nmap"
  print $ map (*2) l2
 
  print $ traverse mul2Identity l2
  print $ runIdentity $ traverse mul2Identity l2
 
  print $ runState (traverse mul2State l2) ()
  print $ evalState (traverse mul2State l2) ()

