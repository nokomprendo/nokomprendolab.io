{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}

import Control.Applicative 
import Control.Monad.State
import Data.Functor.Identity
import Data.Monoid

data Tree a 
  = Leaf
  | Node (Tree a) a (Tree a)
  deriving (Foldable, Functor, Show)

instance Traversable Tree where
  -- traverse :: Applicative f => (a -> f b) -> Tree a -> f (Tree b)

  traverse _ Leaf = pure Leaf
  traverse f (Node l x r) = do
    l' <- traverse f l
    x' <- f x
    r' <- traverse f r
    pure $ Node l' x' r'

--   traverse _ Leaf = pure Leaf
--   traverse f (Node l x r) = Node <$> traverse f l <*> f x <*> traverse f r

main :: IO ()
main = do
  let t1 = Node (Node Leaf 20 Leaf) (-42) (Node Leaf 22 Leaf) :: Tree Int
  print t1
  print $ traverse (Identity . (*2)) t1
  print $ traverse (Const . Sum . max 0) t1

  let f :: Int -> State Int Int
      f x = when (x>=0) (modify (+x)) >> pure (x*2)
  print $ runState (traverse f t1) 0

