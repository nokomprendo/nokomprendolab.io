{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE DeriveFunctor #-}

import Control.Monad.Free 
import Control.Monad.State 
import System.Directory

-------------------------------------------------------------------------------
-- models
-------------------------------------------------------------------------------

data FilesF a
  = FilesLs ([FilePath] -> a)
  | FilesMkdir FilePath a
  | FilesRmdir FilePath a
  deriving (Functor)

type Files = Free FilesF

filesLs :: Files [FilePath]
filesLs = liftF $ FilesLs id

filesMkdir :: FilePath -> Files ()
filesMkdir fp = liftF $ FilesMkdir fp ()

filesRmdir :: FilePath -> Files ()
filesRmdir fp = liftF $ FilesRmdir fp ()

-------------------------------------------------------------------------------
-- apps
-------------------------------------------------------------------------------

app2 :: Files [FilePath]
app2 = do
  filesMkdir "output1"
  files <- filesLs
  filesRmdir "output1"
  return files

-------------------------------------------------------------------------------
-- interpreters 
-------------------------------------------------------------------------------

runFilesFs :: FilesF a -> IO a
runFilesFs (FilesLs next) = do
  putStrLn "running filesLs"
  files <- listDirectory "."
  return $ next files
runFilesFs (FilesMkdir fp next) = do
  putStrLn "running filesMkdir"
  createDirectory fp
  return next
runFilesFs (FilesRmdir fp next) = do
  putStrLn "running filesRmdir"
  removeDirectory fp
  return next

runFilesMock :: FilesF a -> IO a
runFilesMock (FilesLs next) = do
  putStrLn "mock filesLs"
  return $ next ["mock1", "mock2"]
runFilesMock (FilesMkdir fp next) = do
  putStrLn $ "mock filesMkdir " <> fp
  return next
runFilesMock (FilesRmdir fp next) = do
  putStrLn $ "mock filesRmdir " <> fp
  return next

runFilesPure :: FilesF a -> State [String] a
runFilesPure (FilesLs next) = do
  modify' (++ ["filesLs"])
  return $ next ["pure1", "pure2"]
runFilesPure (FilesMkdir fp next) = do
  modify' (++ ["filesMkdir " <> fp])
  return next
runFilesPure (FilesRmdir fp next) = do
  modify' (++ ["filesRmdir " <> fp])
  return next
--
-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

main :: IO ()
main = do
  putStrLn "\napp2 fs"
  foldFree runFilesFs app2 >>= print

  putStrLn "\napp2 mock"
  foldFree runFilesMock app2 >>= print

  putStrLn "\napp2 pure"
  let (a, s) = runState (foldFree runFilesPure app2) []
  print s
  print a

