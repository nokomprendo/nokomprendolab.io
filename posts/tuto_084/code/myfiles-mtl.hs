{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleContexts #-}

import Control.Monad.IO.Class
import Control.Monad.State 
import System.Directory

-------------------------------------------------------------------------------
-- models
-------------------------------------------------------------------------------

class Monad m => MonadFiles m where
  filesLs :: m [FilePath]
  filesMkdir :: FilePath -> m ()
  filesRmdir :: FilePath -> m ()

-------------------------------------------------------------------------------
-- apps
-------------------------------------------------------------------------------

app2 :: MonadFiles m => m [FilePath]
app2 = do
  filesMkdir "output1"
  files <- filesLs
  filesRmdir "output1"
  return files

-------------------------------------------------------------------------------
-- interpreters
-------------------------------------------------------------------------------

newtype FilesFs m a = FilesFs { runFilesFs :: m a }
  deriving (Functor, Applicative, Monad, MonadIO)

instance MonadIO m => MonadFiles (FilesFs m) where
  filesLs = do
    liftIO $ putStrLn "running filesLs"
    liftIO $ listDirectory "."
  filesMkdir fp =  do
    liftIO $ putStrLn "running filesMkdir"
    liftIO $ createDirectory fp
  filesRmdir fp =  do
    liftIO $ putStrLn "running filesRmdir"
    liftIO $ removeDirectory fp

newtype FilesMock m a = FilesMock { runFilesMock :: m a }
  deriving (Functor, Applicative, Monad, MonadIO)

instance MonadIO m => MonadFiles (FilesMock m) where
  filesLs = do
    liftIO $ putStrLn "mock filesLs"
    return ["mock1", "mock2"]
  filesMkdir fp = 
    liftIO $ putStrLn $ "mock filesMkdir " <> fp
  filesRmdir fp = 
    liftIO $ putStrLn $ "mock filesRmdir " <> fp

newtype FilesPure m a = FilesPure { runFilesPure :: State [String] a}
  deriving (Functor, Applicative, Monad, MonadState [String])

instance MonadFiles (FilesPure m) where
  filesLs = do
    modify' (++ ["filesLs"])
    return ["pure1", "pure2"]
  filesMkdir fp = do
    modify' (++ ["filesMkdir " <> fp])
    return ()
  filesRmdir fp = do
    modify' (++ ["filesRmdir " <> fp])
    return ()

-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

main :: IO ()
main = do
  putStrLn "\napp2 fs"
  runFilesFs app2 >>= print

  putStrLn "\napp2 mock"
  runFilesMock app2 >>= print

  putStrLn "\napp2 pure"
  let (a, s) = runState (runFilesPure app2) []
  print s
  print a

