-- https://markkarpov.com/post/free-monad-considered-harmful.html
{-# Language LambdaCase #-}
{-# Language DeriveFunctor #-}

import Control.Monad.Free
import Control.Monad.Free.Church
import System.IO (hFlush, stdout)

data Terminal a
  = TermGet (String -> a)
  | TermPrint String a
  deriving Functor

type TerminalM = F Terminal

termGet :: TerminalM String
termGet = toF (Free (TermGet return))

termPrint :: String -> TerminalM ()
termPrint str = liftF (TermPrint str ())

myProgram :: TerminalM ()
myProgram = do
  a <- termGet
  b <- termGet
  termPrint (a ++ b)

interpret :: TerminalM a -> IO a
interpret = foldF $ \case
  TermGet next -> do
    putStr "> "
    hFlush stdout
    next <$> getLine
  TermPrint str next -> do
    putStrLn str
    return next

main :: IO ()
main = interpret myProgram


