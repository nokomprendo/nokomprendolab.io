-- https://markkarpov.com/post/free-monad-considered-harmful.html

{-# Language LambdaCase #-}
{-# Language DeriveFunctor #-}

import Control.Monad.Free
import System.IO (hFlush, stdout)

data Terminal a
  = TermGet (String -> a)
  | TermPrint String a
  deriving Functor

type TerminalM = Free Terminal

termGet :: TerminalM String
termGet = Free (TermGet return)

termPrint :: String -> TerminalM ()
termPrint str = liftF (TermPrint str ())

myProgram :: TerminalM ()
myProgram = do
  a <- termGet
  b <- termGet
  termPrint (a ++ b)

interpret :: TerminalM a -> IO a
interpret = foldFree $ \case
  TermGet next -> do
    putStr "> "
    hFlush stdout
    next <$> getLine
  TermPrint str next -> do
    putStrLn str
    return next

main :: IO ()
main = interpret myProgram

