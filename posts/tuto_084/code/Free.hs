{-# Language RankNTypes #-}

module Free where

data Free f a
  = Pure a
  | Free (f (Free f a))

instance Functor f => Functor (Free f) where
  fmap f (Pure a) = Pure (f a)
  fmap f (Free x) = Free (fmap (fmap f) x)

instance Functor f => Applicative (Free f) where
  pure = Pure
  Pure f <*> Pure a = Pure (f a)
  Pure f <*> Free x = Free (fmap f <$> x)
  Free x <*> my     = Free ((<*> my) <$> x)

instance Functor f => Monad (Free f) where
  return = pure
  Pure a >>= f = f a
  Free x >>= f = Free ((>>= f) <$> x)

liftF :: Functor f => f a -> Free f a
liftF = Free . fmap return

foldFree
  :: (Functor f, Monad m)
  => (forall t. f t -> m t)
  -> Free f a
  -> m a
foldFree _ (Pure a) = return a
foldFree f (Free xs) = f xs >>= foldFree f 

