(define-module (myhello)
  #:use-module (guix gexp) 
  #:use-module (guix packages)
  #:use-module (gnu packages cmake)
  #:use-module (guix build-system cmake))

(define-public myhello
  (package
    (name "myhello")
    (version "0.1")
    (source (local-file "" #:recursive? #t))
    (build-system cmake-build-system)
    (arguments '(#:tests? #f)) 
    (synopsis "myhello synopsis")
    (description "myhello description")
    (home-page "http://www.example.org")
    (license #f)))

