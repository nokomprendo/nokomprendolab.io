{ pkgs ? import <nixpkgs> {} }:

with pkgs; stdenv.mkDerivation {
  name = "myhello";
  src = ./.;
  buildInputs = [ cmake ];
}

