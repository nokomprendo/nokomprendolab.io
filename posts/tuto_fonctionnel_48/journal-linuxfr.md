---
title: NixOS vs Guix System, premiers pas
---

[NixOS](https://nixos.org/) et [Guix System](https://guix.gnu.org/) sont des distributions Linux basées sur des gestionnaires de paquets utilisant une approche fonctionnelle (Nix et GNU Guix). Cette approche permet de gérer des environnements logiciels reproductibles et composables, ce qui est très pratique pour un développeur ou pour un administrateur système.

Cet article ne compare pas vraiment NixOS et Guix System mais présente, pour les deux solutions, comment installer le système, le configurer et l'utiliser pour développer. L'idée est plutôt d'aider un utilisateur de l'un à découvrir l'autre, et vice et versa.

Voir aussi : [video youtube](https://youtu.be/OhO4TKp_1o0) - [video peertube](https://peertube.fr/videos/watch/f539a2e4-8ea7-4fa4-ac16-446070e6d6bc) - [article de blog](https://nokomprendo.gitlab.io/posts/tuto_fonctionnel_48/2020-12-01-fr-README.html)

# Installer le système

## NixOS

NixOS nécessite une installation manuelle (voir le [manuel NixOS](https://nixos.org/manual/nixos/stable/) ou [cet article de blog](https://nokomprendo.gitlab.io/posts/tuto_fonctionnel_45/2020-11-09-fr-README.html#installation-de-nixos)). Celle-ci n'est pas compliquée mais un peu fastidieuse. Il s'agit notamment de :

- [télécharger l'image ISO](https://nixos.org/download.html) et booter dessus
- préparer les partitions
- générer et éditer la configuration du système à installer
- lancer l'installation et rebooter sur le système installé

## Guix System

Guix System propose un installeur semi-graphique très pratique (voir le [manuel de référence GNU Guix](https://guix.gnu.org/manual/en/html_node/index.html)).  Ainsi l'installation se résume à :

- [télécharger l'image ISO](https://guix.gnu.org/en/download/), la décompresser et booter dessus
- suivre les étapes de l'installeur
- lancer l'installation et rebooter sur le système installé

# Mettre à jour le système

Les deux solutions fonctionnent de façon très similaire :

- un *update* télécharge des dernières versions des paquets (sans les installer)
- un fichier définit la configuration du système
- un *rebuild* reconstruit le système (installe les nouveaux paquets ou la nouvelle configuration système)

## NixOS

- update :

```
$ sudo nix-channel --update
```

- rebuild (utilise le `channel` du `root`) :

```
$ sudo nixos-rebuild switch
```

- exemple de fichier de configuration `/etc/nixos/configuration.nix` :

```nix
{ config, pkgs, ... }: {

  imports = [ ./hardware-configuration.nix ];

  boot.loader.grub = {
    enable = true;
    device = "/dev/sda";
    version = 2;
  };  

  i18n.defaultLocale = "fr_FR.UTF-8";
  system.stateVersion = "20.09";
  time.timeZone = "Europe/Paris";

  console = {
    font = "Lat2-Terminus16";
    keyMap = "fr-bepo";
  };

  environment.systemPackages = with pkgs; [ vim ];

  networking = {
    hostName = "mynixos";
    useDHCP = false;
    interfaces.enp0s3.useDHCP = true;
  };

  services = {
    xserver = {
      enable = true;
      layout = "fr";
      displayManager.lightdm.enable = true;
      desktopManager.xfce.enable = true;
    };
  };

  users.users.toto = {
    isNormalUser = true;
    extraGroups = [ "wheel" ];
  };
}
```

## Guix System

- update :

```
$ guix pull
```

- rebuild (utilise le `PATH` courant) :

```
$ sudo guix system reconfigure /etc/config.scm
```

- exemple de fichier de configuration `/etc/config.scm` :

```scheme
(use-modules (gnu) (gnu packages vim) (gnu packages admin) (gnu packages certs))
(use-service-modules desktop networking ssh xorg)

(operating-system
  (locale "fr_FR.utf8")
  (timezone "Europe/Paris")
  (keyboard-layout (keyboard-layout "fr" "bepo"))
  (host-name "myguix")

  (users (cons* (user-account
                  (name "toto")
                  (comment "Toto")
                  (group "users")
                  (home-directory "/home/toto")
                  (supplementary-groups '("wheel" "netdev" "audio" "video")))
                %base-user-accounts))
  (packages 
    (append
      (list vim htop nss-certs)
      %base-packages))
  (services 
    (append
      (list (service xfce-desktop-service-type)
            (set-xorg-configuration
              (xorg-configuration (keyboard-layout keyboard-layout))))
      %desktop-services))
  (bootloader
    (bootloader-configuration
      (bootloader grub-bootloader)
      (target "/dev/sda")
      (keyboard-layout keyboard-layout)))
  (file-systems
    (cons* (file-system
             (mount-point "/")
             (device (uuid "ad23de44-8331-487f-b86f-8a5f940bd702" 'ext4))
             (type "ext4"))
           %base-file-systems)))
```

# Installer des logiciels

Les deux systèmes permettent à un utilisateur standard de gérer ses logiciels (installer, supprimer, rollbacks...).

## Nix

- chercher un logiciel :

```
$ nix search geany

* nixpkgs.geany (geany)
  Small and lightweight IDE
...
```

- installer un logiciel :

```
$ nix-env -iA nixos.geany

installing 'geany-1.36'
...
```

- lister les logiciels installés :

```
$ nix-env -q

geany-1.36
...
```

- désinstaller un logiciel :

```
$ nix-env -e geany

uninstalling 'geany-1.36'
```

- lister les générations :

```
$ nix-env --list-generations

  72   2020-11-28 19:31:13   
  73   2020-12-02 21:18:48   (current)
```

## Guix

- chercher un logiciel :

```
$ guix search geany

name: geany
version: 1.37
...
```

- installer un logiciel :

```
$ guix install geany

Le paquet suivant sera installé :
   geany 1.37
...
```

- lister les logiciels installés :

```
$ guix packages --list-installed

geany	1.37	out	/gnu/store/bsffsj4iqaq0f14m8zpds81gklrs0n4x-geany-1.37
...
```

- désinstaller un logiciel :

```
$ guix remove geany

Le paquet suivant sera supprimé :
   geany 1.37
```

- lister les générations :

```
$ guix packages --list-generations

Génération 6	02 déc. 2020 19:14:03
 + geany	1.37	out	/gnu/store/bsffsj4iqaq0f14m8zpds81gklrs0n4x-geany-1.37

Génération 7	02 déc. 2020 21:50:15	(actuelle)
 - geany	1.37	out	/gnu/store/bsffsj4iqaq0f14m8zpds81gklrs0n4x-geany-1.37
```

# Quelques fonctionnalités pour développer 

Nix et Guix sont très pratique pour un développeur. En gros, on ajoute un fichier de packaging à un projet et on peut ensuite lancer un environnement de développement, construire le projet, installer le projet comme un logiciel classique, etc.

## Projet d'exemple

- code source C++ (`myhello.cpp`) :

```cpp
#include <iostream>

int main() {
    std::cout << "This is myhello!" << std::endl;
    return 0;
}
```

- fichier de configuration cmake (`CMakeLists.txt`) :

```cmake
project( myhello )
add_executable( myhello myhello.cpp )
install( TARGETS myhello DESTINATION bin )
```

## Nix

- fichier de packaging (`default.nix`) :

```nix
{ pkgs ? import <nixpkgs> {} }:

with pkgs; stdenv.mkDerivation {
  name = "myhello";
  src = ./.;
  buildInputs = [ cmake ];
}
```

- lancer un environnement de développement (`exit` ou Ctrl-d pour quitter) :

```
$ nix-shell

[nix-shell]$ cmake -S . -B build
...

$ cmake --build build
...

$ ./build/myhello
This is hello!
```

- construire le projet (penser à supprimer le dossier `build` avant) :

```
$ nix-build
...

$ ./result/bin/myhello
This is hello!
```

- installer le projet :

```
$ nix-env -f . -i
...

$ myhello
This is hello!
```

## Guix

- fichier de packaging (`myhello.scm`) :

```scheme
(define-module (myhello)
  #:use-module (guix gexp) 
  #:use-module (guix packages)
  #:use-module (gnu packages cmake)
  #:use-module (guix build-system cmake))

(define-public myhello
  (package
    (name "myhello")
    (version "0.1")
    (source (local-file "" #:recursive? #t))
    (build-system cmake-build-system)
    (arguments '(#:tests? #f)) 
    (synopsis "myhello synopsis")
    (description "myhello description")
    (home-page "http://www.example.org")
    (license #f)))
```

- lancer un environnement de développement (`exit` ou Ctrl-d pour quitter) :

```
$ guix environment --load-path=. myhello
...

[env]$ cmake -S . -B build
...
```

- construire le projet :

```
$ guix build --load-path=. myhello
...
/gnu/store/pxl4lx7h01a7mpn7ddqyp2r4xbdyhaxy-myhello-0.1

$ /gnu/store/pxl4lx7h01a7mpn7ddqyp2r4xbdyhaxy-myhello-0.1/bin/myhello
This is hello!
```

- installer le projet :

```
$ guix install --load-path=. myhello
...

$ myhello
This is hello!
```

# Conclusion

Nix et Guix sont tous les deux inspirés des [travaux de thèse de Eelco Dolstra](https://edolstra.github.io/pubs/phd-thesis.pdf) (modèle de déploiement logiciel purement fonctionnel). Si la mise en œuvre diffère dans ces deux projets (implémentations, langages, etc), certaines fonctionnalités de base y sont assez similaires.

Bien-sûr, cet article n'a fait qu'effleurer le sujet. Nix et Guix ont beaucoup d'autres fonctionnalités, et c'est sur ces fonctionnalités plus avancées que les deux projets vont certainement se différencier.

