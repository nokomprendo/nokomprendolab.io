# myrepeat_v2

## build & run a docker image

```
docker build -t myrepeat:v2 .
docker run --rm -it -e PORT=3000 -p 3000:3000 myrepeat:v2
```

image size: 83MB
 

## deploy on heroku

```
heroku container:login
heroku create myrepeat
heroku container:push web --app myrepeat
heroku container:release web --app myrepeat
```

