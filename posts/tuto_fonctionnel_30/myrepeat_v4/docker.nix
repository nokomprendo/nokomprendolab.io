{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.09.tar.gz") {} }:

let

  mywt = pkgs.callPackage ./wt.nix {};

  myapp = import ./default.nix { inherit pkgs; wt = mywt; };

  entrypoint = pkgs.writeScript "entrypoint.sh" ''
    #!${pkgs.stdenv.shell}
    $@ --docroot . --http-address 0.0.0.0 --http-port $PORT
  '';

in

  pkgs.dockerTools.buildImage {
    name = "myrepeat";
    tag = "v4";
    config = {
      Entrypoint = [ entrypoint ];
      Cmd = [ "${myapp}/bin/myrepeat" ];
    };
  }

