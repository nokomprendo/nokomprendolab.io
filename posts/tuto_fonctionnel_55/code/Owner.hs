{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Owner where

import Data.Aeson
import Data.Text
import GHC.Generics

data Owner = Owner
    { mylogin :: Text
    , myid :: Int
    } deriving (Generic, Show)

instance FromJSON Owner where
    parseJSON = withObject "Owner" $ \v -> Owner
        <$> v .: "login"
        <*> v .: "id"

instance ToJSON Owner where
    toEncoding (Owner l i) = 
        pairs (  "login" .= l
              <> "id"    .= i
              )

