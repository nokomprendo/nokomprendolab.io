{-# LANGUAGE OverloadedStrings #-}

import Data.Aeson
import Data.ByteString.Lazy.Char8 as C

import Owner
import Repo

main :: IO ()
main = do

    repoE <- eitherDecodeFileStrict "data.json"
    case repoE of
        Left err -> print err
        Right repo -> print (repo :: Repo)

