{-# LANGUAGE OverloadedStrings #-}

import Data.Aeson
import Data.ByteString.Lazy.Char8 as C

import Owner

main :: IO ()
main = C.putStrLn $ encode Owner { mylogin="haskell", myid=450574 }

