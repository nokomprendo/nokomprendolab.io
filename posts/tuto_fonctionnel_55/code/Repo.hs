{-# LANGUAGE DeriveGeneric #-}

module Repo where

import Data.Aeson
import Data.Text
import GHC.Generics

import Owner

data Repo = Repo
    { id :: Int
    , name :: Text
    , private :: Bool
    , description :: Text
    , owner :: Owner
    } deriving (Generic, Show)

instance FromJSON Repo
instance ToJSON Repo

