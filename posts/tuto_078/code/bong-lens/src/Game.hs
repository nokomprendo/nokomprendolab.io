{-# LANGUAGE TemplateHaskell #-}

module Game where

import Control.Lens 
import Linear.V2

import Collision

-------------------------------------------------------------------------------
-- params
-------------------------------------------------------------------------------

gameXMin, gameXMax, gameYMin, gameYMax :: Float
gameXMin = -300
gameXMax = 300
gameYMin = -200
gameYMax = 200

ballY0, bobY0, bobRadius, ballRadius :: Float
ballY0 = -130
bobY0 = -170
bobRadius = 35
ballRadius = 10

ballVel0 :: V2 Float
ballVel0 = V2 0 300

-------------------------------------------------------------------------------
-- types
-------------------------------------------------------------------------------

data Status = Running | Launching

data Game = Game
    { _ballPos :: V2 Float
    , _ballVel :: V2 Float
    , _bobPos :: V2 Float
    , _status :: Status
    }
makeLenses ''Game

-------------------------------------------------------------------------------
-- functions
-------------------------------------------------------------------------------

newGame :: Game
newGame = Game (V2 0 ballY0) (V2 0 0) (V2 0 bobY0) Launching

moveGame :: Float -> Game -> Game
moveGame x g = 
    let g1 = g & bobPos . _x .~ x
        g2 = g1 & ballPos . _xy .~ V2 x ballY0
    in case g^.status of
        Running -> g1
        Launching -> g2

launchGame :: Game -> Game
launchGame g = g & status .~ Running
                 & ballPos . _xy .~ V2 (g^.bobPos._x) ballY0
                 & ballVel . _xy .~ ballVel0

animateGame :: Float -> Game -> Game
animateGame dt g = case g^.status of
    Running -> 
        let objects =
                [ Paddle (_bobPos g) bobRadius
                , WallTop gameYMax
                , WallLeft gameXMin
                , WallRight gameXMax
                ]
            b0 = Ball (g ^. ballPos) (g ^. ballVel) ballRadius
            b1 = animateBall dt b0 objects
        in if b1^.pos._y < gameYMin 
            then g & status .~ Launching 
            else g & ballPos .~ b1^.pos
                   & ballVel .~ b1^.vel
    _ -> g

