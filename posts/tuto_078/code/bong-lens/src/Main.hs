{-# LANGUAGE TemplateHaskell #-}

import Control.Lens 
import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
import Linear.V2

import Game

data Model = Model
    { _bobPicture :: Picture
    , _bobScale :: Float
    , _game :: Game
    }
makeLenses ''Model

main :: IO ()
main = do
    bob <- loadBMP "bob.bmp"
    let window = InWindow "Bong" (gameWidth, gameHeight) (0, 0) 
        bgcolor = makeColor 0.4 0.6 1.0 1.0
        fps = 30 
        model = Model bob 1 newGame
    play window bgcolor fps model handleDisplay handleEvent handleTime

gameWidth, gameHeight :: Int
gameWidth = round $ gameXMax - gameXMin
gameHeight = round $ gameYMax - gameYMin

handleDisplay :: Model -> Picture
handleDisplay (Model bobPicture bobScale game) = 
    let (V2 ballX ballY) = _ballPos game
        (V2 bobX bobY) = _bobPos game
    in Pictures
        [ translate bobX bobY $ scale bobScale 1 $ Pictures 
            [bobPicture, color black $ circle bobRadius]
        , translate ballX ballY $ color white $ circleSolid ballRadius
        ]

handleEvent :: Event -> Model -> Model
handleEvent (EventMotion (x, _)) m = 
    m & bobScale .~ (if m ^. game . bobPos . _x > x then 1 else -1)
      & game %~ moveGame x
handleEvent (EventKey (MouseButton LeftButton) Up _ _) m = 
    m & game %~ launchGame
handleEvent _ m = m

handleTime :: Float -> Model -> Model
handleTime dt m = m & game %~ animateGame dt

