{-# LANGUAGE TemplateHaskell #-}

module Collision where

import Control.Lens 
import Linear.Affine
import Linear.Metric
import Linear.V2
import Linear.Vector

-------------------------------------------------------------------------------
-- types
-------------------------------------------------------------------------------

data Ball = Ball 
    { _pos :: V2 Float
    , _vel :: V2 Float
    , _rad :: Float 
    }
makeLenses ''Ball

data Shape 
    = Paddle    { _paddlePos :: V2 Float , _paddleRad :: Float }
    | WallLeft  { _xLeft :: Float }
    | WallRight { _xRight :: Float }
    | WallTop   { _yTop :: Float }
makeLenses ''Shape

-------------------------------------------------------------------------------
-- Hit
-------------------------------------------------------------------------------

data Hit = Hit
    { _hitTime :: Float
    , _hitBall :: Ball
    }

tryHit :: Float -> Ball -> Shape -> Maybe Hit

tryHit t b@(Ball p v r) (WallLeft x0) = 
    let ti = (x0 + r - p^._x) / v^._x
        o1 = moveBall ti b & vel . _x %~ negate
    in if ti>0 && ti<t then Just (Hit ti o1) else Nothing

tryHit t b@(Ball p v r) (WallRight x1) = 
    let ti = (x1 - r - p^._x) / v^._x 
        o1 = moveBall ti b & vel . _x %~ negate
    in if ti>0 && ti<t then Just (Hit ti o1) else Nothing

tryHit t b@(Ball p v r) (WallTop y1) = 
    let ti = (y1 - r - p^._y) / v^._y
        o1 = moveBall ti b & vel . _y %~ negate
    in if ti>0 && ti<t then Just (Hit ti o1) else Nothing

tryHit t b1@Ball{} b2@Paddle{} = 
    let (Ball p1@(V2 p1x p1y) v1@(V2 v1x v1y) r1) = b1
        (Paddle p2@(V2 p2x p2y) r2) = b2
        -- detect collision
        dvx = v1x
        dvy = v1y
        dpx = p1x-p2x
        dpy = p1y-p2y
        dmin = r1+r2
        a = dvx*dvx + dvy*dvy
        b = dpx*dvx + dpy*dvy
        c = dpx*dpx + dpy*dpy - dmin*dmin
        delta = b*b - a*c
        rd = sqrt delta
        ti = min ((-b-rd)/a) ((-b+rd)/a)
        -- compute bounces
        pI = p1 .+^ ti *^ v1
        n = normalize (pI ^-^ p2)
        v1' = v1 ^-^ (2 * dot n v1 *^ n)
        b1' = b1 { _pos = pI, _vel = v1' }
    in if (abs a > 0) && delta>0 && ti>=0 && ti<=t
        then Just (Hit ti b1')
        else Nothing

-------------------------------------------------------------------------------
-- functions
-------------------------------------------------------------------------------

moveBall :: Float -> Ball -> Ball
moveBall dt b = b & pos +~ dt *^ _vel b

computeFirstHit :: Float -> Ball -> [Shape] -> Maybe Hit
computeFirstHit dt ball objects =
    let go [] hitM = hitM
        go (o:os) hitM = 
            go os $ case (hitM, tryHit dt ball o) of
                (Just hit0, Just hit1) -> 
                    Just $ if _hitTime hit0 < _hitTime hit1 then hit0 else hit1
                (Nothing, hitM') -> hitM'
                _ -> hitM
    in go objects Nothing

animateBall :: Float -> Ball -> [Shape] -> Ball
animateBall dt ball objects
    | dt <= 0 = ball
    | otherwise = 
        case computeFirstHit dt ball objects of
            Nothing -> moveBall dt ball
            Just (Hit dti bi) -> animateBall (dt-dti) bi objects

