import Data.Function

data Point = Point Double Double
    deriving (Eq, Show)

data Line = Line Point Point
    deriving (Eq, Show)

mkLine :: Double -> Double -> Double -> Double -> Line
mkLine x1 y1 x2 y2 = 
    Line (Point x1 y1) (Point x2 y2)

getX1 :: Line -> Double
getX1 (Line (Point x1 _) _) = x1

resetP1 :: Line -> Line
resetP1 (Line _ p2) = Line (Point 0 0) p2

moveXs :: Double -> Line -> Line
moveXs dx (Line (Point x1 y1) (Point x2 y2)) = 
    Line (Point (x1+dx) y1) 
         (Point (x2+dx) y2)

main :: IO ()
main = do
    let line = mkLine 1 2 3 4

    print $ getX1 line
    print $ resetP1 line
    print $ moveXs 10 line

    print $ moveXs 10 $ resetP1 line
    print (line & resetP1 & moveXs 10)

