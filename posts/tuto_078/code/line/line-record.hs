import Data.Function

data Point = Point 
    { _x :: Double 
    , _y :: Double
    } deriving (Eq, Show)

data Line = Line 
    { _p1 :: Point 
    , _p2 :: Point
    } deriving (Eq, Show)

mkLine :: Double -> Double -> Double -> Double -> Line
mkLine x1 y1 x2 y2 = 
    Line (Point x1 y1) (Point x2 y2)

getX1 :: Line -> Double
getX1 l = _x $ _p1 l

resetP1 :: Line -> Line
resetP1 l = l { _p1 = Point 0 0 }

moveXs :: Double -> Line -> Line
moveXs dx l = 
    let p1 = _p1 l
        p2 = _p2 l
        x1 = _x p1
        x2 = _x p2
    in l { _p1 = p1 { _x = x1+dx }
         , _p2 = p2 { _x = x2+dx }
         }

main :: IO ()
main = do
    let line = mkLine 1 2 3 4

    print $ getX1 line
    print $ resetP1 line
    print $ moveXs 10 line

    print $ moveXs 10 $ resetP1 line
    print (line & resetP1 & moveXs 10)

