
let

  pkgs = import <nixpkgs> {};

  ghc = pkgs.haskellPackages.ghcWithPackages (ps: with ps; [
    lens
  ]);

in pkgs.stdenv.mkDerivation {
  name = "haskell-env";
  buildInputs = [
    pkgs.nim
    ghc 
  ];
  shellHook = "eval $(egrep ^export ${ghc}/bin/ghc)";
}

