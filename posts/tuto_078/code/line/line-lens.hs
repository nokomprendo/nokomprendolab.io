{-# Language TemplateHaskell #-}

import Control.Lens
import Data.Function

data Point = Point 
    { _x :: Double 
    , _y :: Double
    } deriving (Eq, Show)

makeLenses ''Point

data Line = Line 
    { _p1 :: Point 
    , _p2 :: Point
    } deriving (Eq, Show)

makeLenses ''Line

mkLine :: Double -> Double -> Double -> Double -> Line
mkLine x1 y1 x2 y2 = 
    Line (Point x1 y1) (Point x2 y2)

getX1 :: Line -> Double
getX1 l = l ^. p1 . x

resetP1 :: Line -> Line
resetP1 l = l & p1 .~ Point 0 0

moveXs :: Double -> Line -> Line
moveXs dx l = l & p1 . x +~ dx
                & p2 . x +~ dx

main :: IO ()
main = do
    let line = mkLine 1 2 3 4

    print $ getX1 line
    print $ resetP1 line
    print $ moveXs 10 line

    print $ moveXs 10 $ resetP1 line
    print (line & resetP1 & moveXs 10)

