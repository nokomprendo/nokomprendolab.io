# nim c -r line1.nim

{.experimental: "strictFuncs".}

type 
  Point = object
    x: float
    y: float
  Line = object
    p1: Point
    p2: Point

func mkLine(x1,y1,x2,y2: float): Line =
  Line(p1: Point(x: x1, y: y1), p2: Point(x: x2, y: y2))

func getX1(l: Line): float =
  l.p1.x

func resetP1(l: Line): Line =
  Line(p1: Point(x: 0, y: 0), p2: l.p2)

func moveXs(l: Line, dx: float): Line =
  var ll = l
  ll.p1.x += dx
  ll.p2.x += dx
  ll

when isMainModule:
  let line = mkLine(1, 2, 3, 4)

  echo getX1(line)
  echo resetP1(line)
  echo moveXs(line, 10)

  echo moveXs(resetP1(line), 10)

