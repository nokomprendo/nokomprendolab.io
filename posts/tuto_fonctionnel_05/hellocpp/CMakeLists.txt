cmake_minimum_required( VERSION 3.0 )
project( hellocpp )
add_executable( hellocpp hellocpp.cpp )
install( TARGETS hellocpp DESTINATION bin )

