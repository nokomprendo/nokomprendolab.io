---
title: Architectures de code en Haskell (5/6), Free monades
description: Dans l'article précédent, on a vu une façon classique d'architecturer un code Haskell, en utilisant des transformateurs de monade. On va maintenant voir une autre architecture classique, en utilisant des free monades.
---

Voir aussi : [video youtube](https://youtu.be/Ik9pTjaRYAc) -
[code source](https://gitlab.com/nokomprendo/harchi)

Dans l'article précédent, on a vu une façon classique d'architecturer un code
Haskell, en utilisant des transformateurs de monade. On va maintenant voir une
autre architecture classique, en utilisant des free monades.

Pour rappel, le projet de code est un serveur d'API JSON qui fournit des
informations sur des personnes stockées dans une base Sqlite et génère des
messages de log. Le projet contient aussi des tests automatisés et une
application de test.  Enfin, le code est découpé en différentes parties pour
réduire le couplage de code (domaine métier, effets, interpréteurs,
applications). 


# Implémentation avec des free monades

## Messages de log

Les free monades permettent de définir une monade à partir d'un foncteur.
Ainsi, pour définir notre effet de messages de log, on peut écrire un foncteur
`LoggerF` puis en obtenir automatiquement une free monade `Logger` :

```haskell
-- Harchi.Free.Effects.Logger

data LoggerF a
  = LogMsg String (() -> a)
  deriving (Functor)

type Logger = Free LoggerF
```

`LogMsg` est la valeur du foncteur qui va nous permettre de générer un message
de log. Elle contient le message de log et la fonction à appliquer ensuite, sur
le résultat (ici, on aurait pu simplifier en `LogMsg String a`).

Pour simplifier l'utilisation de notre monade `Logger`, on définit une fonction
`logMsg`, qui construit un `LogMsg` : 

```haskell
logMsg :: String -> Logger ()
logMsg msg = liftF $ LogMsg msg id
```

On écrit ensuite un interpréteur de notre foncteur, par exemple, pour afficher
les messages de log à l'écran :

```haskell
-- Harchi.Free.Interpreters.LoggerStdout

interpretLoggerStdout :: MonadIO m => String -> LoggerF a -> m a
interpretLoggerStdout prefix = \case
  LogMsg str next -> do
    liftIO $ putStrLn $ "[" <> prefix <> "] " <> str
    pure $ next ()
```

On peut alors écrire une application dans la monade `Logger` et l'interpréter :

```haskell
appLogger1 :: Logger ()
appLogger1 = do
    logMsg "foo"
    logMsg "bar"

runLoggerStdout :: MonadIO m => String -> Logger a -> m a
runLoggerStdout prefix = foldFree (interpretLoggerStdout prefix)
```

Résultat de l'exécution :

```sh
ghci> import Harchi.Free.Interpreters.LoggerStdout 

ghci> runLoggerStdout "test" appLogger1 
[test] foo
[test] bar
```


## Base de données

Idem pour définir un effet permettant d'accéder à nos données. On écrit un
foncteur représentant les fonctionnalités voulues :

```haskell
-- Harchi.Free.Effects.Db

data DbF a
  = GetPersons ([Person] -> a)
  | GetPersonFromId Int ([Person] -> a)
  deriving (Functor)
```

Puis on en génère une free monade et ses fonctions associées :

```haskell
type Db = Free DbF

getPersons :: Db [Person]
getPersons = liftF $ GetPersons id

getPersonFromId :: Int -> Db [Person]
getPersonFromId i = liftF $ GetPersonFromId i id
```

Et on écrit un interpréteur qui implémente cet effet pour notre base de données
Sqlite :

```haskell
-- Harchi.Free.Interpreters.DbSql

interpretDbSql :: MonadIO m => FilePath -> DbF a -> m a
interpretDbSql sqlFile = \case

  GetPersons next -> do
    fmap next $ liftIO $ withConnection sqlFile $ \conn -> 
      query_ conn 
        "SELECT personId, personName, personCountry \
        \FROM persons"

  GetPersonFromId i next -> do
    fmap next $ liftIO $ withConnection sqlFile $ \conn -> 
      query conn 
        "SELECT personId, personName, personCountry \
        \FROM persons \
        \WHERE personId = (?)" (Only i)
```

Les free monades permettent d'écrire des effets et des interpréteurs assez
facilement. Par contre, combiner plusieurs free monades est plus compliqué. Les
sections suivantes présentent deux façons de faire cela.


# Architecture avec le foncteur Sum

La bibliothèque de base d'Haskell définit un type
[Sum](https://hackage.haskell.org/package/base-4.20.0.0/docs/Data-Functor-Sum.html)
qui implémente la somme de deux foncteurs et qui est lui-même un foncteur :

```haskell
data Sum f g a
  = InL (f a)
  | InR (g a)

instance (Functor f, Functor g) => Functor (Sum f g) where
    fmap f (InL x) = InL (fmap f x)
    fmap f (InR y) = InR (fmap f y)
```


## Regrouper plusieurs effets

Avec ce type `Sum`, on peut donc définir une pile de foncteurs (un peu comme
les piles de monades, vues dans l'article précédent) et en générer une
free monade :

```haskell
-- Harchi.Free.Applications.DbLogger

type DbLoggerF m = Sum DbF (Sum LoggerF m)

type DbLogger m = Free (DbLoggerF m)
```

Ici, `DbLogger m` est une free monade qui supporte nos effets de message
de log et de base de données et d'une monade `m`.

On définit également les fonctions suivantes, pour accéder aux différents
effets et pour lancer leurs interpréteurs respectifs :

```haskell
liftDb :: Monad m => Db a -> DbLogger m a
liftDb = hoistFree InL

liftLogger :: Monad m => Logger a -> DbLogger m a
liftLogger = hoistFree InR . hoistFree InL

interpretSum :: (f a -> t a) -> (g a -> t a) -> Sum f g a -> t a
interpretSum interpretL _  (InL x) = interpretL x
interpretSum _  interpretR (InR x) = interpretR x
```


## Application de test

On peut maintenant écrire notre application de test, en utilisant la (free)
monade `DbLogger` :

```haskell
-- Harchi.Free.Applications.Test

testApp :: MonadIO m => DbLogger m [Person]
testApp = do
  liftLogger $ logMsg "Applications Test testApp getPerson 2"
  res1 <- liftDb (getPersonFromId 2) 
  liftLogger $ logMsg "Applications Test testApp getPersons"
  res2 <- liftDb getPersons 
  return (res1 ++ res2)
```

Enfin, on spécifie les différents interpréteurs voulus, pour exécuter
l'application :

```haskell
runApp :: DbLogger IO a -> IO a
runApp app = foldFree (interpretSum db (interpretSum logger id)) app
  where
    db = interpretDbSql "persons.db"
    logger = interpretLoggerStdout "Free"
```

Résultat de l'exécution :

```sh
ghci> import Harchi.Free.Applications.Test 

ghci> runApp testApp 
[Free] Applications Test testApp getPerson 2
[Free] Applications Test testApp getPersons
[Person {personId = 2, personName = "Alan Turing", personCountry = "GB"}, ...]
```


## Tests automatisés 

Pour tester automatiquement l'application `testApp`, peut définir un nouvel
interpréteur de l'effet `Logger`, qui récupère les messages de log dans un
`Writer` :

```haskell
-- Harchi.Free.Applications.TestSpec

interpretLoggerWriter :: LoggerF a -> WriterT [String] IO a
interpretLoggerWriter (LogMsg str next) = do
  tell [str]
  pure $ next ()
```

On définit enfin un nouvel interpréteur pour exécuter l'application et
vérifier ses différents résultats dans un test unitaire : 

```haskell
runTestApp :: DbLogger (WriterT [String] IO) [Person] -> IO ([Person], [String])
runTestApp = runWriterT . foldFree (interpretSum db (interpretSum logger id))
  where
    db = interpretDbSql "persons.db"
    logger = interpretLoggerWriter

spec :: Spec
spec = do

  describe "Applications Test" $ do

    it "logs + db" $ do
      (ps, logs) <- runTestApp testApp 
      ps `shouldBe` (person2 : persons)
      logs `shouldBe` 
        [ "Applications Test testApp getPerson 2"
        , "Applications Test testApp getPersons" ]
```

Ainsi, le functeur `Sum` permet de combiner plusieurs effets, sous forme de
free monade, via une pile de foncteurs.


# Architecture "Hierarchical Free Monad"

L'architecture HFM consiste à regrouper les effets non plus via une pile de
foncteurs mais directement dans un type (lui-même foncteur). 


## Regrouper plusieurs effets

Pour pouvoir utiliser nos deux effets `Logger` et `Db`, on écrit un foncteur
`AppF`, permettant d'utiliser ces deux effets :

```haskell
-- Harchi.HFM.Effects.App

data AppF a where
  AppLogger :: Logger r -> (r -> a) -> AppF a
  AppDb :: Db r -> (r -> a) -> AppF a

instance Functor AppF where
  fmap f (AppLogger action next) = AppLogger action (f . next)
  fmap f (AppDb action next) = AppDb action (f . next)
```

On en déduit une free monade et des fonctions d'accès :

```haskell
type App = Free AppF

appLogger :: Logger a -> App a
appLogger action = liftF $ AppLogger action id

appDb :: Db a -> App a
appDb action = liftF $ AppDb action id
```

Et on écrit facilement un interpréteur pour `AppF`, en réutilisant nos
interpréteurs  de `Logger` et de `Db` :

```haskell
-- Harchi.HFM.Interpreters.AppSql

interpretAppSql :: MonadIO m => String -> FilePath -> AppF a -> m a
interpretAppSql prefix sqlFile = \case
  AppLogger action next -> next <$> foldFree (interpretLoggerStdout prefix) action
  AppDb action next     -> next <$> foldFree (interpretDbSql sqlFile) action

runAppSql :: MonadIO m => String -> FilePath -> App a -> m a
runAppSql prefix sqlFile app = foldFree (interpretAppSql prefix sqlFile) app
```


## Application de test

On a une monade `App` pour écrire l'application de test et l'interpréteur
`runAppSql` pour l'exécuter :

```haskell
-- Harchi.HFM.Applications.Test

testApp :: App [Person]
testApp = do
  appLogger $ logMsg "Applications Test testApp getPerson 2"
  res1 <- appDb (getPersonFromId 2) 
  appLogger $ logMsg "Applications Test testApp getPersons"
  res2 <- appDb getPersons 
  return (res1 ++ res2)

runApp :: App a -> IO a
runApp app = runAppSql "HFM" "persons.db" app
```


## Tests automatisés 

Pour les tests automatisés, il suffit d'écrire un nouvel interpréteur pour
`Logger` et pour `App` :

```haskell
-- Harchi.HFM.Applications.TestSpec

interpretLoggerWriter :: LoggerF a -> WriterT [String] IO a 
interpretLoggerWriter (LogMsg str next) = do
  tell [str]
  pure $ next ()

interpretApp :: AppF a -> WriterT [String] IO a
interpretApp = \case
  AppLogger action next -> next <$> foldFree interpretLoggerWriter action 
  AppDb action next     -> next <$> foldFree (interpretDbSql sqlFile) action

runTestApp :: App [Person] -> IO ([Person], [String])
runTestApp app = runWriterT $ foldFree interpretApp app
```

L'architecture HFM permet de regrouper plusieurs effets dans un type foncteur,
plutôt que dans un pile. Dans projet de code complexe, cela permet de réduire
les niveaux de profondeurs nécessaires pour spécifier les différents effets
voulus.


# Conclusion

Le principe des free monades est plutôt élégant et pratique. Cependant,
architecturer un code avec un ensemble de free monades n'est pas complètement
trivial. Dans ce cas, il est probablement plus interessant d'utiliser un
système d'effet, comme Polysemy, qui s'occupe de gérer cette complexité.


Voir aussi :

- [free: Monads for free](https://hackage.haskell.org/package/free)
- [Free monad considered harmful](https://markkarpov.com/post/free-monad-considered-harmful.html)
- [Combining free monads in Haskell](https://blog.ploeh.dk/2017/07/24/combining-free-monads-in-haskell/)
- [Hierarchical Free Monads: The Most Developed Approach In Haskell](https://github.com/graninas/hierarchical-free-monads-the-most-developed-approach-in-haskell/blob/master/README.md)
- [Software Design and Architecture in Haskell](https://github.com/graninas/software-design-in-haskell)


