#!/usr/bin/env python3
import numpy as np

def randMat():
  return np.random.random((2,3))

if __name__ == "__main__":
  m = randMat()
  print(m)

