import Bot
import Cmp

import Control.Monad
import Control.Monad.ST
import System.Random.MWC
import System.TimeIt

main :: IO ()
main = do
    -- botR <- BotRandom <$> createSystemRandom
    -- botY <- BotMc 4 <$> createSystemRandom

    -- botR <- BotMc 64 <$> createSystemRandom
    -- botY <- BotMcts 256 <$> createSystemRandom

    botR <- BotMc 128 <$> createSystemRandom
    botY <- BotMcts 512 <$> createSystemRandom

    let nGames = 100
        nRepeat = 10 :: Int

    putStrLn "winR WinY tie ry ryt dt nGames"
    forM_ [1..nRepeat] $ const $ do
        (dt, (r, y, t)) <- timeItT $ stToIO (run botR botY nGames)
        putStrLn $ unwords (map show [r, y, t, r+y, r+y+t, dt] ++ [show nGames])

