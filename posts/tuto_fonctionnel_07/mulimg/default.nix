{ system ? builtins.currentSystem }:

let
    pkgs = import <nixpkgs> { inherit system; };
    myopencv3 = pkgs.opencv3.override { enableOpenblas = true; };
in

with pkgs;
stdenv.mkDerivation rec {
    name = "mulimg";
    buildInputs = [ 
        #opencv3
        myopencv3
        gnumake
        pkgconfig
        python3Packages.opencv3
        python3Packages.pandas
    ];
    src = ./.;
    installPhase = ''
        mkdir -p $out/bin
        cp mulimg.out $out/bin/
    '';
}

