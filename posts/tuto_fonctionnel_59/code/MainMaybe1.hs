import Text.Read

readName :: IO (Maybe String)
readName = do
    putStrLn "name?"
    Just <$> getLine

readAge :: IO (Maybe Int)
readAge = do
    putStrLn "age?"
    readMaybe <$> getLine

readHaskeller :: IO (Maybe Bool)
readHaskeller = do
    putStrLn "haskeller?"
    readMaybe <$> getLine

main :: IO ()
main = do
    maybeName <- readName
    case maybeName of
        Nothing -> putStrLn "error"
        Just name -> do
            maybeAge <- readAge
            case maybeAge of
                Nothing -> putStrLn "error"
                Just age -> do
                    maybeHaskeller <- readHaskeller
                    case maybeHaskeller of
                        Nothing -> putStrLn "error"
                        Just haskeller -> 
                            print (name, age, haskeller)

