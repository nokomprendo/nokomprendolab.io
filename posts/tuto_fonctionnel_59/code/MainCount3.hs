import Control.Monad.Trans.Class
import Control.Monad.Trans.State.Lazy

newtype MyState = MyState
    { _numbers :: [Int]
    } deriving Show

myState0 :: MyState
myState0 = MyState [42, 13, 42, 37]

count42 :: Monad m => StateT MyState m Int
count42 = do
    s0 <- get
    case _numbers s0 of
        [] -> return 0
        (n:ns) -> do
            let r1 = if n==42 then 1 else 0
                s1 = MyState ns
            put s1
            return r1

app :: StateT MyState IO (Int, Int)
app = do
    c0 <- count42
    lift $ putStrLn $ "count42: " <> show c0
    c1 <- count42
    lift $ putStrLn $ "count42: " <> show c1
    return (c0, c1)

main :: IO ()
main = do
    (r, s) <- runStateT app myState0
    putStrLn $ "result: " <> show r
    putStrLn $ "state: " <> show s

