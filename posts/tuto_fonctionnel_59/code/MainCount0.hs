newtype MyState = MyState
    { _numbers :: [Int]
    } deriving Show

myState0 :: MyState
myState0 = MyState [42, 13, 42, 37]

count42 :: MyState -> (Int, MyState)
count42 s0 = 
    case _numbers s0 of
        [] -> (0, s0)
        (n:ns) -> 
            let r1 = if n==42 then 1 else 0
                s1 = MyState ns
            in (r1, s1)

app :: MyState -> ((Int, Int), MyState)
app s0 = 
    let (c1, s1) = count42 s0
        (c2, s2) = count42 s1
    in ((c1, c2), s2)

main :: IO ()
main = do
    let (r, s) = app myState0
    putStrLn $ "result: " <> show r
    putStrLn $ "state: " <> show s

