import Control.Applicative
import Control.Monad.Trans.State
import Data.Char

-------------------------------------------------------------------------------
-- parser type
-------------------------------------------------------------------------------

type Parser = StateT String Maybe

runParser :: Parser a -> String -> Maybe (a, String)
runParser = runStateT 

itemP :: Parser Char
itemP = do
    c:cs <- get
    put cs
    return c

-------------------------------------------------------------------------------
-- parsers
-------------------------------------------------------------------------------

satisfy :: (Char -> Bool) -> Parser Char
satisfy p = do
    x <- itemP
    if p x then return x else empty

symbolP :: Char -> Parser Char
symbolP c = satisfy (==c)

digitP :: Parser Char
digitP = satisfy isDigit

digitsP :: Parser String
digitsP = some digitP

intP :: Parser Int
intP = posIntP <|> negIntP
    where 
        posIntP = read <$> digitsP
        negIntP = do
                _ <- symbolP '-'
                x <- digitsP
                return (- read x)

doubleP :: Parser Double
doubleP = 
    do  c0 <- symbolP '-' <|> return ' '
        s1 <- digitsP
        s2 <- ((:) <$> symbolP '.' <*> digitsP) <|> return ""
        return $ read (c0 : s1 ++ s2)

-------------------------------------------------------------------------------
-- main parsers
-------------------------------------------------------------------------------

main :: IO ()
main = do
    print $ runParser digitP "f"
    print $ runParser digitP "4"
    print $ runParser digitsP "42"
    print $ runParser intP "42"
    print $ runParser intP "-42"
    print $ runParser intP "+42"
    print $ runParser doubleP "42"
    print $ runParser doubleP "-13.37"
    print $ runParser doubleP "3.37"

