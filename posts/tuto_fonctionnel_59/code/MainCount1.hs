import Control.Monad.Trans.State.Lazy

newtype MyState = MyState
    { _numbers :: [Int]
    } deriving Show

myState0 :: MyState
myState0 = MyState [42, 13, 42, 37]

count42 :: State MyState Int
count42 = do
    s0 <- get
    case _numbers s0 of
        [] -> return 0
        (n:ns) -> do
            let r1 = if n==42 then 1 else 0
                s1 = MyState ns
            put s1
            return r1

app :: State MyState (Int, Int)
app = do
    c1 <- count42
    c2 <- count42
    return (c1, c2)

main :: IO ()
main = do
    let (r, s) = runState app myState0
    putStrLn $ "result: " <> show r
    putStrLn $ "state: " <> show s

