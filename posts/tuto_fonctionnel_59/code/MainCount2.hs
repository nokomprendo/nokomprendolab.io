import Control.Monad.Trans.State.Lazy

newtype MyState = MyState
    { _numbers :: [Int]
    } deriving Show

myState0 :: MyState
myState0 = MyState [42, 13, 42, 37]

count42 :: State MyState Int
count42 = do
    s0 <- get
    case _numbers s0 of
        [] -> return 0
        (n:ns) -> do
            let r1 = if n==42 then 1 else 0
                s1 = MyState ns
            put s1
            return r1

app :: MyState -> IO ((Int, Int), MyState)
app s0 = do
    let (c1, s1) = runState count42 s0
    putStrLn $ "count42: " <> show c1
    let (c2, s2) = runState count42 s1
    putStrLn $ "count42: " <> show c2
    return ((c1, c2), s2)

main :: IO ()
main = do
    (r, s) <- app myState0
    putStrLn $ "result: " <> show r
    putStrLn $ "state: " <> show s

