import Text.Read

readName :: String -> Maybe String
readName = Just

readAge :: String -> Maybe Int
readAge = readMaybe 

readHaskeller :: String -> Maybe Bool
readHaskeller = readMaybe 

main :: IO ()
main = do
    putStrLn "name?"
    strName <- getLine
    putStrLn "age?"
    strAge <- getLine
    putStrLn "haskeller?"
    strHaskeller <- getLine

    let maybeNameAgeHaskeller = do
            name <- readName strName
            age <- readAge strAge
            haskeller <- readHaskeller strHaskeller
            return (name, age, haskeller)

    case maybeNameAgeHaskeller of
        Nothing -> putStrLn "error"
        Just info -> print info

