mul2debug :: Int -> IO Int
mul2debug x = do
    putStrLn $ "mul2debug " <> show x
    return $ x*2

main :: IO ()
main = do
    x <- mul2debug 21
    print x

