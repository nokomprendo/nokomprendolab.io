{-# LANGUAGE OverloadedStrings #-}
import Control.Monad.IO.Class
import Data.Text.Lazy
import Data.Text.Lazy.IO as L
import Web.Scotty

handleRoute :: Text -> ActionM ()
handleRoute msg = do
    liftIO $ L.putStrLn $ "sending: " <> msg
    text msg

main :: IO ()
main = 
    scotty 3000 $ do
        get "/" $ handleRoute "this is /"
        get "/info" $ handleRoute "this is /info"

