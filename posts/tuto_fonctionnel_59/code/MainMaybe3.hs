import Control.Monad.Trans as Trans
import Control.Monad.Trans.Maybe
import Text.Read hiding (lift)

readName :: MaybeT IO String
readName = MaybeT $ do
    putStrLn "name?"
    Just <$> getLine

readAge :: MaybeT IO Int
readAge = MaybeT $ do
    putStrLn "age?"
    readMaybe <$> getLine

readHaskeller :: MaybeT IO Bool
readHaskeller = MaybeT $ do
    putStrLn "haskeller?"
    readMaybe <$> getLine

main :: IO ()
main = do
    maybeNameAgeHaskeller <- runMaybeT $ do
        name <- readName 
        age <- readAge 
        haskeller <- readHaskeller 
        lift $ putStrLn "ok"
        return (name, age, haskeller)

    case maybeNameAgeHaskeller of
        Nothing -> putStrLn "error"
        Just info -> print info

