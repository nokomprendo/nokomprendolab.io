{ stdenv, fetchFromGitHub }:

stdenv.mkDerivation {
  name = "mustache";

  src = fetchFromGitHub {
    owner = "kainjow";
    repo = "mustache";
    rev = "3cf1aadff9f37d248af534f25e8c13b59948b762";
    sha256 = "099clndahw3d1kl0yhyishfcdqbbxarrfb59j229pg2w5nqil1ji";
  };

  buildPhase = "";

  installPhase = ''
    mkdir -p $out/include
    cp mustache.hpp $out/include
  '';
}


