{ stdenv, fetchFromGitHub, sqlite }:

stdenv.mkDerivation {
  name = "sqlite_modern_cpp";

  src = fetchFromGitHub {
    owner = "SqliteModernCpp";
    repo = "sqlite_modern_cpp";
    rev = "56af65d2c5085dd34a2c1a4ad20cf8b93ad1024f";
    sha256 = "1g8kn3r55p6s17kbqj9rl121r24y5h7lh7sa7rsc2xq1bd97i07b";
  };

  buildInputs = [ sqlite ];
}

