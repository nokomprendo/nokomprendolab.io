#ifndef VIEW_HPP
#define VIEW_HPP

#include "Animal.hpp"

// render the about page to HTML
std::string renderAbout();

// render the home page to HTML
std::string renderHome(
    std::string myquery, 
    std::vector<Animal> animals);

#endif

