{-# LANGUAGE OverloadedStrings #-}

import           Animal
import           View
import           Control.Monad.Trans (liftIO) 
import           Network.Wai.Middleware.RequestLogger (logStdoutDev)
import           Network.Wai.Middleware.Static (addBase, staticPolicy)
import           Web.Scotty (get, html, middleware, param, rescue, scotty) 

-- run a server on port 3000
main = scotty 3000 $ do

    -- show logs
    middleware logStdoutDev

    -- serve the about page
    get "/about" $ html aboutPage

    -- serve the home page (and filter the animals using the myquery parameter)
    get "/" $ do
        myquery <- param "myquery" `rescue` (\_ -> return "")
        animals <- liftIO $ getAnimals myquery
        html $ homePage myquery animals

    -- serve static files (located in the "static" directory)
    middleware $ staticPolicy $ addBase "static"

