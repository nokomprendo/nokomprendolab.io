with import <nixpkgs> {};

let
  _cpprestsdk = callPackage ./nix/cpprestsdk.nix {};
  _ctml = callPackage ./nix/ctml.nix {};
  _sqlite_orm = callPackage ./nix/sqlite_orm.nix {};
in

mkShell {
  buildInputs = [
    boost
    _cpprestsdk
    _ctml
    gnumake
    openssl
    sqlite
    _sqlite_orm
  ];
}

