#include "Animal.hpp"

#include <cppdb/frontend.h>

// deserialize an Animal from a database result 
cppdb::result & operator>>(cppdb::result & res, Animal & a) {
  res >> a.name >> a.image;
  return res;
}

std::vector<Animal> getAnimals(const std::string & myquery) {

  // query database
  cppdb::session sql("sqlite3:db=animals.db");
  cppdb::result res = sql 
    << "SELECT name,image FROM animals WHERE name like ?||'%'"
    << myquery;

  // fetch results
  std::vector<Animal> animals;
  while(res.next()) {
    Animal animal;
    res >> animal;
    animals.push_back(animal);
  }
  return animals;
}

