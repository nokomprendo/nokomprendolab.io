#ifndef HOME_APP_HPP
#define HOME_APP_HPP

#include "Animal.hpp"

#include <Wt/Dbo/Dbo.h>
#include <Wt/Dbo/backend/Sqlite3.h>

#include <Wt/WApplication.h>
#include <Wt/WAnchor.h>
#include <Wt/WImage.h>
#include <Wt/WLineEdit.h>
#include <Wt/WTemplate.h>
#include <Wt/WText.h>

#include <numeric>
#include <functional>

bool isPrefixOf(const std::string & txt, const std::string & fullTxt) {
  return std::inner_product(std::begin(txt), std::end(txt), std::begin(fullTxt), 
      true, std::logical_and<bool>(), std::equal_to<char>());
} 

// widget showing an animal (name + image + anchor) 
class AnimalWidget : public Wt::WAnchor {
  private:
    // pointer to the WText that contains the animal name
      Wt::WText * _animalName;

  public:
    AnimalWidget(const Animal & animal) {
      // set anchor href
      const std::string imagePath = "img/" + animal.image;
      setLink(Wt::WLink(imagePath));
      // create a container widget, inside the anchor widget
      auto cAnimal = addWidget(std::make_unique<Wt::WContainerWidget>());
      cAnimal->setStyleClass("divCss");
      // create a text widget, inside the container
      auto cText = cAnimal->addWidget(std::make_unique<Wt::WContainerWidget>());
      cText->setPadding(Wt::WLength("1em"));
      _animalName = cText->addWidget(std::make_unique<Wt::WText>(animal.name));
      // create an image widget, inside the container
      auto img = cAnimal->addWidget(std::make_unique<Wt::WImage>(imagePath));
      img->setStyleClass("imgCss");
    }

    void filter(const std::string & txt) {
      // show the widget if txt is null or if it is a prefix of the animal name
      setHidden(txt != "" and not isPrefixOf(txt, _animalName->text().toUTF8()));
    }
};

// Application class implementing the home page
class HomeApp : public Wt::WApplication {
  private:
    // the line edit widget (for querying animal to show/hide)
      Wt::WLineEdit * _myquery;

    // the animal widgets 
    std::vector<AnimalWidget*> _animalWidgets;

    // main HTML template of the application
    const std::string _app_template = R"(
        <h1>Animals (Wt)</h1>
        <p>${myquery}</p>
        ${animals}
        <p style="clear: both"><a href="/about">About</a></p>
    )";

    // show all animals that match the _myquery prefix
    void filterAnimals() {
      for(auto aw : _animalWidgets)
        aw->filter(_myquery->text().toUTF8());
    }

  public:
    // create the application
    HomeApp(const Wt::WEnvironment & env) : WApplication(env) {
      // load css
      useStyleSheet({"style.css"});
      // create the main widget using the HTML template
      auto r = root()->addWidget(std::make_unique<Wt::WTemplate>(_app_template));
      // create the remaining widgets and bind them to the template placeholders
      _myquery = r->bindWidget("myquery", std::make_unique<Wt::WLineEdit>());
      // connect the widget _myquery to the function filterAnimals 
      _myquery->textInput().connect(this, &HomeApp::filterAnimals);

      // create a container widget for the animals
      auto w = r->bindWidget("animals", std::make_unique<Wt::WContainerWidget>());
      // open the database
      Wt::Dbo::Session session;
      session.setConnection(std::make_unique<Wt::Dbo::backend::Sqlite3>("animals.db"));
      session.mapClass<AnimalDb>("animals");
      // query the database
      Wt::Dbo::Transaction transaction(session);
      Wt::Dbo::collection<Wt::Dbo::ptr<AnimalDb>> dboAnimals = session.find<AnimalDb>();
      for (const Wt::Dbo::ptr<AnimalDb> & dboAnimal : dboAnimals) {
        // add a widget
        auto aw = w->addWidget(std::make_unique<AnimalWidget>(*dboAnimal));
        // store a pointer, for future updates
        _animalWidgets.push_back(aw);
      }
    }
};

#endif

