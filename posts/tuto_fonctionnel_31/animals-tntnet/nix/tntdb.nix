{ stdenv, fetchurl, cxxtools, postgresql, mysql, sqlite, zlib, openssl }:

stdenv.mkDerivation rec {
  name = "tntdb-${version}";
  version = "1.4rc2";

  src = fetchurl {
    url = "http://www.tntnet.org/download/${name}.tar.gz";
    sha256 = "0b0gzidfrz4qw6lj88vwrsxhvjaq0vkh0vhqzqnzi4whlwqpz3rc";
  };

  buildInputs = [ cxxtools postgresql mysql.connector-c sqlite zlib openssl ];

  enableParallelBuilding = true;
}

