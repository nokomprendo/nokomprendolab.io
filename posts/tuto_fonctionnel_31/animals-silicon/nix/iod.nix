{ stdenv, fetchFromGitHub, cmake, boost }:

stdenv.mkDerivation {
  name = "iod";

  src = fetchFromGitHub {
    owner = "matt-42";
    repo = "iod";
    rev = "81289c6bcb1969385ff68ce7876bca07545c8e85";
    sha256 = "13i9a5qqpgyyswk6mqw2pb7f6w1s0w0acqzsvfy82hk2n42r50i4";
  };

  buildInputs = [ cmake boost ];
}



