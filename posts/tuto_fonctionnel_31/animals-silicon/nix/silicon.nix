{ stdenv, fetchFromGitHub, cmake }:

stdenv.mkDerivation {
  name = "silicon";

  src = fetchFromGitHub {
    owner = "matt-42";
    repo = "silicon";
    rev = "18e1a3057586c409cf7f53f86ef8533a1635556a";
    sha256 = "0lx6rm0d3gbx8q49c41jpn1v6jv8n78k2m7yhpr0v1zpym4p35zv";
  };

  buildInputs = [ cmake ];
}

