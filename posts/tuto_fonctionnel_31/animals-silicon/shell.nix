with import <nixpkgs> {};

let
  _iod = callPackage ./nix/iod.nix {};
  _silicon = callPackage ./nix/silicon.nix {};
in

mkShell {
  buildInputs = [
    boost
    gnumake
    _iod
    libmicrohttpd
    _silicon
    sqlite
  ];
}

