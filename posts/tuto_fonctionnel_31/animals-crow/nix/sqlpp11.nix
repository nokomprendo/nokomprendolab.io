{ stdenv, fetchFromGitHub, cmake, hinnantdate, pythonPackages }:

stdenv.mkDerivation {
  name = "sql11";

  src = fetchFromGitHub {
    owner = "rbock";
    repo = "sqlpp11";
    rev = "32abab9da44f6746553edb6bd64995001d3eccc9";
    sha256 = "0yf0lisjbyfxcj8cmh8gjsi80a50a52fzbp6dli8l2jh71rsk609";
  };

  enableParallelBuilding = true;

  cmakeFlags = [
    "-DHinnantDate_ROOT_DIR=${hinnantdate}"
  ];

  buildInputs = [
    cmake
    hinnantdate
  ];

  propagatedBuildInputs = [
    pythonPackages.pyparsing
  ];
}

