{ stdenv, fetchFromGitHub, cmake, sqlite, hinnantdate, sqlpp11 }:

stdenv.mkDerivation {
  name = "sqlpp11-sqlite3";

  src = fetchFromGitHub {
    owner = "rbock";
    repo = "sqlpp11-connector-sqlite3";
    rev = "bde9fb214f36fee22e9d2f81dce95759cf4de287";
    sha256 = "1jvj76652kpdifzkqi3f8y769iy88pabb8bhy5z21fyf8swq70ld";
  };

  enableParallelBuilding = true;

  cmakeFlags = [
    "-DSQLPP11_INCLUDE_DIR=${sqlpp11}/include"
  ];

  buildInputs = [
    cmake
    hinnantdate
    sqlite
    sqlpp11
  ];
}


