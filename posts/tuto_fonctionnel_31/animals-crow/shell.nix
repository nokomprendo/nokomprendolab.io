with import <nixpkgs> {};

let
  _crow = callPackage ./nix/crow.nix {};

  _hinnantdate = callPackage ./nix/hinnantdate.nix {};

  _sqlpp11 = callPackage ./nix/sqlpp11.nix {
    hinnantdate = _hinnantdate;
    pythonPackages = python3Packages;
  };

  _sqlpp11_sqlite3 = callPackage ./nix/sqlpp11-sqlite3.nix {
    hinnantdate = _hinnantdate;
    sqlpp11 = _sqlpp11;
  };
in

mkShell {
  buildInputs = [
    boost
    _crow
    gnumake
    _hinnantdate
    sqlite
    _sqlpp11
    _sqlpp11_sqlite3
  ];
}

