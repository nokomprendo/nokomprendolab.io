
import qualified Data.Vector as V

import Control.Monad.Cont
import System.IO

import Maze

type Event = Char

handleDisplay :: MonadIO m => Maze -> ContT r m ()
handleDisplay (Maze b ni nj ip jp) = do
  liftIO $ putChar '\n'
  forM_ [0 .. ni-1] $ \i -> do
    forM_ [0 .. nj-1] $ \j -> do
      let c = if i==ip && j== jp
                then 'O'
                else cell2char (b V.! ij2k i j ni nj)
      liftIO $ putChar c
    liftIO $ putChar '\n'

handleEvent :: Monad m => Event -> Maze -> ContT r m Maze
handleEvent event maze = 
  case event of
    'h' -> return $ play MoveLeft maze
    'j' -> return $ play MoveDown maze
    'k' -> return $ play MoveUp maze
    'l' -> return $ play MoveRight maze
    _ -> return maze

getEvent :: MonadIO m => ContT r m Event
getEvent = liftIO getChar

loop :: MonadIO m => Maze -> ContT r m ()
loop maze = 
  when (isRunning maze) $ do
    handleDisplay maze 
    move <- getEvent
    maze' <- handleEvent move maze
    loop maze'

main :: IO ()
main = runMaze $ \maze -> do
  hSetBuffering stdin NoBuffering
  runContT (loop maze) return

