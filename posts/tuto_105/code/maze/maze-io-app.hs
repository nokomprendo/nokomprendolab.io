
import qualified Data.Vector as V

import Control.Monad
import Control.Monad.IO.Class
import System.IO

import Maze

type Event = Char

newtype App = App
  { _maze :: Maze
  }

handleDisplay :: MonadIO m => App -> m ()
handleDisplay app = do
  let (Maze b ni nj ip jp) = _maze app
  liftIO $ putChar '\n'
  forM_ [0 .. ni-1] $ \i -> do
    forM_ [0 .. nj-1] $ \j -> do
      let c = if i==ip && j== jp
                then 'O'
                else cell2char (b V.! ij2k i j ni nj)
      liftIO $ putChar c
    liftIO $ putChar '\n'

handleEvent :: Monad m => Event -> App -> m App
handleEvent event app@(App maze) = 
  case event of
    'h' -> return $ app { _maze = play MoveLeft maze }
    'j' -> return $ app { _maze = play MoveDown maze }
    'k' -> return $ app { _maze = play MoveUp maze }
    'l' -> return $ app { _maze = play MoveRight maze }
    _ -> return app

getEvent :: MonadIO m => m Event
getEvent = liftIO getChar

loop :: MonadIO m => App -> m ()
loop app = 
  when (isRunning $ _maze app) $ do
    handleDisplay app 
    event <- getEvent
    app' <- handleEvent event app
    loop app'

main :: IO ()
main = runMaze $ \maze -> do
  hSetBuffering stdin NoBuffering
  loop (App maze)

