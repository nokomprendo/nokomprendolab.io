
import qualified Data.Vector as V

import Control.Monad
import Control.Monad.IO.Class
import System.IO

import Maze

type Event = Char

handleDisplay :: MonadIO m => Maze -> (() -> m a) -> m a
handleDisplay (Maze b ni nj ip jp) k = do
  liftIO $ putChar '\n'
  forM_ [0 .. ni-1] $ \i -> do
    forM_ [0 .. nj-1] $ \j -> do
      let c = if i==ip && j== jp
                then 'O'
                else cell2char (b V.! ij2k i j ni nj)
      liftIO $ putChar c
    liftIO $ putChar '\n'
  k ()

handleEvent :: Event -> Maze -> (() -> m a) -> (Maze -> m a) -> m a
handleEvent event maze exit k = 
  case event of
    'h' -> k $ play MoveLeft maze
    'j' -> k $ play MoveDown maze
    'k' -> k $ play MoveUp maze
    'l' -> k $ play MoveRight maze
    'q' -> exit ()
    _ -> k maze

getEvent :: MonadIO m => (Event -> m a) -> m a
getEvent k = liftIO getChar >>= k

loop :: MonadIO m => Maze -> (() -> m a) -> m a
loop maze k =
  if isRunning maze
  then
    handleDisplay maze $ \() ->
      getEvent $ \event ->
        handleEvent event maze k (\maze' -> loop maze' k)
  else k ()

main :: IO ()
main = runMaze $ \maze -> do
  hSetBuffering stdin NoBuffering
  loop maze return


