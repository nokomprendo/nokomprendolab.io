
let

  pkgs = import <nixpkgs> {};

  ghc = pkgs.haskellPackages.ghcWithPackages (ps: with ps; [
    mtl
    sdl2
    transformers
    vector
  ]);

in pkgs.stdenv.mkDerivation {
  name = "haskell-env";
  buildInputs = [
    ghc 
  ];
  shellHook = "eval $(egrep ^export ${ghc}/bin/ghc)";
}


