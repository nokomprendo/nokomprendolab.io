
module Maze where

import qualified Data.Vector as V

import Data.Maybe
import Text.Read
import System.Environment

data Cell
  = CellEmpty
  | CellWall
  | CellTarget
  deriving (Eq)

data Maze = Maze 
  { _board :: V.Vector Cell
  , _ni :: Int
  , _nj :: Int
  , _ip :: Int
  , _jp :: Int
  }

data Move
  = MoveUp
  | MoveDown
  | MoveLeft
  | MoveRight

ij2k :: Int -> Int -> Int -> Int -> Int
ij2k i j _ni nj = i*nj + j

isRunning :: Maze -> Bool
isRunning (Maze b ni nj ip jp) = CellTarget /= b V.! ij2k ip jp ni nj

play :: Move -> Maze -> Maze
play move maze@(Maze b ni nj i0 j0) = 
  let (i1, j1) = case move of
        MoveUp    -> (i0-1, j0)
        MoveDown  -> (i0+1, j0)
        MoveLeft  -> (i0, j0-1)
        MoveRight -> (i0, j0+1)
      (i2, j2) = 
        if i1<0 || i1>=ni || j1<0 || j1>=nj || b V.! ij2k i1 j1 ni nj == CellWall
        then (i0, j0) 
        else (i1, j1)
  in maze { _ip=i2, _jp=j2 }

char2cell :: Char -> Cell
char2cell ' ' = CellEmpty
char2cell '+' = CellWall
char2cell '?' = CellTarget
char2cell _ = error "unknown cell"

cell2char :: Cell -> Char
cell2char CellEmpty = ' '
cell2char CellWall = '+'
cell2char CellTarget = '?'

parseMaze :: String -> Either String Maze
parseMaze contents = do
  case lines contents of
    (infoLine:boardLines) ->
      case mapMaybe readMaybe (words infoLine) of
        [ni, nj, ip, jp] -> 
          let board = V.fromList (map char2cell $ concat boardLines)
              ninj = ni*nj
              len = V.length board
          in if len < ninj
              then Left $ "invalid length: expected " <> show ninj <> ", got " <> show len
              else Right $ Maze board ni nj ip jp
        _ -> Left "cannot read empty"
    _ -> Left "empty file"

startMaze :: (Maze -> IO ()) -> IO ()
startMaze loop = do
 do
    args <- getArgs
    case args of
      [filename] -> do
        contents <- readFile filename
        case parseMaze contents of
          Left err -> putStrLn $ "error in " <> filename <> ": " <> err
          Right maze -> loop maze
      _ -> putStrLn "usage: <filename>"

