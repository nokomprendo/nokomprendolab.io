{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Vector as V

import Control.Monad 
import Control.Monad.IO.Class
import SDL

import Maze
import MazeSdl

handleDisplay :: MonadIO m => App -> (() -> m a) -> m a
handleDisplay (App maze renderer) k = do
  clear renderer
  let (Maze b ni nj ip jp) = maze
  forM_ [0 .. ni-1] $ \i -> do
    forM_ [0 .. nj-1] $ \j -> do
      let c = if i==ip && j== jp
                then playerColor
                else cell2color (b V.! ij2k i j ni nj)
          x = fromIntegral j * cellSize
          y = fromIntegral i * cellSize
      rendererDrawColor renderer $= c
      fillRect renderer $ Just $ Rectangle (P (V2 x y)) (V2 cellSize cellSize)
  present renderer
  k ()

handleEvent :: Event -> App -> (App -> r) -> r
handleEvent event app@(App maze _) k
  | eventIsKeyPressed KeycodeH event = k $ app { _maze = play MoveLeft maze }
  | eventIsKeyPressed KeycodeJ event = k $ app { _maze = play MoveDown maze }
  | eventIsKeyPressed KeycodeK event = k $ app { _maze = play MoveUp maze }
  | eventIsKeyPressed KeycodeL event = k $ app { _maze = play MoveRight maze }
  | otherwise = k app 

getEvents :: MonadIO m => ([Event] -> m a) -> m a
getEvents k = liftIO pollEvents >>= k

loop :: MonadIO m => App -> (() -> m a) -> m a
loop app k = 
  if isRunning (_maze app) 
  then 
    handleDisplay app $ \() ->
      getEvents $ \events -> do
        app' <- foldM (\a e -> handleEvent e a return) app events 
        loop app' k
   else k ()

main :: IO ()
main = startMaze $ \maze -> do
  initializeAll
  let width = cellSize * fromIntegral (_nj maze)
      height = cellSize * fromIntegral (_ni maze)
      winParams = defaultWindow { windowInitialSize = V2 width height }
  window <- createWindow "Maze" winParams
  renderer <- createRenderer window (-1) defaultRenderer
  loop (App maze renderer) return
  destroyWindow window

