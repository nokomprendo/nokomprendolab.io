
import qualified Data.Vector as V

import Control.Monad
import Control.Monad.IO.Class
import System.Exit
import System.IO

import Maze

type Event = Char

handleDisplay :: MonadIO m => Maze -> m ()
handleDisplay (Maze b ni nj ip jp) = do
  liftIO $ putChar '\n'
  forM_ [0 .. ni-1] $ \i -> do
    forM_ [0 .. nj-1] $ \j -> do
      let c = if i==ip && j== jp
                then 'O'
                else cell2char (b V.! ij2k i j ni nj)
      liftIO $ putChar c
    liftIO $ putChar '\n'

handleEvent :: Monad m => Event -> Maze -> m Maze -> m Maze
handleEvent event maze exit = do
  case event of
    'h' -> return $ play MoveLeft maze
    'j' -> return $ play MoveDown maze
    'k' -> return $ play MoveUp maze
    'l' -> return $ play MoveRight maze
    'q' -> exit
    _ -> return maze

getEvent :: MonadIO m => m Event
getEvent = liftIO getChar

loop :: MonadIO m => Maze -> m ()
loop maze = 
  when (isRunning maze) $ do
    handleDisplay maze 
    event <- getEvent
    maze' <- handleEvent event maze (liftIO exitSuccess)
    loop maze'

main :: IO ()
main = runMaze $ \maze -> do
  hSetBuffering stdin NoBuffering
  loop maze

