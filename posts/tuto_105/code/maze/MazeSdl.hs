module MazeSdl where

import Data.Word
import Foreign.C.Types
import SDL

import Maze

data App = App
  { _maze :: Maze
  , _renderer :: Renderer
  }

cell2color :: Cell -> V4 Word8
cell2color CellEmpty = V4 0 0 0 255
cell2color CellWall = V4 150 150 150 255
cell2color CellTarget = V4 255 255 0 255

playerColor :: V4 Word8
playerColor = V4 255 0 0 255

cellSize :: CInt
cellSize = 15

eventIsKeyPressed :: Keycode -> Event -> Bool
eventIsKeyPressed key event =
  case eventPayload event of
    KeyboardEvent keyboardEvent ->
      keyboardEventKeyMotion keyboardEvent == Pressed &&
      keysymKeycode (keyboardEventKeysym keyboardEvent) == key
    _ -> False


