{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Vector as V

import Control.Monad 
import Control.Monad.IO.Class
import Control.Monad.Cont
import SDL

import Maze
import MazeSdl

handleDisplay :: MonadIO m => App -> m ()
handleDisplay (App maze renderer) = do
  clear renderer
  let (Maze b ni nj ip jp) = maze
  forM_ [0 .. ni-1] $ \i -> do
    forM_ [0 .. nj-1] $ \j -> do
      let c = if i==ip && j== jp
                then playerColor
                else cell2color (b V.! ij2k i j ni nj)
          x = fromIntegral j * cellSize
          y = fromIntegral i * cellSize
      rendererDrawColor renderer $= c
      fillRect renderer $ Just $ Rectangle (P (V2 x y)) (V2 cellSize cellSize)
  present renderer

handleEvent :: Monad m => Event -> App -> m App
handleEvent event app@(App maze _) 
  | eventIsKeyPressed KeycodeH event = return $ app { _maze = play MoveLeft maze }
  | eventIsKeyPressed KeycodeJ event = return $ app { _maze = play MoveDown maze }
  | eventIsKeyPressed KeycodeK event = return $ app { _maze = play MoveUp maze }
  | eventIsKeyPressed KeycodeL event = return $ app { _maze = play MoveRight maze }
  | otherwise = return app 

getEvents :: MonadIO m => m [Event]
getEvents = liftIO pollEvents

loop :: MonadIO m => App -> m ()
loop app = 
  when (isRunning $ _maze app) $ do
    handleDisplay app
    events <- getEvents
    app' <- foldM (flip handleEvent) app events
    loop app'

main :: IO ()
main = startMaze $ \maze -> do
  initializeAll
  let width = cellSize * fromIntegral (_nj maze)
      height = cellSize * fromIntegral (_ni maze)
      winParams = defaultWindow { windowInitialSize = V2 width height }
  window <- createWindow "Maze" winParams
  renderer <- createRenderer window (-1) defaultRenderer
  runContT (loop $ App maze renderer) return
  destroyWindow window

