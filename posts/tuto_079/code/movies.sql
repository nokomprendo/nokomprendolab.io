
-- sqlite3 movies.db < movies.sql

CREATE TABLE director (
    dir_id INTEGER PRIMARY KEY AUTOINCREMENT,
    dir_firstname TEXT NOT NULL,
    dir_lastname TEXT NOT NULL
);

CREATE TABLE movie (
    mov_id INTEGER PRIMARY KEY AUTOINCREMENT,
    mov_dir INTEGER NOT NULL,
    mov_title TEXT NOT NULL,
    mov_year INTEGER,
    FOREIGN KEY(mov_dir) REFERENCES director(dir_id)
);

INSERT INTO director VALUES(1, 'Ted', 'Kotcheff');
INSERT INTO director VALUES(2, 'Stanley', 'Kubrick');

INSERT INTO movie VALUES(1, 1, 'First Blood', 1982);
INSERT INTO movie VALUES(2, 2, 'Paths of Glory', 1957);
INSERT INTO movie VALUES(3, 2, 'Full Metal Jacket', NULL);

