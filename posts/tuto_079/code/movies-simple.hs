{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

import Data.Text
import Database.SQLite.Simple
import GHC.Generics

selectDirectors :: Connection -> IO [(Text, Text)]
selectDirectors conn = query_ conn 
    "SELECT dir_firstname, dir_lastname FROM director"

data Production = Production
    { dir_firstname :: Text
    , dir_lastname :: Text
    , mov_title :: Text
    , mov_year :: Maybe Int
    } deriving (Generic, Show)

instance FromRow Production where
    fromRow = Production <$> field <*> field <*> field <*> field

selectProductions :: (Text, Text) -> Connection -> IO [Production]
selectProductions director conn = query conn 
    "SELECT dir_firstname, dir_lastname, mov_title, mov_year \
    \FROM director \
    \INNER JOIN movie ON mov_dir = dir_id \
    \WHERE dir_firstname = ? AND dir_lastname = ?"
    director

-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

main :: IO ()
main = withConnection "movies.db" $ \conn -> do

    putStrLn "\n**** selectDirectors ****"
    selectDirectors conn >>= mapM_ print

    putStrLn "\n**** selectMoviesByDirector ****"
    selectProductions ("Stanley", "Kubrick") conn >>= mapM_ print

