{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeOperators #-}

import Database.Selda
import Database.Selda.SQLite

-------------------------------------------------------------------------------
-- Director
-------------------------------------------------------------------------------

data Director = Director
    { dir_id :: ID Director
    , dir_firstname :: Text
    , dir_lastname :: Text
    } deriving (Generic, Show)

instance SqlRow Director

directoryTable :: Table Director
directoryTable = table "director" [#dir_id :- autoPrimary]

selectDirectors :: SeldaT SQLite IO [Director]
selectDirectors = query $ select directoryTable

-------------------------------------------------------------------------------
-- Movie
-------------------------------------------------------------------------------

data Movie = Movie
  { mov_id:: ID Movie
  , mov_dir :: ID Director
  , mov_title :: Text
  , mov_year :: Maybe Int
  } deriving (Generic, Show)

instance SqlRow Movie

movieTable :: Table Movie
movieTable = table "movie"
    [ #mov_id :- autoPrimary
    , #mov_dir :- foreignKey directoryTable #dir_id ]

-------------------------------------------------------------------------------
-- productions
-------------------------------------------------------------------------------

selectProductions :: (Text, Text) -> SeldaT SQLite IO [Director :*: Movie]
selectProductions (firstname, lastname) = query $ do
    directors <- select directoryTable
    movies <- select movieTable
    restrict (movies ! #mov_dir .== directors ! #dir_id)
    restrict (directors ! #dir_firstname .== literal firstname)
    restrict (directors ! #dir_lastname .== literal lastname)
    return $ directors :*: movies

-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

main :: IO ()
main = withSQLite "movies.db" $ do

    liftIO $ putStrLn "\n**** selectDirectors ****"
    selectDirectors >>= liftIO . mapM_ print

    liftIO $ putStrLn "\n**** selectProductions ****"
    selectProductions ("Stanley", "Kubrick") >>= liftIO . mapM_ print

