
import Data.Function
import Data.Functor.Identity
import Streamly.Data.Fold as Fold
import Streamly.Data.Stream as Stream

sumAndLength :: (Monad m, Num a) => Fold m a (a, Int)
sumAndLength = unTee ((,) <$> Tee Fold.sum <*> Tee Fold.length) 

main :: IO ()
main = do
  print $ runIdentity 
    $ Stream.fromList [1 .. 1_000_000 :: Integer]
    & Stream.fold sumAndLength
    -- & Stream.fold Fold.sum

