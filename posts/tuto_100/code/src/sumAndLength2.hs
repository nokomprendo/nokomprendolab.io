-- https://hackage.haskell.org/package/foldl

import Data.List

sumAndLength :: Num a => [a] -> (a, Int)
sumAndLength xs = foldl' step (0, 0) xs
  where
    step (x, y) n = (x + n, y + 1)

main :: IO ()
main = print $ sumAndLength [1 .. 1_000_000 :: Integer]

