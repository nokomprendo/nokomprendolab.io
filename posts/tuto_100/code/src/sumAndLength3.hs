-- https://hackage.haskell.org/package/foldl

import Data.List (foldl')

data Pair a b = Pair !a !b
  deriving Show

sumAndLength :: Num a => [a] -> (Pair a Int)
sumAndLength xs = foldl' step (Pair 0 0) xs      -- le fold est strict
  where
    step (Pair x y) n = Pair (x + n) (y + 1)

_sumAndLength :: Num a => [a] -> (a, Int)
_sumAndLength xs = done (foldl' step (Pair 0 0) xs)
  where
    step (Pair x y) n = Pair (x + n) (y + 1)
    done (Pair x y) = (x, y)

main :: IO ()
main = print $ sumAndLength [1 .. 1_000_000 :: Integer]

