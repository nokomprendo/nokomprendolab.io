
import Control.Monad
import Streaming as S (Of(..), Stream, runIdentity)
import Streaming.Prelude qualified as S

progress :: Show a => Integer -> Stream (Of a) IO r -> IO r
progress step stream = 
  S.effects 
    $ S.chain (\(i,n) -> when (mod i step == 0) (putStrLn $ show i ++ " -> " ++ show n))
    $ S.zip (S.enumFrom 1) stream


sumAndLength :: (Monad m, Num a) => Stream (Of a) m r -> m (Of a (Of Int r))
sumAndLength = S.sum . S.store S.length 

main :: IO ()
main = do
  -- (t :> s :> ()) <- progress 100_000 $ S.store S.length $ S.store S.sum $ S.each $ reverse [1 .. 1_000_000 :: Integer]
  -- (t :> s :> ()) <- progress 100_000 $ S.store S.length $ S.store S.sum $ S.each [1 .. 1_000_000 :: Integer]

  -- (t :> s :> ()) <- progress 100_000 $ S.store S.length $ S.store S.sum $ S.each $ replicate 1_000_000 (1 :: Integer)
  -- putStrLn $ "\n" ++ show t ++ " " ++ show s

  -- (t :> s :> ()) <- S.length $ S.store S.sum $ S.each [1 .. 1_000_000 :: Integer]
  -- putStrLn $ show t ++ " " ++ show s

  -- print $ runIdentity $ S.length $ S.store S.sum $ S.each [1 .. 1_000_000 :: Integer]
  -- (S.length $ S.sum $ S.copy $ S.each [1 .. 1_000_000 :: Integer]) >>= print

  print $ runIdentity $ sumAndLength $ S.each [1 .. 1_000_000 :: Integer]


{-
main = do 
  S.effects (hilo 30 )
  putStrLn "Right, 30 is correct!"

hilo :: Int -> Stream (Of Int) IO ()
hilo n = void $ S.break (== n)
              $ S.chain (\a -> when (a < n) $ putStrLn $ "Too small!") 
              $ S.chain (\a -> when (a > n) $ putStrLn $ "Too big!")
              $ S.map snd
              $ S.zip (S.cycle $ do lift (putStrLn "\nEnter a number") 
                                    S.yield ())
              $ S.delay 0.1
              $ S.readLn
-}

