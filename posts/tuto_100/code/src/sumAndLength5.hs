
import qualified Control.Foldl as Fold

sumAndLength :: Num a => [a] -> (a, Int)
sumAndLength xs = Fold.fold ((,) <$> Fold.sum <*> Fold.length) xs

main :: IO ()
main = do
  print $ sumAndLength [1 .. 1_000_000 :: Integer]

  -- print $ Fold.fold Fold.sum [1 .. 1_000_000 :: Integer]
 
