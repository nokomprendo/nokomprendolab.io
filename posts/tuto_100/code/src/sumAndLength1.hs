-- https://hackage.haskell.org/package/foldl

sumAndLength :: Num a => [a] -> (a, Int)
sumAndLength xs = (sum xs, length xs)

main :: IO ()
main = print $ sumAndLength [1 .. 1_000_000 :: Integer]

