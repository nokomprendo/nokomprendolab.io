-- idris2 -p contrib all2.idr -o all2 && ./build/exec/all2
-- https://idris2docs.sinyax.net/

import Control.Algebra

data All = MkAll Bool

Show All where
  show (MkAll True)  = "MkAll True"
  show (MkAll False)  = "MkAll False"

Semigroup All where
  MkAll True <+> y = y
  _ <+> _ = MkAll False
  -- _ <+> y = y

Monoid All where
  neutral = MkAll True

SemigroupV All where
  semigroupOpIsAssociative (MkAll True) _ _ = Refl
  semigroupOpIsAssociative (MkAll False) _ _ = Refl

MonoidV All where
  monoidNeutralIsNeutralL (MkAll True) = Refl
  monoidNeutralIsNeutralL (MkAll False) = Refl

  monoidNeutralIsNeutralR (MkAll True) = Refl
  monoidNeutralIsNeutralR (MkAll False) = Refl

main : IO ()
main = printLn $ MkAll True <+> MkAll False

