-- idris2 -p contrib all1.idr -o all1 && ./build/exec/all1

data All = MkAll Bool

Show All where
  show (MkAll True)  = "MkAll True"
  show (MkAll False)  = "MkAll False"

Semigroup All where
  MkAll True <+> y = y
  _ <+> y = MkAll False
  -- _ <+> y = y

Monoid All where
  neutral = MkAll True

allIsAssociative : (x, y, z : All) -> x <+> (y <+> z) = (x <+> y) <+> z
allIsAssociative (MkAll True)  _ _ = Refl
allIsAssociative (MkAll False)  _ _ = Refl

allIsNeutralL : (x : All) -> x <+> MkAll True = x
allIsNeutralL (MkAll False) = Refl
allIsNeutralL (MkAll True) = Refl

allIsNeutralR : (x : All) -> MkAll True <+> x = x
allIsNeutralR (MkAll False) = Refl
allIsNeutralR (MkAll True) = Refl

main : IO ()
main = printLn $ MkAll True <+> MkAll False

