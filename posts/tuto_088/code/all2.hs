
import Test.QuickCheck
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes

newtype All = All Bool
  deriving (Eq, Show)

instance Semigroup All where
  All True <> y = y
  _ <> _ = All False
  -- _ <> y = y

instance Monoid All where
  mempty = All True

instance Arbitrary All where
  arbitrary = All <$> arbitrary

instance EqProp All where 
  (=-=) = eq

main :: IO ()
main = quickBatch (monoid $ All True)

