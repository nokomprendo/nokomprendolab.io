import Test.QuickCheck


semigroupAssoc :: (Eq m, Monoid m) => m -> m -> m -> Bool
semigroupAssoc a b c = (a <> (b <> c)) == ((a <> b) <> c)

monoidLeftId :: (Eq m, Monoid m) => m -> Bool
monoidLeftId a = (mempty <> a) == a

monoidRightId :: (Eq m, Monoid m) => m -> Bool
monoidRightId a = (a <> mempty) == a


newtype All = All Bool
  deriving (Eq, Show)

instance Semigroup All where
  All True <> y = y
  _ <> _ = All False
  -- _ <> y = y

instance Monoid All where
  mempty = All True

instance Arbitrary All where
  arbitrary = All <$> arbitrary


main :: IO ()
main = do
  quickCheck (semigroupAssoc :: All -> All -> All -> Bool)
  quickCheck (monoidLeftId :: All -> Bool)
  quickCheck (monoidRightId :: All -> Bool)

