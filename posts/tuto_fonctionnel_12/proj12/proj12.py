#!/usr/bin/env python

import pycounter.utils as pcu
import pykounter.utils as pku

if __name__ == '__main__':

    print('\ntest counter')
    cntr = pcu.create_counter42()
    pcu.count_and_print(cntr)
    pcu.count_and_print(cntr)

    print('\ntest kounter')
    kntr = pku.create_kounter42()
    pku.count_and_print(kntr)
    pku.count_and_print(kntr)

