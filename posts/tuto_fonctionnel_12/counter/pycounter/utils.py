#!/usr/bin/env python

import counter

def create_counter42():
    return counter.Counter(42)

def count_and_print(cntr):
    cntr.countK()
    print('pycounter:', cntr.getN())

if __name__ == '__main__':
    cntr = create_counter42()
    count_and_print(cntr)
    count_and_print(cntr)

