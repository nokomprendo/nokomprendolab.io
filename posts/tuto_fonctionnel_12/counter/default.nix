{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/17.09.tar.gz") {} } :

pkgs.python3Packages.buildPythonPackage {
  name = "pycounter";
  src = ./.;
  buildInputs = [ pkgs.python3Packages.boost ];
}

