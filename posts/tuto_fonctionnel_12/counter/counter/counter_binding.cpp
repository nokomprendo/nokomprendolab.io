#include <counter.hpp>

#include <boost/python.hpp>

using namespace boost::python;

BOOST_PYTHON_MODULE( counter ) {
  class_<Counter>("Counter", init<int>())
    .def("getN", &Counter::getN)
    .def("countK", &Counter::countK)
    ;
}

