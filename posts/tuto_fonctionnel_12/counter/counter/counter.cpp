#include <counter.hpp>

Counter::Counter(int k) : _n(0), _k(k) {
}

int Counter::getN() const {
  return _n;
}

void Counter::countK() {
  _n += _k;
}

