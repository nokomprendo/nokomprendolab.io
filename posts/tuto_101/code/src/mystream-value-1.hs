
-------------------------------------------------------------------------------
-- data types
-------------------------------------------------------------------------------

data Stream e m r 
  = Step e (Stream e m r)
  | Effect (m (Stream e m r))
  | Return r

data Pair a b = Pair !a !b
  deriving Show

-------------------------------------------------------------------------------
-- producers
-------------------------------------------------------------------------------

each :: [e] -> Stream e m ()
each = foldr (\x acc -> Step x acc) (Return ())

-------------------------------------------------------------------------------
-- streams
-------------------------------------------------------------------------------

stream1 :: Stream Int IO Bool
stream1 = 
  Step 1
    (Effect (putStrLn "effect1 (stream1)" >>
             pure (Step 2
                    (Effect (putStrLn "effect2 (stream1)" >> 
                             pure (Return True))))))

-------------------------------------------------------------------------------
-- consumers
-------------------------------------------------------------------------------

toList :: Monad m => Stream e m r -> m ([e], r)
toList (Step e str) = (\(es, r) -> (e:es, r)) <$> toList str
toList (Effect mstr) = mstr >>= toList 
toList (Return r) = return ([], r)

foldStream :: Monad m => (x -> a -> x) -> x -> Stream a m r -> m (x, r)
foldStream step begin str =  fold_loop str begin
  where
    fold_loop stream !x = case stream of
      Step a rest -> fold_loop rest $! step x a
      Effect m         -> do  str' <- m
                              fold_loop str' x
      Return r         -> return (x, r)

sumStream :: (Monad m, Num a) => Stream a m r -> m (a, r)
sumStream = foldStream (+) 0

sumAndLength :: (Monad m, Num a) => Stream a m r -> m ((a, Int), r)
sumAndLength = foldStream (\(s,l) n -> (s+n, l+1)) (0, 0)

sumAndLength' :: (Monad m, Num a) => Stream a m r -> m (Pair a Int, r)
sumAndLength' = foldStream (\(Pair s l) n -> Pair (s+n) (l+1)) (Pair 0 0)

-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

main :: IO ()
main = sumAndLength' (each [1 .. 1_000_000 :: Integer]) >>= print

_main :: IO ()
_main = do

  putStrLn "\n*** sum stream1 ***"
  sumStream stream1 >>= print

  putStrLn "\n*** sumAndLength ***"
  sumAndLength (each [1 .. 10 :: Integer]) >>= print

  putStrLn "\n*** toList stream1 ***"
  toList stream1 >>= print


