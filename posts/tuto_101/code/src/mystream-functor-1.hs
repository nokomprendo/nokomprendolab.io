-------------------------------------------------------------------------------
-- data types
-------------------------------------------------------------------------------

data Pair a b = Pair !a b
  deriving (Show)

instance Functor (Pair a) where
  fmap f (Pair a b) = Pair a (f b)

data Stream f m r 
  = Step !(f (Stream f m r))
  | Effect (m (Stream f m r))
  | Return r

instance (Functor f, Monad m) => Functor (Stream f m) where
  fmap f = loop where
    loop stream = case stream of
      Step   g -> Step (fmap loop g)
      Effect m -> Effect (loop <$> m)
      Return r -> Return (f r)

instance (Functor f, Monad m) => Applicative (Stream f m) where
  pure = Return
  streamf <*> streamx = do 
    f <- streamf
    f <$> streamx

instance (Functor f, Monad m) => Monad (Stream f m) where
  stream >>= f =
    loop stream where
    loop stream0 = case stream0 of
      Step fstr -> Step (fmap loop fstr)
      Effect m  -> Effect (fmap loop m)
      Return r  -> f r

-------------------------------------------------------------------------------
-- producers
-------------------------------------------------------------------------------

empty :: Stream f m ()
empty = Return ()

yield :: a -> Stream (Pair a) m ()
yield a = Step (Pair a empty)

effect :: Monad m => m r -> Stream f m r
effect eff = Effect $ Return <$> eff

each :: [e] -> Stream (Pair e) m ()
each = foldr (\x acc -> Step (Pair x acc)) (Return ())

-------------------------------------------------------------------------------
-- streams
-------------------------------------------------------------------------------

stream1 :: Stream (Pair Int) IO Bool
stream1 = 
  Step (Pair 1
    (Effect (putStrLn "effect1 (stream1)" >>
             pure (Step (Pair 2
                    (Effect (putStrLn "effect2 (stream1)" >> 
                     pure (Return True))))))))

stream1' :: Stream (Pair Int) IO Bool
stream1' = do
  yield 1
  effect (putStrLn "effect1 (stream1')")
  yield 2
  effect (putStrLn "effect2 (stream1')")
  return True

-------------------------------------------------------------------------------
-- consumers
-------------------------------------------------------------------------------

toList :: Monad m => Stream (Pair a) m r -> m (Pair [a] r)
toList (Step (Pair e str)) = (\(Pair es r) -> Pair (e:es) r) <$> toList str
toList (Effect mstr) = mstr >>= toList 
toList (Return r) = return (Pair [] r)

foldStream :: Monad m => (b -> a -> b) -> b -> Stream (Pair a) m r -> m (Pair b r)
foldStream step acc0 str =  fold_loop str acc0
  where
    fold_loop stream !acc = case stream of
      Step (Pair x rest) -> fold_loop rest $! step acc x
      Effect m         -> do  str' <- m 
                              fold_loop str' acc
      Return r         -> return (Pair acc r)

sumStream :: (Monad m, Num a) => Stream (Pair a) m r -> m (Pair a r)
sumStream = foldStream (+) 0

sumAndLength :: (Monad m, Num a) => Stream (Pair a) m r -> m (Pair (a, Int) r)
sumAndLength = foldStream (\(s,l) n -> (s+n, l+1)) (0, 0)

sumAndLength' :: (Monad m, Num a) => Stream (Pair a) m r -> m (Pair (Pair a Int) r)
sumAndLength' = foldStream (\(Pair s l) n -> Pair (s+n) (l+1)) (Pair 0 0)

-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

main :: IO ()
main = sumAndLength' (each [1 .. 1_000_000 :: Integer]) >>= print

_main :: IO ()
_main = do

  putStrLn "\n*** sum stream1 ***"
  sumStream stream1 >>= print

  putStrLn "\n*** sumAndLength ***"
  sumAndLength (each [1 .. 10 :: Integer]) >>= print

  putStrLn "\n*** toList stream1 ***"
  toList stream1' >>= print

