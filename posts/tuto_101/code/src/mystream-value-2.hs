
-------------------------------------------------------------------------------
-- data types
-------------------------------------------------------------------------------

data Stream e m r 
  = Step e (Stream e m r)
  | Effect (m (Stream e m r))
  | Return r

-------------------------------------------------------------------------------
-- helpers
-------------------------------------------------------------------------------

empty :: Stream e m ()
empty = Return ()

yield :: a -> Stream a m ()
yield a = Step a empty

each :: [e] -> Stream e m ()
each = foldr Step (Return ())
-- each [] = Return ()
-- each (x:xs) = Step x (each xs)

effect :: Monad m => m r -> Stream e m r
effect eff = Effect $ Return <$> eff

-------------------------------------------------------------------------------
-- examples
-------------------------------------------------------------------------------

stream1 :: Stream Int IO ()
stream1 = 
  Step 1
    (Effect (putStrLn "some effect (stream1)" >>
             pure (Step 2
                    (Effect (putStrLn "finish (stream1)" >> pure empty)))))

stream2 :: Stream () IO ()
stream2 = effect (putStrLn "some effect (stream2)")

-------------------------------------------------------------------------------
-- consumers
-------------------------------------------------------------------------------

ssum :: (Num e, Monad m) => Stream e m r -> m (e, r)
ssum (Return r) = pure (0, r)
ssum (Effect m) = m >>= ssum
ssum (Step e str) = (\(acc, r) -> (acc + e, r)) <$> ssum str

printStream :: (Show e, Show r) => Stream e IO r -> IO ()
printStream (Return r) = putStrLn $ "Return: " <> show r
printStream (Step e str) = do
  putStrLn $ "Step: " <> show e
  printStream str
printStream (Effect mstr) = do
  putStr "Run effect: "
  str <- mstr
  printStream str

-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

main :: IO ()
main = do
  printStream stream1
  printStream stream2
  ssum stream1 >>= print
  ssum (each [1..10 :: Int]) >>= print
  printStream $ yield (1 :: Int)

