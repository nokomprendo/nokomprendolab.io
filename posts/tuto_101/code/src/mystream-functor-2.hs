{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}

import Control.Monad (ap)
import Data.Bifunctor ( Bifunctor(first, bimap) )
import Data.Functor.Compose ( Compose(Compose) )

-------------------------------------------------------------------------------
-- data types
-------------------------------------------------------------------------------

data Stream f m r 
  = Step (f (Stream f m r))
  | Effect (m (Stream f m r))
  | Return r

data Pair a b = Pair !a b
  deriving (Show)

instance Functor (Pair a) where
  fmap f (Pair a b) = Pair a (f b)

instance Bifunctor Pair where
  bimap f g (Pair a b) = Pair (f a) (g b)

  -- bimap :: (a -> b) -> (c -> d) -> p a c -> p b d
  -- first :: (a -> b) -> p a c -> p b c
  -- second :: (b -> c) -> p a b -> p a c

instance (Functor f, Monad m) => Functor (Stream f m) where
  fmap :: forall a b. (a -> b) -> Stream f m a -> Stream f m b
  fmap fun = loop
    where
      loop :: Stream f m a -> Stream f m b
      loop (Return r) = Return (fun r)
      loop (Effect m) = Effect (fmap loop m)
      loop (Step f) = Step (fmap loop f)

instance (Functor f, Monad m) => Applicative (Stream f m) where
  pure = Return
  (<*>) = ap

instance (Functor f, Monad m) => Monad (Stream f m) where
  (>>=) :: forall r r1.  Stream f m r -> (r -> Stream f m r1) -> Stream f m r1
  stream >>= fun = loop stream
    where
      loop :: Stream f m r -> Stream f m r1
      loop (Return r) = fun r
      loop (Effect m) = Effect (fmap loop m)
      loop (Step f) = Step (fmap loop f)

-------------------------------------------------------------------------------
-- helpers
-------------------------------------------------------------------------------

empty :: Stream f m ()
empty = Return ()

yield :: a -> Stream (Pair a) m ()
yield a = Step (Pair a empty)

each :: [e] -> Stream (Pair e) m ()
each [] = Return ()
each (x:xs) = Step $ Pair x (each xs)

effect :: Monad m => m r -> Stream f m r
effect eff = Effect $ Return <$> eff

-------------------------------------------------------------------------------
-- examples
-------------------------------------------------------------------------------

stream1 :: Stream (Pair Int) IO ()
stream1 = 
  Step (Pair 1
    (Effect (putStrLn "some effect (stream1)" >>
             pure (Step (Pair 2
                    (Effect (putStrLn "finish (stream1)" >> pure empty)))))))

stream2 :: Stream (Pair ()) IO ()
stream2 = effect (putStrLn "some effect (stream2)")

stream1' :: Stream (Pair Int) IO ()
stream1' = do
  effect (putStrLn "eff1 (stream1')")
  effect (putStrLn "eff2 (stream1')")
  yield 1
  effect (putStrLn "some effect (stream1')")
  yield 2
  effect (putStrLn "finish (stream1')")

-------------------------------------------------------------------------------
-- consumers
-------------------------------------------------------------------------------

ssum :: (Num e, Monad m) => Stream (Pair e) m r -> m (Pair e r)
ssum (Return r) = pure (Pair 0 r)
ssum (Effect m) = m >>= ssum
ssum (Step (Pair e str)) = first (+e) <$> ssum str

printStream :: (Show e, Show r) => Stream (Pair e) IO r -> IO ()
printStream (Return r) = putStrLn $ "Return: " <> show r
printStream (Step (Pair e str)) = do
  putStrLn $ "Step: " <> show e
  printStream str
printStream (Effect mstr) = do
  putStr "Run effect: "
  str <- mstr
  printStream str

-------------------------------------------------------------------------------
-- more helpers
-------------------------------------------------------------------------------

maps :: (Functor f, Monad m) => (forall x. f x -> g x) -> Stream f m r -> Stream g m r
maps fun = loop
  where
    loop (Return r) = Return r
    loop (Effect m) = Effect (fmap loop m)
    loop (Step f) = Step (fun (fmap loop f))

mapFirst :: Monad m => (a -> b) -> Stream (Pair a) m r -> Stream (Pair b) m r
mapFirst fun = maps (first fun)

zipsWith :: Monad m => 
  (forall x y p . (x -> y -> p) -> f x -> g y -> h p) -> 
  Stream f m r -> 
  Stream g m r -> 
  Stream h m r
zipsWith fun = loop
  where
    loop (Return r) _ = Return r
    loop _ (Return r) = Return r
    loop (Effect m) t = Effect $ fmap (`loop` t) m
    loop s (Effect n) = Effect $ fmap (loop s) n
    loop (Step fs) (Step gs) = Step $ fun loop fs gs

zipPair :: Monad m =>
  Stream (Pair a) m r -> Stream (Pair b) m r -> Stream (Pair (a, b)) m r
zipPair = zipsWith fun
  where
    fun p (Pair e1 x) (Pair e2 y) = Pair (e1, e2) (p x y)

zips :: (Monad m, Functor f, Functor g) => 
  Stream f m r -> Stream g m r -> Stream (Compose f g) m r
zips = zipsWith fun
  where
    fun p fx gy = Compose (fmap (\x -> fmap (\y -> p x y) gy) fx)

decompose :: (Monad m, Functor f) =>
  Stream (Compose m f) m r -> Stream f m r
decompose = loop
  where
    loop (Return r) = Return r
    loop (Effect m) = Effect (fmap loop m)
    loop (Step (Compose mstr)) = Effect $ do
      str <- mstr
      pure (Step (fmap loop str))

mapsM :: (Monad m, Functor f, Functor g) =>
  (forall x . f x -> m (g x)) -> Stream f m r -> Stream g m r
mapsM fun = decompose . maps (Compose . fun)

withEffect :: Monad m =>
  (e -> m ()) -> Stream (Pair e) m r -> Stream (Pair e) m r
withEffect eff = mapsM go
  where
    go p@(Pair e _) = eff e >> pure p

splitsAt :: (Monad m, Functor f) =>
  Int -> Stream f m r -> Stream f m (Stream f m r)
splitsAt = loop
  where
    loop n str = 
      if n <= 0
        then Return str
        else case str of
          Return r -> Return (Return r)
          Effect m -> Effect (loop n <$> m) 
          Step f -> Step (loop (n-1) <$> f)

chunksOf :: forall f m r. (Monad m, Functor f) =>
  Int -> Stream f m r -> Stream (Stream f m) m r
chunksOf n = loop
  where
    cutChunk :: Stream f m r -> Stream f m (Stream (Stream f m) m r)
    cutChunk str = fmap loop (splitsAt (n-1) str)

    loop :: Stream f m r -> Stream (Stream f m) m r
    loop (Return r) = Return r
    loop (Effect m) = Effect (fmap loop m)
    loop (Step fs) = Step (Step (fmap cutChunk fs))

-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

main :: IO ()
main = do
  printStream stream1
  printStream stream1'
  printStream stream2
  ssum stream1 >>= print
  ssum (each [1..10 :: Int]) >>= print
  printStream $ yield (1 :: Int)

  let maybeToList :: Maybe Int -> [Int]
      maybeToList Nothing = []
      maybeToList (Just x) = [x]

  printStream $ mapFirst maybeToList (yield $ Just 42)

  ssum (mapFirst (*2) $ each [1..3 :: Int]) >>= print

  printStream (zipPair stream1 stream1')

  ssum (withEffect print $ each [1..3 :: Int]) >>= print
  ssum (withEffect print $ withEffect (const $ putStr "Element: ") $ each [1..3 :: Int]) >>= print

  ssum (withEffect print $ mapsM ssum $ chunksOf 2 $ each [1..5 :: Int]) >>= print

