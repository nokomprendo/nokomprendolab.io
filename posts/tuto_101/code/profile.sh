#!/bin/sh

if [ $# -lt 1 ] ; then
    echo "usage: $0 <app> [params...]"
    exit
fi

APP=$1
shift
PARAMS=$@

echo "APP = ${APP}"
echo "PARAMS = ${PARAMS}"

cabal run ${APP} -- "${PARAMS}" +RTS -hy -l-agu
#cabal run ${APP} -- "${PARAMS}" +RTS -hy -l-agu -M1M -A100k
#cabal run ${APP} -- "${PARAMS}" +RTS -hy -l-agu -M400k -A10k

eventlog2html ${APP}.eventlog

