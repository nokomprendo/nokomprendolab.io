{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE PolyKinds                  #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE TypeOperators              #-}
{-# LANGUAGE TypeApplications           #-}

import           Common
import           Control.Concurrent (Chan, newChan, writeChan)
import           Control.Monad.IO.Class (liftIO)
import           Data.Aeson (encode)
import           Data.ByteString.Builder
import           Data.IORef (newIORef, readIORef, modifyIORef', IORef)
import           Data.Proxy (Proxy(..))
import qualified Lucid as L
import           Miso
import           Network.Wai (Application)
import           Network.Wai.EventSource (eventSourceAppChan, ServerEvent(..))
import           Network.Wai.Handler.Warp (run)
import           Network.Wai.Middleware.RequestLogger (logStdout)
import           Servant

-- server API (data type and server function)

type ServerApi
    =    "static" :> Raw    -- "/static" endpoint, for static files
    :<|> "ssePath" :> Raw   -- "/ssePath" endpoint, for registering SSE...
    :<|> "xhrPath" :> ReqBody '[JSON] Path :> Post '[JSON] NoContent
    :<|> "api" :>  Get '[JSON] [Path] 
    :<|> ToServerRoutes (View Action) HtmlPage Action    -- "/" endpoint

serverApp :: Chan ServerEvent -> IORef [Path] -> Application
serverApp chan pathsRef = serve (Proxy @ServerApi)
    (    serveDirectoryFileServer "static"  -- serve the "/static" endpoint (using the "static" folder)
    :<|> Tagged (eventSourceAppChan chan)   -- serve the "/ssePath" endpoint...
    :<|> handleXhrPath chan pathsRef
    :<|> handleApi pathsRef
    :<|> handleClientRoute 
    )

-- handling function, for serving endpoints

-- when a path is sent to "/xhrPath", add the path in pathsRef and update clients using SSE
handleXhrPath :: Chan ServerEvent -> IORef [Path] -> Path -> Handler NoContent
handleXhrPath chan pathsRef path = do
    liftIO $ do
        modifyIORef' pathsRef (\ paths -> path:paths)
        writeChan chan (ServerEvent Nothing Nothing [lazyByteString $ encode path])
    pure NoContent

-- when a client requests "/api", send all paths
handleApi :: IORef [Path] -> Handler [Path]
handleApi pathsRef = liftIO (readIORef pathsRef)

-- when a client requests "/", render and send the home view 
handleClientRoute :: Handler (HtmlPage (View Action))
handleClientRoute = pure $ HtmlPage $ homeView initialModel

-- rendering

newtype HtmlPage a = HtmlPage a deriving (Show, Eq)

instance L.ToHtml a => L.ToHtml (HtmlPage a) where

    toHtmlRaw = L.toHtml

    -- main function, for rendering a view to a HTML page
    toHtml (HtmlPage x) = L.doctypehtml_ $ do
        L.head_ $ do
            L.meta_ [L.charset_ "utf-8"]
            L.with 
                (L.script_ mempty) 
                [L.src_ "static/all.js", L.async_ mempty, L.defer_ mempty] 
        L.body_ (L.toHtml x)  -- render the view and include it in the HTML page

-- server main app

main :: IO ()
main = do
    pathsRef <- newIORef []
    chan <- newChan
    run 3000 $ logStdout (serverApp chan pathsRef)

