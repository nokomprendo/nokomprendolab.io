{-# LANGUAGE OverloadedStrings          #-}

import Data.Aeson (encode, decodeStrict)
import Data.Maybe (fromMaybe, fromJust)
import Common
import Control.Monad (void, when)
import JavaScript.Web.Canvas
import JavaScript.Web.XMLHttpRequest
import Miso
import Miso.String hiding (length)

-- update

updateModel :: Action -> Model -> Effect Action Model

-- nothing to do
updateModel NoOp m = noEff m

-- mouse down: begin drawing a path
updateModel MouseDown m = noEff m { currentPath_ = [], drawing_ = True }

-- mouse move: get position and ask to update the model using a SetXy action
updateModel (MouseMove (x,y)) m = m <#
    if drawing_ m 
    then do 
        left <- jsRectLeft
        top <- jsRectTop
        let x' = fromIntegral $ x - left
        let y' = fromIntegral $ y - top
        pure $ SetXy (x', y')
    else pure NoOp

-- update position and ask to redraw the canvas using a RedrawCanvas action
updateModel (SetXy xy) m =
    m { currentPath_ = xy : currentPath_ m } <# pure RedrawCanvas

-- mouse up: finish drawing the current path (send the path to the server)
updateModel MouseUp (Model a c xy _) = Model a [] xy False <# pure (SendXhr c)

-- send a path to the server
updateModel (SendXhr path) m = m <# (xhrPath path >> pure NoOp)

-- register to Server-Sent Event, for receiving new paths from other clients
updateModel (RecvSse Nothing) m = noEff m
updateModel (RecvSse (Just path)) m =
    m { allPaths_ = path : allPaths_ m } <# pure RedrawCanvas

-- clear the canvas and redraw the paths
updateModel RedrawCanvas m = m <# do
    w <- jsWidth
    h <- jsHeight
    ctx <- jsCtx
    clearRect 0 0 w h ctx
    lineCap LineCapRound ctx
    mapM_ (drawPath ctx) $ allPaths_ m
    drawPath ctx $ currentPath_ m
    pure NoOp

-- initialize paths: ask all paths to the server then update using a SetPaths action
updateModel InitAllPaths m = m <# do SetPaths <$> xhrAllPaths

-- update paths then ask to redraw the canvas
updateModel (SetPaths paths) m = m { allPaths_ = paths } <# pure RedrawCanvas

-- helper functions

-- send a new path to the server ("/xhrPath" endpoint)
xhrPath :: Path -> IO ()
xhrPath path = void $ xhrByteString $ Request POST "/xhrPath" Nothing hdr False dat 
    where hdr = [("Content-type", "application/json")]
          dat = StringData $ toMisoString $ encode path

-- ask for the paths ("/xhrPath" endpoint)
xhrAllPaths :: IO [Path]
xhrAllPaths = fromMaybe [] . decodeStrict . fromJust . contents <$> 
    xhrByteString (Request GET "/api" Nothing [] False NoData)

-- handle a Server-Sent Event by generating a RecvSse action
ssePath :: SSE Path -> Action
ssePath (SSEMessage path) = RecvSse (Just path)
ssePath _ = RecvSse Nothing

drawPath :: Context -> Path -> IO ()
drawPath ctx points = 
    when (length points >= 2) $ do
        let ((x0,y0):ps) = points
        lineWidth 4 ctx
        beginPath ctx
        moveTo x0 y0 ctx
        mapM_ (\(x,y) -> lineTo x y ctx) ps
        stroke ctx

-- FFI

foreign import javascript unsafe "$r = canvas_draw.getContext('2d');"
    jsCtx :: IO Context

foreign import javascript unsafe "$r = canvas_draw.clientWidth;"
    jsWidth :: IO Double

foreign import javascript unsafe "$r = canvas_draw.clientHeight;"
    jsHeight :: IO Double

foreign import javascript unsafe "$r = canvas_draw.getBoundingClientRect().left;"
    jsRectLeft :: IO Int

foreign import javascript unsafe "$r = canvas_draw.getBoundingClientRect().top;"
    jsRectTop :: IO Int

-- client main app

main :: IO ()
main = miso $ const App 
    { initialAction = InitAllPaths
    , model = initialModel
    , update = updateModel
    , view = homeView
    , events = defaultEvents
    , subs = [
        sseSub "/ssePath" ssePath,  -- register Server-Sent Events to the ssePath function
        mouseSub MouseMove          -- register mouseSub events to the MouseMove action
        ]
    , mountPoint = Nothing
    }

