#include "Controller.hpp"

#include <Wt/WAnchor.h>
#include <Wt/WBreak.h>
#include <Wt/WContainerWidget.h>
#include <Wt/WText.h>

// Painter

Painter::Painter(Controller & controller, int width, int height) : 
    Wt::WPaintedWidget(),
    _controller(controller),
    _paths(controller.getPaths())
{
    // initialize the widget
    resize(width, height);
    Wt::WCssDecorationStyle deco;
    deco.setBorder(Wt::WBorder(Wt::BorderStyle::Solid));
    setDecorationStyle(deco);
    // initialize the pen
    _pen.setCapStyle(Wt::PenCapStyle::Round);
    _pen.setJoinStyle(Wt::PenJoinStyle::Round);
    _pen.setWidth(4);
    // connect callback functions (for handling mouse events)
    mouseDragged().connect(this, &Painter::mouseDrag);
    mouseWentDown().connect(this, &Painter::mouseDown);
    mouseWentUp().connect(this, &Painter::mouseUp);
}

void Painter::addPath(const Wt::WPainterPath & path) {
    // add a path (sent by the server)
    _paths.push_back(path);
    // update widget
    update();
}

void Painter::paintEvent(Wt::WPaintDevice * paintDevice) {
    Wt::WPainter painter(paintDevice);
    painter.setPen(_pen);
    // draw common paths (sent by the server)
    for (const auto & p : _paths)
        painter.drawPath(p);
    // draw the local path (being drawn by the user)
    painter.drawPath(_currentPath);
}

void Painter::mouseDown(const Wt::WMouseEvent & e) {
    // begin drawing a path: remember the initial position
    Wt::Coordinates c = e.widget();
    _currentPath = Wt::WPainterPath(Wt::WPointF(c.x, c.y));
}

void Painter::mouseDrag(const Wt::WMouseEvent & e) {
    // mouse move: add the new position into the current path
    Wt::Coordinates c = e.widget();
    _currentPath.lineTo(c.x, c.y);
    // update widget
    update();
}

void Painter::mouseUp(const Wt::WMouseEvent &) {
    // send the path to the server, which will send it to the clients
    _controller.addPath(_currentPath);
    // reset current path
    _currentPath = Wt::WPainterPath();
}

// AppDrawing

AppDrawing::AppDrawing(const Wt::WEnvironment & env, Controller & controller) :
    Wt::WApplication(env), _controller(controller)
{
    // build the interface
    root()->addWidget(std::make_unique<Wt::WText>("isopaint_wt "));
    root()->addWidget(std::make_unique<Wt::WBreak>());
    _painter = root()->addWidget(std::make_unique<Painter>(controller, 400, 300));
    // register the client in the controller
    _controller.addClient(this);
    // enable updates from the server
    enableUpdates(true);
}

AppDrawing::~AppDrawing() {
    // unregister the client
    _controller.removeClient(this);
}

void AppDrawing::addPath(const Wt::WPainterPath & path) {
    // add a path sent by the server
    _painter->addPath(path);
    // request updating the interface
    triggerUpdate();
}

