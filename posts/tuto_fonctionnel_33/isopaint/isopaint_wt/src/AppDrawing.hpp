#ifndef APP_DRAWING_HPP
#define APP_DRAWING_HPP

#include <Wt/WApplication.h>
#include <Wt/WPaintedWidget.h>
#include <Wt/WPainter.h>
#include <Wt/WEnvironment.h>

class Controller;

// drawing widget
class Painter : public Wt::WPaintedWidget {
    protected:
        Controller & _controller;
        Wt::WPen _pen;
        Wt::WPainterPath _currentPath;         // local path (being drawn by the user)
        std::vector<Wt::WPainterPath> _paths;  // common paths (sent by the server)

    public:
        Painter(Controller & controller, int width, int height);

        // add a path (sent by the server)
        void addPath(const Wt::WPainterPath & path);

    private:
        // main display function
        void paintEvent(Wt::WPaintDevice * paintDevice) override;

        // callback functions for handling mouse events
        void mouseDown(const Wt::WMouseEvent & e);
        void mouseUp(const Wt::WMouseEvent &);
        void mouseDrag(const Wt::WMouseEvent & e);
};

// client application
class AppDrawing : public Wt::WApplication {
    private:
        Controller & _controller;
        Painter * _painter;

    public:
        AppDrawing(const Wt::WEnvironment & env, Controller & controller);
        ~AppDrawing();

        // add a path (sent by the server)
        void addPath(const Wt::WPainterPath & path);
};

#endif

