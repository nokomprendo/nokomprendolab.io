with import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.09.tar.gz") {};
{
  cpu = python3Packages.buildPythonPackage {
    name = "dnncmp-tensorflow-cpu";
    src = ./.;
    propagatedBuildInputs = [ 
      python3Packages.tensorflow
    ];
  };
  gpu = python3Packages.buildPythonPackage {
    name = "dnncmp-tensorflow-gpu";
    src = ./.;
    propagatedBuildInputs = [ 
      cudnn
      python3Packages.tensorflowWithCuda
    ];
  };
}

