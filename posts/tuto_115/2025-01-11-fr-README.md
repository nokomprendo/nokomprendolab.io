---
title: AOC 2024 en Idris 2 (1/5), Retour d'expérience
description: En décembre 2024, j'ai enfin fait mon premier Advent Of Code, et en ai profité pour apprendre un peu plus le langage Idris 2. Cette série d'articles présente un retour d'expérience, avec quelques exemples d'exercices et de codes.
---

Voir aussi : [video youtube](https://youtu.be/QO6jey8fGcg) -
[code source](https://gitlab.com/nokomprendo/nokomprendo.gitlab.io/-/tree/master/posts/tuto_115/code)


En décembre 2024, j'ai enfin fait mon premier Advent Of Code, et en ai profité
pour apprendre un peu plus le langage Idris 2. Cette série d'articles présente
un retour d'expérience, avec quelques exemples d'exercices et de codes.

# L'Advent Of Code

## Présentation

L'[Advent Of Code](https://adventofcode.com/) est un exercice de programmation
à la façon des calendriers de l'Avent : chaque jour, du 1er au 25 décembre, un
exercice est dévoilé. Les exercices restent en ligne et peuvent également être
réalisés en dehors de cette période.

L'AOC est également un événement social. Le site officiel affiche quelques
statistiques, comme le nombre de personnes qui ont réussi les différents
exercices et un classement des personnes qui ont résolu les exercices le plus
rapidement. Il y a également beaucoup de discussions sur les différents réseaux
(bluesky/mastodon, serveurs discord, reddit, github/gitlab, etc).

![](images/aoc2024.png)

Chaque exercice est composé de deux parties et il faut d'abord résoudre la
partie 1 pour avoir accès à la partie 2. À chaque fois, le site de l'AOC
présente le problème et fournit un jeu de données personnalisé (un fichier
texte); on doit alors résoudre le problème sur notre jeu de données et
soumettre la réponse (généralement un nombre ou un texte) sur le site,
pour validation. Il n'y a pas de langage de programmation imposé ni même de
code à fournir.

![](images/day02.png)


## Ce que j'ai bien aimé

- Les exercices sont généralement très bien expliqués, notamment à partir d'un
  ou plusieurs exemples. 

- La variété des structures de données (tableaux 2D, dictionnaires, graphes...)
  et des algorithmes (parsing, parcours de graphe/région/chemin...) à
  utiliser/implémenter.

- Le côté social de l'événement (un exercice chaque jour, discussion sur les
  réseaux...) est motivant.

- La progressivité des exercices (simples les premiers jours, plus compliqués
  ensuite).

- La possibilité d'utiliser n'importe quel langage de programmation, même ceux
  réputés "lents". En effet, les exercices nécessitent souvent d'écrire un
  algorithme relativement optimisé, et même les langages "rapides" ne pourront
  généralement pas compenser une implémentation naïve brute-force.


## Ce que j'ai moins aimé

- Certains exercices peuvent être très chronophages (surtout lorsqu'on fait
  l'AOC pour la première fois et/ou avec un langage de programmation qu'on
  maitrise peu).

- Parfois, on fait des choix de conception "au plus simple" pour répondre à la
  partie 1, puis lorsqu'on découvre la partie 2, on se rend compte qu'il va
  falloir tout ré-écrire car ces choix ne sont plus adaptés. 


# Le langage Idris 2

## Présentation

[Idris 2](https://www.idris-lang.org/) est un langage de programmation
fonctionnel. Sa syntaxe est inspirée d'Haskell mais certaines caractéristiques
sont assez différentes, notamment les types dépendants, l'évaluation stricte
par défaut, la vérification de la totalité... Idris 2 permet également
d'implémenter des preuves.

Il existe des outils de gestion de projets/dépendances (notamment
[pack](https://github.com/stefan-hoeck/idris2-pack)) ainsi que différentes
documentations, par exemple :

- [Documentation for the Idris 2 Language](https://idris2.readthedocs.io/en/stable/)
- [Functional Programming in Idris 2](https://github.com/stefan-hoeck/idris2-tutorial)
- [Idris2 Documentation Browser](https://idris2docs.sinyax.net/)


## Ce que j'ai bien aimé

- La proximité avec Haskell : on peut rapidement commencer à coder en Idris si
  on connait déjà Haskell.

- Quelques défauts classiques de Haskell sont corrigés en Idris 2 : le type
  `String`, la syntaxe "dot record", la totalité des fonctions head/tail...

- Le système de types évolué, qui permet de simplifier certaines
  implémentations et d'écrire des preuves.

- Le "totality checker" (qui m'a permis notamment de détecter rapidement des
  bugs dans des fonctions récursives).


## Ce que j'ai moins aimé

- La proximité avec Haskell : au début, on a tendance à écrire "comme en
  Haskell" alors que le système de type d'Idris permettrait d'autres
  implémentations, plus judicieuses.

- L'écosystème (documentations, bibliothèques, outils...) moins abouti que
  celui d'Haskell. 

- Les messages d'erreurs, parfois peu explicites.

- Pas de `deriving` natif (par exemple pour `Eq`, `Show`...). Alternativement,
  on peut utiliser le système de réflexivité ("elaborator reflection") mais
  c'est moins pratique.


# Bilan

L'Advent Of Code est un événement intéressant, notamment pour s'exercer à
l'algorithmique, pratiquer un langage de programmation, échanger avec d'autres
développeurs. Certains exercices sont assez compliqués ce qui peut être
chronophage ou décourageant mais les discussions sur les réseaux aident à y
faire face.

Il est probablement plus raisonnable de faire son premier AOC avec un langage
que l'on maitrise car il y a déjà pas mal de choses à découvrir/comprendre dans
l'AOC. Il peut même être intéressant de mettre en place un projet et des
modules de base (tableaux 2D, parsers, dictionnaires...) qu'on pourra utiliser
au cours de l'événement.

Idris 2 est tout à fait adapté pour l'AOC. Les bibliothèques (base,
contrib) proposent la plupart des structures de données nécessaires. Certains
modules sont très pratiques, notamment les combinateurs de parsers (pour
analyser les données d'entrée) et la monade State (pour mettre en cache des
calculs ou pour implémenter des récursivités un peu compliquées).
Il manque juste les tableaux 2D mais on peut facilement se faire un module
adapté, à partir des tableaux 1D.

A noter que l'AOC est essentiellement un exercice d'algorithmique et n'est pas
forcément très adapté pour découvrir les spécificités d'un langage (par
exemple, les types dépendants d'Idris 2), ou alors, il faut en discuter, en
parallèle, sur les réseaux plus spécifiques à ce langage.

Voir aussi :

- [Lessons I learned after solving all the Advent Of Code challenges last December](https://newsletter.francofernando.com/p/advent-of-code)

