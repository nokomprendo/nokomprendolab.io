
module Day07

import Data.List1
import Data.String.Parser

record Equation where
  constructor MkEquation 
  testVal : Integer 
  numbers : List1 Integer

covering
equationP : Parser Equation
equationP = do
  t <- integer
  ignore $ string ": "
  ns <- sepBy1 integer (char ' ')
  pure $ MkEquation t ns

covering
equationsP : Parser (List Equation)
equationsP = sepBy equationP (char '\n')

covering
parseInput : String -> List Equation
parseInput str =
  case parse equationsP str of
    Left err => []
    Right (equations, len) => equations

isSolvable : Equation -> Bool
isSolvable eq@(MkEquation _ (n:::ns)) = go n ns
  where
    go : Integer -> List Integer -> Bool
    go result [] = result == eq.testVal
    go result (x::xs) = result <= eq.testVal && (go (x+result) xs || go (x*result) xs)

export
covering
run1 : String -> Integer
run1 = sum . map testVal . filter isSolvable . parseInput 

