
module Array2d

import Data.IOArray
import Data.String

record Array2d t where
  constructor MkArray2d
  arr : IOArray t
  ni : Int
  nj : Int

stringToArray2d : HasIO io => String -> io (Array2d Char)
stringToArray2d str = do
  let mat = map unpack $ lines str
      ni = cast $ length mat
      nj = maybe 0 (cast . length) $ head' mat
  arr <- fromList $ map Just $ concat mat
  pure (MkArray2d arr ni nj)

ijToK : Array2d t -> Int -> Int -> Int
ijToK (MkArray2d _ _ nj) i j = nj*i + j

getIjChar : HasIO io => Array2d Char -> Int -> Int -> io Char
getIjChar a2d i j = maybe ' ' id <$> readArray a2d.arr (ijToK a2d i j)

drawArray2d : HasIO io => Array2d Char -> io ()
drawArray2d a2d@(MkArray2d arr ni nj) = do
  for_ [0 .. ni-1] $ \i => do
    for_ [0 .. nj-1] $ \j => do
      c <- getIjChar a2d i j
      putChar c
    putStrLn ""

input1 : String
input1 = 
  "###############\n" ++
  "#...#...#.....#\n" ++
  "#.#.#.#.#.###.#\n" ++
  "#S#...#.#.#...#\n" ++
  "#######.#.#.###\n" ++
  "#######.#.#...#\n" ++
  "#######.#.###.#\n" ++
  "###..E#...#...#\n" ++
  "###.#######.###\n" ++
  "#...###...#...#\n" ++
  "#.#####.#.###.#\n" ++
  "#.#...#.#.#...#\n" ++
  "#.#.#.#.#.#.###\n" ++
  "#...#...#...###\n" ++
  "###############"

export
test1 : HasIO io => io ()
test1 = do
  a2d <- stringToArray2d input1
  drawArray2d a2d

