
module Fibo

import Control.Monad.State
import Data.IOArray
import Data.List
import Data.SortedMap

fiboNaive : Nat -> Nat
fiboNaive Z          = 0
fiboNaive (S Z)      = 1
fiboNaive (S (S n2)) = fiboNaive (S n2) + fiboNaive n2

fiboNaiveMem : Nat -> Nat
fiboNaiveMem x = fst $ go x empty

  where
    go : Nat -> SortedMap Nat Nat -> (Nat, SortedMap Nat Nat)
    go Z             cache0 = (0, cache0)
    go (S Z)         cache0 = (1, cache0)
    go n0@(S (S n2)) cache0 = 
      case lookup n0 cache0 of
           Just fn0 => (fn0, cache0)
           Nothing => 
             let (fn1, cache1) = go (S n2) cache0
                 (fn2, cache2) = go n2 cache1
                 fn0 = fn1 + fn2
                 cache = insert n0 fn0 cache2
             in (fn0, cache)

fiboNaiveMemSt : Nat -> Nat
fiboNaiveMemSt x = evalState empty $ go x

  where
    go : Nat -> State (SortedMap Nat Nat) Nat
    go Z             = pure 0
    go (S Z)         = pure 1
    go n0@(S (S n2)) = do
      cache <- get
      case lookup n0 cache of
           Just fn0 => pure fn0
           Nothing => do
             fn0 <- (+) <$> go (S n2) <*> go n2
             modify (insert n0 fn0)
             pure fn0

fiboIterative : Nat -> Nat
fiboIterative x = go 0 1 x

  where
    go : Nat -> Nat -> Nat -> Nat
    go fn2 fn1 Z     = fn2
    go fn2 fn1 (S n) = go fn1 (fn1+fn2) n

-------------------------------------------------------------------------------

Array : Type
Array = (IOArray Integer, Int)  -- array, length

mkMem : HasIO io => io Array
mkMem = (,2) <$> fromList [Just 0, Just 1]

myRead : HasIO io => IOArray Integer -> Int -> io Integer
myRead arr n = maybe (-1) id <$> readArray arr n

expand : (HasIO m, MonadState Array m) => m () 
expand = do
  (arr0, len0) <- get
  let len1 = len0*2
  arr1 <- newArray len1
  for_ [0 .. len0-1] $ \i => do
    x <- myRead arr0 i
    writeArray arr1 i x
  for_ [len0 .. len1-1] $ \i => do
    fn1 <- myRead arr1 (i-1)
    fn2 <- myRead arr1 (i-2)
    writeArray arr1 i (fn1+fn2)
  put (arr1, len1)

covering
fiboMem : (HasIO m, MonadState Array m) => Int -> m Integer
fiboMem n = do
  mem@(arr, len) <- get
  if n < len 
     then myRead arr n 
     else expand >> fiboMem n

-------------------------------------------------------------------------------

export
-- covering
test1 : HasIO io => io ()
test1 = do
  let num = 44
  -- putStrLn $ "fiboNaive " ++ show num ++ " -> " ++ show (fiboNaive num)
  -- putStrLn $ "fiboNaiveMem " ++ show num ++ " -> " ++ show (fiboNaiveMem num)
  putStrLn $ "fiboNaiveMemSt " ++ show num ++ " -> " ++ show (fiboNaiveMemSt num)
  -- putStrLn $ "fiboIterative " ++ show num ++ " -> " ++ show (fiboIterative num)

  -- mem <- mkMem
  -- evalStateT mem (fiboMem num) >>= printLn
  -- let nums = [1000 .. 4000]
  -- printLn $ last' $ map fiboIterative nums 
  -- traverse (evalStateT mem . fiboMem) nums >>= printLn . last'

