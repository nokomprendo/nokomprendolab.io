
module Day02

import Data.String

isSafe : List Int -> Bool
isSafe report = isSafeIncreasing report || isSafeIncreasing (reverse report)
  where
    isSafeIncreasing : List Int -> Bool
    isSafeIncreasing (x1::x2::xs) = 
      let dx = x2 - x1
      in dx >= 1 && dx <= 3 && isSafeIncreasing (x2::xs)
    isSafeIncreasing _ = True

parseMatrix : String -> List (List Int)
parseMatrix = map (map cast . words) . lines

export
run1 : String -> Nat
run1 = length . filter isSafe . parseMatrix

export 
test1 : HasIO io => String -> io ()
test1 input = do

  -- printLn $ isSafe [1, 2, 7, 8, 9]
  -- printLn $ isSafe [1, 3, 6, 7, 9]

  printLn $ parseMatrix "1 2 3\n4 5 6"

  -- let reports = parseMatrix input
  -- traverse_ printLn reports
  -- traverse_ (printLn . isSafe) reports

