
import Data.String
import System.File.ReadWrite

import Array2d
import Day01
import Day02
import Day07
import Day06
import Fibo
import MyList
import MyMap
import MySet
import Tuto119

covering
run : Show a => String -> (String -> a) -> IO ()
run filename f = do
  res <- readFile filename
  case res of
       Left err => printLn err
       Right input => printLn $ f input

covering
runIO : String -> (String -> IO ()) -> IO ()
runIO filename f = do
  putStrLn $ "\nReading " ++ filename
  res <- readFile filename
  case res of
       Left err => printLn err
       Right input => f input

covering
main : IO ()
main = do

  putStrLn "aoc2024"

  -- runIO "data/Test02.txt" Day02.test1
  -- run "data/Test02.txt" Day02.run1
  -- runIO "data/Test01.txt" Day01.test1
  -- run "data/Test01.txt" Day01.run1

  -- run "data/Test07.txt" Day07.run1

  -- Fibo.test1

  Tuto119.test1
  -- MyList.test1
  -- MyMap.test1
  -- MySet.test1
  -- Array2d.test1
  -- run "data/Test06.txt" Day06.run1


