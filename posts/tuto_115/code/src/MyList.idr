
module MyList

-------------------------------------------------------------------------------
-- Mylist
--
-- cons en O(1)
-- elem en O(n)
-------------------------------------------------------------------------------

data MyList a 
  = Nil 
  | Cons a (MyList a)

Functor MyList where 
  map f Nil = Nil
  map f (Cons x xs) = Cons (f x) (map f xs)

showInner : Show a => MyList a -> String
showInner Nil = ""
showInner (Cons x Nil) = show x
showInner (Cons x xs) = show x ++ ", " ++ showInner xs

Show a => Show (MyList a) where
  show l = "[" ++ showInner l ++ "]"

elem : Eq a => a -> MyList a -> Bool
elem e Nil = False
elem e (Cons x xs) = e == x || elem e xs

partial
myHead : MyList a -> a
myHead (Cons x xs) = x

melem : Eq a => a -> List a -> Bool
melem e Nil = False
melem e (x :: xs) = e == x || melem e xs

-------------------------------------------------------------------------------
-- Mylist1
-------------------------------------------------------------------------------

data MyList1 a
  = Cons1 a (MyList a)

Functor MyList1 where
  map f (Cons1 x xs) = Cons1 (f x) (map f xs)

Show a => Show (MyList1 a) where
  show (Cons1 x Nil) = "[" ++ show x ++ "]"
  show (Cons1 x xs) = "[" ++ show x ++ ", " ++ showInner xs ++ "]"

myHead1 : MyList1 a -> a
myHead1 (Cons1 x xs) = x

-------------------------------------------------------------------------------
-- test
-------------------------------------------------------------------------------

myList : MyList Int
myList = Cons 13 (Cons 37 Nil)

myEmptyList : MyList Int
myEmptyList = Nil

myList1 : MyList1 Int
myList1 = Cons1 42 myList

export
test1 : HasIO io => io ()
test1 = do

  putStrLn "\nMyList"
  printLn myList
  printLn $ map (*2) myList
  -- printLn $ myHead myList
  -- printLn $ myHead myEmptyList
  printLn $ MyList.elem 42 myList
  printLn $ MyList.elem 13 myList

  putStrLn "\nMyList1"
  printLn myList1
  printLn $ map (*2) myList1
  printLn $ myHead1 myList1

