
module MyMap

-------------------------------------------------------------------------------
-- Map
-- 
-- insert/lookup en O(log(n))
-------------------------------------------------------------------------------


data MyMap k v
  = Leaf
  | Node k v (MyMap k v) (MyMap k v)

insert : Ord k => k -> v -> MyMap k v -> MyMap k v
insert ke ve Leaf = Node ke ve Leaf Leaf
insert ke ve (Node ki vi n1 n2) =
  if ke < ki then Node ki vi (insert ke ve n1) n2
  else if ke > ki then Node ki vi n1 (insert ke ve n2)
  else Node ke ve n1 n2

fromList : Ord k => List (k, v) -> MyMap k v
fromList = foldr (uncurry insert) Leaf

toList : MyMap k v -> List (k, v)
toList Leaf = []
toList (Node k v n1 n2) = toList n1 ++ [(k, v)] ++ toList n2

lookup : Ord k => k -> MyMap k v -> Maybe v
lookup ke Leaf = Nothing
lookup ke (Node ki vi n1 n2) =
  if ke == ki then Just vi
  else if ke < ki then lookup ke n1
  else lookup ke n2

-------------------------------------------------------------------------------
-- test
-------------------------------------------------------------------------------

myMap : MyMap Int String
myMap = fromList [(13, "treize"), (42, "quarante deux"), (37, "trente sept")]

export
test1 : HasIO io => io ()
test1 = do
  putStrLn "\nMyMap"
  printLn $ MyMap.toList myMap
  printLn $ lookup 42 myMap
  printLn $ lookup 41 myMap


