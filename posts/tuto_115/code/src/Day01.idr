
module Day01

import Data.String

parseMatrix : String -> List (List Int)
parseMatrix = map (map cast . words) . lines 

matrixToSortedLists : List (List Int) -> (List Int, List Int)
matrixToSortedLists mat = 
  case transpose mat of
       [xs, ys] => (sort xs, sort ys)
       _        => ([], [])

computeDistance : List Int -> List Int -> Int
computeDistance = sum .: zipWith (abs .: (-))

export
run1 : String -> Int
run1 = uncurry computeDistance . matrixToSortedLists . parseMatrix

export 
test1 : HasIO io => String -> io ()
test1 input = do
  let mat = parseMatrix input
  traverse_ printLn mat
  printLn $ matrixToSortedLists mat

