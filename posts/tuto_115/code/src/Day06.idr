
module Day06

import Data.SortedMap
import Data.SortedSet
import Data.String

data Direction = Up | Down | Left | Right

Position : Type
Position =  (Int, Int)

record LabMap where
  constructor MkLabMap
  rows : SortedMap Int (List Int)
  cols : SortedMap Int (List Int)
  revRows : SortedMap Int (List Int)
  revCols : SortedMap Int (List Int)
  nrows : Int
  ncols : Int

parseInput : String -> (LabMap, Position)
parseInput input =
  let css = map unpack $ lines input
      rows = map denseToSparse css
      revRows = map reverse rows
      cols = map denseToSparse $ transpose css
      revCols = map reverse cols
      nrows = cast (length rows)
      ncols = cast (length cols)
      cs = concat css
      ks = filter ((=='^') . fst) $ zip cs [0 .. cast (length cs) - 1]
      (i, j) = case ks of
        [(_, k)] => (div k ncols, mod k ncols)
        _ => (0, 0)
  in (MkLabMap (matToMap rows) (matToMap cols) (matToMap revRows) (matToMap revCols) nrows ncols, (i, j))

  where
    denseToSparse : List Char -> List Int
    denseToSparse cs = map snd $ filter ((=='#') . fst) $ zip cs [0 .. cast (length cs)]

    matToMap : List (List a) -> SortedMap Int (List a)
    matToMap xss = fromList $ zip [0 .. cast (length xss)] xss

walk1 : LabMap -> Direction -> Position -> (Position, Bool, Direction, SortedSet Position)
walk1 lm dir p0@(i,j) =
  case dir of
    Up => case dropWhile (>i) (myGetMap j lm.revCols) of
       []     => ((0, j), True, Up, fromList [ (ii,j) | ii<-[0 .. i-1] ])
       (x::_) => ((x+1, j), False, Right, fromList [ (ii,j) | ii<-[x+1 .. i-1] ])

    Down => case dropWhile (<i) (myGetMap j lm.cols) of
       []     => ((lm.nrows-1, j), True, Down, fromList [ (ii,j) | ii<-[i+1 .. lm.nrows-1] ])
       (x::_) => ((x-1, j), False, Left, fromList [ (ii,j) | ii<-[i+1 .. x-1] ])

    Left => case dropWhile (>j) (myGetMap i lm.revRows) of
       []     => ((i, 0), True, Left, fromList [ (i,jj) | jj<-[0 .. j-1] ])
       (x::_) => ((i, x+1), False, Up, fromList [ (i,jj) | jj<-[x+1 .. j-1] ])

    Right => case dropWhile (<j) (myGetMap i lm.rows) of
       []     => ((i, lm.ncols-1), True, Right, fromList [ (i,jj) | jj<-[j+1 .. lm.ncols-1] ])
       (x::_) => ((i, x-1), False, Down, fromList [ (i,jj) | jj<-[j+1 .. x-1] ])

  where
    myGetMap : Int -> SortedMap Int (List Int) -> List Int
    myGetMap k v = maybe [] id $ lookup k v

covering
walkFull : LabMap -> Position -> (Position, SortedSet Position, Direction)
walkFull lm pos = go Up pos (singleton pos)
  where
    go : Direction -> Position -> SortedSet Position -> (Position, SortedSet Position, Direction)
    go dir0 pos0 acc = 
      let (pos1, terminated1, dir1, npos1) = walk1 lm dir0 pos0
      in if terminated1
            then (pos1, union npos1 acc, dir1)
            else go dir1 pos1 (union npos1 acc)

export
covering
run1 : String -> Nat
run1 str = 
  let (lm, pos) = parseInput str
      (_, npos, _) = walkFull lm pos
  in length $ Data.SortedSet.toList npos

