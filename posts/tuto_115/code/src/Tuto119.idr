
module Tuto119

import Data.IOArray
import Data.SortedMap
import Data.SortedSet
import Data.String
import Data.Vect

-------------------------------------------------------------------------------
-- list
-------------------------------------------------------------------------------

{-
data List a 
    = Nil
    | (::) a (List a)
-}

elemList : Eq a => a -> List a -> Bool
elemList e Nil = False
elemList e (x :: xs) = e == x || elemList e xs

myList1 : List Int
myList1 = [1, 2]
-- myList1 = 1 :: 2 :: Nil

-------------------------------------------------------------------------------
-- array
-------------------------------------------------------------------------------

mkArray1 : HasIO io => io (IOArray Int)
mkArray1 = fromList $ map Just [1..6]

covering
elemArray : (Eq a, HasIO io) => a -> IOArray a -> Int -> io Bool
elemArray e arr i = 
  if i < 0
     then pure False
     else do
       x <- readArray arr i
       if x == Just e 
          then pure True
          else elemArray  e arr (i-1)

-------------------------------------------------------------------------------
-- map/set
-------------------------------------------------------------------------------

testMap : HasIO io => io ()
testMap = do
  let myMap1 : SortedMap Int String
      myMap1 = fromList [ (13, "treize")
                        , (42, "quarante-deux")
                        , (37, "trente-sept") ]
  printLn $ lookup 13 myMap1
  printLn $ lookup 12 myMap1

testSet : HasIO io => io ()
testSet = do

  let mySet1 : SortedSet Int
      mySet1 = insert 13 $ insert 42 $ insert 37 empty

  let mySet2 : SortedSet Int
      mySet2 = fromList [12, 42]

  let mySet3 = union mySet1 mySet2

  printLn $ SortedSet.toList mySet3

-------------------------------------------------------------------------------
-- matrix (using list)
-------------------------------------------------------------------------------

MatrixList : Type -> Type
MatrixList a = List (List a)

getIJMatrixList : MatrixList a -> Nat -> Nat -> Maybe a
getIJMatrixList mat i j = getAt i mat >>= getAt j

myMatrixList1 : MatrixList Int
myMatrixList1 =
    [ [1, 2, 3]
    , [4, 5, 6]
    ]

inputMatrix2 : String
inputMatrix2 = 
  "....#.....\n" ++
  ".........#\n" ++
  "..........\n" ++
  "..#.......\n" ++
  ".......#..\n" ++
  "..........\n" ++
  ".#..^.....\n" ++
  "........#.\n" ++
  "#.........\n" ++
  "......#..."

myMatrixList2 : MatrixList Char
myMatrixList2 = map unpack $ lines inputMatrix2

printMatrixList : (HasIO io) => MatrixList Char -> io ()
printMatrixList mat =
  for_ mat $ \line => do
    for_ line $ \c =>
      putChar c
    putChar '\n'

-------------------------------------------------------------------------------
-- matrix (using array)
-------------------------------------------------------------------------------

record MatrixArray t where
  constructor MkMatrixArray
  ni : Int
  nj : Int
  arr : IOArray t

mkMatrixArray1 : HasIO io => io (MatrixArray Int)
mkMatrixArray1 = MkMatrixArray 2 3 <$> fromList (map Just [1..6])

stringToArray2d : HasIO io => String -> io (MatrixArray Char)
stringToArray2d str = do
  let mat = map unpack $ lines str
      ni = cast $ length mat
      nj = maybe 0 (cast . length) $ head' mat
  arr <- fromList $ map Just $ concat mat
  pure (MkMatrixArray ni nj arr)

getIJMatrixArray : HasIO io => MatrixArray a -> Int -> Int -> io (Maybe a)
getIJMatrixArray mat i j = readArray mat.arr (i * mat.nj + j)

printMatrixArray : HasIO io => MatrixArray Char -> io ()
printMatrixArray mat@(MkMatrixArray ni nj arr) = do
  for_ [0 .. ni-1] $ \i => do
    for_ [0 .. nj-1] $ \j => do
      c <- maybe ' ' id <$> getIJMatrixArray mat i j
      putChar c
    putChar '\n'

-------------------------------------------------------------------------------
-- vect
-------------------------------------------------------------------------------

elemVect : Eq a => a -> Vect n a -> Bool
elemVect e Nil = False
elemVect e (x :: xs) = e == x || elemVect e xs

myVect1 : Vect 2 Int
myVect1 = [1, 2]
-- myVect1 = 1 :: 2 :: Nil

-------------------------------------------------------------------------------
-- matrix (using vect)
-------------------------------------------------------------------------------

MatrixVect : Nat -> Nat -> Type -> Type
MatrixVect ni nj a = Vect ni (Vect nj a)

myMatrixVect1 : MatrixVect 2 3 Int
myMatrixVect1 = 
  [ [1, 2, 3]
  , [4, 5, 6]
  ]

addMatrixVect : Num a => MatrixVect n1 n2 a -> MatrixVect n1 n2 a -> MatrixVect n1 n2 a
addMatrixVect = zipWith (zipWith (+))

flatMatrixVect : MatrixVect n1 n2 a -> Vect (n1*n2) a
flatMatrixVect = concat

myMatrixVect2 : MatrixVect 2 2 Int
myMatrixVect2 = 
  [ [1, 2]
  , [4, 5]
  ]

-------------------------------------------------------------------------------
-- aoc2024, day21
-------------------------------------------------------------------------------

GraphList : Type
GraphList = List (Char, Char, Char)     -- from, to, direction

myGraphList1 : GraphList
myGraphList1 =
  [ ('^', 'A', '>'), ('^', 'v', 'v')
  , ('A', '^', '<'), ('A', '>', 'v')
  , ('<', 'v', '>')
  , ('v', '<', '<'), ('v', '^', '^'), ('v', '>', '>')
  , ('>', 'v', '<'), ('>', 'A', '^')
  ]

getNeighboursList : GraphList -> Char -> List Char
getNeighboursList g n = map (fst . snd) $ filter ((==n) . fst) g


GraphMap : Type
GraphMap = SortedMap Char (List (Char, Char))     -- from [(to, direction)]

myGrapMap1 : GraphMap
myGrapMap1 = fromList 
  [ ('^', [('A', '>'), ('v', 'v')])
  , ('A', [('^', '<'), ('>', 'v')])
  , ('<', [('v', '>')])
  , ('v', [('<', '<'), ('^', '^'), ('>', '>')])
  , ('>', [('v', '<'), ('A', '^')])
  ]

getNeighboursMap : GraphMap -> Char -> List Char
getNeighboursMap g n = maybe [] (map fst) $ lookup n g


-------------------------------------------------------------------------------
-- test
-------------------------------------------------------------------------------

export
covering
test1 : HasIO io => io ()
test1 = do
  putStrLn "\nTuto119"

  putStrLn "\nlist"
  printLn myList1
  printLn $ elemList 1 myList1
  printLn $ elemList 12 myList1

  putStrLn "\narray"
  myArray1 <- mkArray1
  elemArray 3 myArray1 5 >>= printLn
  elemArray 12 myArray1 5 >>= printLn

  putStrLn "\ntestMap"
  testMap

  putStrLn "\ntestSet"
  testSet

  putStrLn "\nmatrix list"
  printLn $ getIJMatrixList myMatrixList1 1 2
  printMatrixList myMatrixList2

  putStrLn "\nmatrix array"
  myMatrixArray1 <- mkMatrixArray1
  getIJMatrixArray myMatrixArray1 1 2 >>= printLn
  myMatrixArray2 <- stringToArray2d inputMatrix2
  printMatrixArray myMatrixArray2

  putStrLn "\nvect"
  printLn myVect1
  printLn $ elemVect 1 myVect1
  printLn $ elemVect 12 myVect1
  printLn $ addMatrixVect myMatrixVect1 myMatrixVect1
  -- printLn $ addMatrixVect myMatrixVect1 myMatrixVect2

  putStrLn "\ngraph"
  printLn $ getNeighboursList myGraphList1 '^'

