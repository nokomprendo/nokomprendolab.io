
module MySet

-------------------------------------------------------------------------------
-- Set
-- 
-- insert/lookup en O(log(n))
-------------------------------------------------------------------------------

data MySet k
  = Leaf
  | Node k (MySet k) (MySet k)

insert : Ord k => k -> MySet k -> MySet k
insert e Leaf = Node e Leaf Leaf
insert e (Node x n1 n2) =
  if e < x then Node x (insert e n1) n2
  else if e > x then Node x n1 (insert e n2)
  else Node x n1 n2

fromList : Ord k => List k -> MySet k
fromList = foldr insert Leaf

toList : MySet k -> List k
toList Leaf = []
toList (Node x n1 n2) = toList n1 ++ [x] ++ toList n2

contains : Ord k => k -> MySet k -> Bool
contains e Leaf = False
contains e (Node x n1 n2) =
  e == x || if e < x then contains e n1 else contains e n2

-------------------------------------------------------------------------------
-- test
-------------------------------------------------------------------------------

mySet : MySet Int
mySet = fromList [13, 42, 37]

export
test1 : HasIO io => io ()
test1 = do
  putStrLn "\nMySet"
  printLn $ MySet.toList mySet
  printLn $ contains 42 mySet
  printLn $ contains 41 mySet


