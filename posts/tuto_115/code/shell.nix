{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {

  buildInputs = with pkgs; [
    bash
    bashInteractive
    chez
    git
    gnumake
    gmp
  ];

  shellHooks =''
    export PATH="$PATH:$HOME/.pack/bin"
  '';

}


