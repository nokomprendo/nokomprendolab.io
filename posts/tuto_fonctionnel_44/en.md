---
title: Package management and DevOps with Nix, an overview using a practical example
description: Nix a package manager inspired by functional programming. This has many advantages and allows many additional features. This article gives an overview of the possibilities of Nix from a source-code project.
---

See also :
[example project](https://gitlab.com/nokomprendo/mymathserver) -
[youtube video (french)](https://youtu.be/Z4PhRtA-9gU) -
[peertube video (french)](https://peertube.fr/videos/watch/28955633-7053-456b-a955-38ff98e9e98e)

[Nix](https://nixos.org/nix/) and [GNU Guix](http://guix.gnu.org/) are
"functional" package managers, in the sense of functional programming. This
approach to package management is very different from the approach usually used
by Linux or BSD systems, based on port collections or package repositories.

This functional approach brings many advantages. Not only does it provide
reliable, reproducible, multi-version and multi-user package management, but it
also provides many additional features: handle development environments,
package personal project easily, build Docker images or virtual machines ,
customize the entire software environment, etc.

This article is based on a source code project (a web server) and gradually
illustrates various features of Nix that may be useful for a developer, package
maintainer, or system administrator. These illustrations are given here for
[NixOS](https://nixos.org/) (the Linux distribution based on Nix) but should
also be usable with the Nix tool installed on a classic Linux distribution, or
with GNU Guix.


# Example project: mymathserver

The [mymathserver](https://gitlab.com/nokomprendo/mymathserver) project is
a basic web server. The route "/" returns a welcome text and the route "/mul2"
multiplies a given number by 2.

![](images/mymathserver.mp4)

This project is implemented in C++. It contains a very basic "library" file,
`src/mymath.hpp`:

```cpp
#pragma once

int mul2(int x) {
    return 2*x;
}
```

A unit testing executable, `src/mymathtest.cpp`:

```cpp
#include <gtest/gtest.h>

#include "mymath.hpp"

TEST(Mymath, mul2_1) {
    ASSERT_EQ(0, mul2(0));
}

// ...

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
```

And a web-server executable, `src/mymathserver.cpp`:

```cpp
#include "mymath.hpp"
#include <cpprest/http_listener.h>
// ...

class App : public web::http::experimental::listener::http_listener {
    private:
        void handleGet(web::http::http_request req) {
            // ...
            if (path == "/") {
                // ...
            }
            else if (splitPath.size() == 2 and splitPath[0] == "mul2") {
                // ...
            }
            else {
                req.reply(web::http::status_codes::NotFound);
            }
        }

    public:
        App(std::string url) : web::http::experimental::listener::http_listener(url) {
            support(web::http::methods::GET, 
                    bind(&App::handleGet, this, std::placeholders::_1));
        }
};

int main() {
    const char * portEnv = std::getenv("PORT");
    const std::string port = portEnv == nullptr ? "3000" : portEnv;
    const std::string address = "http://0.0.0.0:" + port;
    App app(address);
    // ...
    return 0;
}
```

The project is configured, in a very classic way, using a `CMakeLists.txt` file:

```cmake
cmake_minimum_required( VERSION 3.0 )
project( mymathserver )

find_package( GTest REQUIRED )
add_executable( mymathtest src/mymathtest.cpp )
target_include_directories( mymathtest PRIVATE ${GTEST_INCLUDE_DIRS} )
target_link_libraries( mymathtest
    ${GTEST_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT} )

find_package( Boost REQUIRED system )
find_package ( Threads REQUIRED )
find_package ( OpenSSL REQUIRED )
add_executable( mymathserver src/mymathserver.cpp )
target_link_libraries( mymathserver PRIVATE
   cpprest ${Boost_LIBRARIES} ${OPENSSL_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT} )

install( TARGETS mymathserver mymathtest DESTINATION bin )
```


# Package a missing dependency

So we have the unit testing program mymathtest, which uses the gtest library,
and the server program mymathserver, which uses boost, openssl and
[cpprestsdk](https://github.com/microsoft/cpprestsdk).  All these dependencies
are classic and therefore present in most Linux repositories. However
cpprestsdk is much less known than the other dependencies and may be missing
when compiling our project. To avoid this problem, we can let the user install
or compile cpprestsdk (which can be complicated or tedious), or integrate
cpprestsdk directly into our project using a git sub-module (which is not very
efficient and makes the project depend on git).

With Nix, we can create a package for `cpprestsdk` ourselves. Some libraries
can be complex to package but in general, it is enough to specify how to
get the source code and to specify the list of dependencies
(`nix/cpprestsdk.nix`) :

```nix
{ stdenv, fetchFromGitHub, cmake, boost, openssl, websocketpp, zlib }:

stdenv.mkDerivation {

  name = "cpprestsdk";

  src = fetchFromGitHub {
    owner = "Microsoft";
    repo = "cpprestsdk";
    rev = "v2.10.14";
    sha256 = "0z1yblqszs7ig79l6lky02jmrs8zmpi7pnzns237p0w59pipzrvs";
  };

  buildInputs = [ boost cmake openssl websocketpp zlib ];
}
```

We may notice that there is no explicit compilation directive in the previous
file. Indeed, as we specified cmake in the dependencies of `cpprestsdk`, Nix
knows that it has to compile `cpprestsdk` with the classic cmake commands.


# Package the project

In the same way that we packaged `cpprestsdk`, we can create a
`nix/mymathserver.nix` file to package our project:

```nix
{ stdenv, cpprestsdk, boost, cmake, gtest, openssl }:

stdenv.mkDerivation {
  name = "mymathserver";
  version = "0.1";
  src = ../.;
  buildInputs = [ boost cmake cpprestsdk gtest openssl ];
}
```

Here again, since we put `cmake` in the dependencies, Nix knows that it has to
use `cmake` and our `CMakeLists.txt` file to compile the project.

At the moment, Nix doesn't know the exact dependency code. It just knows their
name, which are parameters of the package (the first line of the file). Hence
the famous "functional approach" of Nix.

Finally, we write a `default.nix` file, which is the entry point for our
project configuration and links to the previous files:

```nix
{ pkgs ? import <nixpkgs> {} }:

let
  cpprestsdk = pkgs.callPackage ./nix/cpprestsdk.nix {};
  mymathserver = pkgs.callPackage ./nix/mymathserver.nix { inherit cpprestsdk; };

in mymathserver
```

This file has a parameter (pkgs) but with a default value (the `nixpkgs`
software repository). It also calls the previous packages `nix/cpprestsdk.nix`
and `nix/mymathserver.nix` on `pkgs`. The parameters of these two packages
(`cmake`, `openssl`, `boost` etc) are fixed to the values present in `pkgs`.
However, for `mymathserver`, the `cpprestsdk` parameter  is fixed to the
`cpprestsdk` package created just before (thanks to `inherit`). Thus, the
`default.nix` file provides the source code to use for actually building the
project, through the parameters of the package descriptions.

Note that we could have done without the `nix/mymathserver.nix` file and put
everything in the `default.nix` file. The advantage of using two files is to
make our packaging more modular. For example, we could almost directly
integrate `nix/mymathserver.nix` into the
[nixpkgs repository](https://github.com/NixOS/nixpkgs)
and our project would be available in the official Nix package repository!


# Run an isolated environment

Using our `default.nix` file, we can run an isolated environment:

```sh
nix-shell
```

This installs the dependencies, builds `cpprestsdk` and initializes the
environment. We can then build our project using the usual `cmake` commands:

```sh
mkdir mybuild
cd mybuild
cmake ..
make
./mymathserver
```

![](images/mymathserver-shell.png)


# Build and install a package of the project

Using the `default.nix` file, we can also build a package for our project. The
package is stored in a specific folder in `/nix/store` and a symbolic link
`result` is created in the current folder:

```sh
nix-build
./result/bin/mymathserver
```

We can also install our project as if it were a package of the system
repository:

```sh
nix-env -i f .
mymathserver
```

We can even download an archive of the source code then build and install the
package automatically:

```sh
nix-env -if "https://gitlab.com/nokomprendo/mymathserver/-/archive/master/mymathserver-master.tar.gz"
```


# Reproducible build

By default, the `default.nix` file builds the project from the current system
repository, which can vary over time or depending on the users. Nix allows us
to specify the version of the software repository to use, and therefore to have
a reproducible package. For example with the `nix/release.nix` file:

```nix
let
  rev = "1c92cdaf7414261b4a0e0753ca9383137e6fef06";

  pkgs-src = fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
    sha256 = "0d3fqa1aqralj290n007j477av54knc48y1bf1wrzzjic99cykwh";
  };

  pkgs = import pkgs-src {};
  
in pkgs.callPackage ../default.nix {}
```

Again, the functional approach of Nix allows us to compose configuration files.
Here the `nix/release.nix` file just gets a particular `pkgs` and uses it as a
default.nix parameter to build the project and its dependencies.

We can thus build the reproducible package:

```sh
nix-build nix/release.nix
```


# Continuous integration

Since our Nix configuration completely describes the environment and the
construction of our project, it can be used in a continuous integration
pipeline. For example, with `gitlab-ci`, we can add the following
`.gitlab-ci.yml` file to build the project and run the unit tests.

```yaml
build:
    image: nixos/nix
    script:
        - nix-build nix/release.nix
        - ./result/bin/mymathtest
```

![](images/mymathserver-gitlab0.png)


# Binary cache

Nix can use pre-built binary packages to avoid having to recompile everything.
For example, `nixpkgs` provides a [binary cache](http://cache.nixos.org/).
However, `cpprestsdk` is not available in this cache because we packaged it
ourselves. We can set up our own binary cache using the Nix continuous build
system ([Hydra](https://nixos.org/hydra/)) but this is not very simple to do. 

Another solution is to use [Cachix](https://cachix.org/), a binary cache
hosting service, to upload pre-built Nix packages and then download them to
another system.

TODO

L'utilisation de cachix est très simple. Il suffit de créer un compte et un
dépôt de cache. On peut ensuite envoyer des paquets binaires avec la commande
`cachix push` et activer le téléchargement depuis un dépôt de cache avec la
commande `cachix use`.

Par exemple, pour accélerer l'intégration continue précédente, on peut
construire `cpprestsdk` avec notre configuration `release`,  uploader les
paquets binaires correspondants dans le dépôt `nokomprendo` puis utiliser ce
cache dans le processus d'intégration continue (`.gitlab-ci.yml`) :

```yaml
build:
    image: nixos/nix
    script:
        - nix-env -iA nixpkgs.cachix
        - cachix use nokomprendo
        - nix-build nix/release.nix
        - ./result/bin/mymathtest
```

Ceci fait passer l'exécution d'un processus d'intégration continue de 20
minutes à moins de 2 minutes.

![](images/mymathserver-gitlab1.png)


# Construire et déployer une image Docker

Nix permet de construire des images Docker. Par exemple, le fichier
`nix/docker.nix` suivant définit une image Docker à partir de notre
configuration `release`:

```nix
{ pkgs ? import <nixpkgs> {} }:

let
  mymathserver = import ./release.nix;

  entrypoint = pkgs.writeScript "entrypoint.sh" ''
    #!${pkgs.stdenv.shell}
    $@
  '';

in pkgs.dockerTools.buildImage {
  name = "mymathserver";
  tag = "latest";
  config = {
    WorkingDir = "${mymathserver}";
    Entrypoint = [ entrypoint ];
    Cmd = [ "${mymathserver}/bin/mymathserver" ];
  };
}
```

On peut alors construire l'image, la charger dans Docker et la tester :

```sh
nix-build nix/docker.nix
docker load < result
docker run --rm -it -p 3000:3000 mymathserver:latest
```

![](images/mymathserver-docker.png)


Cette image Docker peut être déployée, par exemple sur [Heroku](https://heroku.com/) :

```sh
heroku login
heroku container:login
heroku create nokomprendo-mymathserver
docker tag mymathserver:latest registry.heroku.com/nokomprendo-mymathserver/web
docker push registry.heroku.com/nokomprendo-mymathserver/web
heroku container:release web --app nokomprendo-mymathserver
heroku logs --tail
```

L'application est alors accessible à l'adresse http://nokomprendo-mymathserver.herokuapp.com

![](images/mymathserver-heroku.png)


# Construire et déployer une machine virtuelle

Nix permet de définir et de déployer des machines virtuelles, avec `nixops`.
Par exemple, le fichier `nix/virtualbox.nix` suivant reprend notre programme
`mymathserver`, le lance dans un service `systemd` et déploie le tout dans une
`virtualbox` :

```nix
{
  network.description = "mynetwork";

  myserver = { config, pkgs, ... }: 

  let
    myapp = import ./release.nix;

  in {
    networking.firewall.allowedTCPPorts = [ 3000 ];

    systemd.services.myservice = {
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      script = "${myapp}/bin/mymathserver";
    };

    deployment = {
      targetEnv = "virtualbox";
      virtualbox = {
        memorySize = 512; 
        vcpu = 1; 
        headless = true;
      };
    };
  };
}
```

Les commandes suivantes créent et déploient la machine virtuelle sous le nom
`myvm` :

```sh
nixops create -d myvm nix/virtualbox.nix
nixops deploy -d myvm --force-reboot
```

A l'issu du déploiement, une adresse IP est fournie et permet d'accèder à notre
serveur sur la machine virtuelle.

![](images/mymathserver-virtualbox.png)


# Personnaliser des paquets existants

Enfin, une fonctionnalité originale de Nix, liée à son "approche
fonctionnelle", est la possibilité de modifier les paramètres des paquets et de
répercuter automatiquement et efficacement ces modifications dans l'ensemble de
l'environnement logiciel.

Les paquets peuvent être surchargés, c'est-à-dire remplacés par des versions
modifiées. Par exemple, le paquet de la bibliothèque
[zlib](https://github.com/NixOS/nixpkgs/blob/master/pkgs/development/libraries/zlib/default.nix)
contient un attribut `configureFlags` qui contient les options de compilation à
utiliser. Si on veut utiliser une version de `zlib` compilée avec l'option
`--zprefix`, on peut utiliser le fichier `nix/custom0.nix` suivant :

```nix
let
  pkgs = import <nixpkgs> {};

  zlib = pkgs.zlib.overrideDerivation (attrs: {
     configureFlags = [ attrs.configureFlags "--zprefix" ];
  });

  cpprestsdk = pkgs.callPackage ./cpprestsdk.nix { inherit zlib; };

  mymathserver = pkgs.callPackage ./mymathserver.nix { inherit cpprestsdk; };

in pkgs.stdenv.mkDerivation rec {
  name = "mymathserver-custom0";
  src = ./.;
  buildPhase = "";
  installPhase = ''
    mkdir -p $out/bin
    cp ${mymathserver}/bin/mymathserver $out/bin/${name}
  '';
}
```

Les paramètres des paquets Nix peuvent également contenir des options
paramétrables.  Par exemple, la bibliothèque
[openssl](https://github.com/NixOS/nixpkgs/blob/master/pkgs/development/libraries/openssl/default.nix)
a une option `enableSSL2`, qu'on peut spécifier comme dans le fichier
`nix/custom1.nix` suivant :

```nix
let
  pkgs = import <nixpkgs> {};

  openssl = pkgs.openssl.override { enableSSL2 = true; };

  cpprestsdk = pkgs.callPackage ./cpprestsdk.nix { inherit openssl; };

  mymathserver = pkgs.callPackage ./mymathserver.nix {
    inherit cpprestsdk;
    inherit openssl;
  };

in pkgs.stdenv.mkDerivation rec {
  name = "mymathserver-custom1";
  src = ./.;
  buildPhase = "";
  installPhase = ''
    mkdir -p $out/bin
    cp ${mymathserver}/bin/mymathserver $out/bin/${name}
  '';
}
```

Enfin, pour éviter tout risque d'incompatibilité d'ABI, on peut surcharger la
configuration lors du chargement de la logithèque. Ainsi, tous les paquets
dépendant des paquets surchargés seront recompilés en prenant en compte ces
modifications. Par exemple, le fichier `nix/custom2.nix` suivant surcharge
`openssl` via la configuration de la logithèque (il existe aussi un système
d'overlays pour cela) :

```nix
let

  config = {
    packageOverrides = pkgs: {
      openssl = pkgs.openssl.override {
        enableSSL2 = true;
      };
    };
  };

  pkgs = import <nixpkgs> { inherit config; };

  mymathserver = pkgs.callPackage ../default.nix {};

in pkgs.stdenv.mkDerivation rec {
  name = "mymathserver-custom2";
  src = ./.;
  buildPhase = "";
  installPhase = ''
    mkdir -p $out/bin
    cp ${mymathserver}/bin/mymathserver $out/bin/${name}
  '';
}
```

Nix installe chaque paquet dans un dossier spécifique ce qui signifie qu'on
peut utiliser en même temps plusieurs versions ou plusieurs configurations d'un
même logiciel. Par exemple, dans les trois fichiers de personnalisation
précédents, on a renommé l'exécutable `mymathserver` produit. On peut donc les
installer et les exécuter en même temps dans l'environnement utilisateur
courant :

```sh
nix-env -i -f nix/custom0.nix
nix-env -i -f nix/custom1.nix
nix-env -i -f nix/custom2.nix
mymathserver-custom0
...
```


# Conclusion

L'approche fonctionnelle de Nix constitue une base solide pour la gestion de
paquet. Elle permet de construire des paquets de façon fiable, reproductible et
personnalisable. Sur cette base, Nix propose également des fonctionnalités
intéressantes pour le développeur, l'empaqueteur et l'administrateur système.

Ainsi, en 120 lignes de code Nix, le projet d'exemple `mymathserver` :

- package une dépendance manquante (`nix/cpprestsdk.nix`)
- package le projet de base (`nix/mymathserver.nix`)
- définit un point d'entrée pour créer des paquets et des environnements
  virtuels (`default.nix`)
- définit une release (`nix/release.nix`)
- définit une image Docker (`nix/docker.nix`)
- définit une machine virtuelle (`nix/virtualbox.nix`)
- définit trois personnalisations différentes de l'environnement logiciel
  (`nix/custom0.nix`, etc)
- permet de réaliser de l'intégration continue, d'utiliser un cache binaire,
  d'installer le projet depuis une archive gitlab, etc.

```sh
mymathserver/
├── CMakeLists.txt
├── default.nix
├── nix
│   ├── cpprestsdk.nix
│   ├── custom0.nix
│   ├── custom1.nix
│   ├── custom2.nix
│   ├── docker.nix
│   ├── mymathserver.nix
│   ├── release.nix
│   └── virtualbox.nix
└── src
    ├── mymath.hpp
    ├── mymathserver.cpp
    └── mymathtest.cpp
```

