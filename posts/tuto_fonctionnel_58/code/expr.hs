import Data.Char
import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer

-------------------------------------------------------------------------------
-- DSL
-------------------------------------------------------------------------------

data Op
    = OpAdd
    | OpMul
    deriving (Show)

data Expr 
    = ExprVal Int
    | ExprOp Op Expr Expr
    deriving (Show)

eval :: Expr -> Int
eval (ExprVal x) = x
eval (ExprOp OpAdd e1 e2) = eval e1 + eval e2
eval (ExprOp OpMul e1 e2) = eval e1 * eval e2

-------------------------------------------------------------------------------
-- Parser
-------------------------------------------------------------------------------

type Parser = Parsec Void String

valP :: Parser Expr
valP = ExprVal <$> decimal

mulP :: Parser Expr
mulP = try (ExprOp OpMul <$> valP <*> (string "*" *> mulP)) <|> valP

addP :: Parser Expr
addP = try (ExprOp OpAdd <$> mulP <*> (string "+" *> addP)) <|> mulP

parseExpr :: String -> Maybe Expr
parseExpr = parseMaybe addP

-------------------------------------------------------------------------------
-- Parser
-------------------------------------------------------------------------------

main :: IO ()
main = do

    let expr1 = ExprOp OpMul (ExprVal 2) (ExprVal 21)
    print expr1
    print $ eval expr1

    let (Just expr2) = parseExpr "2*21"
    print expr2
    print $ eval expr2

