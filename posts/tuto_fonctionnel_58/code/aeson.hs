{-# LANGUAGE DeriveGeneric #-}

import Data.Aeson
import Data.ByteString.Lazy.Char8
import GHC.Generics

data Person = Person 
    { name :: String
    , age  :: Int
    } deriving (Generic, Show)

instance FromJSON Person
instance ToJSON Person

main :: IO ()
main = do
    let person1 = Person "John Doe" 42
    print person1
    print (toJSON person1 :: Value)
    print (encode person1 :: ByteString)

