{-# LANGUAGE OverloadedStrings #-}

import Data.Text
import Lucid

myblock1 :: Html ()
myblock1 = p_ "this is myblock1"

myblock2 :: Text -> Html ()
myblock2 x = p_ ("this is myblock2, using " <> toHtml x)

mypage :: Html ()
mypage = do
    html_ $ do
        head_ $ title_ "mypage"
        body_ $ do
            h1_ "this is mypage"
            p_ "this is a paragraph"
            myblock1
            myblock2 "foobar"

main :: IO ()
main = renderToFile "lucid.html" mypage

