module Main where

import Control.Monad.Aff (Aff)
import Control.Monad.Eff (Eff)
import Data.Argonaut ((.?), class DecodeJson, decodeJson, Json)
import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
import Data.Traversable (traverse)
import Halogen as H
import Halogen.Aff as HA
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Halogen.VDom.Driver (runUI)
import Network.HTTP.Affjax as AX
import Prelude

main :: Eff (HA.HalogenEffects (ajax :: AX.AJAX)) Unit
main = HA.runHalogenAff do
    body <- HA.awaitBody
    io <- runUI ui unit body
    io.query $ H.action $ QueryAnimals ""

ui :: forall eff. H.Component HH.HTML Query Unit Void (Aff (ajax :: AX.AJAX | eff))
ui = H.component
    { initialState: const initialState
    , render
    , eval
    , receiver: const Nothing
    }

-- Model

newtype Animal = Animal
  { animalType :: String
  , animalImage :: String
  } 

instance decodeJsonBlogPost :: DecodeJson Animal where
  decodeJson json = do
    obj <- decodeJson json
    animalType <- obj .? "animalType"
    animalImage <- obj .? "animalImage"
    pure $ Animal { animalType, animalImage }

decodeAnimalArray :: Json -> Either String (Array Animal)
decodeAnimalArray json = decodeJson json >>= traverse decodeJson

type Model = { modelAnimals :: Array Animal }

initialState :: Model
initialState = { modelAnimals: [] }

-- Controler

data Query a = QueryAnimals String a

eval :: forall eff. Query ~> H.ComponentDSL Model Query Void (Aff (ajax :: AX.AJAX | eff))
eval (QueryAnimals animal_type next) = do
    H.modify (_ { modelAnimals = [] })
    response <- H.liftAff $ AX.get ("http://localhost:3000/api/animals/" <> animal_type)
    let animals = case decodeAnimalArray response.response of
                    Left _ -> []
                    Right ra -> ra
    H.modify (_ { modelAnimals = animals })
    pure next

-- View

render :: Model -> H.ComponentHTML Query
render m =
    HH.span []
        [ HH.h1 [] [ HH.text "Animals (Purescript)" ]
        , HH.p [] [ HH.input [ HE.onValueInput (HE.input QueryAnimals) ] ]
        , HH.span [] 
            (map (\ (Animal {animalType, animalImage}) 
                  -> HH.div [] 
                        [ HH.p [] [ HH.text animalType ]
                        , HH.img [ HP.src ("http://localhost:3000/img/" <> animalImage)
                                 , HP.width 320
                                 , HP.height 240
                                 ]
                        ]
                 ) m.modelAnimals)
         ]

