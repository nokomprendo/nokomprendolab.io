# animals-purs

Ne pas oublier de lancer le serveur (animals-scotty).


## avec nix-build

```
nix-build
firefox result/index.html
```


## avec nix-shell

```
nix-shell
```

- la première fois :

```
bower install
npm run build
firefox index.html
```

- les fois suivantes :

```
npm run build
```


