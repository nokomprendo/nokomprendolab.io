{ pkgs ? import <nixpkgs> {} }:

let 

  rp_ref = "ea3c9a1536a987916502701fb6d319a880fdec96";  # 2018-04-17
  rp = import (fetchTarball "https://github.com/reflex-frp/reflex-platform/archive/${rp_ref}.tar.gz") {};

  app = rp.project ({ pkgs, ... }: {
    packages = {
      animals-reflex = ./animals-reflex;
    };
    shells = {
      ghcjs = ["animals-reflex"];
    };
  });
 
  animals_reflex_pkg = pkgs.stdenv.mkDerivation rec {
    name = "animals-reflex";
    src = ./.;
    buildInputs = [ app pkgs.closurecompiler ];
    appdir = "${app.ghcjs.animals-reflex}/bin/animals-reflex.jsexe";
    buildPhase = ''
      ${pkgs.closurecompiler}/bin/closure-compiler ${appdir}/all.js --compilation_level=ADVANCED_OPTIMIZATIONS --jscomp_off=checkVars --externs=${appdir}/all.js.externs > all.js
    '';
    installPhase = ''
      mkdir -p $out
      cp index.html all.js $out/
    '';
  };

in

if pkgs.lib.inNixShell then app.shells.ghcjs else animals_reflex_pkg

