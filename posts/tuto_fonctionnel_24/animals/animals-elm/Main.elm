module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (height, href, src, width)
import Html.Events exposing (onClick, onInput)
import Http
import Json.Decode as JD

main : Program Never Model Msg
main = Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }

-- Model

type alias Animal = 
  { animalType : String 
  , animalImage : String
  }

decodeAnimal : JD.Decoder Animal
decodeAnimal = JD.map2 Animal
        (JD.field "animalType" JD.string)
        (JD.field "animalImage" JD.string)

decodeAnimalList : JD.Decoder (List Animal)
decodeAnimalList = JD.list decodeAnimal

type alias Model = { modelAnimals : List Animal }

init : ( Model, Cmd Msg )
init = ( Model [], queryAnimals "" )

-- Controler

subscriptions : Model -> Sub Msg
subscriptions _ = Sub.none

type Msg 
  = MsgInput String 
  | MsgAnimals (Result Http.Error (List Animal))

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        MsgInput animalType -> ( model, queryAnimals animalType )
        MsgAnimals (Ok animals)  -> ( Model animals, Cmd.none )
        MsgAnimals (Err _) -> ( Model [], Cmd.none )

queryAnimals : String -> Cmd Msg
queryAnimals txt =
    let url = "http://localhost:3000/api/animals/" ++ txt
    in Http.send MsgAnimals (Http.get url decodeAnimalList)

-- View

view model =
    span [] [ h1 [] [ text "Animals (Elm)" ]
            , p [] [ input [ onInput MsgInput ] [] ]
            , span []
                (List.map
                    (\a -> div [] [ p [] [ text a.animalType ]
                                  , img
                                    [ src ("http://localhost:3000/img/" ++ a.animalImage)
                                    , height 240
                                    , width 320
                                    ]
                                    []
                                  ]
                    ) model.modelAnimals
                )
            ]
