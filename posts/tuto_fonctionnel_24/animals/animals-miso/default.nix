{ pkgs ?  import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/17.09.tar.gz") { } }:

let

  _miso = pkgs.haskell.packages.ghcjs.callCabal2nix "miso" (pkgs.fetchFromGitHub {
    rev = "0.21.1.0";
    sha256 = "19x9ym4399i6ygs0hs9clgrvni0vijfg4ff3jfxgfqgjihbn0w4r";
    owner = "haskell-miso";
    repo = "miso";
  }) {};

  app = pkgs.haskell.packages.ghcjs.callCabal2nix "animals-miso" ./. {
    miso = _miso; 
  };

  inherit (pkgs) closurecompiler;

  pkg = pkgs.runCommand "animals-miso" { inherit app; } ''
    mkdir -p $out
    ls ${app.src}
    ls ${app}
    cp ${app.src}/index.html $out/
    cd ${app}/bin/animals-miso.jsexe
    ${closurecompiler}/bin/closure-compiler all.js > $out/all.js
  '';

in 

if pkgs.lib.inNixShell then app.env else pkg

