# animals-miso

Ne pas oublier de lancer le serveur (animals-scotty).


## avec nix-build

```
nix-build
firefox result/index.html
```


## avec nix-shell


```
nix-shell
```

- la première fois :

```
cabal new-build 
ln -s dist-newstyle/build/x86_64-linux/ghcjs-0.2.0/animals-miso-0.1.0.0/c/animals-miso/build/animals-miso/animals-miso.jsexe/all.js .
firefox index.html
```

- les fois suivantes :

```
cabal new-build 
```

