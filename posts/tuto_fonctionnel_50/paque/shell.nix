let

  discord-haskell-src = fetchTarball {
    url = https://github.com/aquarial/discord-haskell/archive/8e1988e.tar.gz;
    sha256 = sha256:1m074p7xi016qg6wv08zqzz4jhywgm65k1amk2hqji0xm1wr1d76;
  };

  config = {
    allowBroken = true;
    packageOverrides = pkgs: rec {
      haskellPackages = pkgs.haskellPackages.override {
        overrides = self: super: rec {
          discord-haskell = self.callCabal2nix "discord-haskell" discord-haskell-src {};
        };
      };
    };
  };

  pkgs = import <nixpkgs> { inherit config; };

in with pkgs; mkShell {
  buildInputs = [
    (haskellPackages.ghcWithHoogle (ps: with ps; [
      discord-haskell
    ]))
  ];

  shellHook = ''
    export PATH="$PATH:${pkgs.haskellPackages.hoogle}/bin"
  '';
}

