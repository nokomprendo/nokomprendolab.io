import Control.Monad (when, void, unless)
import Data.List (isPrefixOf)
import Data.Text (pack, unpack)
import System.Process (readProcess)
import UnliftIO (liftIO)

import qualified Discord as D
import qualified Discord.Types as D
import qualified Discord.Requests as D

main :: IO ()
main = do
    putStrLn "running..."
    token <- pack <$> readFile "token.txt"
    let opts = D.def { D.discordToken = token, D.discordOnEvent = eventHandler }
    D.runDiscord opts >>= print

eventHandler :: D.Event -> D.DiscordHandler ()
eventHandler (D.MessageCreate m) = do
    let str = unpack $ D.messageText m
        hoogleReq = checkHoogle str
        chan = D.messageChannel m
    when (hoogleReq /= "") $ do
        hoogleRes <- liftIO $ readProcess "hoogle" ["--count=5", hoogleReq] ""
        let txt = pack $ "```hs\n" <> hoogleRes <> "\n```"
        void $ D.restCall (D.CreateMessage chan txt)
eventHandler _ = pure ()

checkHoogle :: String -> String
checkHoogle str = if "hoogle " `isPrefixOf ` str then drop 7 str else ""

