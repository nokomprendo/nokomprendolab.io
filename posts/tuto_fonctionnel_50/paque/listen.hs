import Control.Monad (when, void, unless)
import Data.List (isPrefixOf)
import Data.Text (pack, unpack)
import System.Process (readProcess)
import UnliftIO (liftIO)

import qualified Discord as D
import qualified Discord.Types as D
import qualified Discord.Requests as D

main :: IO ()
main = do
    putStrLn "running..."
    token <- pack <$> readFile "token.txt"
    let opts = D.def { D.discordToken = token, D.discordOnEvent = eventHandler }
    D.runDiscord opts >>= print

eventHandler :: D.Event -> D.DiscordHandler ()
eventHandler (D.MessageCreate m) = liftIO $ print m
eventHandler _ = pure ()

