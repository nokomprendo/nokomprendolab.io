
data List a 
    = Nil
    | Cons a (List a)

instance Show a => Show (List a) where
    -- show :: List a -> String 
    show Nil = "Nil"
    show (Cons x Nil) = "Cons " ++ show x ++ " Nil"
    show (Cons x xs) = "Cons " ++ show x ++ " (" ++ show xs ++ ")"

main :: IO ()
main = do
    print (Nil :: List String)
    print (Cons 1 (Cons 2 Nil) :: List Int)

