
data Figure
    = Rectangle Double Double
    | Disque Double

surface :: Figure -> Double
surface (Rectangle l h) = l * h
surface (Disque r) = pi * r**2

printSurface :: Figure -> IO ()
printSurface = print . surface

main :: IO ()
main = do
    printSurface (Rectangle 2 21)
    printSurface (Disque 3.66)

