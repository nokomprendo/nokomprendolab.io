
data List
    = Nil
    | Cons Int List

instance Show List where
    -- show :: List -> String 
    show Nil = "Nil"
    show (Cons x Nil) = "Cons " ++ show x ++ " Nil"
    show (Cons x xs) = "Cons " ++ show x ++ " (" ++ show xs ++ ")"

main :: IO ()
main = do
    print Nil
    print (Cons 1 (Cons 2 Nil))

