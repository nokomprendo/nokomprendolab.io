
class HasSurface a where
    surface :: a -> Double

data Rectangle = Rectangle Double Double

instance HasSurface Rectangle where
    surface (Rectangle l h) = l * h

newtype Disque = Disque Double

instance HasSurface Disque where
    surface (Disque r) = pi * r**2

printSurface :: HasSurface a => a -> IO ()
printSurface = print . surface

main :: IO ()
main = do
    printSurface (Rectangle 2 21)
    printSurface (Disque 3.66)

