
data List a
    = Nil
    | Cons a (List a)
    deriving Show

instance Functor List where
    -- fmap :: (a -> b) -> List a -> List b
    fmap _ Nil = Nil
    fmap f (Cons x xs) = Cons (f x) (fmap f xs)

main :: IO ()
main = do
    print (Nil :: List String)
    print $ fmap (*2) (Cons 1 (Cons 2 Nil) :: List Int)
    print $ fmap (*2) [1,2::Int]


