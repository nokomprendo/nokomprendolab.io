
data List a
    = Nil
    | Cons a (List a)
    deriving Show

main :: IO ()
main = do
    print (Nil :: List String)
    print (Cons 1 (Cons 2 Nil) :: List Int)

