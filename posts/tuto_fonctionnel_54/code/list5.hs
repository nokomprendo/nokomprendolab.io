{-# LANGUAGE DeriveFunctor #-}

data List a
    = Nil
    | Cons a (List a)
    deriving (Functor, Show)

main :: IO ()
main = do
    print (Nil :: List String)
    print $ fmap (*2) (Cons 1 (Cons 2 Nil) :: List Int)
    print $ fmap (*2) [1,2::Int]

