{-# LANGUAGE DeriveFunctor #-}

data Tree a
    = Leaf
    | Node a (Tree a) (Tree a)
    deriving (Functor, Show)

main :: IO ()
main = do
    let t1 = Node (1::Int)
                (Node 2 Leaf Leaf)
                (Node 3 Leaf Leaf)
    print t1
    print $ fmap (*2) t1

