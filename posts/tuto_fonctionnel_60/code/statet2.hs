import Control.Monad.Trans.Class (lift, MonadTrans)

newtype StateT s m a = StateT { runStateT :: s -> m (a, s) }

get :: Monad m => StateT s m s
get = StateT $ \s -> return (s, s)

put :: Monad m => s -> StateT s m ()
put s = StateT $ \_ -> return ((), s)

instance Monad m => Functor (StateT s m) where
    -- fmap :: (a -> b) -> StateT s m a -> StateT s m b
    fmap f (StateT act) = StateT $ \s -> do
        (a1, s1) <- act s
        return (f a1, s1)

instance Monad m => Applicative (StateT s m) where
    -- pure :: a -> StateT s a
    pure a = StateT $ \s -> return (a, s)
    -- (<*>) :: StateT s m (a -> b) -> StateT s m a -> StateT s m b
    StateT act1 <*> StateT act2 = StateT $ \s -> do
        (a1, s1) <- act1 s
        (a2, s2) <- act2 s1
        return (a1 a2, s2)

instance Monad m => Monad (StateT s m) where
    -- (>>=) :: StateT s m a -> (a -> StateT s m b) -> StateT s m b
    StateT act >>= k = StateT $ \s -> do
        (a1, s1) <- act s
        runStateT (k a1) s1

instance MonadTrans (StateT s) where
    -- lift :: m a -> StateT s m a
    lift c = StateT $ \s -> do
        x <- c
        return (x, s)

count42 :: Monad m => StateT [Int] m Bool
count42 = do
    s0 <- get
    case s0 of
        [] -> return False
        (n:ns) -> put ns >> return (n==42)

rep3Count42 :: StateT [Int] IO (Bool, Bool, Bool)
rep3Count42 = do
    lift $ putStrLn "inside rep3Count42"
    (,,) <$> count42 <*> count42 <*> count42

main :: IO ()
main = do
    runStateT count42 [42, 13] >>= print
    runStateT rep3Count42 [42, 13] >>= print

