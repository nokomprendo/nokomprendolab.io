{-# LANGUAGE FlexibleContexts #-}
import Control.Monad.IO.Class (liftIO, MonadIO)
import Control.Monad.State.Class (MonadState)
import Control.Monad.State.Lazy (get, put, runStateT)

count42 :: (MonadState [Int] m) => m Bool
count42 = do
    s0 <- get
    case s0 of
        [] -> return False
        (n:ns) -> put ns >> return (n==42)

rep3Count42 :: (MonadState [Int] m, MonadIO m) => m (Bool, Bool, Bool)
rep3Count42 = do
    liftIO $ putStrLn "inside rep3Count42"
    (,,) <$> count42 <*> count42 <*> count42

main :: IO ()
main = do
    runStateT count42 [42, 13] >>= print
    runStateT rep3Count42 [42, 13] >>= print

