
count42 :: [Int] -> (Bool, [Int])
count42 s0 = 
    case s0 of
        [] -> (False, [])
        (n:ns) -> (n==42, ns)

rep3Count42 :: [Int] -> ((Bool, Bool, Bool), [Int])
rep3Count42 s0 = 
    let (v1, s1) = count42 s0
        (v2, s2) = count42 s1
        (v3, s3) = count42 s2
    in ((v1,v2,v3), s3)

main :: IO ()
main = do
    print $ count42 [42, 13]
    print $ rep3Count42 [13, 42]

