
newtype State s a = State { runState :: s -> (a, s) }

get :: State s s
get = State $ \s -> (s, s)

put :: s -> State s ()
put s = State $ \_ -> ((), s)

instance Functor (State s) where
    -- fmap :: (a -> b) -> State s a -> State s b
    fmap f (State act) = State $ \s ->
        let (a1, s1) = act s
        in (f a1, s1)

instance Applicative (State s) where
    -- pure :: a -> State s a
    pure a = State $ \s -> (a, s)
    -- (<*>) :: State s (a -> b) -> State s a -> State s b
    State act1 <*> State act2 = State $ \s ->
        let (a1, s1) = act1 s
            (a2, s2) = act2 s1
        in (a1 a2, s2)

instance Monad (State s) where
    -- (>>=) :: State s a -> (a -> State s b) -> State s b
    State act >>= k = State $ \s ->
        let (a1, s1) = act s
        in runState (k a1) s1

count42 :: State [Int] Bool
count42 = do
    s0 <- get
    case s0 of
        [] -> return False
        (n:ns) -> put ns >> return (n==42)

rep3Count42 :: State [Int] (Bool, Bool, Bool)
rep3Count42 = (,,) <$> count42 <*> count42 <*> count42

main :: IO ()
main = do
    print $ runState count42 [42, 13]
    print $ runState rep3Count42 [13, 42]

