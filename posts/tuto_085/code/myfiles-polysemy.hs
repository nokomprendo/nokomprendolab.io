{-# LANGUAGE BlockArguments, DataKinds, FlexibleContexts, GADTs, LambdaCase,
  PolyKinds, ScopedTypeVariables, TemplateHaskell, TypeOperators #-}

import Polysemy
import System.Directory

-------------------------------------------------------------------------------
-- models
-------------------------------------------------------------------------------

data Log m a where
  LogPut :: String -> Log m ()
makeSem ''Log

data Files m a where
  FilesLs :: Files m [FilePath]
  FilesMkdir :: FilePath -> Files m ()
  FilesRmdir :: FilePath -> Files m ()
makeSem ''Files

-------------------------------------------------------------------------------
-- interpreters
-------------------------------------------------------------------------------

runLogQuiet :: Sem (Log ': r) a -> Sem r a
runLogQuiet = interpret \case
  LogPut _str -> return ()

runLogVerbose :: Member (Embed IO) r => Sem (Log ': r) a -> Sem r a
runLogVerbose = interpret \case
  LogPut str -> embed (putStrLn ("[Log] " ++ str))

runFilesFs :: Members '[Log, Embed IO] r => Sem (Files ': r) a -> Sem r a
runFilesFs = interpret \case
  FilesLs -> do
    logPut "running filesLs"
    embed $ listDirectory "."
  FilesMkdir fp -> do
    logPut "running filesMkdir"
    embed $ createDirectory fp
  FilesRmdir fp -> do
    logPut "running filesRmdir"
    embed $ removeDirectory fp

runFilesMock :: Member Log r => Sem (Files ': r) a -> Sem r a
runFilesMock = interpret \case
  FilesLs -> do
    logPut "mock filesLs"
    return ["mock1", "mock2"]
  FilesMkdir fp -> 
    logPut $ "mock filesMkdir " <> fp
  FilesRmdir fp ->
    logPut $ "mock filesRmdir " <> fp

-------------------------------------------------------------------------------
-- apps
-------------------------------------------------------------------------------

app1 :: Member Log r => Sem r ()
app1 = do
  logPut "hello"
  logPut "world"

app2 :: Members '[Log, Files, Embed IO] r => Sem r ()
app2 = do
  filesMkdir "output1"
  files <- filesLs
  filesRmdir "output1"
  logPut $ show files
  embed $ print files

-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

main :: IO ()
main = do
  putStrLn "\napp1 verbose"
  runM . runLogVerbose $ app1

  putStrLn "\napp2 quiet mock"
  runM . runLogQuiet . runFilesMock $ app2

  putStrLn "\napp2 verbose fs"
  runM . runLogVerbose . runFilesFs $ app2

