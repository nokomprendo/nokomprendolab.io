{-# LANGUAGE GeneralizedNewtypeDeriving #-}

import Control.Monad.IO.Class
import System.Directory

-------------------------------------------------------------------------------
-- models
-------------------------------------------------------------------------------

class Monad m => MonadLog m where
  logPut :: String -> m ()

class Monad m => MonadFiles m where
  filesLs :: m [FilePath]
  filesMkdir :: FilePath -> m ()
  filesRmdir :: FilePath -> m ()

-------------------------------------------------------------------------------
-- interpreters
-------------------------------------------------------------------------------

newtype LogQuiet m a = LogQuiet { runLogQuiet :: m a }
  deriving (Functor, Applicative, Monad)

instance Monad m => MonadLog (LogQuiet m) where
  logPut _ = return ()

newtype LogVerbose m a = LogVerbose { runLogVerbose :: m a }
  deriving (Functor, Applicative, Monad, MonadIO)

instance MonadIO m => MonadLog (LogVerbose m) where
  logPut str = liftIO (putStrLn ("[Log] " ++ str))

newtype FilesMock m a = FilesMock { runFilesMock :: m a }
  deriving (Functor, Applicative, Monad, MonadLog, MonadIO)

instance MonadLog m => MonadFiles (FilesMock m) where
  filesLs = do
    logPut "mock filesLs"
    return ["mock1", "mock2"]
  filesMkdir fp = 
    logPut $ "mock filesMkdir " <> fp
  filesRmdir fp = 
    logPut $ "mock filesRmdir " <> fp

newtype FilesFs m a = FilesFs { runFilesFs :: m a }
  deriving (Functor, Applicative, Monad, MonadLog, MonadIO)

instance (MonadLog m, MonadIO m) => MonadFiles (FilesFs m) where
  filesLs = do
    logPut "running filesLs"
    liftIO $ listDirectory "."
  filesMkdir fp =  do
    logPut "running filesMkdir"
    liftIO $ createDirectory fp
  filesRmdir fp =  do
    logPut "running filesRmdir"
    liftIO $ removeDirectory fp

-------------------------------------------------------------------------------
-- apps
-------------------------------------------------------------------------------

app1 :: (MonadLog m) => m ()
app1 = do
  logPut "hello"
  logPut "world"

app2 :: (MonadLog m,  MonadFiles m, MonadIO m) => m ()
app2 = do
  filesMkdir "output1"
  files <- filesLs
  filesRmdir "output1"
  logPut $ show files
  liftIO $ print files

-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

main :: IO ()
main = do
  putStrLn "\napp1 verbose"
  runLogVerbose app1

  putStrLn "\napp2 quiet mock"
  runLogQuiet $ runFilesMock app2

  putStrLn "\napp2 verbose fs"
  runLogVerbose $ runFilesFs app2

