{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE DeriveFunctor #-}

import Control.Monad.Trans.Class as T
import Control.Monad.Trans.Free
import System.Directory

-------------------------------------------------------------------------------
-- models
-------------------------------------------------------------------------------

data LogF a
  = LogPut String a
  deriving (Functor)

type Log = Free LogF

logPut :: String -> Log ()
logPut msg = liftF $ LogPut msg ()

data FilesF a
  = FilesLs ([FilePath] -> a)
  | FilesMkdir FilePath a
  | FilesRmdir FilePath a
  deriving (Functor)

type Files = Free FilesF

filesLs :: Files [FilePath]
filesLs = liftF $ FilesLs id

filesMkdir :: FilePath -> Files ()
filesMkdir fp = liftF $ FilesMkdir fp ()

filesRmdir :: FilePath -> Files ()
filesRmdir fp = liftF $ FilesRmdir fp ()

-------------------------------------------------------------------------------
-- interpreters 
-------------------------------------------------------------------------------

runLogVerbose :: Log a -> IO a
runLogVerbose program = case runFree program of
  Pure r -> return r
  Free (LogPut str next) -> do
    putStrLn ("[Log] " ++ str)
    runLogVerbose next

runLogQuiet :: Log a -> IO a
runLogQuiet program = case runFree program of
  Pure r -> return r
  Free (LogPut _str next) -> runLogQuiet next

runFilesMock :: Files a -> IO a
runFilesMock program = case runFree program of
  Pure r -> return r
  Free (FilesLs next) -> do
    putStrLn "mock filesLs"           -- TODO we want LogPut instead of putStrnLn
    runFilesMock $ next ["mock1", "mock2"]
  Free (FilesMkdir fp next) -> do
    putStrLn $ "mock filesMkdir " <> fp
    runFilesMock next
  Free (FilesRmdir fp next) -> do
    putStrLn $ "mock filesRmdir " <> fp
    runFilesMock next

runFilesFs :: Files a -> IO a
runFilesFs program = case runFree program of
  Pure r -> return r
  Free (FilesLs next) -> do
    putStrLn "running filesLs"
    files <- listDirectory "."
    runFilesFs $ next files
  Free (FilesMkdir fp next) -> do
    putStrLn "running filesMkdir"
    createDirectory fp
    runFilesFs next
  Free (FilesRmdir fp next) -> do
    putStrLn "running filesRmdir"
    removeDirectory fp
    runFilesFs next

-------------------------------------------------------------------------------
-- runT
-------------------------------------------------------------------------------

left :: (Functor f, Functor g) => Free f a -> FreeT (Free f) (Free g) a
left = liftF

right :: (Functor f, Functor g) => Free g a -> FreeT (Free f) (Free g) a
right = T.lift

runT runL runR program = do
  r <- runR $ runFreeT program
  case r of
    Pure x -> return x
    Free p -> runL p >>= runT runL runR

-------------------------------------------------------------------------------
-- apps
-------------------------------------------------------------------------------

app1 :: Log ()
app1 = do
  logPut "hello"
  logPut "world"

app2 :: FreeT Log Files ()
app2 = do
  right $ filesMkdir "output1"
  files <- right filesLs
  right $ filesRmdir "output1"
  left $ logPut $ show files

-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

main :: IO ()
main = do
  putStrLn "\napp1 verbose"
  runLogVerbose app1

  putStrLn "\napp2 quiet mock"
  runT runLogQuiet runFilesMock app2

  putStrLn "\napp2 verbose fs"
  runT runLogVerbose runFilesFs app2

