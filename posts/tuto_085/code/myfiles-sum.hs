{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE DeriveFunctor #-}

import Control.Monad.Free 
import Data.Functor.Sum
import System.Directory

-------------------------------------------------------------------------------
-- models
-------------------------------------------------------------------------------

data LogF a
  = LogPut String a
  deriving (Functor)

type Log = Free LogF

logPut :: String -> Log ()
logPut msg = liftF $ LogPut msg ()

data FilesF a
  = FilesLs ([FilePath] -> a)
  | FilesMkdir FilePath a
  | FilesRmdir FilePath a
  deriving (Functor)

type Files = Free FilesF

filesLs :: Files [FilePath]
filesLs = liftF $ FilesLs id

filesMkdir :: FilePath -> Files ()
filesMkdir fp = liftF $ FilesMkdir fp ()

filesRmdir :: FilePath -> Files ()
filesRmdir fp = liftF $ FilesRmdir fp ()

-------------------------------------------------------------------------------
-- interpreters 
-------------------------------------------------------------------------------

runLogVerbose :: LogF a -> IO a
runLogVerbose (LogPut str next) = do
  putStrLn ("[Log] " ++ str)
  return next

runLogQuiet :: LogF a -> IO a
runLogQuiet (LogPut _str next) = do
  return next

runFilesFs :: FilesF a -> IO a
runFilesFs (FilesLs next) = do
  putStrLn "running filesLs"       -- TODO we want LogPut instead of putStrnLn
  files <- listDirectory "."
  return $ next files
runFilesFs (FilesMkdir fp next) = do
  putStrLn "running filesMkdir"
  createDirectory fp
  return next
runFilesFs (FilesRmdir fp next) = do
  putStrLn "running filesRmdir"
  removeDirectory fp
  return next

runFilesMock :: FilesF a -> IO a
runFilesMock (FilesLs next) = do
  putStrLn "mock filesLs"           -- TODO we want LogPut instead of putStrnLn
  return $ next ["mock1", "mock2"]
runFilesMock (FilesMkdir fp next) = do
  putStrLn $ "mock filesMkdir " <> fp
  return next
runFilesMock (FilesRmdir fp next) = do
  putStrLn $ "mock filesRmdir " <> fp
  return next

-------------------------------------------------------------------------------
-- sum
-------------------------------------------------------------------------------

{-
data Sum f g a 
  = InL (f a) 
  | InR (g a) 
  deriving (Functor)
-}

type LogFiles = Free (Sum LogF FilesF)

left :: (Functor f, Functor g) => Free f a -> Free (Sum f g) a
left = hoistFree InL

right :: (Functor f, Functor g) => Free g a -> Free (Sum f g) a
right = hoistFree InR

runSum :: (f a -> t a) -> (g a -> t a) -> Sum f g a -> t a
runSum runL _ (InL x) = runL x
runSum _ runR (InR x) = runR x

-------------------------------------------------------------------------------
-- apps
-------------------------------------------------------------------------------

app1 :: Log ()
app1 = do
  logPut "hello"
  logPut "world"

app2 :: LogFiles ()
app2 = do
  right $ filesMkdir "output1"
  files <- right filesLs
  right $ filesRmdir "output1"
  left $ logPut $ show files

-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

main :: IO ()
main = do
  putStrLn "\napp1 verbose"
  foldFree runLogVerbose app1

  putStrLn "\napp2 quiet mock"
  foldFree (runSum runLogQuiet runFilesMock) app2

  putStrLn "\napp2 verbose fs"
  foldFree (runSum runLogVerbose runFilesFs) app2

