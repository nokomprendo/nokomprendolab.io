{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE RankNTypes #-}

import qualified Prelude
import qualified System.Exit

import Prelude hiding (putStrLn, getLine)

import Data.Function ((&))
import Control.Monad.Freer
import Control.Monad.Freer.TH
import Control.Monad.Freer.Error
import Control.Monad.Freer.State
import Control.Monad.Freer.Writer

--------------------------------------------------------------------------------
                               -- Effect Model --
--------------------------------------------------------------------------------
data Console r where
  PutStrLn    :: String -> Console ()
  GetLine     :: Console String
makeEffect ''Console

--------------------------------------------------------------------------------
                          -- Effectful Interpreter --
--------------------------------------------------------------------------------
runConsole :: Eff '[Console, IO] a -> IO a
runConsole = runM . interpretM (\case
  PutStrLn msg -> Prelude.putStrLn msg
  GetLine -> Prelude.getLine) 

runConsoleM :: forall effs a. LastMember IO effs
            => Eff (Console ': effs) a -> Eff effs a
runConsoleM = interpretM $ \case
  PutStrLn msg -> Prelude.putStrLn msg
  GetLine -> Prelude.getLine

--------------------------------------------------------------------------------
                             -- Pure Interpreter --
--------------------------------------------------------------------------------
runConsolePure :: [String] -> Eff '[Console] w -> [String]
runConsolePure inputs req = snd . fst $
    run (runWriter (runState inputs (runError (reinterpret3 go req))))
  where
    go :: Console v -> Eff '[Error (), State [String], Writer [String]] v
    go (PutStrLn msg) = tell [msg]
    go GetLine = get >>= \case
      [] -> error "not enough lines"
      (x:xs) -> put xs >> pure x

runConsolePureM
  :: forall effs w . [String]
  -> Eff (Console ': effs) w
  -> Eff effs (Maybe w, [String], [String])
runConsolePureM inputs req = do
    ((x, inputs'), output) <- reinterpret3 go req
      & runError & runState inputs & runWriter
    pure (either (const Nothing) Just x, inputs', output)
  where
    go :: Console v
       -> Eff (Error () ': State [String] ': Writer [String] ': effs) v
    go (PutStrLn msg) = tell [msg]
    go GetLine = get >>= \case
      [] -> error "not enough lines"
      (x:xs) -> put xs >> pure x

app :: Eff '[Console, IO] ()
app = do
    putStrLn "Send something to capitalize..."
    l <- getLine
    putStrLn l

main :: IO ()
main = do
  -- print $ run $ runConsolePureM ["cat", "fish", ""] app
  -- print $ runConsolePure ["cat", "fish", ""] app
  runM $ runConsoleM app

