-- https://gist.github.com/ocharles/6b1b9440b3513a5e225e
-- mtl

{-# LANGUAGE GeneralizedNewtypeDeriving #-}

import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Class
import Prelude hiding (log)

--------------------------------------------------------------------------------
-- The API for cloud files.
--------------------------------------------------------------------------------

type Bytes = String
type Path = String

class Monad m => MonadCloud m where
  saveFile :: Path -> Bytes -> m ()
  listFiles :: Path -> m [Path]

--------------------------------------------------------------------------------
-- The API for logging.
--------------------------------------------------------------------------------

data Level
  = Debug
  | Info

class Monad m => MonadLog m where
  log :: Level -> String -> m ()

--------------------------------------------------------------------------------
-- The API for REST clients.
--------------------------------------------------------------------------------

class Monad m => MonadRest m where
  get :: Path -> m Bytes
  put :: Path -> Bytes -> m Bytes

--------------------------------------------------------------------------------
-- An implementation of logging to standard out.
--------------------------------------------------------------------------------

newtype LogStdout m a =
  LogStdout {runLogStdout :: m a}
  deriving (Functor, Applicative, Monad, MonadIO, MonadFail)

instance MonadIO m => MonadLog (LogStdout m) where
  log Info msg = liftIO (putStrLn ("[Info] " ++ msg))
  log Debug msg = liftIO (putStrLn ("[Debug] " ++ msg))

--------------------------------------------------------------------------------
-- An implementation of MonadCloud that uses a REST client.
--------------------------------------------------------------------------------

newtype CloudRest m a =
  CloudRest {runCloudRest :: m a}
  deriving (Functor, Applicative, Monad, MonadRest, MonadLog, MonadFail)

instance MonadRest m => MonadCloud (CloudRest m) where
  saveFile path bytes = do
    _ <- put ("/file/" ++ path) bytes
    return ()
  listFiles path = do
    _ <- get ("/files/" ++ path)
    return ["MockFile"]

--------------------------------------------------------------------------------
-- An implementation of MonadCloud that does not use a REST client.
--------------------------------------------------------------------------------

newtype CloudClient m a =
  CloudClient {runCloudClient :: m a}
  deriving (Functor, Applicative, Monad, MonadLog, MonadFail)

instance (MonadLog m) => MonadCloud (CloudClient m) where
  saveFile p _bytes = do
    log Debug ("Fake saving file: " ++ p)
  listFiles path = do
    log Debug ("Fake listing " ++ path)
    return []

--------------------------------------------------------------------------------
-- A (non-functional) REST client.
--------------------------------------------------------------------------------

newtype RestClient m a =
  RestClient {runRestClient :: m a}
  deriving (Functor, Applicative, Monad, MonadIO, MonadLog, MonadFail)

instance MonadIO m => MonadRest (RestClient m) where
  get path = do
    liftIO (putStrLn $ "I should GET " ++ path)
    return []
  put path bytes = do
    liftIO (putStrLn $ "I should PUT " ++ path ++ " " ++ bytes)
    return []

--------------------------------------------------------------------------------
-- An instrumenting implementation that adds logging to every call.
--------------------------------------------------------------------------------

newtype CloudLogT m a =
  CloudLogT {runCloudLogT :: m a}
  deriving (Functor, Applicative, Monad, MonadLog, MonadFail)

instance (MonadLog m, MonadCloud m) => MonadCloud (CloudLogT m) where
  saveFile p bytes = do
    log Debug ("Saving file: " ++ p)
    lift $ saveFile p bytes
  listFiles path = do
    log Debug ("Listing " ++ path)
    lift $ listFiles path

instance MonadTrans CloudLogT where
  lift = CloudLogT

--------------------------------------------------------------------------------
-- Our application only talks about MonadCloud and MonadLog.
--------------------------------------------------------------------------------

app1 :: (MonadCloud m, MonadLog m, MonadFail m) => m ()
app1 = do
  files <- listFiles "/home/test"
  log Info ("Found " ++ show files)
  -- saveFile files "output"

app2 :: (MonadCloud m, MonadFail m) => m ()
app2 = do
   void $ listFiles "/home/test"

app3 :: (MonadLog m) => m ()
app3 = do
   log Debug "test debug"
   log Info "test info"

--------------------------------------------------------------------------------
-- Running the application chooses to instrument with extra logging, use the
-- REST client and to send all logs to stdout.
--------------------------------------------------------------------------------

main :: IO ()
main = do
  putStrLn "\napp1 debug"
  runLogStdout $ runRestClient $ runCloudRest $ runCloudLogT app1
  putStrLn "\napp1"
  runLogStdout $ runRestClient $ runCloudRest app1
  putStrLn "\napp2"
  runRestClient $ runCloudRest app2
  putStrLn "\napp1 (no rest)"
  runLogStdout $ runCloudClient app1
  putStrLn "\napp3"
  runLogStdout app3

