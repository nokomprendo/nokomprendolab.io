-- https://blog.siraben.dev/2020/02/20/free-monads.html
-- Free + Sum functor

{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}

import Control.Monad.Free 
import Data.Functor.Sum 
-- import Data.Function

newtype MyTermF a = MyImpure a deriving (Functor)

type MyTerm = Free MyTermF

data Incr t = Incr Int t deriving (Functor)
newtype Reader t = Reader (Int -> t) deriving (Functor)
newtype Mem = Mem Int deriving (Show)

efRunCalc :: Functor f => Mem -> Free (Sum Incr (Sum Reader f)) a -> Free f (a, Mem)
efRunCalc s (Pure x) = return (x, s)
efRunCalc (Mem s) (Free (InL (Incr k r))) = efRunCalc (Mem (s + k)) r
efRunCalc (Mem s) (Free (InR (InL (Reader r)))) = efRunCalc (Mem s) (r s)
efRunCalc s (Free (InR (InR t))) = Free (efRunCalc s <$> t)

class (Functor sub, Functor sup) => Inject sub sup where
  inj :: sub a -> sup a
instance Functor f => Inject f f where
  inj = id
instance (Functor f, Functor g) => Inject f (Sum f g) where
  inj = InL
instance  {-# OVERLAPS #-} (Functor f, Functor g, Functor h, Inject f g) => Inject f (Sum h g) where
  inj = InR . inj

inject :: (Inject g f) => g (Free f a) -> Free f a
inject = Free . inj

incr :: (Inject Incr f) => Int -> Free f ()
incr i = inject (Incr i (Pure ()))

recall :: (Inject Reader f) => Free f Int
recall = inject (Reader Pure)

efRun :: MyTerm p -> p
efRun (Pure a) = a
efRun _ = error "efRun Impure"

tick :: (Inject Incr f, Inject Reader f) => Free f Int
tick = do
  y <- recall
  incr 1
  return y

main :: IO ()
main = do
  print $ efRun $ efRunCalc (Mem 4) tick
  print $ efRun $ efRunCalc (Mem 4) $ do
    incr 30
    y <- recall
    incr 8
    return y

