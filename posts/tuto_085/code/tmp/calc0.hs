-- https://blog.siraben.dev/2020/02/20/free-monads.html
-- free monad + inject (functor)

{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}

data Term f a = Pure a
              | Impure (f (Term f a))

instance Functor f => Functor (Term f) where
  fmap f (Pure x) = Pure $ f x
  fmap f (Impure t) = Impure $ fmap (fmap f) t

instance Functor f => Applicative (Term f) where
  pure = Pure
  Pure f <*> Pure x = Pure $ f x
  Pure f <*> Impure b = Impure $ fmap (fmap f) b
  Impure fx <*> a = Impure $ fmap (<*> a) fx

instance Functor f => Monad (Term f) where
  return = pure
  Pure x >>= f = f x
  Impure t >>= f = Impure $ fmap (>>= f) t

data Incr t = Incr Int t deriving (Functor)
newtype Reader t = Reader (Int -> t) deriving (Functor)
newtype Mem = Mem Int deriving (Show)

efRunCalc :: Functor r => Mem
                       -> Term (Incr :+: (Reader :+: r)) a
                       -> Term r (a, Mem)
efRunCalc s (Pure x) = return (x, s)
efRunCalc (Mem s) (Impure (Inl (Incr k r))) = efRunCalc (Mem (s + k)) r
efRunCalc (Mem s) (Impure (Inr (Inl (Reader r)))) = efRunCalc (Mem s) (r s)
efRunCalc s (Impure (Inr (Inr t))) = Impure (efRunCalc s <$> t)

inject :: (g :<: f) => g (Term f a) -> Term f a
inject = Impure . inj

incr :: (Incr :<: f) => Int -> Term f ()
incr i = inject (Incr i (Pure ()))

recall :: (Reader :<: f) => Term f Int
recall = inject (Reader Pure)

infixr :+:

data (f :+: g) r 
  = Inl (f r)
  | Inr (g r)
  deriving (Functor)

class (Functor sub, Functor sup) => sub :<: sup where
  inj :: sub a -> sup a
instance Functor f => f :<: f where
  inj = id
instance (Functor f, Functor g) => f :<: (f :+: g) where
  inj = Inl
instance  {-# OVERLAPS #-} (Functor f, Functor g, Functor h, f :<: g) => f :<: (h :+: g) where
  inj = Inr . inj

data Void t
  deriving (Functor)

efRun :: Term Void a -> a
efRun (Pure a) = a
efRun _ = error "efRun Impure"

tick :: (Incr :<: f, Reader :<: f) => Term f Int
tick = do
  y <- recall
  incr 1
  return y

main :: IO ()
main = do
  print $ efRun $ efRunCalc (Mem 4) tick
  print $ efRun $ efRunCalc (Mem 4) $ do
    incr 30
    y <- recall
    incr 8
    return y

