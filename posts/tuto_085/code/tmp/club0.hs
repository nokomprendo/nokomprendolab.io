-- https://gist.github.com/talyssonoc/b981c0a0e5c9db00bb79c959de74a9b6
-- https://www.reddit.com/r/haskell/comments/lctctr/using_free_monads_after_creating_an_interpreter/
-- Free + Sum (type operators)

{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeOperators #-}

module Main where

import Control.Monad.Free (Free (Free, Pure), foldFree, liftF)
import Data.Functor.Sum (Sum (InL, InR))
import Data.List (intercalate, nub)

main :: IO ()
main = do
  putStrLn "Hello, Haskell!"
  clubIO showClubSiblings

{- Base -}

infixr 0 ~>

type f ~> g = forall x. f x -> g x

freeM :: (Functor f, Functor g) => f ~> g -> Free f ~> Free g
freeM _phi (Pure x) = Pure x
freeM phi (Free fx) = Free $ phi (freeM phi <$> fx)

left :: (Functor f, Functor g) => Free f ~> Free (Sum f g)
left = freeM InL

right :: (Functor f, Functor g) => Free g ~> Free (Sum f g)
right = freeM InR

sumNat :: (f ~> t) -> (g ~> t) -> Sum f g ~> t
sumNat phi _ (InL x) = phi x
sumNat _ psi (InR x) = psi x

-- | Key value store functionality.
data KeyValF a
  = GetKey String (Maybe String -> a)
  | PutKey String String a
  deriving (Functor)

-- | Console functionality.
data ConsoleF a
  = Write String a
  | Read (String -> a)
  deriving (Functor)

-- | Free
type Console = Free ConsoleF

type KeyVal = Free KeyValF

getKey :: String -> KeyVal (Maybe String)
getKey k = liftF (GetKey k id)

write :: String -> Console ()
write s = liftF (Write s ())

read :: Console String
read = liftF (Read id)

-- | Business logic
data ClubF a
  = GetClubMembers String (Maybe [String] -> a)
  | GetMemberClubs String (Maybe [String] -> a)
  | GetInput (String -> a)
  | Display String a
  deriving (Functor)

type Club = Free ClubF

getClubMembers :: String -> Club (Maybe [String])
getClubMembers c = liftF (GetClubMembers c id)

getMemberClubs :: String -> Club (Maybe [String])
getMemberClubs m = liftF (GetMemberClubs m id)

getInput :: Club String
getInput = liftF (GetInput id)

display :: String -> Club ()
display s = liftF (Display s ())

showClubSiblings :: Club ()
showClubSiblings = do
  display "Enter the club id"
  clubId <- getInput
  mmembers <- getClubMembers clubId
  case mmembers of
    Nothing -> display "Sorry, that club does not exist!"
    Just members -> do
      r <- sequence <$> traverse getMemberClubs members
      case r of
        Nothing -> display "Error getting club members."
        Just clubIdGroups -> do
          let siblings = nub $ concat clubIdGroups
          display $ "Here are the siblings of the club" ++ clubId ++ ":"
          display (intercalate ", " siblings)

-- Console in IO:
consoleIO :: ConsoleF ~> IO
consoleIO (Write s v) = do
  putStrLn s
  pure v
consoleIO (Read cb) = do
  s <- getLine
  pure (cb s)

-- KeyValue in IO via logging.
keyValIO :: KeyValF ~> IO
keyValIO (GetKey k cb) = do
  let r = Just ("value for key " ++ k)
  pure (cb r)
keyValIO (PutKey k v n) = do
  putStrLn $ "writing value " ++ v ++ " into key " ++ k
  pure n

clubI :: ClubF ~> Free (Sum ConsoleF KeyValF)
clubI (GetClubMembers clubId next) = do
  r <- right $ getKey ("clubs." ++ clubId ++ ".members")
  pure $ next (words <$> r)
clubI (GetMemberClubs memberId next) = do
  r <- right $ getKey ("users." ++ memberId ++ ".clubs")
  pure $ next (words <$> r)
clubI (GetInput next) = do
  r <- left Main.read
  pure $ next r
clubI (Display o next) = do
  left $ Main.write o
  pure next

freeSumIO :: Free (Sum ConsoleF KeyValF) ~> IO
freeSumIO = foldFree (sumNat consoleIO keyValIO)

clubSum :: Free ClubF ~> Free (Sum ConsoleF KeyValF)
clubSum = foldFree clubI

clubIO :: Free ClubF ~> IO
clubIO = freeSumIO . clubSum


