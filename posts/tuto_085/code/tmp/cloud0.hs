-- https://degoes.net/articles/modern-fp
-- https://degoes.net/articles/modern-fp-part-2
-- todo

{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE DeriveFunctor #-}

import Control.Monad.Free 
-- import Control.Monad.IO.Class
import Prelude hiding (log)
-- import Data.Functor.Sum

-------------------------------------------------------------------------------
-- cloud
-------------------------------------------------------------------------------

type Path = String
type Bytes = String

data CloudF a
  = SaveFile Path Bytes a
  | ListFiles Path ([Path] -> a)
  deriving (Functor)

type Cloud a = Free CloudF a

saveFile :: Path -> Bytes -> Cloud ()
saveFile path bytes = liftF $ SaveFile path bytes ()

listFiles :: Path -> Cloud [Path]
listFiles path = liftF $ ListFiles path id

-------------------------------------------------------------------------------
-- log
-------------------------------------------------------------------------------

data Level
  = Debug
  | Info

data LogF a
  = Log Level String a
  deriving (Functor)

type Log = Free LogF

log :: Level -> String -> Log ()
log lvl msg = liftF $ Log lvl msg ()

-------------------------------------------------------------------------------
-- rest
-------------------------------------------------------------------------------

data RestF a
  = Get Path (Bytes -> a)
  | Put Path Bytes (Bytes -> a)
  deriving (Functor)

type Rest = Free RestF

get :: Path -> Rest Bytes
get p = liftF $ Get p id

put :: Path -> Bytes -> Rest Bytes
put p b = liftF $ Put p b id

-------------------------------------------------------------------------------
-- LogStd
-------------------------------------------------------------------------------

interpretLogStd :: LogF a -> IO a
interpretLogStd (Log Debug str next) = putStrLn ("[Debug] " <> str) >> return next
interpretLogStd (Log Info str next) = putStrLn ("[Info] " <> str) >> return next

-------------------------------------------------------------------------------
-- RestClient
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- CloudRest
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- CloudClient
-------------------------------------------------------------------------------

interpretCloud :: CloudF a -> IO a
interpretCloud (SaveFile p _b next) = putStrLn ("fake SaveFile: " <> p) >> return next
interpretCloud (ListFiles _b next) = putStrLn "fake ListFiles" >> return (next [])

data CloudLogF a 
  = CLLog (Log a)
  | CLCloud (Cloud a)
  deriving (Functor)

type CloudLog a = Free CloudLogF a

clLog :: Log a -> CloudLog a
clLog = liftF . CLLog

clCloud :: Cloud a -> CloudLog a
clCloud = liftF . CLCloud

interpretCloudLog :: CloudLogF a -> IO a
interpretCloudLog (CLLog l@(Free (Log _level str _next))) = do
  putStrLn str
  foldFree interpretLogStd l 
interpretCloudLog (CLLog l) = do
  foldFree interpretLogStd l 
interpretCloudLog (CLCloud c) = do
  foldFree interpretCloud c

app2 :: CloudLog ()
app2 = do
  clLog $ log Debug "test debug app2"
  _ <- clCloud $ listFiles "test"
  clCloud $ saveFile "test" "test"
  return ()

-------------------------------------------------------------------------------
-- CloudLogT
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- app
-------------------------------------------------------------------------------

app3 :: Log ()
app3 = do
   log Debug "test debug"
   log Info "test info"

-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

main :: IO ()
main = do
  putStrLn "\napp3"
  foldFree interpretLogStd app3

  putStrLn "\napp2"
  foldFree interpretCloudLog app2

