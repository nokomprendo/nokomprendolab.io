-- https://blog.ploeh.dk/2017/07/24/combining-free-monads-in-haskell/
-- FreeT

{-# Language DeriveFunctor #-}

import Control.Monad.Trans.Class as T
import Control.Monad.Trans.Free  -- (Free, liftF)
import Text.Read

data CommandLineInstruction next 
  = ReadLine (String -> next)
  | WriteLine String next
  deriving (Functor)

type CommandLineProgram = Free CommandLineInstruction

readLine :: CommandLineProgram String
readLine = liftF (ReadLine id)

writeLine :: String -> CommandLineProgram ()
writeLine s = liftF (WriteLine s ())

data Slot = Slot 
  { slotDate :: String
  , seatsLeft :: Int 
  } deriving (Show)
 
data Reservation = Reservation 
  { reservationDate :: String
  , reservationName :: String
  , reservationEmail :: String
  , reservationQuantity :: Int 
  } deriving (Show)

data ReservationsApiInstruction next 
  = GetSlots String ([Slot] -> next)
  | PostReservation Reservation next
  deriving (Functor)

type ReservationsApiProgram = Free ReservationsApiInstruction

getSlots :: String -> ReservationsApiProgram [Slot]
getSlots d = liftF (GetSlots d id)

postReservation :: Reservation -> ReservationsApiProgram ()
postReservation r = liftF (PostReservation r ())

readParse :: Read a => String -> String -> CommandLineProgram a
readParse prompt errorMessage = do
  writeLine prompt
  l <- readLine
  case readMaybe l of
    Just dt -> return dt
    Nothing -> do
      writeLine errorMessage
      readParse prompt errorMessage

readAnything :: String -> CommandLineProgram String
readAnything prompt = do
  writeLine prompt
  readLine

tryReserve :: FreeT ReservationsApiProgram CommandLineProgram ()
tryReserve = do
  q <- T.lift $ readParse "Please enter number of diners:" "Not an Integer."
  d <- T.lift $ readAnything "Please enter your desired date:" 
  availableSeats <- liftF $ sum . fmap seatsLeft <$> getSlots d
  if availableSeats < q
    then T.lift $ writeLine $ "Only " ++ show availableSeats ++ " remaining seats."
    else do
      n <- T.lift $ readAnything "Please enter your name:"
      e <- T.lift $ readAnything "Please enter your email address:"
      liftF $ postReservation Reservation
        { reservationDate = d
        , reservationName = n
        , reservationEmail = e
        , reservationQuantity = q }

interpretCommandLine :: CommandLineProgram a -> IO a
interpretCommandLine program =
  case runFree program of
    Pure r -> return r
    Free (ReadLine next) -> do
      line <- getLine
      interpretCommandLine $ next line
    Free (WriteLine line next) -> do
      putStrLn line
      interpretCommandLine next

interpretReservationsApi :: ReservationsApiProgram a -> IO a
interpretReservationsApi program =
  case runFree program of
    Pure x -> return x
    Free (GetSlots _zt next) -> do
      putStrLn "getSlots"
      interpretReservationsApi $ next [Slot "2022" 42]
    Free (PostReservation _r next) -> do
      putStrLn "postReservation"
      interpretReservationsApi next

interpret :: FreeT ReservationsApiProgram CommandLineProgram a -> IO a
interpret program = do
  r <- interpretCommandLine $ runFreeT program
  case r of
    Pure x -> return x
    Free p -> do
      y <- interpretReservationsApi p
      interpret y

main :: IO ()
main = interpret tryReserve

