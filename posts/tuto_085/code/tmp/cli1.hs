-- https://blog.ploeh.dk/2017/07/24/combining-free-monads-in-haskell/
-- Free + Sum

{-# Language DeriveFunctor #-}

import Control.Monad.Trans.Free (Free, liftF, iterM)
import Data.Functor.Sum
import Text.Read

data CommandLineInstruction next 
  = ReadLine (String -> next)
  | WriteLine String next
  deriving (Functor)

type Program = Free (Sum CommandLineInstruction ReservationsApiInstruction)

data Slot = Slot 
  { slotDate :: String
  , seatsLeft :: Int 
  } deriving (Show)
 
data Reservation = Reservation 
  { reservationDate :: String
  , reservationName :: String
  , reservationEmail :: String
  , reservationQuantity :: Int 
  } deriving (Show)

data ReservationsApiInstruction next 
  = GetSlots String ([Slot] -> next)
  | PostReservation Reservation next
  deriving (Functor)

liftCommandLine :: CommandLineInstruction a -> Program a
liftCommandLine = liftF . InL

liftReservation :: ReservationsApiInstruction a -> Program a
liftReservation = liftF . InR

readLine :: Program String
readLine = liftCommandLine (ReadLine id)
 
writeLine :: String -> Program ()
writeLine s = liftCommandLine (WriteLine s ())

getSlots :: String -> Program [Slot]
getSlots d = liftReservation (GetSlots d id)
 
postReservation :: Reservation -> Program ()
postReservation r = liftReservation (PostReservation r ())

readParse :: Read a => String -> String -> Program a
readParse prompt errorMessage = do
  writeLine prompt
  l <- readLine
  case readMaybe l of
    Just dt -> return dt
    Nothing -> do
      writeLine errorMessage
      readParse prompt errorMessage

readAnything :: String -> Program String
readAnything prompt = do
  writeLine prompt
  readLine

tryReserve :: Program ()
tryReserve = do
  q <- readParse "Please enter number of diners:" "Not an Integer."
  d <- readAnything "Please enter your desired date:" 
  availableSeats <- sum . fmap seatsLeft <$> getSlots d
  if availableSeats < q
    then writeLine $ "Only " ++ show availableSeats ++ " remaining seats."
    else do
      n <- readAnything "Please enter your name:"
      e <- readAnything "Please enter your email address:"
      postReservation Reservation
        { reservationDate = d
        , reservationName = n
        , reservationEmail = e
        , reservationQuantity = q }

interpretCommandLine :: CommandLineInstruction (IO a) -> IO a
interpretCommandLine (ReadLine next) = getLine >>= next
interpretCommandLine (WriteLine line next) = putStrLn line >> next

interpretReservationsApi :: ReservationsApiInstruction (IO a) -> IO a
interpretReservationsApi (GetSlots _zt next) = do
  putStrLn "getSlots"
  next [Slot "2022" 42]
interpretReservationsApi (PostReservation _r next) = do
  putStrLn "postReservation"
  next 

interpret :: Program a -> IO a
interpret program = 
  let 
    go (InL cmd) = interpretCommandLine cmd
    go (InR res) = interpretReservationsApi res
  in iterM go program

main :: IO ()
main = interpret tryReserve

