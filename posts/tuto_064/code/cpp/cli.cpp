#include "Bot.hpp"

#include <iostream>

//////////////////////////////////////////////////////////////////////
// game
//////////////////////////////////////////////////////////////////////

char formatCell(Cell c) {
    switch (c) {
        case Cell::E: return '.';
        case Cell::R: return 'R';
        case Cell::Y: return 'Y';
        default: abort();
    }
}

std::string formatStatus(Status s) {
    switch (s) {
        case Status::PlayR: return "PlayR";
        case Status::PlayY: return "PlayY";
        case Status::Tie: return "Tie";
        case Status::WinR: return "WinR";
        case Status::WinY: return "WinY";
        default: abort();
    }
}

int j2k(const Game & game, int j) {
    auto moves = game.getMoves();
    auto iter = std::find(moves.begin(), moves.end(), j);
    assert(iter != moves.end());
    return std::distance(moves.begin(), iter);
}

void printGame(const Game & game) {
    std::cout << '\n';
    for (int j=0; j<NJ; j++)
        std::cout << j;
    for (int i=NI-1; i>=0; i--) {
        std::cout << '\n';
        for (int j=0; j<NJ; j++) {
            Cell c = game.getCell(i, j);
            std::cout << formatCell(c);
        }
    }
    std::cout << "\nmoves:";
    for (int j : game.getMoves())
        std::cout << ' ' << j;
    std::cout << "\nstatus: " << formatStatus(game.getStatus()) << std::endl;
}

//////////////////////////////////////////////////////////////////////
// bots
//////////////////////////////////////////////////////////////////////

class BotHuman : public Bot {
    public:
        int genmove(const Game & game) override {
            printGame(game);
            std::cout << "\nj ? ";
            int j;
            std::cin >> j;
            return j2k(game, j);
        }
};

//////////////////////////////////////////////////////////////////////
// main
//////////////////////////////////////////////////////////////////////

#include <chrono>
#include <fstream>
#include <memory>

int main() {
    BotHuman botR;
    // BotMcts botY(32);
    BotMc botY(8);

    Game game;
    char newgame = 'y';
    while (newgame == 'y') {
        playoutBots(game, botR, botY);
        printGame(game);
        std::cout << "new game (y/n) ? ";
        std::cin >> newgame;
        game.newGame();
    }

    return 0;
}

