#include "cmp.hpp"

#include <iostream>

int main() {

    std::cout << "test0\n";

    //BotRandom botR;
    //BotMc botY(4);

    //BotMc botR(64);
    //BotMcts botY(256);

    BotMc botR(128);
    BotMcts botY(512);

    const int nGames = 100;
    std::cout << "winR winY tie ry ryt dt nGames\n";
    auto [ winR, winY, tie, dt ] = run(botR, botY, nGames);
    std::cout << winR << ' ' << winY << ' ' << tie << ' ' 
        << winR+winY << ' ' << winR+winY+tie << ' ' 
        << dt << ' ' << nGames << '\n';

    return 0;
}

