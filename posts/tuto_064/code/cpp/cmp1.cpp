#include "cmp.hpp"

#include <iostream>

int main() {

    std::cout << "test0\n";
    BotMc botR(128);
    BotMcts botY(512);
    const int nGames = 10;
    std::cout << "winR winY tie ry ryt dt nGames\n";
    auto [ winR, winY, tie, dt ] = run(botR, botY, nGames);
    std::cout << winR << ' ' << winY << ' ' << tie << ' ' 
        << winR+winY << ' ' << winR+winY+tie << ' ' 
        << dt << ' ' << nGames << '\n';

    return 0;
}

