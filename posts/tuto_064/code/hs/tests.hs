import Bot
import Game

import qualified Data.Vector.Mutable as M
import qualified Data.Vector.Unboxed as U

import Control.Monad
import Data.Massiv.Array hiding (map, reverse)
import Data.STRef
import System.Random.MWC

import Control.Monad.ST
import Test.Hspec

main :: IO ()
main = hspec $ do

    describe "Game" $ do

        it "mkGame 1" $ do
            g <- stToIO $ mkGame PlayerR
            _firstPlayer g `shouldBe` PlayerR
            _status g `shouldBe` PlayR
            _currentPlayer g `shouldBe` PlayerR
            _moves g `shouldBe` U.fromList [0 .. 6]

        it "nextGame 1" $ do
            g <- stToIO (mkGame PlayerR >>= nextGame)
            _status g `shouldBe` PlayY
            _firstPlayer g `shouldBe` PlayerY
            _currentPlayer g `shouldBe` PlayerY
            _moves g `shouldBe` U.fromList [0 .. 6]

        it "lineLength 1" $ do
            l1 <- stToIO (newMArray (Sz2 nI nJ) CellE
                            >>= lineLength 0 1 0 1 CellR)
            l1 `shouldBe` 0

        it "lineLength 2" $ do
            l1 <- stToIO $ do 
                    b0 <- newMArray (Sz2 nI nJ) CellE
                    writeM b0 (Ix2 0 2) CellR
                    writeM b0 (Ix2 0 3) CellR
                    lineLength 0 1 0 1 CellR b0
            l1 `shouldBe` 2

        it "lineLength 3" $ do
            l1 <- stToIO $ do 
                    b0 <- newMArray (Sz2 nI nJ) CellE
                    writeM b0 (Ix2 0 0) CellR
                    writeM b0 (Ix2 0 1) CellR
                    lineLength 0 2 0 (-1) CellR b0
            l1 `shouldBe` 2

        it "playK 1" $ do
            g <- stToIO (mkGame PlayerR >>= playK 1)
            _status g `shouldBe` PlayY
            _firstPlayer g `shouldBe` PlayerR
            _currentPlayer g `shouldBe` PlayerY
            _moves g `shouldBe` U.fromList [0 .. 6]

        it "playK 2" $ do
            g <- stToIO (mkGame PlayerR 
                        >>= playK 6
                        >>= playK 6
                        >>= playK 6
                        >>= playK 6
                        >>= playK 6
                        >>= playK 6)
            _status g `shouldBe` PlayR
            _firstPlayer g `shouldBe` PlayerR
            _currentPlayer g `shouldBe` PlayerR
            _moves g `shouldBe` U.fromList [0 .. 5]

    describe "BotMcts" $ do

        it "mkRoot 1" $ do
            x <- stToIO $ mkGame PlayerR >>= mkRoot
            stToIO (readSTRef (nodeReward x)) >>= (`shouldBe` 0)
            stToIO (readSTRef (nodeNsims x)) >>= (`shouldBe` 0)
            _status (nodeGame x) `shouldBe` PlayR
            nodePlayer x `shouldBe` PlayerR
            nodeNmoves x `shouldBe` 7
            stToIO (readSTRef (nodeLastI x)) >>= (`shouldBe` 0)

        it "mkRoot 2" $ do
            x <- stToIO $ mkGame PlayerR >>= playK 6 >>= mkRoot
            stToIO (readSTRef (nodeReward x)) >>= (`shouldBe` 0)
            stToIO (readSTRef (nodeNsims x)) >>= (`shouldBe` 0)
            _status (nodeGame x) `shouldBe` PlayY
            nodePlayer x `shouldBe` PlayerY
            nodeNmoves x `shouldBe` 7
            stToIO (readSTRef (nodeLastI x)) >>= (`shouldBe` 0)

        it "mkRoot 3" $ do
            x <- stToIO $ mkGame PlayerR
                            >>= playK 2
                            >>= playK 2
                            >>= playK 2
                            >>= playK 2
                            >>= playK 2
                            >>= playK 2
                            >>= mkRoot
            stToIO (readSTRef (nodeReward x)) >>= (`shouldBe` 0)
            stToIO (readSTRef (nodeNsims x)) >>= (`shouldBe` 0)
            _status (nodeGame x) `shouldBe` PlayR
            nodePlayer x `shouldBe` PlayerR
            nodeNmoves x `shouldBe` 6
            stToIO (readSTRef (nodeLastI x)) >>= (`shouldBe` 0)

        it "mkLeaf 1" $ do
            x <- stToIO $ mkGame PlayerR >>= mkRoot >>= newSTRef >>= mkLeaf 2 
            stToIO (readSTRef (nodeReward x)) >>= (`shouldBe` 0)
            stToIO (readSTRef (nodeNsims x)) >>= (`shouldBe` 0)
            _status (nodeGame x) `shouldBe` PlayY
            nodePlayer x `shouldBe` PlayerR
            nodeNmoves x `shouldBe` 7
            stToIO (readSTRef (nodeLastI x)) >>= (`shouldBe` 0)


        it "iterate 1" $ do
            x <- stToIO $ do
                    root <- mkGame PlayerR >>= playK 6 >>= mkRoot
                    leaf <- selectAndExpand root
                    gen <- create
                    status <- simulate gen leaf
                    backpropagate status leaf
                    return root
            stToIO (readSTRef (nodeNsims x)) >>= (`shouldBe` 1)
            _status (nodeGame x) `shouldBe` PlayY
            nodePlayer x `shouldBe` PlayerY
            nodeNmoves x `shouldBe` 7
            stToIO (readSTRef (nodeLastI x)) >>= (`shouldBe` 1)

        it "iterate 2" $ do
            x <- stToIO $ do
                    root <- mkGame PlayerR >>= playK 6 >>= mkRoot
                    replicateM_ 2 $ do
                        leaf <- selectAndExpand root
                        gen <- create
                        status <- simulate gen leaf
                        backpropagate status leaf
                    return root
            stToIO (readSTRef (nodeNsims x)) >>= (`shouldBe` 2)
            _status (nodeGame x) `shouldBe` PlayY
            nodePlayer x `shouldBe` PlayerY
            nodeNmoves x `shouldBe` 7
            stToIO (readSTRef (nodeLastI x)) >>= (`shouldBe` 2)

        it "iterate 3" $ do
            x <- stToIO $ do
                    root <- mkGame PlayerR >>= playK 6 >>= mkRoot
                    replicateM_ 7 $ do
                        leaf <- selectAndExpand root
                        gen <- create
                        status <- simulate gen leaf
                        backpropagate status leaf
                    return root
            stToIO (readSTRef (nodeNsims x)) >>= (`shouldBe` 7)
            _status (nodeGame x) `shouldBe` PlayY
            nodePlayer x `shouldBe` PlayerY
            nodeNmoves x `shouldBe` 7
            stToIO (readSTRef (nodeLastI x)) >>= (`shouldBe` 7)

        it "iterate 4" $ do
            x <- stToIO $ do
                    root <- mkGame PlayerR >>= playK 6 >>= mkRoot
                    replicateM_ 8 $ do
                        leaf <- selectAndExpand root
                        gen <- create
                        status <- simulate gen leaf
                        backpropagate status leaf
                    return root
            stToIO (readSTRef (nodeNsims x)) >>= (`shouldBe` 8)
            _status (nodeGame x) `shouldBe` PlayY
            nodePlayer x `shouldBe` PlayerY
            nodeNmoves x `shouldBe` 7
            stToIO (readSTRef (nodeLastI x)) >>= (`shouldBe` 7)

        it "iterate 5" $ do
            x <- stToIO $ do
                    root <- mkGame PlayerR >>= playK 6 >>= mkRoot
                    replicateM_ 10 $ do
                        leaf <- selectAndExpand root
                        gen <- create
                        status <- simulate gen leaf
                        backpropagate status leaf
                    return root
            stToIO (readSTRef (nodeNsims x)) >>= (`shouldBe` 10)
            _status (nodeGame x) `shouldBe` PlayY
            nodePlayer x `shouldBe` PlayerY
            nodeNmoves x `shouldBe` 7
            stToIO (readSTRef (nodeLastI x)) >>= (`shouldBe` 7)

        it "iterate 6" $ do
            x <- stToIO $ do
                    root <- mkGame PlayerR >>= playK 6 >>= mkRoot
                    replicateM_ 100 $ do
                        leaf <- selectAndExpand root
                        gen <- create
                        status <- simulate gen leaf
                        backpropagate status leaf
                    return root
            stToIO (readSTRef (nodeNsims x)) >>= (`shouldBe` 100)
            _status (nodeGame x) `shouldBe` PlayY
            nodePlayer x `shouldBe` PlayerY
            nodeNmoves x `shouldBe` 7
            stToIO (readSTRef (nodeLastI x)) >>= (`shouldBe` 7)


