{-# Language FlexibleContexts, FlexibleInstances, MultiParamTypeClasses #-}

import Bot
import Cmp

import Control.Monad
import Control.Monad.ST
import System.Random.MWC
import System.IO
import System.TimeIt

test1 :: (Bot RealWorld b1, Bot RealWorld b2) 
    => String -> Int -> (Int -> Gen RealWorld -> b1) -> b2 -> [Int] -> IO ()
test1 name nGames mkBotR botY values = 
    let filename = "out-test1-" ++ name ++ ".csv"
    in withFile filename WriteMode $ \h -> do
        putStrLn filename
        hPutStrLn h "winR WinY tie ry ryt dt nGames value"
        forM_ values $ \v -> do
            botR <- mkBotR v <$> createSystemRandom
            (dt, (r, y, t)) <- timeItT $ stToIO (run botR botY nGames)
            hPutStrLn h $ unwords (map show [r, y, t, r+y, r+y+t, dt] 
                ++ [show nGames, show v])

test2 :: (Bot RealWorld b1, Bot RealWorld b2) 
    => String -> Int -> (Int -> Gen RealWorld -> b1) 
    -> (Int -> Gen RealWorld -> b2) -> [(Int, Int)] -> IO ()
test2 name nGames mkBotR mkBotY values = 
    let filename = "out-test2-" ++ name ++ ".csv"
    in withFile filename WriteMode $ \h -> do
        putStrLn filename
        hPutStrLn h "winR WinY tie ry ryt dt nGames value"
        forM_ values $ \(vr, vy) -> do
            botR <- mkBotR vr <$> createSystemRandom
            botY <- mkBotY vy <$> createSystemRandom
            (dt, (r, y, t)) <- timeItT $ stToIO (run botR botY nGames)
            hPutStrLn h $ unwords (map show [r, y, t, r+y, r+y+t, dt] 
                ++ map show [nGames, vr, vy])

main :: IO ()
main = do

    let nGames = 300
    
    botRandom <- BotRandom <$> createSystemRandom
    botMc128 <- BotMc 128 <$> createSystemRandom
    botMcts512 <- BotMcts 512 <$> createSystemRandom

    test1 "McX-Random-full" nGames BotMc botRandom 
        [2, 4, 8, 16, 32, 64, 128, 256]
    test1 "MctsX-Random-full" nGames BotMcts botRandom 
        [8, 16, 32, 64, 128, 256, 512, 1024]

    test1 "McX-Random" nGames BotMc botRandom 
        [2, 4, 8]
    test1 "MctsX-Random" nGames BotMcts botRandom 
        [8, 16, 32]

    test1 "McX-Mc128" nGames BotMc botMc128 
        [16, 32, 64, 128, 256]
    test1 "MctsX-Mcts512" nGames BotMcts botMcts512
        [64, 128, 256, 512, 1024]

    test1 "McX-Mcts512" nGames BotMc botMcts512 
        [16, 32, 64, 128, 256]
    test1 "MctsX-Mc128" nGames BotMcts botMc128 
        [64, 128, 256, 512, 1024]

    test2 "McX-MctsX" nGames BotMc BotMcts
        $ zip [16, 32, 64, 128, 256] [64, 128, 256, 512, 1024]

