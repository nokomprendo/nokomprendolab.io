import Bot
import Cmp

import Control.Monad.ST
import System.Random.MWC

main :: IO ()
main = do
    putStrLn "cmp1"
    botR <- BotMc 128 <$> createSystemRandom
    botY <- BotMcts 512 <$> createSystemRandom
    let nGames = 10

    (r, y, t) <- stToIO (run botR botY nGames)
    putStrLn "winR WinY tie ry ryt nGames"
    putStrLn $ unwords (map show [r, y, t, r+y, r+y+t] ++ [show nGames])

