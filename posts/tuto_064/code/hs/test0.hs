{-# Language FlexibleContexts, FlexibleInstances, MultiParamTypeClasses #-}

import Bot
import Game

import qualified Data.Vector.Mutable as M
import qualified Data.Vector.Unboxed as U
import Data.Massiv.Array hiding (map, reverse, mapM_)

import Control.Monad
import Control.Monad.ST
import Data.STRef
import System.Random.MWC

----------------------------------------------------------------------
-- game
----------------------------------------------------------------------

formatCell :: Cell -> String
formatCell CellE = "."
formatCell CellR = "R"
formatCell CellY = "Y"

showGame :: Game s -> ST s String
showGame g = do
    cs <- reverse . toLists2 <$> freezeS (_cells g)
    let bb = unlines $ map (concatMap formatCell) cs
    return $ "\n0123456\n" ++ bb 
        ++ "moves: " ++ unwords (map show $ U.toList $ _moves g)
        ++ "\nstatus: " ++ show (_status g) ++ "\n"

----------------------------------------------------------------------
-- bots
----------------------------------------------------------------------

class BotIO b where
    genmoveIO :: b -> Game RealWorld -> IO Int

instance BotIO (BotRandom RealWorld) where
    genmoveIO b g = stToIO (genmove b g)

instance BotIO (BotMc RealWorld) where
    genmoveIO b g = stToIO (genmove b g)

instance BotIO (BotMcts RealWorld) where
    genmoveIO b g = stToIO (genmove b g)

----------------------------------------------------------------------
-- main
----------------------------------------------------------------------

printNode s n = do
    putStrLn "**************************************************"
    putStrLn s
    stToIO (readSTRef (nodeReward n)) >>= print
    stToIO (readSTRef (nodeNsims n)) >>= print
    print (nodePlayer n)
    -- stToIO (showGame $ nodeGame n) >>= putStrLn

printChild r k =
    M.read (nodeChildren r) k >>= printNode ("n" ++ show k)

main :: IO ()
main = do

    let niters = 30
        nchild = min 7 niters

    g0 <- stToIO (mkGame PlayerR
                    >>= playK 1 >>= playK 2
                    >>= playK 1 >>= playK 2
                    >>= playK 1 >>= playK 2
                    -- >>= playK 1
                    )
    putStrLn "\ng0"
    stToIO (showGame g0) >>= putStrLn

    gen <- createSystemRandom
    -- botY <- BotMcts 100 <$> createSystemRandom 

    root <- stToIO $ mkRoot g0 
    stToIO (replicateM_ niters $ do
            leaf <- selectAndExpand root
            status <- simulate gen leaf
            backpropagate status leaf) 

    printNode "root" root

    mapM_ (printChild root) [0 .. nchild-1]

    putStr "\nbest: "
    stToIO (bestNode root) >>= print

    putStrLn "the end"

