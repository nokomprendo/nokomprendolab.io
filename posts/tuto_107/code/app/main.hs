import Control.Monad
import Data.Function

import Dict
import List
import Lowstr
import Myshow
import Tree

treeIntBool :: Tree Int Bool
treeIntBool = mkTree 
  & insertDict 1 True 
  & insertDict 2 False 
  & insertDict 3 True 

listIntBool :: List Int Bool
listIntBool = mkList 
  & insertDict 1 True 
  & insertDict 2 False 
  & insertDict 3 True 

treeStringInt :: Tree String Int
treeStringInt = mkTree
  & insertDict "foo" 13
  & insertDict "Bar" 37

treeLowstrInt :: Tree Lowstr Int
treeLowstrInt = mkTree
  & insertDict (mkLowstr "foo") 13
  & insertDict (mkLowstr "Bar") 37

elems :: (Dict d, Ord k, Myshow v, Myshow k) => [k] -> d k v -> IO ()
elems ks d = forM_ ks $ \k -> 
  let str_v = maybe "not found" myshow (lookupDict k d) 
      str_k = myshow k
  in putStrLn $ str_k ++ " -> " ++ str_v

main :: IO ()
main = do
  elems [1, 2, 4] treeIntBool
  elems [1, 2, 4] listIntBool
  elems ["foo", "Foo"] treeStringInt
  elems [mkLowstr "foo", mkLowstr "Foo"] treeLowstrInt

