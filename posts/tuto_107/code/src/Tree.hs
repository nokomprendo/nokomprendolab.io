module Tree where

import Dict

data Tree k v
  = Node k v (Tree k v) (Tree k v)
  | Leaf

mkTree :: Tree k v
mkTree = Leaf

instance Dict Tree where
  insertDict k v Leaf = Node k v Leaf Leaf
  insertDict k v (Node ki vi left right)
    | k < ki = Node ki vi (insertDict k v left) right
    | k > ki = Node ki vi left (insertDict k v right)
    | otherwise = Node k v left right

  lookupDict _ Leaf = Nothing
  lookupDict k (Node ki vi left right)
    | k < ki = lookupDict k left
    | k > ki = lookupDict k right
    | otherwise = Just vi

