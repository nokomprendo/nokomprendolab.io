module Lowstr
  ( Lowstr
  , mkLowstr
  ) where

import Data.Char

import Myshow

newtype Lowstr = Lowstr String
  deriving (Eq, Ord, Show)

mkLowstr :: String -> Lowstr
mkLowstr = Lowstr . map toLower

instance Myshow Lowstr where
  myshow (Lowstr str) = str

