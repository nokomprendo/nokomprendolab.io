module Dict where

class Dict d where
  insertDict :: Ord k => k -> v -> d k v -> d k v
  lookupDict :: Ord k => k -> d k v -> Maybe v

