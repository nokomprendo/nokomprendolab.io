{-# LANGUAGE FlexibleInstances #-}

module Myshow where

class Show a => Myshow a where
  myshow :: a -> String
  myshow = show 

instance Myshow Int

instance Myshow Bool where
  myshow True = "true"
  myshow False = "false"

instance Myshow String where
  myshow str = str

