module List where

import Dict

newtype List k v = List { unList :: [(k, v)] }

mkList :: List k v
mkList = List []

instance Dict List where
  insertDict k v (List []) = List [(k, v)]
  insertDict k v (List ((ki, vi):xs)) = 
    if k == ki 
    then List ((k, v) : xs)
    else List ((ki, vi) : unList (insertDict k v (List xs)))

  lookupDict _ (List []) = Nothing
  lookupDict k (List ((ki, vi):xs)) =
    if k == ki
    then Just vi
    else lookupDict k (List xs)

