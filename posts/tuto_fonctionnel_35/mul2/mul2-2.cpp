#include <iostream>
#include <typeinfo>

using namespace std;

template <typename T>
T mul2(T x) {
    cout << "\nT mul2(T x)... T=" << typeid(x).name() << endl;
    return x*2;
}

int main() {

    int n1 = 2;
    int r1 = mul2(n1);
    cout << n1 << " x 2 = " << r1 << endl;

    double n2 = 2.1;
    double r2 = mul2(n2);
    cout << n2 << " x 2 = " << r2 << endl;

    short n3 = 2;
    short r3 = mul2(n3);
    cout << n3 << " x 2 = " << r3 << endl;

    /*
    string n4 = "2.1";
    auto r4 = mul2<string>(n4);
    cout << n4 << " x 2 = " << r4 << endl;
    */

    return 0;
}

