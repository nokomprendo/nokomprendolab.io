#include <iostream>
#include <type_traits>
#include <typeinfo>

using namespace std;

int mul2(int x) {
    int y = x*2;
    return y;
}

int main() {

    {
        int n = 3;
        int r = mul2(n);
        cout << n << " x 2 = " << r << endl;
    }

    /*
    {
        double n = 3.1;
        double r = mul2(n);
        cout << n << " x 2 = " << r << endl;
    }

    {
        short n = 3;
        short r = mul2(n);
        cout << n << " x 2 = " << r << endl;
    }

    {
        string n = "3.1";
        auto r = mul2(n);
        cout << n << " x 2 = " << r << endl;
    }

    */

    return 0;
}


