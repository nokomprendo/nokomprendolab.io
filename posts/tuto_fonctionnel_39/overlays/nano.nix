self: super: {
  nano = super.nano.overrideAttrs (oldAttrs: rec {
    pname = oldAttrs.pname;
    version = "4.5";
    src = super.fetchurl {
      url = "mirror://gnu/nano/${pname}-${version}.tar.xz";
      sha256 = "0czmz1yq8s5qcxcmfjdxzg9nkhbmlc9q1nz04jvf57fdbs7w7mfy";
    };
  });
}

