---
title: Les rollbacks avec Manjaro, Btrs et Timeshift
description: Cet article explique comment installer et configurer un système Manjaro + Btrfs + Timeshift avec snapshots automatiques accessibles au démarrage.
---

Voir aussi : [video youtube](https://youtu.be/twQugVrtD3E) - 
[video erratum youtube](https://youtu.be/M-fnYoW9rJs) - 
[video peertube](https://peertube.fr/videos/watch/0cfc0d25-9d78-4dcb-8400-188561fedb95) - 
[video erratum peertube](https://peertube.fr/videos/watch/6fec3864-6128-460d-a13f-edd894f8c84d) - 
[journal linuxfr](https://linuxfr.org/users/nokomprendo-3/journaux/les-rollbacks-avec-manjaro-btrs-et-timeshift)

D'après son site web, [Manjaro](https://manjaro.org/) est un système
d'exploitation "pour tout le monde" et qui permet "d'apprécier la simplicité".
En effet, comme il s'agit, entre autres, d'une distribution Linux en rolling
release, l'utilisateur n'aura jamais à faire de montée de version majeure.
Cependant, en pratique, on trouve quelques témoignages d'utilisateurs pour qui
une mise-à-jour "a tout cassé" et ne permettait pas de retrouver un système
exploitable facilement. Si ceci ne prouve pas grand chose sur la stabilité
réelle de Manjaro, cela confirme néanmoins l'intérêt des systèmes de rollbacks
automatiques et accessibles au démarrage.

Dans [un article
précédent](https://nokomprendo.gitlab.io/posts/tuto_fonctionnel_47/2020-11-23-fr-README.html),
je montrais comment NixOS permettait, de base, d'utiliser un tel système de
rollbacks. Ici, je vais essayer d'expliquer comment mettre en place et utiliser
un système équivalent avec Manjaro.

![](images/manjaro-web.jpg)

# À propos du système de fichiers btrfs

Le système de rollback illustré dans cet article repose sur [btrfs](https://fr.wikipedia.org/wiki/Btrfs). Il s'agit d'un
système de fichiers moderne qui propose toutes sortes de fonctionnalités
intéressantes comme les sous-volumes, le chiffrement, la compression,
l'écriture transactionnelle...

Btrfs propose un système de snapshots, c'est-à-dire d'instanés permettant de
fixer des états successifs d'une partition (un peu comme les commits avec git).
L'intérêt de gérer les snapshots dans le système de fichiers est d'optimiser
son implémentation (le système retient les différences entre snapshots, sans
stocker de copie superflue) et de pouvoir accéder à ces snapshots avant même le
démarrage du système d'exploitation.

Attention, les snapshots btrfs ne sont pas un système de sauvegarde : ils sont
gérés au niveau d'une même partition et si celle-ci crashe alors les données
sont perdues.

# Installer Manjaro sur une partition btrfs

L'installation de Manjaro est très simple. Il suffit de télécharger l'image
ISO, de préparer et de booter le support d'installation, puis de lancer
l'installeur graphique.

Ici, la seule particularité est d'utiliser une partition racine en btrfs. Pour
cela, on peut, par exemple, créer une table `GPT` et les partitions suivantes :

- `/dev/sda1` de 8M en `unformatted` et avec le drapeau `bios-grub`
- `/dev/sda2` en `btrfs` sur `/` 

Attention : avec la version testée (Manjaro 20.2), si on crée ce
partitionnement dans l'installeur graphique, l'installation va échouer. Il faut
lancer `gparted` avant (mot de passe : `manjaro`), y faire le partitionnement,
puis lancer l'installeur.

# Afficher le menu grub 

Une fois le système installé, on peut le démarrer. Par défaut le système
d'amorçage (`grub`) est caché. Pour afficher le menu au démarrage, on peut
éditer le fichier `/etc/default/grub` (en `root` ou via `sudo`) et régler les
paramètres suivants :

```html
GRUB_TIMEOUT_STYLE=menu

GRUB_GFXPAYLOAD_LINUX=1024x768  # pour le test en VM, éventuellement
```

Pour prendre en compte la modification, il faut mettre à jour le grub. Ceci
sera fait automatiquement lors des étapes suivantes mais on peut le faire
manuellement avec la commande : 

```html
$ sudo update-grub
```

# Configurer timeshift et timeshift-autosnap

Pour gérer les snapshots btrfs, on installe les paquets suivants :

```html
grub-btrfs timeshift timeshift-autosnap
```

## Timeshift

Timeshift est un outil classique pour gérer les snapshots (btrfs ou autres). Il
s'agit à la fois d'un programme en ligne de commande et d'une interface graphique.

Pour initialiser les snapshots, on lance timeshift et on suit l'assistant
(normalement les paramètres par défaut conviennent).

![](images/timeshift-1.jpg)

On peut créer un premier snapshot avec l'interface graphique et les lister avec
la ligne de commande :

```html
$ sudo timeshift --list

/dev/sda3 is mounted at: /run/timeshift/backup, options: rw,relatime,space_cache,subvolid=5,subvol=/

Device : /dev/sda3
UUID   : 218b6029-172b-4eba-b553-09d7605a463e
Path   : /run/timeshift/backup
Mode   : BTRFS
Status : OK
1 snapshots, 6.2 GB free

Num     Name                 Tags  Description  
------------------------------------------------------------------------------
0    >  2020-12-05_03-12-40  O                  

```

## Timeshift-autosnap 

Timeshift-autosnap est un outil en ligne de commande qui permet de prendre un
snapshot et de mettre à jour le grub automatiquement, en y ajoutant une entrée
pour démarrer sur ce snapshot :

```html
$ sudo timeshift-autosnap 
Using system disk as snapshot device for creating snapshots in BTRFS mode

/dev/sda3 is mounted at: /run/timeshift/backup, options: rw,relatime,space_cache,subvolid=5,subvol=/

Creating new backup...(BTRFS)
Saving to device: /dev/sda3, mounted at path: /run/timeshift/backup
Created directory: /run/timeshift/backup/timeshift-btrfs/snapshots/2020-12-05_03-13-38
Created subvolume snapshot: /run/timeshift/backup/timeshift-btrfs/snapshots/2020-12-05_03-13-38/@
Created control file: /run/timeshift/backup/timeshift-btrfs/snapshots/2020-12-05_03-13-38/info.json
BTRFS Snapshot saved successfully (0s)
Tagged snapshot '2020-12-05_03-13-38': ondemand
------------------------------------------------------------------------------
Création du fichier de configuration GRUB…
Thème trouvé : /usr/share/grub/themes/manjaro/theme.txt
Image Linux trouvée : /boot/vmlinuz-5.9-x86_64
Image mémoire initiale trouvée : /boot/intel-ucode.img /boot/initramfs-5.9-x86_64.img
Found initrd fallback image: /boot/initramfs-5.9-x86_64-fallback.img
Detecting snapshots ...
Info: Separate boot partition detected 
Found snapshot: 2020-12-05 03:13:38 | timeshift-btrfs/snapshots/2020-12-05_03-13-38/@
Found snapshot: 2020-12-05 03:12:40 | timeshift-btrfs/snapshots/2020-12-05_03-12-40/@
Found 2 snapshot(s)
Found memtest86+ image: /boot/memtest86+/memtest.bin
fait
```

Les snapshots ainsi créés apparaissent également dans Timeshift (ligne de
commande et interface graphique) :

```html
$ sudo timeshift --list

/dev/sda3 is mounted at: /run/timeshift/backup, options: rw,relatime,space_cache,subvolid=5,subvol=/

Device : /dev/sda3
UUID   : 218b6029-172b-4eba-b553-09d7605a463e
Path   : /run/timeshift/backup
Mode   : BTRFS
Status : OK
2 snapshots, 6.2 GB free

Num     Name                 Tags  Description                                    
------------------------------------------------------------------------------
0    >  2020-12-05_03-12-40  O                                                    
1    >  2020-12-05_03-13-38  O     {timeshift-autosnap} {created before upgrade}  
```

# Ajouter un hook pacman

Avec la configuration présentée jusqu'ici, on peut lancer un
`timeshift-autosnap` puis faire une mise-à-jour des logiciels (via `pacman` ou
une de ses interfaces graphiques), et ainsi pouvoir revenir sur le snapshot si
la mise-à-jour pose problème.

Cependant, cette configuration nécessite de faire le snapshot manuellement et
en ligne de commande. Pour automatiser cette étape, on peut ajouter un [hook
pacman](https://wiki.archlinux.org/index.php/pacman#Hooks), par exemple en
ajoutant un fichier `/etc/pacman.d/hooks/autosnap.hook` :

```html
[Trigger]
Operation = Install
Operation = Upgrade
Operation = Remove
Type = Path
Target = *
[Action]
When = PreTransaction
Exec = /usr/bin/timeshift-autosnap
```

Ainsi, avant chaque installation, suppression ou mise-à-jour,
`timeshift-autosnap` est lancé automatiquement, ce qui crée un snapshot et met
à jour le grub :

```html
$ sudo pacman -S geany
résolution des dépendances…
recherche des conflits entre paquets…

Paquets (1) geany-1.37-1

Taille totale du téléchargement :   3,65 MiB
Taille totale installée :        13,23 MiB

:: Procéder à l’installation ? [O/n] 
:: Récupération des paquets…
 geany-1.37-1-x86_64             3,7 MiB  1730 KiB/s 00:02 [###############################] 100%
(1/1) vérification des clés dans le trousseau              [###############################] 100%
(1/1) vérification de l’intégrité des paquets              [###############################] 100%
(1/1) chargement des fichiers des paquets                  [###############################] 100%
(1/1) analyse des conflits entre fichiers                  [###############################] 100%
(1/1) vérification de l’espace disque disponible           [###############################] 100%
:: Exécution des crochets de pré-transaction…
(1/1) autosnap.hook
Using system disk as snapshot device for creating snapshots in BTRFS mode

/dev/sda3 is mounted at: /run/timeshift/backup, options: rw,relatime,space_cache,subvolid=5,subvol=/

Creating new backup...(BTRFS)
Saving to device: /dev/sda3, mounted at path: /run/timeshift/backup
Created directory: /run/timeshift/backup/timeshift-btrfs/snapshots/2020-12-05_03-16-02
Created subvolume snapshot: /run/timeshift/backup/timeshift-btrfs/snapshots/2020-12-05_03-16-02/@
Created control file: /run/timeshift/backup/timeshift-btrfs/snapshots/2020-12-05_03-16-02/info.json
BTRFS Snapshot saved successfully (0s)
Tagged snapshot '2020-12-05_03-16-02': ondemand
------------------------------------------------------------------------------
Création du fichier de configuration GRUB…
Thème trouvé : /usr/share/grub/themes/manjaro/theme.txt
Image Linux trouvée : /boot/vmlinuz-5.9-x86_64
Image mémoire initiale trouvée : /boot/intel-ucode.img /boot/initramfs-5.9-x86_64.img
Found initrd fallback image: /boot/initramfs-5.9-x86_64-fallback.img
Detecting snapshots ...
Info: Separate boot partition detected 
Found snapshot: 2020-12-05 03:16:02 | timeshift-btrfs/snapshots/2020-12-05_03-16-02/@
Found snapshot: 2020-12-05 03:13:38 | timeshift-btrfs/snapshots/2020-12-05_03-13-38/@
Found snapshot: 2020-12-05 03:12:40 | timeshift-btrfs/snapshots/2020-12-05_03-12-40/@
Found 3 snapshot(s)
Found memtest86+ image: /boot/memtest86+/memtest.bin
fait
:: Traitement des changements du paquet…
(1/1) installation de geany                                [###############################] 100%
Dépendances optionnelles pour geany
    geany-plugins: additional functionality
    vte3: embedded terminal support [installé]
:: Exécution des crochets de post-transaction…
(1/3) Arming ConditionNeedsUpdate...
(2/3) Updating icon theme caches...
(3/3) Updating the desktop file MIME type cache...

```

# Faire un rollback en cas de problème

Avec cette configuration, on peut désormais redémarrer sur un état précédent du système en cas de problème lors d'une mise-à-jour. Pour cela, on sélectionne un snapshot au démarrage :

![](images/grub1.jpg)

Le système devrait alors démarrer sur un état précédent et fonctionnel. Si on
veut rétablir cet état (et donc "annuler" la mise-à-jour problématique), on
peut lancer `timeshift`, sélectionner le snapshot correspondant et le
restaurer : 

![](images/timeshift-2.jpg)

Éventuellement, on peut aussi faire un peu de ménage dans les snapshots et
mettre à jour le grub avec un `update-grub`.

# Conclusion

En utilisant btrfs et timeshift, on peut mettre en place, sur une Manjaro, un
système de snapshots automatiques accessibles au démarrage, similaire à ce que
propose NixOS de base.

La méthode décrite ici présente une façon de faire. Elle peut certainement
être améliorée et mériterait, dans tous les cas, d'être testée un peu plus
sérieusement. Enfin, concernant la simplicité, chacun appréciera...

