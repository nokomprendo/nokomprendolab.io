#!/usr/bin/env julia

struct Carre
  c :: Int
end

struct Rectangle
  w :: Int
  h :: Int
end

# définition multiple de fonction
surface(f :: Carre) = f.c * f.c
surface(f :: Rectangle) = f.w * f.h
surface(f1 :: Carre, f2 :: Rectangle) = surface(f1) + surface(f2)

formes = [Carre(2), Rectangle(2, 3)]

# multiple dispatch : 
# appelle la fonction correspondant aux paramètres
for f in formes
  println(surface(f))
end

println(surface(Carre(2), Rectangle(2, 3)))

