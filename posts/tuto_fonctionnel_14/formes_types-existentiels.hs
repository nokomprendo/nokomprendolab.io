{-# LANGUAGE ExistentialQuantification #-}

class Surfacable a where
  surface :: a -> Double

data Carre = Carre Double
instance Surfacable Carre where
  surface (Carre c) = c*c

data Rectangle = Rectangle Double Double
instance Surfacable Rectangle where
  surface (Rectangle w h) = w*h

-- type existentiel :
-- empaquette des données de classe Surfacable
data MkSurfacable = forall a . (Surfacable a) => MkSurfacable a

-- instancie la classe Surfacable pour le type existentiel (pas obligatoire)
instance Surfacable MkSurfacable where
   surface (MkSurfacable x) = surface x

-- liste hétérogène de valeurs de classe Surfacable
surfacables :: [MkSurfacable]
surfacables = [ MkSurfacable (Carre 2), MkSurfacable (Rectangle 2 3) ]

main = do
  -- avec instance de Surfacable
  print $ map surface surfacables
  -- sans instance de Surfacable
  print $ map (\(MkSurfacable x) -> surface x) surfacables

