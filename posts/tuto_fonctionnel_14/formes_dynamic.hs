import Data.Dynamic

data Carre = Carre Double

surfaceCarre :: Carre -> Double
surfaceCarre (Carre c) = c*c

data Rectangle = Rectangle Double Double

surfaceRectangle :: Rectangle -> Double
surfaceRectangle (Rectangle w h) = w*h

-- liste hétérogène (Dynamic représente le type universel)
formes :: [Dynamic]
formes = [ toDyn (Carre 2), toDyn (Rectangle 2 3) ]

-- calcule la surface d'une valeur inclu dans un Dynamic, si possible 
dynToSurface :: Dynamic -> Maybe Double
dynToSurface x = case fromDynamic x :: Maybe Carre of
    Just c -> Just $ surfaceCarre c
    Nothing -> case fromDynamic x :: Maybe Rectangle of
        Just r -> Just $ surfaceRectangle r
        Nothing -> Nothing

main = print (map dynToSurface formes)

