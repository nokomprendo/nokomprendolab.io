-- type algébrique
data Forme = Carre Double
           | Rectangle Double Double

-- fonction traitant toutes les valeurs possibles du type
surface :: Forme -> Double
surface (Carre c) = c*c
surface (Rectangle w h) = w*h

formes :: [Forme]
formes = [Carre 2, Rectangle 2 3]

main = print $ map surface formes

