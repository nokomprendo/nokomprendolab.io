#!/usr/bin/env python3

class Carre:
    def __init__(self, c):
        self.c = c

    def surface(self):
        return self.c * self.c

class Rectangle:
    def __init__(self, w, h):
        self.w = w
        self.h = h

    def surface(self):
        return self.w * self.h

formes = [Carre(2), Rectangle(2, 3), 42]

for f in formes:
    # introspection : teste si surface est une méthode de f
    if 'surface' in dir(f):
        print(f.surface())

