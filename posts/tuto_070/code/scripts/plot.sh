#!/bin/sh

FILES=$(find . -maxdepth 1 -name "out-test*.csv")

for file in ${FILES} ; do

    name="${file%.*}"
    echo "plotting ${name}"

    gnuplot -e "set out '${name}.png'; \
        set terminal png size 640,360; \
        set title '${name}.png'; \
        plot '${name}.csv' using 8:1 notitle with filledcurve x1 lc rgb 'red', \
        '' using 8:1:4 notitle with filledcurves lc rgb 'yellow', \
        '' using 8:4:5 notitle with filledcurve lc rgb 'blue'"

done

