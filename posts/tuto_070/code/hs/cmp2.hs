{-# Language FlexibleContexts, FlexibleInstances, MultiParamTypeClasses #-}

import Bot
import Cmp

import Control.Monad
import Control.Monad.ST
import System.Random.MWC
import System.IO
import System.TimeIt

test1 :: (Bot RealWorld b1, Bot RealWorld b2) 
    => String -> Int -> (Int -> Gen RealWorld -> b1) -> b2 -> [Int] -> IO ()
test1 name nGames mkBotR botY values = 
    let filename = "out-test1-" ++ name ++ ".csv"
    in withFile filename WriteMode $ \h -> do
        putStrLn filename
        hPutStrLn h "winR WinY tie ry ryt dt nGames value"
        forM_ values $ \v -> do
            botR <- mkBotR v <$> createSystemRandom
            (dt, (r, y, t)) <- timeItT $ stToIO (run botR botY nGames)
            hPutStrLn h $ unwords (map show [r, y, t, r+y, r+y+t, dt] 
                ++ [show nGames, show v])

{-
test2 :: (Bot RealWorld b1, Bot RealWorld b2) 
    => String -> Int -> (Int -> Gen RealWorld -> b1) 
    -> (Int -> Gen RealWorld -> b2) -> [(Int, Int)] -> IO ()
test2 name nGames mkBotR mkBotY values = 
    let filename = "out-test2-" ++ name ++ ".csv"
    in withFile filename WriteMode $ \h -> do
        putStrLn filename
        hPutStrLn h "winR WinY tie ry ryt dt nGames value"
        forM_ values $ \(vr, vy) -> do
            botR <- mkBotR vr <$> createSystemRandom
            botY <- mkBotY vy <$> createSystemRandom
            (dt, (r, y, t)) <- timeItT $ stToIO (run botR botY nGames)
            hPutStrLn h $ unwords (map show [r, y, t, r+y, r+y+t, dt] 
                ++ map show [nGames, vr, vy])
-}

main :: IO ()
main = do
    botRandom <- BotRandom <$> createSystemRandom
    botMc32 <- BotMc 32 <$> createSystemRandom
    let nGames = 300
    test1 "McX-Random" nGames BotMc botRandom [1, 2, 4, 8, 16, 32, 64]
    test1 "McX-Mc32" nGames BotMc botMc32 [4, 8, 16, 32, 64]

