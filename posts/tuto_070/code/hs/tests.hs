import Bot
import Game

import qualified Data.Vector.Mutable as M
import qualified Data.Vector.Unboxed as U

import Control.Monad
import Data.Massiv.Array hiding (map, reverse, sum, all)
import System.Random.MWC

import Control.Monad.ST
import Test.Hspec

main :: IO ()
main = hspec $ do

    describe "Game" $ do

        it "mkGame 1" $ do
            g <- stToIO $ mkGame PlayerR
            _firstPlayer g `shouldBe` PlayerR
            _status g `shouldBe` PlayR
            _currentPlayer g `shouldBe` PlayerR
            _moves g `shouldBe` U.fromList [0 .. 6]

        it "nextGame 1" $ do
            g <- stToIO (mkGame PlayerR >>= nextGame)
            _status g `shouldBe` PlayY
            _firstPlayer g `shouldBe` PlayerY
            _currentPlayer g `shouldBe` PlayerY
            _moves g `shouldBe` U.fromList [0 .. 6]

        it "lineLength 1" $ do
            l1 <- stToIO (newMArray (Sz2 nI nJ) CellE
                            >>= lineLength 0 1 0 1 CellR)
            l1 `shouldBe` 0

        it "lineLength 2" $ do
            l1 <- stToIO $ do 
                    b0 <- newMArray (Sz2 nI nJ) CellE
                    writeM b0 (Ix2 0 2) CellR
                    writeM b0 (Ix2 0 3) CellR
                    lineLength 0 1 0 1 CellR b0
            l1 `shouldBe` 2

        it "lineLength 3" $ do
            l1 <- stToIO $ do 
                    b0 <- newMArray (Sz2 nI nJ) CellE
                    writeM b0 (Ix2 0 0) CellR
                    writeM b0 (Ix2 0 1) CellR
                    lineLength 0 2 0 (-1) CellR b0
            l1 `shouldBe` 2

        it "playK 1" $ do
            g <- stToIO (mkGame PlayerR >>= playK 1)
            _status g `shouldBe` PlayY
            _firstPlayer g `shouldBe` PlayerR
            _currentPlayer g `shouldBe` PlayerY
            _moves g `shouldBe` U.fromList [0 .. 6]

        it "playK 2" $ do
            g <- stToIO (mkGame PlayerR 
                        >>= playK 6
                        >>= playK 6
                        >>= playK 6
                        >>= playK 6
                        >>= playK 6
                        >>= playK 6)
            _status g `shouldBe` PlayR
            _firstPlayer g `shouldBe` PlayerR
            _currentPlayer g `shouldBe` PlayerR
            _moves g `shouldBe` U.fromList [0 .. 5]


    describe "BotZero" $ do

        it "genmove 1" $ do
            (g, s) <- stToIO $ do
                        g0 <- mkGame PlayerR
                        s0 <- playoutBots BotZero BotZero g0
                        return (g0, s0)
            s`shouldBe` WinR
            b <- stToIO $ toLists2 <$> freezeS (_cells g)
            b `shouldBe` 
                    [[CellR, CellR, CellR, CellR, CellE, CellE, CellE]
                    ,[CellY, CellY, CellY, CellE, CellE, CellE, CellE]
                    ,[CellR, CellR, CellR, CellE, CellE, CellE, CellE]
                    ,[CellY, CellY, CellY, CellE, CellE, CellE, CellE]
                    ,[CellR, CellR, CellR, CellE, CellE, CellE, CellE]
                    ,[CellY, CellY, CellY, CellE, CellE, CellE, CellE]]

        it "genmove 2" $ do
            (g, s) <- stToIO $ do
                        g0 <- mkGame PlayerY
                        s0 <- playoutBots BotZero BotZero g0
                        return (g0, s0)
            s`shouldBe` WinY
            b <- stToIO $ toLists2 <$> freezeS (_cells g)
            b `shouldBe` 
                    [[CellY, CellY, CellY, CellY, CellE, CellE, CellE]
                    ,[CellR, CellR, CellR, CellE, CellE, CellE, CellE]
                    ,[CellY, CellY, CellY, CellE, CellE, CellE, CellE]
                    ,[CellR, CellR, CellR, CellE, CellE, CellE, CellE]
                    ,[CellY, CellY, CellY, CellE, CellE, CellE, CellE]
                    ,[CellR, CellR, CellR, CellE, CellE, CellE, CellE]]


    describe "BotRandom" $ do

        it "random 1" $ do
            gen <- createSystemRandom 
            let n = 100
            xs <- stToIO $ replicateM n $ uniformR (0, 10::Int) gen
            xs  `shouldSatisfy` all (\x -> x>=0 && x<=10)

        it "random 2" $ do
            gen <- createSystemRandom 
            let n = 10000
            xs <- stToIO $ replicateM n $ uniformR (0, 10::Int) gen
            let m = fromIntegral (sum xs) / fromIntegral n
            abs (m - 5.0) `shouldSatisfy` (<0.1)

        it "random 3" $ do
            gen <- createSystemRandom 
            let n = 10000
            xs <- stToIO $ replicateM n $ uniformR (0, 10::Int) gen
            let nD = fromIntegral n
                h = [ fromIntegral (length (filter (==i) xs)) / nD | i <- [0 .. 10] ]
            h `shouldSatisfy` 
                all (\hi -> abs (hi - 0.1) < 0.05)

        it "genmove 1" $ do
            br <- BotRandom <$> createSystemRandom
            by <- BotRandom <$> createSystemRandom
            let n = 1000
            let go :: Int -> Game RealWorld -> ST RealWorld [Status]
                go 0 _ = return []
                go i g = do
                    gi <- nextGame g 
                    si <- playoutBots br by gi
                    ss <- go (i-1) gi
                    return (si : ss)
            xs <- stToIO (mkGame PlayerY >>= go n)
            let ratio status = fromIntegral (length $ filter (==status) xs) / fromIntegral n
            abs (ratio WinR - 0.5) `shouldSatisfy` (<0.05)
            abs (ratio WinY - 0.5) `shouldSatisfy` (<0.05)
            abs (ratio Tie - 0.0) `shouldSatisfy` (<0.05)

