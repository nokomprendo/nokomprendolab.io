import Bot
import Cmp

import Control.Monad.ST
import System.Random.MWC

main :: IO ()
main = do

    let botR = BotZero
    let botY = BotZero

    -- botR <- BotRandom <$> createSystemRandom
    -- botY <- BotMc 4 <$> createSystemRandom

    let nGames = 10

    (r, y, t) <- stToIO (run botR botY nGames)
    putStrLn "winR WinY tie ry ryt nGames"
    putStrLn $ unwords (map show [r, y, t, r+y, r+y+t] ++ [show nGames])

