{-# Language FlexibleContexts, FlexibleInstances, MultiParamTypeClasses #-}

module Bot where

import Game

import Control.Monad.ST
import System.Random.MWC

----------------------------------------------------------------------
-- Bot 
----------------------------------------------------------------------

class Bot s b where
    genmove :: b -> Game s -> ST s Int

----------------------------------------------------------------------
-- Bot Zero
----------------------------------------------------------------------

data BotZero = BotZero

instance Bot s BotZero where
    genmove _bot _game = return 0

playoutBots :: (Bot s b1, Bot s b2) => b1 -> b2 -> Game s -> ST s Status
playoutBots botR botY g0 
    | isRunning g0 =
        let moveFunc = if _currentPlayer g0 == PlayerR then genmove botR
                                                       else genmove botY 
        in moveFunc g0 >>= (`playK` g0) >>= playoutBots botR botY 
    | otherwise = return (_status g0) 

----------------------------------------------------------------------
-- BotRandom
----------------------------------------------------------------------

randomMove :: GenST s -> Game s -> ST s Int
randomMove gen game = uniformR (0, nMovesGame game - 1) gen

newtype BotRandom s = BotRandom { randomGen :: GenST s }

instance Bot s (BotRandom s) where
    genmove (BotRandom gen) = randomMove gen

----------------------------------------------------------------------
-- BotMc
----------------------------------------------------------------------

playoutRandom :: GenST s -> Game s -> ST s Status
playoutRandom gen g0 
    | isRunning g0 = randomMove gen g0 >>= (`playK` g0) >>= playoutRandom gen
    | otherwise = return (_status g0)

computeScore :: Player -> Status -> Double
computeScore PlayerR WinR = 1.0
computeScore PlayerY WinY = 1.0
computeScore _ Tie = 0.5
computeScore _ _ = 0.0

data BotMc s = BotMc
    { mcNsims :: !Int
    , mcGen :: GenST s
    }

instance Bot s (BotMc s) where
    genmove (BotMc nsims gen) game =
        let aux ki k s = if ki == nMovesGame game then return k else do
                            si <- evalMove game gen nsims ki
                            if si>s then aux (ki+1) ki si else aux (ki+1) k s
        in aux 0 0 (-1)

evalMove :: Game s -> GenST s -> Int -> Int -> ST s Double
evalMove game0 gen nsims k = do
    let player0 = _currentPlayer game0
    game1 <- cloneGame game0 >>= playK k
    let aux 0 s = return s
        aux i s = do status2 <- cloneGame game1 >>= playoutRandom gen 
                     aux (i - 1) (s + computeScore player0 status2)
    aux nsims 0

