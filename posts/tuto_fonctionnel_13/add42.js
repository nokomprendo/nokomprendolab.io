// fonction classique à plusieurs paramètres
// évaluation partielle via un "wrapper"
add = function(x, y) {
  return x + y
}
add42 = function(y) {
  return add(42, y)
}

// fonction curryfiée
// évaluation partielle réelle
add_curry = function(x) {
  return function(y) {
    return x + y
  }
}
add42_curry = add_curry(42)

// test
console.log('add(42, 2)\t =', add(42, 2))
console.log('add42(2)\t =', add42(2))
console.log('add_curry(42)(2) =', add_curry(42)(2))
console.log('add42_curry(2)\t =', add42_curry(2))

