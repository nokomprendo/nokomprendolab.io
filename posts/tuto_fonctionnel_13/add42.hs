-- fonction qui prend un tuple pour gérer plusieurs paramètres
add :: (Int, Int) -> Int
add (x, y) = x + y

-- fonction wrapper simulant l'évaluation partielle de add
add42 :: Int -> Int
add42 y = add (42, y)

-- fonction curryfiée "à plusieurs paramètres"
add_curry :: Int -> Int -> Int
add_curry x y = x + y

-- évaluation partielle de add_curry
add42_curry = add_curry 42

main = do
  print $ add (42, 2)
  print $ add42 2
  print $ add_curry 42 2
  print $ add42_curry 2

