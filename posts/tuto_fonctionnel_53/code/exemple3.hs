{-# LANGUAGE GeneralizedNewtypeDeriving #-}

data Km
data Mille

newtype Distance a = Distance Double

newtype Vitesse a = Vitesse Double
    deriving (Num, Show)

calculerVitesse :: Distance a -> Double -> Vitesse a
calculerVitesse (Distance dx) dt = Vitesse $ dx / dt

main :: IO ()
main = do
    let v1 = calculerVitesse (Distance 21 :: Distance Km) 0.5
        v2 = calculerVitesse (Distance 11.33 :: Distance Mille) 0.5
    print v1
    print v2
    print $ v1 + v1
    print $ v2 + v2
    -- print $ v1 + v2      -- erreur à la compilation

