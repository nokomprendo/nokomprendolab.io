data Distance
    = Km Double
    | Mille Double
    deriving (Show)

data Vitesse
    = KmH Double
    | Noeud Double
    deriving (Show)

calculerVitesse :: Distance -> Double -> Vitesse
calculerVitesse (Km dx) dt = KmH $ dx / dt
calculerVitesse (Mille dx) dt = Noeud $ dx / dt

main :: IO ()
main = do
    let d1 = Km 21
        d2 = Mille 11.33
        v1 = calculerVitesse d1 0.5
        v2 = calculerVitesse d2 0.5
    print v1
    print v2
    print $ v1 + v1
    print $ v2 + v2
    print $ v1 + v2     -- erreur à l'exécution

instance Num Vitesse where
    KmH x + KmH y = KmH (x + y)
    Noeud x + Noeud y = Noeud (x + y)
    _ + _ = error "Vitesse + Vitesse"

    KmH x * KmH y = KmH (x * y)
    Noeud x * Noeud y = Noeud (x * y)
    _ * _ = error "Vitesse * Vitesse"

    negate (KmH x) = KmH (- x)
    negate (Noeud x) = Noeud (- x)

    abs (KmH x) = KmH (abs x)
    abs (Noeud x) = Noeud (abs x)

    signum (KmH x) = KmH (signum x)
    signum (Noeud x) = Noeud (signum x)

    fromInteger i = KmH (fromInteger i)

