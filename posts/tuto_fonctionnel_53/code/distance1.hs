{-# LANGUAGE GeneralizedNewtypeDeriving #-}

data Kilometer
data Mile

newtype Distance a = Distance Double
  deriving (Num, Show)

marathonDistance :: Distance Kilometer
marathonDistance = Distance 42.195

distanceKmToMiles :: Distance Kilometer -> Distance Mile
distanceKmToMiles (Distance km) = Distance (0.621371 * km)

marathonDistanceInMiles :: Distance Mile
marathonDistanceInMiles = distanceKmToMiles marathonDistance

main :: IO ()
main = do
    print marathonDistance
    print marathonDistanceInMiles

