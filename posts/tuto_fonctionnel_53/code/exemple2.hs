{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE KindSignatures #-}

data Km
data Mille

data Distance a where
    DistanceKm :: Double -> Distance Km
    DistanceMille :: Double -> Distance Mille

data Vitesse a where
    VitesseKmH :: Double -> Vitesse Km
    VitesseNoeud :: Double -> Vitesse Mille

calculerVitesse :: Distance a -> Double -> Vitesse a
calculerVitesse (DistanceKm dx) dt = VitesseKmH $ dx / dt
calculerVitesse (DistanceMille dx) dt = VitesseNoeud $ dx / dt

main :: IO ()
main = do
    let v1 = calculerVitesse (DistanceKm 21) 0.5
        v2 = calculerVitesse (DistanceMille 11.33) 0.5
    print v1
    print v2
    print $ v1 + v1
    print $ v2 + v2
    -- print $ v1 + v2      -- erreur à la compilation

instance Show (Vitesse a) where
    show (VitesseKmH x) = "VitesseKmH " ++ show x
    show (VitesseNoeud x) = "VitesseNoeud " ++ show x

instance Num (Vitesse a) where
    VitesseKmH x + VitesseKmH y = VitesseKmH (x + y)
    VitesseNoeud x + VitesseNoeud y = VitesseNoeud (x + y)

    VitesseKmH x * VitesseKmH y = VitesseKmH (x * y)
    VitesseNoeud x * VitesseNoeud y = VitesseNoeud (x * y)

    negate (VitesseKmH x) = VitesseKmH (- x)
    negate (VitesseNoeud x) = VitesseNoeud (- x)

    abs (VitesseKmH x) = VitesseKmH (abs x)
    abs (VitesseNoeud x) = VitesseNoeud (abs x)

    signum (VitesseKmH x) = VitesseKmH (signum x)
    signum (VitesseNoeud x) = VitesseNoeud (signum x)

    fromInteger i = fromInteger i

