
calculerVitesse :: Double -> Double -> Double
calculerVitesse dx dt = dx / dt

main :: IO ()
main = do
    let d1 = 21                         -- km
        d2 = 11.33                      -- mille
        v1 = calculerVitesse d1 0.5     -- km/h
        v2 = calculerVitesse d2 0.5     -- noeud
    print v1
    print v2
    print $ v1 + v1
    print $ v2 + v2
    print $ v1 + v2     -- erreur de logique, non détectée

