import Data.Foldable
import Data.Monoid

main :: IO ()
main = do

  print $ 2 + (4 + (5 + 0))
  print $ foldr (+) 0 [2, 4, 5]
  print $ foldr (*) 1 [2, 4, 5]
  print $ foldr (++) "" ["foo", "bar", "baz"]

  print $ foldr (<>) mempty [2, 4, 5 :: Sum Int]
  print $ getSum $ foldr (<>) mempty [2, 4, 5]
  print $ getSum $ foldMap Sum [2, 4, 5]
  print $ getSum $ fold [2, 4, 5]

  print $ getProduct $ fold [2, 4, 5]
  print $ fold ["foo", "bar", "baz"]

