import Data.Foldable
import Data.Monoid

data Tree a
  = Leaf
  | Node (Tree a) a (Tree a)
  deriving (Show)

insert :: Ord a => a -> Tree a -> Tree a
insert e Leaf = Node Leaf e Leaf
insert e (Node left x right) = 
  if e <= x
  then Node (insert e left) x right
  else Node left x (insert e right)

fromList :: Ord a => [a] -> Tree a
fromList = foldr insert Leaf

{-
instance Foldable Tree where
  foldr _ acc Leaf = acc
  foldr f acc (Node left x right) = 
    let acc1 = foldr f acc right
        acc2 = f x acc1
        acc3 = foldr f acc2 left
    in acc3
-}

instance Foldable Tree where
  foldMap _ Leaf = mempty
  foldMap f (Node left x right) = foldMap f left <> f x <> foldMap f right

elem' :: Ord a => a -> Tree a -> Bool
elem' _ Leaf = False
elem' e (Node left x right)
  | e < x = elem' e left
  | e > x = elem' e right
  | otherwise = True

main :: IO ()
main = do

  let t1 = fromList [3, 2, 5, 4]
  print t1
  print $ toList t1
  print $ sum t1
  print $ elem 1 t1
  print $ elem 2 t1
  print $ elem' 1 t1
  print $ elem' 2 t1

  print $ getSum $ fold $ fromList [3, 2, 5, 4]
  print $ getProduct $ fold $ fromList [3, 2, 5, 4]

