
#include <vector>
#include <numeric>
#include <iostream>

int main() {
  std::vector<int> v1 {2, 4, 5};

  int p1 = 1;
  for (int x : v1)
    p1 *= x;
  std::cout << "p1 = " << p1 << std::endl;

  auto fProd = [] (int acc, int x) { return acc * x; };
  int p2 = std::accumulate(v1.begin(), v1.end(), 1, fProd);
  std::cout << "p2 = " << p2 << std::endl;

  int p3 = std::accumulate(v1.begin(), v1.end(), 1, std::multiplies<int>());
  std::cout << "p3 = " << p3 << std::endl;

  return 0;
}

