---
title: Gestion de paquets évoluée avec Nix. cachix, overlays, home-manager
---

[Nix](https://nixos.org/nix/) est un gestionnaire de paquets dont les débuts remonte à 2003. Il a pour but de rendre la gestion de paquets fiable, reproductible et personnalisable. Il permet notamment de faire des mises-à-jour atomiques, d'annuler des mises-à-jour, d'installer des multiples versions d'un même paquet, de définir précisemment des environnements logiciels… Nix a également inspiré un projet assez similaire : [GNU Guix](http://guix.gnu.org/).

De nombreux outils ont été développés, sur la base de Nix : la logithèque [Nixpkgs](https://nixos.org/nixpkgs/), la distribution linux [NixOS](https://nixos.org/nixos/), l'outil de déploiement de machines [Nixops](https://nixos.org/nixops/), le système de construction et de cache de paquets binaires [Hydra](https://nixos.org/hydra/), l'outil de construction d'image docker [dockerTools](https://nixos.org/nixpkgs/manual/#sec-pkgs-dockerTools), etc.

Cette dépêche présente trois outils très pratiques pour gérer son environnement utilisateur : un service de cache de paquets binaires ([Cachix](https://cachix.org/)), un outil de personnalisation de la logithèque ([Overlays](https://nixos.org/nixpkgs/manual/#chap-overlays)) et un outil de configuration de l'environnement utilisateur ([Home-manager](https://github.com/rycee/home-manager)).

# Cachix

[Cachix](https://cachix.org/) permet de gérer facilement un cache personnel de paquets binaires.  Concrètement, il s'agit d'un service de cloud et d'un logiciel client, le tout basé sur le gestionnaire de paquets [Nix](https://nixos.org/nix/). Cachix propose différentes offres, dont une offre gratuite de 10 Go de stockage en accès public.

Quand on utilise la [logithèque officielle de Nix](https://github.com/NixOS/nixpkgs), on télécharge des paquets binaires déjà compilés.  Nix permet très facilement d'empaqueter des programmes/bibliothèques personnels ou de modifier les paquets officiels. Mais dans ce cas, les paquets binaires correspondants ne sont pas dans le cache officiel et doivent donc être compilés sur la machine locale.  Cachix permet d'envoyer les paquets binaires ainsi compilés sur un serveur. On peut alors récupérer ces paquets sur d'autres machines sans avoir à recompiler.

## Exemple de cas d'utilisation 

Imaginons qu'on a développé une bibliothèque personnelle [tuto38lib](https://gitlab.com/nokomprendo/tuto38lib), potentiellement longue à compiler, et qu'on l'utilise désormais pour développer un projet [tuto38appli](https://gitlab.com/nokomprendo/tuto38appli).  Ces deux projets sont empaquetés via Nix si bien qu'on peut intégrer tuto38lib dans tuto38appli avec le fichier `tuto38appli/default.nix` suivant :

```nix
{ pkgs ? import <nixpkgs> {} }:

let 

  tuto38lib-src = fetchTarball {
    url = "https://gitlab.com/nokomprendo/tuto38lib/-/archive/v1.0/tuto38lib-v1.0.tar.gz";
    sha256 = "1di8ms0g1j9kih9qg1s42p9wi5xxbm7h3n9as6fbxqfbfa75w9nf";
  };

  tuto38lib = pkgs.callPackage tuto38lib-src {};

in pkgs.stdenv.mkDerivation {
  name = "tuto38appli";
  src = ./.;
  buildInputs = with pkgs; [ cmake tuto38lib ];
}
```

Pour travailler sur le projet tuto38appli, il suffit de récupérer le code et de lancer un `nix-shell`. Toutes les dépendances sont récupérées et on peut alors travailler sur le projet. Ici il s'agit d'un projet C++/Cmake :

```sh
$ nix-shell 
these derivations will be built:
  /nix/store/zasgjbqnp89nnzb3fyrvi0daq2bwnacq-tuto38lib.drv
building '/nix/store/zasgjbqnp89nnzb3fyrvi0daq2bwnacq-tuto38lib.drv'...
unpacking sources
...

[nix-shell]$ mkdir build

[nix-shell]$ cd build/

[nix-shell]$ cmake ..
...

[nix-shell]$ make
...

[nix-shell]$ ./tuto38appli 
42

[nix-shell]$ exit
```

On constate que pour tuto38lib, le code source est récupéré, compilé et installé sur la machine locale, dans le `/nix/store`. Ainsi, si on lance de nouveau un `nix-shell` utilisant tuto38lib, cette bibliothèque est déjà sur le système local et n'est donc pas recompilée.

```sh
$ find /nix/store -maxdepth 1 -name "*tuto38lib*"
/nix/store/zasgjbqnp89nnzb3fyrvi0daq2bwnacq-tuto38lib.drv
/nix/store/js0gbdilqjg3y9yhpjfc7slja902kfxy-tuto38lib

$ nix-shell 

[nix-shell]$ 
```

Si on nettoie les paquets de tuto38lib dans le `/nix/store` ou qu'on passe une autre machine, tuto38lib n'est plus disponible.  Si on lance un nix-shell dans le projet tuto38appli, la bibliothèque tuto38lib est donc de nouveau téléchargée, compilée et installée.

```sh
$ find /nix/store -maxdepth 1 -name "*tuto38lib*" -exec nix-store --delete {} \+
finding garbage collector roots...
deleting '/nix/store/js0gbdilqjg3y9yhpjfc7slja902kfxy-tuto38lib'
deleting '/nix/store/h85xxfmc0lf5g8srhv7jqr8kbjdijia1-tuto38appli.drv'
deleting '/nix/store/ss39dl3l4y0kbccpkjzr4dbypr0fz5c2-tuto38appli.drv'
deleting '/nix/store/zasgjbqnp89nnzb3fyrvi0daq2bwnacq-tuto38lib.drv'
deleting '/nix/store/trash'
deleting unused links...
note: currently hard linking saves -0.00 MiB
4 store paths deleted, 0.02 MiB freed

$ nix-shell 
these derivations will be built:
  /nix/store/zasgjbqnp89nnzb3fyrvi0daq2bwnacq-tuto38lib.drv
building '/nix/store/zasgjbqnp89nnzb3fyrvi0daq2bwnacq-tuto38lib.drv'...
unpacking sources
...

[nix-shell]$ 
```

Cachix permet de mettre les paquets binaires produits par la première compilation sur un serveur et de récupérer ensuite ces paquets sans avoir à les recompiler.

## Installation du client cachix

Sur NixOS, ceci est assez simple. Il suffit d'éditer le fichier `/etc/nixos/configuration.nix` pour ajouter le paquet `cachix` et spécifier l'utilisateur (ici `nokomprendo`) dans les `trustedUsers` :

```nix
  environment.systemPackages = with pkgs; [
    cachix
    ...
  ];

  nix.trustedUsers = [ "nokomprendo" "root" ];
```

Il faut ensuite mettre à jour la configuration du système :

```sh
$ sudo nixos-rebuild switch
```

Et, éventuellement, nettoyer le cache du `root` :

```sh
$ sudo rm -rf /root/.cache/nix
```

## Création d'un dépôt de cache

Tout est expliqué sur le [site de cachix](https://cachix.org).  Il faut d'abord se connecter (via un compte github) puis créer un dépôt de cache (par exemple, `tuto38`).

On génère ensuite une clé d'authentification du dépôt :

```sh
cachix authtoken ...
...
```

Et une clé de signature :

```sh
$ cachix generate-keypair tuto38
...
```

## Mettre en cache des paquets

La commande `cachix push` permet d'envoyer dans un cache, un paquet local et ses dépendances :

```sh
$ cachix push tuto38 /nix/store/js0gbdilqjg3y9yhpjfc7slja902kfxy-tuto38lib
pushing /nix/store/4l35nqpaiwzhfafrpby1xf7kfik7ai7c-gcc-8.3.0-lib
...
```

## Utilisation du cache

La commande `cachix use` indique à Nix qu'il faut regarder dans un dépôt de cache si un paquet binaire n'est pas déjà disponible. 

```sh
$ cachix use tuto38
```

Par exemple, avec le projet tuto38appli précédent, si on nettoie l'installation locale de tuto38lib et qu'on lance de nouveau un `nix-shell`, c'est désormais le paquet binaire qui est directement téléchargé du cache et installé sur la machine.

```sh
$ find /nix/store -maxdepth 1 -name "*tuto38lib*" -exec nix-store --delete {} \+
...

$ nix-shell 
these paths will be fetched (0.00 MiB download, 0.02 MiB unpacked):
  /nix/store/js0gbdilqjg3y9yhpjfc7slja902kfxy-tuto38lib
copying path '/nix/store/js0gbdilqjg3y9yhpjfc7slja902kfxy-tuto38lib' from 'https://tuto38.cachix.org'...

[nix-shell]$ 
```

# Overlays

Le gestionnaire de paquets [Nix](https://nixos.org/nix/) est hautement personnalisable. Pour cela, il reprend le principe de la composition de fonctions : un paquet est en fait une fonction qui indique comment ajouter ou modifier un logiciel à partir d'un environnement logiciel d'entrée. 

Ainsi, la [logithèque Nix](https://nixos.org/nixpkgs/) n'est qu'un ensemble de paquets qui peuvent être appliqués pour construire l'environnement logiciel final.  Les overlays permettent de modifier les paquets de cet ensemble ou d'en ajouter de nouveaux.

## Principe des overlays Nix

Pour ajouter un paquet personnel ou pour modifier un paquet déjà existant, Nix permet d'ajouter des overlays. Concrètement, il s'agit de fichiers Nix que l'on place dans le dossier `~/.config/nixpkgs/overlays/`. Ces overlays sont alors appliqués automatiquement sur la logithèque.

Par exemple, si on ajoute le fichier `~/.config/nixpkgs/overlays/monOverlay1.nix` suivant, on modifie le paquet `boost` de la logithèque et on y ajoute un paquet `monAppli` :

```nix
self: super: {

  boost = super.boost.override {
    python = self.python3;
  };

  monAppli = super.callPackage ./pkgs/monAppli.nix {};

}
```

Dans cet overlay, `self` et `super` sont les paramètres de la fonction à appliquer sur la logithèque d'entrée; `super` est la version initiale de la logithèque et `self` la version modifiée.

Ces modifications seront alors automatiquement appliquées, par exemple si on installe un de ces paquets ou si on lance un nix-shell qui les utilisent.

Les overlays sont détaillés dans le [manuel de nixpkgs](https://nixos.org/nixpkgs/manual/#chap-overlays) et dans le [wiki nixos](https://nixos.wiki/wiki/Overlays).

## Exemple avec GNU Nano

Prenons le paquet GNU Nano et modifions-le grâce aux overlays Nix.

### Le paquet de base

Ici, le paquet de base de GNU nano est la version 4.4 compilée avec les options `--disable-libmagic` et `--enable-utf8`.

```sh
$ nano --version
 GNU nano, version 4.4
 (C) 1999-2011, 2013-2019 Free Software Foundation, Inc.
 (C) 2014-2019 les contributeurs de nano
 Adr. él. : nano@nano-editor.org	Site : http://nano-editor.org/
 Compilé avec les options : --disable-libmagic --enable-utf8
```

### Paramétrer le paquet de base

Les paquets Nix peuvent avoir des paramètres. Par exemple, le [paquet nano de base](https://github.com/NixOS/nixpkgs/blob/master/pkgs/applications/editors/nano/default.nix) contient le paramètre `enableNls ? true`, qui permet de régler l'option de compilation `nls` et est activée par défaut.  Si on veut créer un paquet `nano-no-nls` qui reprend ce paquet mais en désactivant l'option `nls`, on peut ajouter le fichier `~/.config/nixpkgs/overlays/nano-no-nls.nix` suivant :

```nix
self: super: {

  nano-no-nls = super.nano.override {
    enableNls = false;
  };

}
```

On peut alors installer notre nouveau paquet `nano-no-nls` (qui sera compilé automatiquement) et vérifier que `nano` est désormais notre version personnalisée :

```sh
$ nix-env -iA nixos.nano-no-nls
installing 'nano-4.4'
these derivations will be built:
  /nix/store/6pg25sqj2vv2jq8dn00ajbc9xx2s96r3-nano-4.4.drv
...

$ nano --version
 GNU nano, version 4.4
 (C) 1999-2011, 2013-2019 Free Software Foundation, Inc.
 (C) 2014-2019 the contributors to nano
 Email: nano@nano-editor.org	Web: https://nano-editor.org/
 Compiled options: --disable-libmagic --disable-nls --enable-utf8
```

### Redéfinir le paquet de base

Nix permet de modifier les paquets encore plus profondément. Par exemple, on peut modifier le paquet existant `nano` de façon à utiliser la version 4.5 du code source de nano :

```nix
self: super: {

  nano = super.nano.overrideAttrs (oldAttrs: rec {
    pname = oldAttrs.pname;
    version = "4.5";
    src = super.fetchurl {
      url = "mirror://gnu/nano/${pname}-${version}.tar.xz";
      sha256 = "0czmz1yq8s5qcxcmfjdxzg9nkhbmlc9q1nz04jvf57fdbs7w7mfy";
    };
  });

}
```

Cet overlays reprend certains attributs du paquet initial (`pname`) et en modifie certains autres (`version`, `src`).

Si on installe le paquet `nano`, l'overlay est appliqué et on se retrouve bien avec la version 4.5 :

```sh
$ nix-env -iA nixos.nano
replacing old 'nano-4.4'
installing 'nano-4.5'
these derivations will be built:
  /nix/store/cg60jqfw8fk4fkkamvjrhhkkas79z0w2-nano-4.5.drv
these paths will be fetched (1.42 MiB download, 1.42 MiB unpacked):
  /nix/store/pbs1pf1vsk4zx6zis9a352j9lz88jrx2-nano-4.5.tar.xz
...

$ nano --version
 GNU nano, version 4.5
 (C) 1999-2011, 2013-2019 Free Software Foundation, Inc.
 (C) 2014-2019 les contributeurs de nano
 Adr. él. : nano@nano-editor.org	Site : http://nano-editor.org/
 Compilé avec les options : --disable-libmagic --enable-utf8
```

### Mise en cache des paquets recompilés, avec cachix

Les overlays sont compatibles avec cachix, le service cloud de cache binaire.  Par exemple, si on veut mettre en cache notre paquet `nano 4.5` dans le dépôt de cache `nokomprendo`, on utilise la commande habituelle `cachix push` : 

```sh
$ find /nix/store -maxdepth 1 -name "*nano-4.5*" -exec cachix push nokomprendo {} \;
pushing /nix/store/23wg5gf404zmnn8fixrg8rm38f06hqny-ncurses-6.1-20190112
pushing /nix/store/pnd2kl27sag76h23wa5kl95a76n3k9i3-glibc-2.27
pushing /nix/store/qq8nfcxvak3iic0dvw1dfk3jnr9acv4m-nano-4.5
...
```

Pour tester notre cache, on peut annuler l'installation de `nano-4.5` et nettoyer les fichiers correspondants dans le `/nix/store` :

```sh
$ nix-env --rollback 
switching from generation 32 to 31

$ nix-env --delete-generations 32
removing generation 32

$ find /nix/store/ -maxdepth 1 -name "*nano-4.5*" -exec nix-store --delete {} \+
finding garbage collector roots...
deleting '/nix/store/cg60jqfw8fk4fkkamvjrhhkkas79z0w2-nano-4.5.drv'
deleting '/nix/store/9csadwrlh6yvxl55143y55i8jx893k09-nano-4.5.tar.xz.drv'
deleting '/nix/store/jg6kivw3b0c2dak03ylvsn72jnd38774-nano-4.5-info'
deleting '/nix/store/pbs1pf1vsk4zx6zis9a352j9lz88jrx2-nano-4.5.tar.xz'
deleting '/nix/store/s9y641283gbkv1wpsnl5yr4b8vqsja0r-user-environment'
deleting '/nix/store/yjh8mcpxbyq8qbb6ndw6jizd3knfsclg-user-environment.drv'
deleting '/nix/store/y0856674ymjdzp277scsvg3qa084381i-env-manifest.nix'
deleting '/nix/store/qq8nfcxvak3iic0dvw1dfk3jnr9acv4m-nano-4.5'
deleting '/nix/store/trash'
deleting unused links...
note: currently hard linking saves -0.00 MiB
8 store paths deleted, 3.68 MiB freed
```

Désormais, si on active le cache et qu'on lance l'installation de notre paquet `nano-4.5`, Nix ne recompile pas le paquet mais télécharge directement le binaire depuis le cache :

```sh
$ cachix use nokomprendo
Configured https://nokomprendo.cachix.org binary cache in /home/nokomprendo/.config/nix/nix.conf

$ nix-env -iA nixos.nano
installing 'nano-4.5'
these paths will be fetched (0.51 MiB download, 2.19 MiB unpacked):
  /nix/store/jg6kivw3b0c2dak03ylvsn72jnd38774-nano-4.5-info
  /nix/store/qq8nfcxvak3iic0dvw1dfk3jnr9acv4m-nano-4.5
copying path '/nix/store/jg6kivw3b0c2dak03ylvsn72jnd38774-nano-4.5-info' from 'https://nokomprendo.cachix.org'...
copying path '/nix/store/qq8nfcxvak3iic0dvw1dfk3jnr9acv4m-nano-4.5' from 'https://nokomprendo.cachix.org'...
building '/nix/store/yjh8mcpxbyq8qbb6ndw6jizd3knfsclg-user-environment.drv'...
created 1185 symlinks in user environment

$ nano --version
 GNU nano, version 4.5
 (C) 1999-2011, 2013-2019 Free Software Foundation, Inc.
 (C) 2014-2019 les contributeurs de nano
 Adr. él. : nano@nano-editor.org	Site : http://nano-editor.org/
 Compilé avec les options : --disable-libmagic --enable-utf8
```

# Home-manager

[Home-manager](https://github.com/rycee/home-manager) est un outil qui permet de gérer son environnement utilisateur : environnement de bureau, thème de fenêtre, thème d'icones, logiciels, paramètres des logiciels, services utilisateur, "dot files"...  

Home-manager est prévu pour fonctionner sur la distribution linux [NixOS](https://nixos.org/nixos/) et utilise le gestionnaire de paquets [Nix](https://nixos.org/nix/).

## Présentation de home-manager

Pour faire simple, le but de home-manager est de fournir l'équivalent du fichier `/etc/nixos/configuration.nix` mais pour l'environnement utilisateur (au lieu de l'environnement système). Plus précisemment, l'utilisateur décrit sa configuration via un fichier `~/.config/nixpkgs/home.nix` et lance une commande `home-manager` pour construire et installer la configuration correspondante. 

Home-manager est capable de gérer finement l'environnement logiciel. Il permet non seulement d'indiquer l'environnement de bureau, les logiciels et les services à installer mais également de paramètrer tous ces éléments. Par exemple, home-manager peut configurer le `user.name` de `git`, les extensions de `firefox`, la police de caractères de `vscode`...

**Attention**, home-manager n'est pas un outil officiel de Nix. Il est encore en développement, et donc incomplet. Enfin, il est prévu pour NixOS; son fonctionnement sur une autre distribution n'est pas garanti.

Voir le [site officiel de home-manager](https://github.com/rycee/home-manager) et le [wiki NixOS](https://nixos.wiki/wiki/Home_Manager).

## Installation

Home-manager est fourni dans les dépôts de paquets de Nix. Il est donc très simple à installer :

```sh
$ nix-env -iA nixos.home-manager
```

## Premier exemple de configuration utilisateur

Le point d'entrée d'une configuration home-manager est le fichier `~/.config/nixpkgs/home.nix`. Par exemple, le fichier suivant installe les logiciels `geany`, `meld` et `vlc`, et configure le clavier en français bépo.

```nix
{ pkgs, ... }: {

  home.packages = with pkgs; [
    geany
    meld
    vlc
  ];

  home.keyboard = {
    layout = "fr";
    variant = "bepo";
  };

}
```

Home-manager est documenté dans les pages man. Pour connaitre les options de configuration disponibles, il suffit de lancer :

```sh
$ man home-configuration.nix
```

Enfin, pour installer ou mettre à jour la configuration, on lance la commande :

```sh
$ home-manager switch
```

## Séparer les fichiers de configuration

Pour éviter d'avoir un gros fichier `home.nix` difficile à lire, on peut le découper en plusieurs fichiers. Par exemple, on peut mettre les logiciels à installer dans un fichier `~/.config/nixpkgs/packages.nix` :

```nix
{ pkgs, ... }: {

  home.packages = with pkgs; [
    geany
    meld
    vlc
  ];

}
```

et importer ce fichier dans le `~/.config/nixpkgs/home.nix` : 

```nix
{ pkgs, ... }: {

  imports = [
    ./packages.nix
  ];

  home.keyboard = {
    layout = "fr";
    variant = "bepo";
  };
}
```

## Configurer des logiciels utilisateurs

Home-manager permet de régler les paramètres de certains programmes. Par exemple, on peut ajouter le code suivant dans le `home.nix` pour installer `firefox` et `bash` (avec des alias shell) :

```nix
  programs = {

    firefox.enable = true;

    bash = {
      enable = true;
      shellAliases = {
        ll = "ls -lh";
        la = "ls -a";
      };
    };

  };
```

Autre exemple, on peut configurer `git` dans un fichier `git.nix`, que l'on importera dans `home.nix` :

```nix
{ pkgs, ... }: {

  programs.git = {
    enable = true;
    userName = "nokomprendo";
    userEmail = "nokomprendo@example.com";

    ignores =  [
      "*~"
      "*.swp"
    ];

}
```

Après mise-à-jour, le client `git` est configuré :

```sh
$ home-manager switch
...

$ git config --get user.name
nokomprendo
```

## Configurer l'environnement de bureau

Home-manager peut configurer le thème de fenêtre et le thème d'icones. Par exemple, pour un thème "dark" :

```nix
  gtk = {
    enable = true;
    iconTheme = {
      name = "Adwaita";
      package = pkgs.gnome3.adwaita-icon-theme;
    };
    theme = {
      name = "Shades-of-gray";
      package = pkgs.shades-of-gray-theme;
    };
  };

  qt = {
    enable = true;
    platformTheme = "gtk";
  };
```

Home-manager peut également configurer finement certains environnements de bureau.  Par exemple, avec [i3](https://i3wm.org/) :

```nix
  xsession.enable = true;

  xsession.windowManager.i3 = {
    enable = true;
    config = let mod = "Mod4"; in {
      fonts = [ "DejaVu Sans 12" ];
      modifier = mod;
      keybindings = pkgs.lib.mkOptionDefault {
        "${mod}+m" = "exec ${pkgs.i3lock}/bin/i3lock -n -c 000000";
      };
    };
  };
```

Penser, dans ce cas, à activer le paquet `dconf` du service `dbus`, dans `/etc/nixos/configuration.nix` :

```nix
  services.dbus.packages = [ pkgs.gnome3.dconf ];
```

## Gérer des "dot files"

Home-manager suit le fonctionnement habituel de Nix. Par exemple, quand on configure `bash` dans le `home.nix` (cf précédemment), Nix crée un fichier de configuration `.bashrc` dans le `/nix/store` et ajoute un lien symbolique dans le dossier utilisateur :

```sh
$ ll ~/.bashrc 
lrwxrwxrwx 1 toto users 70 11 nov.  15:48 /home/toto/.bashrc 
  -> /nix/store/z2arwbwyhvvwhy2caazlxasw5jnscyg3-home-manager-files/.bashrc
```

Cependant, home-manager peut également gérer des "dot files" classiques.  Par exemple, on peut centraliser des "dot files" dans le dossier `~/.config/nixpkgs/` et demander à home-manager de les gérer, via le `home.nix` (il va alors les copier dans le `/nix/store` et créer les liens symboliques) :

```nix
  home.file.".i3status.conf".source = ./i3status.conf;
```

## Utiliser les overlays et cachix

Comme home-manager est basé sur Nix, on peut utiliser les outils Nix classiques, comme les overlays ou cachix.

Par exemple, on peut empaqueter le plugin Vim `minibufexpl` en ajoutant un fichier dans le dossier `~/.config/nixpkgs/overlays/` :

```nix
self: super: {
  vimPlugins = super.vimPlugins // {
    minibufexpl = super.vimUtils.buildVimPluginFrom2Nix { 
      name = "minibufexpl.vim-2013-06-16";
      src = self.fetchgit {
        url = "https://github.com/fholgado/minibufexpl.vim";
        rev = "ad72976ca3df4585d49aa296799f14f3b34cf953";
        sha256 = "1bfq8mnjyw43dzav8v1wcm4rrr2ms38vq8pa290ig06247w7s7ng";
      };
      dependencies = [];
    };
  };
}
```

On peut ensuite utiliser ce plugin dans notre configuration Vim. Si on a déjà empaqueté ce plugin sur une autre machine et envoyé le paquet binaire sur un dépôt cachix, on peut utiliser ce cache et éviter de reconstruire le paquet :

```sh
$ cachix use nokomprendo
Configured https://nokomprendo.cachix.org binary cache in /home/toto/.config/nix/nix.conf

$ home-manager switch
...
copying path '/nix/store/2pkqy6nv5vmqq6cw7zhyd44qw7vigg4l-vimplugin-minibufexpl.vim-2013-06-16' 
  from 'https://nokomprendo.cachix.org'...
...
```

# Conclusion

Nix est un gestionnaire de paquets suffisamment solide et évolutif pour servir de base à de nombreux outils, ciblant du paquet logiciel au parc de machines en passant par les services. Les trois outils présentés dans cette dépêche sont plutôt destinés à gérer un environnement utilisateur.

Cachix permet de mettre en cache des paquets binaires Nix pour pouvoir les récupérer ultérieurement sans avoir à les recompiler.  Il est très pratique pour récupérer plus rapidement des paquets personnels ou des paquets officiels modifiés, par exemple pour mettre en place un environnement logiciel local ou pour optimiser un processus d'intégration continue.  Dans un environnement Nix, cachix est facile à mettre en place et à utiliser.  Enfin, il est assez complémentaire avec Hydra, qui permet de dérouler une intégration continue et de générer des paquets binaires mais qui est plus difficile à mettre en place.

Avec les overlays, Nix permet de modifier très facilement la logithèque (ajouter des nouveaux paquets, modifier les paquets existants...). Pour cela, il suffit d'ajouter nos fichiers d'overlays dans le dossier `~/.config/nixpkgs/overlays/` et les modifications correspondantes seront automatiquement appliquées.  Les overlays s'intègrent complétement à l'écosystème Nix; on peut notamment mettre en cache les binaires générés et les réutiliser sur une autre machine sans avoir à les recompiler.

Enfin, Home-manager est un outil basé sur Nix et qui permet de configurer son environnement utilisateur. Le projet est encore en développement mais il est déjà très exploitable, s'intègre bien avec l'écosystème Nix et apporte des fonctionnalités intéressantes : spécifier et paramétrer l'environnement de bureau, les thèmes et les logiciels, gérer des "dot files" classiques, utiliser des overlays, utiliser cachix, etc.
