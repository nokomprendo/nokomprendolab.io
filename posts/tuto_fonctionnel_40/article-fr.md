---
title: Gérer son environnement utilisateur NixOS, avec Home-manager
---

[Home-manager](https://github.com/rycee/home-manager) est un outil qui permet de gérer son environnement utilisateur : environnement de bureau, thème de fenêtre, thème d'icones, logiciels, paramètres des logiciels, services utilisateur, "dot files"...  

Home-manager est prévu pour fonctionner sur la distribution linux [NixOS](https://nixos.org/nixos/) et utilise le gestionnaire de paquets [Nix](https://nixos.org/nix/).

[code source](https://gitlab.com/nokomprendo/nokomprendo.gitlab.io/tree/master/posts/tuto_fonctionnel_40) - [vidéo youtube](https://youtu.be/yfAuVFbu_yA) - [vidéo peertube](https://peertube.fr/videos/watch/5dcd4e08-fc52-4faf-a63f-900ba84872bc)


# Présentation de home-manager

Pour faire simple, le but de home-manager est de fournir l'équivalent du fichier `/etc/nixos/configuration.nix` mais pour l'environnement utilisateur (au lieu de l'environnement système). Plus précisemment, l'utilisateur décrit sa configuration via un fichier `~/.config/nixpkgs/home.nix` et lance une commande `home-manager` pour construire et installer la configuration correspondante. 

Home-manager est capable de gérer finement l'environnement logiciel. Il permet non seulement d'indiquer l'environnement de bureau, les logiciels et les services à installer mais également de paramètrer tous ces éléments. Par exemple, home-manager peut configurer le `user.name` de `git`, les extensions de `firefox`, la police de caractères de `vscode`...

**Attention**, home-manager n'est pas un outil officiel de Nix. Il est encore en développement, et donc incomplet. Enfin, il est prévu pour NixOS; son fonctionnement sur une autre distribution n'est pas garanti.

Voir le [site officiel de home-manager](https://github.com/rycee/home-manager) et le [wiki NixOS](https://nixos.wiki/wiki/Home_Manager).


# Installation

Home-manager est fourni dans les dépôts de paquets de Nix. Il est donc très simple à installer :

```sh
$ nix-env -iA nixos.home-manager
```

# Premier exemple de configuration utilisateur

Le point d'entrée d'une configuration home-manager est le fichier `~/.config/nixpkgs/home.nix`. Par exemple, le fichier suivant installe les logiciels `geany`, `meld` et `vlc`, et configure le clavier en français bépo.

```nix
{ pkgs, ... }: {

  home.packages = with pkgs; [
    geany
    meld
    vlc
  ];

  home.keyboard = {
    layout = "fr";
    variant = "bepo";
  };

}
```

Home-manager est documenté dans les pages man. Pour connaitre les options de configuration disponibles, il suffit de lancer :

```sh
$ man home-configuration.nix
```

Enfin, pour installer ou mettre à jour la configuration, on lance la commande :

```sh
$ home-manager switch
```


# Séparer les fichiers de configuration

Pour éviter d'avoir un gros fichier `home.nix` difficile à lire, on peut le découper en plusieurs fichiers. Par exemple, on peut mettre les logiciels à installer dans un fichier `~/.config/nixpkgs/packages.nix` :

```nix
{ pkgs, ... }: {

  home.packages = with pkgs; [
    geany
    meld
    vlc
  ];

}
```

et importer ce fichier dans le `~/.config/nixpkgs/home.nix` : 

```nix
{ pkgs, ... }: {

  imports = [
    ./packages.nix
  ];

  home.keyboard = {
    layout = "fr";
    variant = "bepo";
  };
}
```



# Configurer des logiciels utilisateurs

Home-manager permet de régler les paramètres de certains programmes. Par exemple, on peut ajouter le code suivant dans le `home.nix` pour installer `firefox` et `bash` (avec des alias shell) :

```nix
  programs = {

    firefox.enable = true;

    bash = {
      enable = true;
      shellAliases = {
        ll = "ls -lh";
        la = "ls -a";
      };
    };

  };
```

Autre exemple, on peut configurer `git` dans un fichier `git.nix`, que l'on importera dans `home.nix` :

```nix
{ pkgs, ... }: {

  programs.git = {
    enable = true;
    userName = "nokomprendo";
    userEmail = "nokomprendo@example.com";

    ignores =  [
      "*~"
      "*.swp"
    ];

}
```

Après mise-à-jour, le client `git` est configuré :

```sh
$ home-manager switch
...

$ git config --get user.name
nokomprendo
```



# Configurer l'environnement de bureau

Home-manager peut configurer le thème de fenêtre et le thème d'icones. Par exemple, pour un thème "dark" :

```nix
  gtk = {
    enable = true;
    iconTheme = {
      name = "Adwaita";
      package = pkgs.gnome3.adwaita-icon-theme;
    };
    theme = {
      name = "Shades-of-gray";
      package = pkgs.shades-of-gray-theme;
    };
  };

  qt = {
    enable = true;
    platformTheme = "gtk";
  };
```

Home-manager peut également configurer finement certains environnements de bureau.  Par exemple, avec [i3](https://i3wm.org/) :

```nix
  xsession.enable = true;

  xsession.windowManager.i3 = {
    enable = true;
    config = let mod = "Mod4"; in {
      fonts = [ "DejaVu Sans 12" ];
      modifier = mod;
      keybindings = pkgs.lib.mkOptionDefault {
        "${mod}+m" = "exec ${pkgs.i3lock}/bin/i3lock -n -c 000000";
      };
    };
  };
```

Penser, dans ce cas, à activer le paquet `dconf` du service `dbus`, dans `/etc/nixos/configuration.nix` :

```nix
  services.dbus.packages = [ pkgs.gnome3.dconf ];
```




# Gérer des "dot files"

Home-manager suit le fonctionnement habituel de Nix. Par exemple, quand on configure `bash` dans le `home.nix` (cf précédemment), Nix crée un fichier de configuration `.bashrc` dans le `/nix/store` et ajoute un lien symbolique dans le dossier utilisateur :

```sh
$ ll ~/.bashrc 
lrwxrwxrwx 1 toto users 70 11 nov.  15:48 /home/toto/.bashrc 
  -> /nix/store/z2arwbwyhvvwhy2caazlxasw5jnscyg3-home-manager-files/.bashrc
```

Cependant, home-manager peut également gérer des "dot files" classiques.  Par exemple, on peut centraliser des "dot files" dans le dossier `~/.config/nixpkgs/` et demander à home-manager de les gérer, via le `home.nix` (il va alors les copier dans le `/nix/store` et créer les liens symboliques) :

```nix
  home.file.".i3status.conf".source = ./i3status.conf;
```



# Utiliser les overlays et cachix

Comme home-manager est basé sur Nix, on peut utiliser les outils Nix classiques, comme les overlays ou cachix.

Par exemple, on peut empaqueter le plugin Vim `minibufexpl` en ajoutant un fichier dans le dossier `~/.config/nixpkgs/overlays/` :

```nix
self: super: {
  vimPlugins = super.vimPlugins // {
    minibufexpl = super.vimUtils.buildVimPluginFrom2Nix { 
      name = "minibufexpl.vim-2013-06-16";
      src = self.fetchgit {
        url = "https://github.com/fholgado/minibufexpl.vim";
        rev = "ad72976ca3df4585d49aa296799f14f3b34cf953";
        sha256 = "1bfq8mnjyw43dzav8v1wcm4rrr2ms38vq8pa290ig06247w7s7ng";
      };
      dependencies = [];
    };
  };
}
```

On peut ensuite utiliser ce plugin dans notre configuration Vim. Si on a déjà empaqueté ce plugin sur une autre machine et envoyé le paquet binaire sur un dépôt cachix, on peut utiliser ce cache et éviter de reconstruire le paquet :

```sh
$ cachix use nokomprendo
Configured https://nokomprendo.cachix.org binary cache in /home/toto/.config/nix/nix.conf

$ home-manager switch
...
copying path '/nix/store/2pkqy6nv5vmqq6cw7zhyd44qw7vigg4l-vimplugin-minibufexpl.vim-2013-06-16' 
  from 'https://nokomprendo.cachix.org'...
...
```


# Conclusion

Home-manager est un outil basé sur Nix et qui permet de configurer son environnement utilisateur. Le projet est encore en développement mais il est déjà très exploitable, s'intègre bien avec l'écosystème Nix et apporte des fonctionnalités intéressantes : spécifier et paramétrer l'environnement de bureau, les thèmes et les logiciels, gérer des "dot files" classiques, utiliser des overlays, utiliser cachix, etc.

