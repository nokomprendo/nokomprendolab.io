{ pkgs, ... }: {

  home.packages = with pkgs; [
    geany
    meld
    vlc
  ];

  home.keyboard = {
    layout = "fr";
    variant = "bepo";
  };

}

