{ pkgs, ... }: {

  programs.vim = {

    enable = true;

    plugins = with pkgs.vimPlugins; [
      airline
      minibufexpl
      vim-colorschemes
    ];

    extraConfig = builtins.readFile ./vimrc;
  };

}

