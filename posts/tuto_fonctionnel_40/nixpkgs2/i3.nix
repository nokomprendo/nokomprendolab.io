{ pkgs, ... }: {

  xsession.windowManager.i3 = {

    enable = true;

    config = let mod = "Mod4"; in {

      fonts = [ "DejaVu Sans 12" ];

      bars = [{
        fonts = [ "DejaVu Sans 12" ];
      }];

      modifier = mod;

      workspaceLayout = "tabbed";

      keybindings = pkgs.lib.mkOptionDefault {

        "${mod}+m" = "exec ${pkgs.i3lock}/bin/i3lock -n -c 000000";

        "${mod}+c" = "split h";

        "${mod}+h" = "focus left";
        "${mod}+j" = "focus down";
        "${mod}+k" = "focus up";
        "${mod}+l" = "focus right";

        "${mod}+Shift+h" = "move left";
        "${mod}+Shift+j" = "move down";
        "${mod}+Shift+k" = "move up";
        "${mod}+Shift+l" = "move right";

      };

    };

  };

}

