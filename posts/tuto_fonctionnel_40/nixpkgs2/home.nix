{ pkgs, ... }: {

  imports = [
    ./packages.nix
    # ./git.nix
    # ./i3.nix
    # ./vim.nix
  ];

  home.keyboard = {
    layout = "fr";
    variant = "bepo";
  };



  # programs = {

  #   bash = {
  #     enable = true;
  #     shellAliases = {
  #       ll = "ls -lh";
  #       la = "ls -a";
  #     };
  #   };

  #   firefox.enable = true;

  #   urxvt = {
  #     enable = true;
  #     fonts = ["xft:DejaVu Sans Mono:size=13"];
  #       extraConfig = {
  #         reverseVideo = true;
  #         termName = "xterm-256color";
  #       };
  #     };

  # };

  # home.sessionVariables = {
  #   EDITOR = "nano";
  #   LS_COLORS = "rs=0:di=01;33:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32";
  # };



  # xsession.enable = true;

  # gtk = {
  #   enable = true;
  #   iconTheme = {
  #     name = "Adwaita";
  #     package = pkgs.gnome3.adwaita-icon-theme;
  #   };
  #   theme = {
  #     name = "Shades-of-gray";
  #     package = pkgs.shades-of-gray-theme;
  #   };
  # };

  # qt = {
  #   enable = true;
  #   platformTheme = "gtk";
  # };



  # home.file.".i3status.conf".source = ./i3status.conf;

}

