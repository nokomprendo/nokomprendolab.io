{ pkgs, ... }: {

  home.packages = with pkgs; [
    geany
    meld
    vlc
  ];

}

