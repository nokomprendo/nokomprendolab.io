{ pkgs, ... }: {

  programs.git = {
    enable = true;
    userName = "nokomprendo";
    userEmail = "nokomprendo@tutanota.com";

    ignores =  [
      "*~"
      "*.swp"
    ];

    extraConfig = {
      credential = {
        helper = "cache --timeout=10800";
      };
    };
  };

}

