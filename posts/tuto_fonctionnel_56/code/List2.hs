
data List a
    = Nil
    | Cons a (List a)
    deriving (Show)

instance Functor List where
    -- fmap :: (a -> b) -> f a -> f b
    -- fmap :: (a -> b) -> List a -> List b
    fmap f Nil = Nil
    fmap f (Cons x xs) = Cons (f x) (fmap f xs)

concatList :: List a -> List a -> List a
concatList (Cons x xs) ys = Cons x (concatList xs ys)
concatList Nil ys = ys

instance Applicative List where
    -- pure :: a -> f a
    -- pure :: a -> List a
    pure x = Cons x Nil

    -- (<*>) :: f (a -> b) -> f a -> f b
    -- (<*>) :: List (a -> b) -> List a -> List b
    (Cons f fs) <*> xs =
        let ys = fs <*> xs
        in concatList (f <$> xs) ys
    Nil <*> _ = Nil

instance Monad List where
    -- (>>=) :: m a -> (a -> m b) -> m b
    -- (>>=) :: List a -> (a -> List b) -> List b
    (Cons x xs) >>= f = concatList (f x) (xs >>= f)
    Nil >>= _ = Nil


main :: IO ()
main = do

    -- print ((*3) <$> [1,2])
    print ((*3) <$> Cons 1 (Cons 2 Nil))

    -- print ((*) <$> [1,2] <*> [3])
    print ((*) <$> Cons 1 (Cons 2 Nil) <*> Cons 3 Nil)

    -- print ((*) <$> [1,2] <*> [3,4])
    print ((*) <$> Cons 1 (Cons 2 Nil) <*> Cons 3 (Cons 4 Nil))

    -- print $ do
    --     x <- [1,2]
    --     y <- [3,4]
    --     return (x*y)

    print (Cons 1 (Cons 2 Nil) >>= \x -> Cons 3 (Cons 4 Nil) >>= \y -> return (x*y))

    print $ do
        x <- Cons 1 (Cons 2 Nil)
        y <- Cons 3 (Cons 4 Nil)
        return (x*y)

