
mul2 :: Num a => [a] -> [a]
mul2 xs = map (*2) xs

mul2F :: (Num a, Functor f) => f a -> f a
mul2F xs = fmap (*2) xs

main :: IO ()
main = do
    print $ mul2 [1,2]
    print $ mul2F [1,2]
    print $ mul2F (Just 21)

