
main :: IO ()
main = do

    print $ (*21) <$> Just 2 

    print $ (*) <$> Just 2 <*> pure 21

    print $ (*) <$> Nothing <*> return 21

    print $ do { x <- Just 2; y <- Just 21; return (x*y) }

    print $ do
        x <- Just 2
        y <- Just 21
        return (x*y)

    print $ do
        x <- Nothing
        y <- Just 21
        return (x*y)

    print (Just 2 >>= \x -> Just 21 >>= \y -> return (x*y))

    let f Nothing = Nothing
        f (Just x) = Just (x*3)

    print $ f <$> (Just <$> [1,2])
    
    print $ do  -- Maybe Int
        y <- do -- [Int]
            x <- [1,2]
            return (Just x)
        return (f y)

