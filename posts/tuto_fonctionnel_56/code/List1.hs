
main :: IO ()
main = do

    print ((*3) <$> [1,2])

    print ((*) <$> [1,2] <*> [3])

    print ((*) <$> [1,2] <*> [3,4])

    print $ do { x <- [1,2]; y <- [3,4]; return (x*y) }

    print $ do
        x <- [1,2]
        y <- [3,4]
        return (x*y)

    print ([1,2] >>= \x -> [2,3] >>= \y -> return (x*y))

    print $ [ x*y | x <- [1,2], y <- [3,4] ]

