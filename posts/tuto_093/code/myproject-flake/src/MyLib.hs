{-# Language OverloadedStrings #-}

module MyLib (myPage, render) where

import Data.Text.Lazy as T
import Lucid

render :: Html () -> T.Text
render = renderText

myPage :: Html ()
myPage = h1_ "Hello World!"

