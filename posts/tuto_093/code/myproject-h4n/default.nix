{ pkgs ? import <nixpkgs> {} }:

let
  ghc = pkgs.haskellPackages;             # default version
  # ghc = pkgs.haskell.packages.ghc925;   # specific version

in ghc.developPackage {
  root = ./.;
  modifier = drv:
  pkgs.haskell.lib.addBuildTools drv (with ghc; [
    cabal-install
  ]);
}

