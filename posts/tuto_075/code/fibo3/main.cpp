
#include <native.hpp>
#include <binding.hpp>
#include <iostream>

int main() {

    fibohsInit();
    for (int i=0; i<=5; ++i)
        std::cout << "myfibo(" << i << ") = " << myfibo(i) << std::endl;
    fibohsExit();

    std::cout << "mul2(21) = " << mul2(21) << std::endl;

    return 0;
}

