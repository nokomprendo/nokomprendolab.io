{ pkgs ? import <nixpkgs> {} }:
let drv = pkgs.haskellPackages.callCabal2nix "fibohs" ./. {};
in with pkgs; stdenv.mkDerivation {
  name = "fibohs";
  src = ./.;
  installPhase = ''
    mkdir -p $out/{include,lib}
    cp `find ${drv} -name "*.so"` $out/lib/
    cp `find ${drv} -name "*.hpp"` $out/include/
  '';
}

