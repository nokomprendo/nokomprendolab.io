
#include "binding.hpp"

#include "HsFFI.h"

void fibohsInit(void) {
    char *argv[] = { (char *)"+RTS", (char *)"-A32m", 0 };
    int argc = sizeof(argv)/sizeof(argv[0]) - 1;
    char **pargv = argv;
    hs_init(&argc, &pargv);
}

void fibohsExit(void) {
    hs_exit();
}

