module Fibo where

fibo :: Int -> Int
fibo = go 0 1
    where go x _ 0 = x
          go x y n = go (x+y) x (n-1)

