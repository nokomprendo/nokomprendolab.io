{-# LANGUAGE CApiFFI #-}
module Binding where

import Fibo
import Foreign.C.Types

myfibo :: CInt -> CInt
myfibo = fromIntegral . fibo . fromIntegral

foreign export capi myfibo :: CInt -> CInt

