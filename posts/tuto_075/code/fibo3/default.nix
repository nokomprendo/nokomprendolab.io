{ pkgs ? import <nixpkgs> {} }:
let fibohs = pkgs.callPackage ./fibohs/default.nix {};
in pkgs.stdenv.mkDerivation {
  name = "fibo3";
  src = ./.;
  buildInputs = with pkgs; [ cmake fibohs ];
}

