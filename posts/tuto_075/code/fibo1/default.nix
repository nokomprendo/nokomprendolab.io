{ pkgs ? import <nixpkgs> {} }:
let drv = pkgs.haskellPackages.callCabal2nix "fibo1" ./. {};
in if pkgs.lib.inNixShell then drv.env else drv

