{-# LANGUAGE CApiFFI #-}

import Foreign.C.Types

foreign import capi "fiboc.h fibo" c_fibo :: CInt -> CInt

myfibo :: Int -> Int
myfibo = fromIntegral . c_fibo . fromIntegral

main :: IO ()
main = print $ map myfibo [0 .. 10]

