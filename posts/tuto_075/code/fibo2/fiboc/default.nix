{ pkgs ? import <nixpkgs> {} }:
pkgs.stdenv.mkDerivation {
  name = "fiboc";
  src = ./.;
  buildInputs = with pkgs; [ cmake ];
}

