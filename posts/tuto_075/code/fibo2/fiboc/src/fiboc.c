#include <fiboc.h>

#include <assert.h>

int fibo(int n) {
    assert(n>=0);
    assert(n<=46);
    int x = 0;
    int y = 1;
    int i = n;
    while (i>0) {
        int tmp = x;
        x += y;
        y = tmp;
        --i;
    }
    return x;
}

