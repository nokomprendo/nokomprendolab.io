cmake_minimum_required( VERSION 3.0 )
project( fiboc )
include_directories( include )
add_library( fiboc SHARED src/fiboc.c )
set_target_properties( fiboc PROPERTIES PUBLIC_HEADER "include/fiboc.h" )
install( TARGETS fiboc LIBRARY DESTINATION lib PUBLIC_HEADER DESTINATION include )

