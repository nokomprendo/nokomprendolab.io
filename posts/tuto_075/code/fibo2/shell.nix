{ pkgs ? import <nixpkgs> {} }:
(pkgs.haskellPackages.callCabal2nix "fibo2" ./. {
  fiboc = (pkgs.callPackage ./fiboc/default.nix {});
}).env

