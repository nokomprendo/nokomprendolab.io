import Data.Char
import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char

type Parser = Parsec Void String

digitP :: Parser Char
digitP = satisfy isDigit

digitsP :: Parser String
digitsP = some digitP

intP :: Parser Int
intP = posIntP <|> negIntP
    where 
        posIntP = read <$> digitsP
        negIntP = do
                _ <- char '-'
                x <- digitsP
                return (- read x)

doubleP :: Parser Double
doubleP = 
    do  c0 <- char '-' <|> return ' '
        s1 <- digitsP
        s2 <- ((:) <$> char '.' <*> digitsP) <|> return ""
        return $ read (c0 : s1 ++ s2)

main :: IO ()
main = do
    print $ parseMaybe digitP "f"
    print $ parseMaybe digitP "4"
    print $ parseMaybe digitsP "42"
    print $ parseMaybe intP "42"
    print $ parseMaybe intP "-42"
    print $ parseMaybe intP "+42"
    print $ parseMaybe doubleP "42"
    print $ parseMaybe doubleP "-13.37"
    print $ parseMaybe doubleP "3.37"

