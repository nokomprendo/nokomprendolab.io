
main :: IO ()
main = do

    print ((*3) <$> Just 2)
    print ((*3) <$> Nothing)

    print ((*) <$> Just 2 <*> Just 3)
    print ((*) <$> Nothing <*> Just 3)

    print $ do
        x <- Just 2
        y <- Just 3
        return (x*y)

    print $ do
        x <- Nothing
        y <- Just 3
        return (x*y)

    print (Just 2 >>= \x -> Just 3 >>= \y -> return (x*y))

    print (Just 2 >>= return . (*3) >>= return . (+1))

