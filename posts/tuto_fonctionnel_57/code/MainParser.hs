import Control.Applicative
import Data.Char

import Parser

{-
digitP :: Parser Char
digitP = do
    x <- itemP
    if isDigit p then return x else empty
-}

satisfy :: (Char -> Bool) -> Parser Char
satisfy p = do
    x <- itemP
    if p x then return x else empty

symbolP :: Char -> Parser Char
symbolP c = satisfy (==c)

digitP :: Parser Char
digitP = satisfy isDigit

digitsP :: Parser String
digitsP = some digitP

intP :: Parser Int
intP = posIntP <|> negIntP
    where 
        posIntP = read <$> digitsP
        negIntP = do
                _ <- symbolP '-'
                x <- digitsP
                return (- read x)


doubleP :: Parser Double
doubleP = 
    do  c0 <- symbolP '-' <|> return ' '
        s1 <- digitsP
        s2 <- ((:) <$> symbolP '.' <*> digitsP) <|> return ""
        return $ read (c0 : s1 ++ s2)

main :: IO ()
main = do
    print $ fst <$> runParser digitP "f"
    print $ fst <$> runParser digitP "4"
    print $ fst <$> runParser digitsP "42"
    print $ fst <$> runParser intP "42"
    print $ fst <$> runParser intP "-42"
    print $ fst <$> runParser intP "+42"
    print $ fst <$> runParser doubleP "42"
    print $ fst <$> runParser doubleP "-13.37"
    print $ fst <$> runParser doubleP "3.37"

