{-# LANGUAGE LambdaCase #-}

module Parser where

import Control.Applicative

newtype Parser v = Parser { runParser :: String -> Maybe (v, String) }


itemP :: Parser Char 
itemP = Parser $ \case "" -> Nothing
                       (x:xs) -> Just (x, xs)

{-
itemP :: Parser Char 
itemP = Parser p
    where p "" = Nothing
          p (x:xs) = Just (x, xs)
-}


instance Functor Parser where

    fmap f (Parser p) = 
        Parser $ \s0 -> do
            (x, s1) <- p s0
            return (f x, s1)

{-
isf :: Parser Bool
isf = (=='f') <$> itemP
-}

instance Applicative Parser where

    pure x = Parser $ \s -> Just (x, s)

    (Parser p1) <*> (Parser p2) =
        Parser $ \s0 -> do
            (f, s1) <- p1 s0
            (x, s2) <- p2 s1
            return (f x, s2)


instance Monad Parser where

    (Parser p) >>= f = 
        Parser $ \s0 -> do
            (x1, s1) <- p s0
            (x2, s2) <- runParser (f x1) s1
            return (x2, s2)


instance Alternative Parser where

    empty = Parser $ const empty

    (Parser p1) <|> (Parser p2)
        = Parser $ \input -> p1 input <|> p2 input


