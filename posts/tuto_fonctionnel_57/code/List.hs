
main :: IO ()
main = do

    print ((*3) <$> [1,2])
    print ((*3) <$> [])

    print ((*) <$> [1,2] <*> [3])
    print ((*) <$> [] <*> [3])

    print $ do
        x <- [1,2]
        y <- [3,4]
        return (x*y)

    print $ do
        x <- []
        y <- [3,4]
        return (x*y)

    print ([1,2] >>= \x -> [2,3] >>= \y -> return (x*y))

    print ([1,2] >>= return . (*3) >>= return . (+1))

