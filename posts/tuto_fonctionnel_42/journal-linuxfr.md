title: Configurer VSCode pour Haskell (Debian/Nix/NixOS)

Comme beaucoup de langage de programmation, Haskell n'a pas d'environnement de développement officiel ni même consensuel. Cependant, il existe différentes configurations classiques : Emacs + Intero/Dante, Vim + Ghcid, IntelliJ-Haskell...

Depuis quelques temps, l'éditeur de texte Visual Studio Code propose un environnement intéressant pour développer en Haskell, notamment couplé à HIE et à Stack. Cette configuration apporte les principales fonctionnalités d'un IDE : coloration syntaxique, navigation de code, compilation, documentation, auto-complétion...  Cependant, l'installation de ces outils n'est pas complètement triviale. Ce journal explique comment la réaliser avec Debian, avec Debian + Nix et avec NixOS. 

[vidéo youtube](https://youtu.be/xYWEJ3eLxFE)

# Aperçu des outils

[Visual Studio Code](https://code.visualstudio.com/) est un éditeur de texte extensible, supporté par Microsoft. VSCode est un projet open-source mais on installe généralement un binaire non-libre ([VS Codium](https://vscodium.com/) fournit des binaires sous licence libre, selon le même principe que Chrome/Chromium).

Le [Language Server Protocol (LSP)](https://microsoft.github.io/language-server-protocol/overview)), également supporté par Microsoft, est un protocole permettant de réaliser plus facilement des outils de développement (navigation de code, auto-complétion...).

[Haskell IDE Engine (HIE)](https://github.com/haskell/haskell-ide-engine) est un backend implémentant LSP pour le langage Haskell.

[Haskell Language Server Client](https://github.com/alanz/vscode-hie-server) est une extension VSCode permettant d'utiliser HIE.

Enfin, [Stack](https://docs.haskellstack.org/en/stable/README/) est un outil permettant de définir et de construire un projet Haskell. Stack permet de faire fonctionner HIE et est utilisable depuis VSCode.

![](https://nokomprendo.gitlab.io/posts/tuto_fonctionnel_42/images/vscode.png)

# Installation avec Debian

L'installation sous Debian de la toolchain "VS code - HIE - client HIE - Stack" est assez pénible. Ces outils ne sont pas fournis par la logithèque (ou alors dans des versions obsolètes), il faut donc les installer "à la main" et parfois même les compiler (pour HIE, prévoir 4 Go de RAM et 1h de compilation).

- installer les dépendances :

```sh
sudo apt install libicu-dev libncurses-dev libgmp-dev libtinfo-dev 
```

- installer Stack :

```sh
curl -sSL https://get.haskellstack.org/ | sh
```

- [télécharger VSCode](https://code.visualstudio.com/docs/setup/linux) et l'installer :

```sh
sudo apt install ./<file>.deb
```

- compiler et installer HIE :

```sh
git clone https://github.com/haskell/haskell-ide-engine --recurse-submodules
cd haskell-ide-engine
./install.hs build
```

- lancer VSCode et installer l'extension ``Haskell Language Server``

- configurer l'extension ou le ``PATH`` (avec ``$HOME/.local/bin`` et ``$(stack path --compiler-bin)``)

Et avec un peu de chance, la toolchain fonctionne. Personnellement, je n'ai pas réussi à la faire fonctionner complètement...

# Debian + Nix

[Nix](https://nixos.org/nix/) est un gestionnaire de paquets utilisable sur les distributions linux. Nix permet également d'utiliser le système de cache [Cachix](https://cachix.org/), ce qui permet d'installer notre toolchain Haskell/VSCode en quelques minutes.

- installer les dépendances Debian :

```sh
sudo apt install curl rsync build-essential libgmp-dev
```

- installer et configurer Nix :

```sh
curl https://nixos.org/nix/install | sh
nix-channel --add https://nixos.org/channels/nixos-19.09 nixpkgs
nix-channel --update
echo ". $HOME/.nix-profile/etc/profile.d/nix.sh" >> ~/.bashrc
source ~/.bashrc
```

- autoriser les logiciels non-libres, dans ``~/.config/nixpkgs/config.nix`` :

```nix
{ allowUnfree = true; }
```

- installer cachix et activer le [cache pour HIE](https://github.com/Infinisil/all-hies) :

```sh
nix-env -iA nixpkgs.cachix
cachix use all-hies
```

- ajouter un overlay pour HIE, dans ``~/.config/nixpkgs/overlays/hies.nix``

```nix
self: super: 
let 
  all-hies = import (fetchTarball "https://github.com/infinisil/all-hies/tarball/master") {};
in {
  hies = all-hies.selection { selector = p: { inherit (p) ghc865 ghc844; }; };
}
```

- installer Stack, VSCode, HIE :

```sh
nix-env -iA nixpkgs.stack nixpkgs.vscode nixpkgs.hies
```

- lancer VSCode et installer l'extension ``Haskell Language Server``

Et normalement la toolchain fonctionne.

# NixOS + home-manager

[NixOS](https://nixos.org/nixos/) est une distribution linux basée sur Nix.  [Home-manager](https://github.com/rycee/home-manager) est un système de gestion de configuration utilisateur basée sur Nix (voir l'[article sur home-manager](https://linuxfr.org/news/gestion-de-paquets-evoluee-avec-nix-cachix-overlays-et-home-manager)).

- autoriser les logiciels non-libres, dans ``~/.config/nixpkgs/config.nix`` :

```nix
{ allowUnfree = true; }
```

- installer cachix et activer le [cache pour HIE](https://github.com/Infinisil/all-hies) :

```sh
nix-env -iA nixpkgs.cachix
cachix use all-hies
```

- ajouter un overlay pour HIE, dans ``~/.config/nixpkgs/overlays/hies.nix``

```nix
self: super: 
let 
  all-hies = import (fetchTarball "https://github.com/infinisil/all-hies/tarball/master") {};
in {
  hies = all-hies.selection { selector = p: { inherit (p) ghc865 ghc844; }; };
}
```

- activer VSCode + HIE, dans ``~/.config/nixpkgs/home.nix`` :

```nix
  programs.vscode = {
    enable = true;
    haskell = {
      enable = true;
      hie = {
         enable = true;
         executablePath = "${pkgs.hies}/bin/hie-wrapper";
      };
    };
  };
```

- mettre à jour la configuration utilisateur :

```sh
home-manager switch
```

Et c'est tout. L'intégration de HIE dans VSCode est faite automatiquement.

# Tester l'installation

- créer un projet Haskell et lancer VSCode :

```sh
stack new myproject --resolver=lts-14
cd myproject
code .
```

- une fois VSCode lancé, vérifier la navigation de code, l'auto-complétion, la compilation avec Stack dans la console VSCode...

- si HIE donne une erreur dans VSCode, supprimer le dossier de cache ``.cache/cabal-helper``.

