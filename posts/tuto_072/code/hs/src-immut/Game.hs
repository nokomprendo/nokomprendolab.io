{-# Language Strict #-}

module Game (nI, nJ, Game(..), Player(..), Status(..), 
    isRunning, nMovesGame, playK, mkGame, nextGame) where

import qualified Data.Vector.Unboxed as U

import Data.Massiv.Array as M

nI, nJ :: Int
(nI, nJ) = (6, 7)

data Cell = CellE | CellR | CellY deriving (Eq)

data Player = PlayerR | PlayerY deriving (Eq, Show)

player2Cell :: Player -> Cell
player2Cell PlayerR = CellR
player2Cell PlayerY = CellY

nextPlayer :: Player -> Player
nextPlayer PlayerR = PlayerY
nextPlayer PlayerY = PlayerR

data Status = PlayR | PlayY | Tie | WinR | WinY deriving (Eq, Show)

type Board = Array B Ix2 Cell

data Game = Game
    { _status           :: Status
    , _currentPlayer    :: Player
    , _firstPlayer      :: Player
    , _moves            :: U.Vector Int
    , _cells            :: Board
    }

checkIJ :: Int -> Int -> Bool
checkIJ i j = i>=0 && i<nI && j>=0 && j<nJ

{-# INLINE lineLength #-}
lineLength :: Int -> Int -> Int -> Int -> Cell -> Board -> Int
lineLength i0 j0 di dj c0 cs = 
    let aux i j n = if checkIJ i j && (cs M.! Ix2 i j) == c0 
                        then aux (i+di) (j+dj) (n+1)
                        else n
    in aux (i0+di) (j0+dj) 0

{-# INLINE checkLine #-}
checkLine :: Int -> Int -> Int -> Int -> Cell -> Board -> Bool
checkLine i0 j0 di dj c0 cs = 
    let l1 = lineLength i0 j0 di dj c0 cs
        l2 = lineLength i0 j0 (-di) (-dj) c0 cs
    in l1+l2 >= 3

mkGame :: Player -> Game
mkGame p = 
    let status = if p == PlayerR then PlayR else PlayY
    in Game status p p (U.fromList [0 .. nJ-1]) (M.replicate M.Seq (Sz2 nI nJ) CellE)

nextGame :: Game -> Game
nextGame g0 = mkGame (nextPlayer $ _firstPlayer g0)

{-# INLINE playK #-}
playK :: Int -> Game -> Game
playK k g@(Game _ cp _ ms cs0) = 

    let j0 = ms U.! k

    -- find and play cell
        findI 0 = 0
        findI i = if (cs0 M.! Ix2 (i-1) j0) /= CellE then i else findI (i-1)

        i0 = findI nI
        c0 = player2Cell cp
        cs1 = M.compute $ M.imap (\ix c -> if ix == Ix2 i0 j0 then c0 else c) cs0
        ms1 = if i0/=(nI-1) then ms else U.filter (/=j0) ms
        g1 = g  { _moves = ms1
                , _cells = cs1
                , _currentPlayer = nextPlayer cp } 

    -- update status/current/moves
        resRow = checkLine i0 j0 0 1 c0 cs1 
        resCol = checkLine i0 j0 1 0 c0 cs1
        resDiag1 = checkLine i0 j0 1 1 c0 cs1
        resDiag2 = checkLine i0 j0 1 (-1) c0 cs1
    in case (resRow||resCol||resDiag1||resDiag2, U.null ms1) of
        (True, _) -> g1  { _status = if cp==PlayerR then WinR else WinY }
        (_, True) -> g1  { _status = Tie }
        _         -> g1  { _status = if cp==PlayerR then PlayY else PlayR }

isRunning :: Game -> Bool
isRunning (Game status _ _ _ _) = status == PlayR || status == PlayY

nMovesGame :: Game -> Int
nMovesGame = U.length . _moves

