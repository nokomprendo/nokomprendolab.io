let
  rev = "21.05"; # 2021-05-16
  channel = fetchTarball "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
  pkgs = import channel {};
  drv0 = pkgs.haskellPackages.callCabal2nix "connect4" ./. {};
  drv = drv0.overrideAttrs ( old: { nativeBuildInputs = old.nativeBuildInputs ++ [ pkgs.llvm_9 ]; });
  drvEnv = drv0.env.overrideAttrs ( old: { nativeBuildInputs = old.nativeBuildInputs ++ [ pkgs.llvm_9 ]; });

in if pkgs.lib.inNixShell then drvEnv else drv

