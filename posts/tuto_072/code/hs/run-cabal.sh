#!/bin/sh

# run in a nix-shell

cabal build

cabal run app           | tail -n +2 > out-baseline.csv
cabal run app-unopt     | tail -n +2 > out-unopt.csv
cabal run app-notco     | tail -n +2 > out-notco.csv
cabal run app-lazy      | tail -n +2 > out-lazy.csv
cabal run app-noinline  | tail -n +2 > out-noinline.csv
cabal run app-immut     | tail -n +2 > out-immut.csv
cabal run app-no        | tail -n +2 > out-no.csv

gnuplot plot.gnu

