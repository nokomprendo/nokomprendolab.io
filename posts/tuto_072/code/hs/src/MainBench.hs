import Game

import Control.Monad.ST
import Criterion.Main

main :: IO ()
main = defaultMain 
    [ bench "mkGame" $ nfIO $ stToIO 
        (isRunning <$> mkGame PlayerR)
    , bench "mkGame+playK" $ nfIO $ stToIO 
        (isRunning <$> (mkGame PlayerR >>= playK 1 >>= playK 2))
    ]

