import Bot
import Cmp

import Control.Monad.ST
import System.Random.MWC
import System.TimeIt

main :: IO ()
main = do
    let nGames = 5
    botR <- BotMc 100 <$> createSystemRandom
    botY <- BotMcts 600 <$> createSystemRandom
    putStrLn "winR WinY tie ry ryt dt nGames"
    (dt, (r, y, t)) <- timeItT $ stToIO (run botR botY nGames)
    putStrLn $ unwords (map show [r, y, t, r+y, r+y+t, dt] ++ [show nGames])

