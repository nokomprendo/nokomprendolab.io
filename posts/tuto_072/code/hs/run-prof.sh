#!/bin/sh

HPFILES=`find . -maxdepth 1 -name "*.hp"`

for f in $HPFILES ; do
    b="${f%.*}"
    hp2ps -c "${b}.hp"
    ps2pdf "${b}.ps"
    pdfcrop "${b}.pdf"
    pdf2svg "${b}-crop.pdf" "${b}.svg"
    rm "${b}.ps" "${b}.pdf" "${b}-crop.pdf" "${b}.aux" 
done

