#!/bin/sh

nix-build

./result/bin/app > out-baseline.csv
./result/bin/app-unopt > out-unopt.csv
./result/bin/app-notco > out-notco.csv
./result/bin/app-lazy > out-lazy.csv
./result/bin/app-noinline > out-noinline.csv
./result/bin/app-immut > out-immut.csv
./result/bin/app-no > out-no.csv

gnuplot plot.gnu

