set terminal png size 400,400
set style data boxplot
set style fill solid 0.5 border -1
set style boxplot nooutliers
set xtics nomirror
set ytics nomirror
set yrange [0:]

set out 'out-unopt.png'
set title 'Compiler options'
set xtics ('baseline' 1, 'unoptimized' 2)
plot 'out-baseline.csv' using (1):6 notitle, 'out-unopt.csv' using (2):6 notitle

set out 'out-notco.png'
set title 'Tail-Call Optimization'
set xtics ('baseline' 1, 'no TCO' 2)
plot 'out-baseline.csv' using (1):6 notitle, 'out-notco.csv' using (2):6 notitle

set out 'out-lazy.png'
set title 'Lazy evaluation'
set xtics ('baseline' 1, 'lazy' 2)
plot 'out-baseline.csv' using (1):6 notitle, 'out-lazy.csv' using (2):6 notitle

set out 'out-noinline.png'
set title 'Inlining'
set xtics ('baseline' 1, 'no inline hints' 2)
plot 'out-baseline.csv' using (1):6 notitle, 'out-noinline.csv' using (2):6 notitle

set out 'out-no.png'
set title 'TCO + Strict eval + Inlining'
set xtics ('baseline' 1, 'notco-lazy-noinline' 2)
plot 'out-baseline.csv' using (1):6 notitle, 'out-no.csv' using (2):6 notitle

set out 'out-immut.png'
set title 'Immutability'
set xtics ('baseline' 1, 'immutable array' 2)
plot 'out-baseline.csv' using (1):6 notitle, 'out-immut.csv' using (2):6 notitle

