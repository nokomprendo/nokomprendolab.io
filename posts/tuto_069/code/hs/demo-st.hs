
import Control.Monad.ST
import System.Random.MWC

add1 :: Int -> Int
add1 x = x+1

mul2 :: Int -> ST s Int
mul2 x = return (x*2)

randMul2Add1 :: GenST s -> ST s Int
randMul2Add1 gen = do
    x <- uniformR (1, 100) gen
    y <- mul2 x
    let z = add1 y
    return z
-- randMul2Add1 gen = add1 <$> (uniformR (1, 100) gen >>= mul2)

main :: IO ()
main = do

    let x0 = add1 2
    print x0

    let x1 = runST (mul2 3)
    print x1

    x2 <- stToIO (mul2 4)
    print x2

    gen <- createSystemRandom
    x3 <- stToIO (randMul2Add1 gen)
    print x3

    stToIO (randMul2Add1 gen) >>= print

