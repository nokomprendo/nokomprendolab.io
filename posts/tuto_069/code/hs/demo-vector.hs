import Control.Monad.ST
import Data.Vector as V
import Data.Vector.Unboxed as U
import Data.Vector.Mutable as M

mul2head :: M.STVector s Int -> ST s ()
mul2head m = M.modify m (*2) 0

createSTVector :: ST s (M.STVector s Int)
createSTVector = do
    m <- M.new 2
    M.write m 0 2
    M.write m 1 3
    return m

mySTFunc :: Int -> ST s Int
mySTFunc x = do
    m <- createSTVector
    M.write m 0 x
    mul2head m
    M.read m 0

main :: IO ()
main = do

    let v1 = V.fromList [2, 3] :: V.Vector Int
        v2 = V.map (*2) v1
    print v1
    print v2
    print $ V.map (*2) (V.fromList [2, 3] :: V.Vector Int)

    let u1 = U.fromList [2, 3] :: U.Vector Int
        u2 = U.map (*2) u1
    print u1
    print u2
    print $ U.map (*2) (U.fromList [2, 3] :: U.Vector Int)

    m1 <- M.new 2
    M.write m1 0 2
    M.write m1 1 3
    M.mapM_ (return .(*2)) m1
    M.mapM_ print m1


    m2 <- stToIO createSTVector
    stToIO (mul2head m2)
    M.mapM_ print m2

    let y = runST $ mySTFunc 2
    print y

