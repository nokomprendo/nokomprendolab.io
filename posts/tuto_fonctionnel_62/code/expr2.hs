newtype Expr a = Expr { unExpr :: a }

intConst :: Int -> Expr Int
intConst x = Expr x

realConst :: Double -> Expr Double
realConst x = Expr x

add :: Num a => Expr a -> Expr a -> Expr a
add (Expr x) (Expr y) = Expr (x+y)

expr1 :: Expr Int
expr1 = add (intConst 20) (intConst 22)

expr2 :: Expr Double
expr2 = add (realConst 20) (realConst 22)

main :: IO ()
main = do
    print $ unExpr expr1
    print $ unExpr expr2

