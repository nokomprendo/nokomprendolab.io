class Expr repr where
    intConst  :: Int -> repr Int
    realConst :: Double -> repr Double
    add       :: Num a => repr a -> repr a -> repr a


expr1 :: (Expr repr) => repr Int
expr1 = add (intConst 20) (intConst 22)

expr2 :: (Expr repr) => repr Double
expr2 = add (realConst 20) (realConst 22)


newtype Eval a = Eval { runEval :: a }

instance Expr Eval where
    intConst x = Eval x
    realConst x = Eval x
    add a b = Eval $ runEval a + runEval b


newtype Format a = Format { runFormat :: String }

instance Expr Format where
    intConst x = Format $ show x
    realConst x = Format $ show x
    add a b = Format $ "(+ " ++ runFormat a ++ " " ++ runFormat b ++ ")"


class Less repr where
    less :: Ord a => repr a -> repr a -> repr Bool

expr3 :: (Expr repr, Less repr) => repr Bool
expr3 = less (intConst 20) (add (intConst 10) (intConst 12))


instance Less Eval where
    less a b = Eval $ runEval a < runEval b

instance Less Format where
    less a b = Format $ "(< " ++ runFormat a ++ " " ++ runFormat b ++ ")"


main :: IO ()
main = do
    print $ runEval expr1
    print $ runEval expr2
    print $ runEval expr3

    putStrLn $ runFormat expr1
    putStrLn $ runFormat expr2
    putStrLn $ runFormat expr3

