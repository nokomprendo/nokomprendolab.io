data Expr
    = IntConst  Int
    | RealConst Double
    | Add       Expr Expr

expr1 :: Expr
expr1 = Add (IntConst 20) (IntConst 22)

expr2 :: Expr
expr2 = Add (RealConst 20) (RealConst 22)

data Result
    = IntResult  Int
    | RealResult Double
    deriving Show

eval :: Expr -> Maybe Result
eval (IntConst x) = Just (IntResult x)
eval (RealConst x) = Just (RealResult x)
eval (Add x0 y0) = do
    x1 <- eval x0
    y1 <- eval y0
    case (x1, y1) of
        (IntResult x, IntResult y)   -> Just $ IntResult (x + y)
        (RealResult x, RealResult y) -> Just $ RealResult (x + y)
        _                            -> Nothing

view :: Expr -> String
view (IntConst x) = show x
view (RealConst x) = show x
view (Add x y) = "(+ " ++ view x ++ " " ++ view y ++ ")"

main :: IO ()
main = do
    print $ eval expr1
    print $ eval expr2

    putStrLn $ view expr1
    putStrLn $ view expr2

