class Expr repr where
    intConst  :: Int -> repr Int
    realConst :: Double -> repr Double
    add       :: Num a => repr a -> repr a -> repr a

expr1 :: (Expr repr) => repr Int
expr1 = add (intConst 20) (intConst 22)

expr2 :: (Expr repr) => repr Double
expr2 = add (realConst 20) (realConst 22)

newtype Eval a = Eval { runEval :: a }

instance Expr Eval where
    intConst x = Eval x
    realConst x = Eval x
    add a b = Eval $ runEval a + runEval b

main :: IO ()
main = do
    print $ runEval expr1
    print $ runEval expr2

