{-# LANGUAGE GADTs #-}

data Expr a where
    IntConst  :: Int -> Expr Int
    RealConst :: Double -> Expr Double
    Add       :: Num a => Expr a -> Expr a -> Expr a
    Less      :: Ord a => Expr a -> Expr a -> Expr Bool

expr1 :: Expr Int
expr1 = Add (IntConst 20) (IntConst 22)

expr2 :: Expr Double
expr2 = Add (RealConst 20) (RealConst 22)

expr3 :: Expr Bool
expr3 = Less (IntConst 20) (Add (IntConst 10) (IntConst 12))

eval :: Expr a -> a
eval (IntConst x)  = x
eval (RealConst x) = x
eval (Add x y)     = eval x + eval y
eval (Less x y)    = eval x < eval y

view :: Expr a -> String
view (IntConst x)  = show x
view (RealConst x) = show x
view (Add x y)     = "(+ " ++ view x ++ " " ++ view y ++ ")"
view (Less x y)    = "(< " ++ view x ++ " " ++ view y ++ ")"

main :: IO ()
main = do
    print $ eval expr1
    print $ eval expr2
    print $ eval expr3
    putStrLn $ view expr1
    putStrLn $ view expr2
    putStrLn $ view expr3
