{-# LANGUAGE GeneralizedNewtypeDeriving #-}
import Control.Concurrent.MVar
import Control.Monad.Reader

data User = User
    { _name :: String
    , _age :: Int
    } deriving (Show)


class Monad m => MonadLog m where
    logMsg :: String -> m ()

class Monad m => MonadUsers m where
    getUsers   :: m [User]
    deleteUser :: String -> m ()


data Env = Env
    { _usersVar :: MVar [User]
    , _logFunc  :: String -> IO ()
    }

newtype AppM a =
    AppM { unAppM :: ReaderT Env IO a }
    deriving (Functor, Applicative, Monad, MonadIO, MonadReader Env)

runAppM :: AppM a -> Env -> IO a
runAppM app env = runReaderT (unAppM app) env


instance MonadLog AppM where
    logMsg msg = do
        f <- asks _logFunc
        liftIO $ f msg

instance MonadUsers AppM where
    getUsers = do
        usersVar <- asks _usersVar
        liftIO $ readMVar usersVar

    deleteUser name = do
        usersVar <- asks _usersVar
        liftIO $ modifyMVar_ usersVar (return . filter ((/=name) . _name))


app1 :: (MonadUsers m, MonadLog m) => m [User]
app1 = do
    users0 <- getUsers
    logMsg $ "users0: " <> unwords (map _name users0)
    deleteUser "John"
    users1 <- getUsers
    logMsg $ "users1: " <> unwords (map _name users1)
    return users1


newtype AppM2 a =
    AppM2 { unAppM2 :: ReaderT () IO a }
    deriving (Functor, Applicative, Monad, MonadIO, MonadReader ())

runAppM2 :: AppM2 a -> IO a
runAppM2 app = runReaderT (unAppM2 app) ()

instance MonadLog AppM2 where
    logMsg = liftIO . putStrLn

app2 :: (MonadLog m) => m ()
app2 = do
    logMsg "log 1"
    logMsg "log 2"


main :: IO ()
main = do
    myVar <- newMVar [User "Pedro" 13, User "John" 42]
    let myLog msg = putStrLn $ "-> " <> msg
    res <- runAppM app1 (Env myVar myLog)
    print res

    runAppM2 app2

