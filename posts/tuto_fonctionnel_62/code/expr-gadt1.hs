{-# LANGUAGE GADTs #-}

data Expr a where
    IntConst  :: Int -> Expr Int
    RealConst :: Double -> Expr Double
    Add       :: Num a => Expr a -> Expr a -> Expr a

expr1 :: Expr Int
expr1 = Add (IntConst 20) (IntConst 22)

expr2 :: Expr Double
expr2 = Add (RealConst 20) (RealConst 22)

eval :: Expr a -> a
eval (IntConst x)  = x
eval (RealConst x) = x
eval (Add x y)     = eval x + eval y

view :: Expr a -> String
view (IntConst x) = show x
view (RealConst x) = show x
view (Add x y) = "(+ " ++ view x ++ " " ++ view y ++ ")"

main :: IO ()
main = do
    print $ eval expr1
    print $ eval expr2
    putStrLn $ view expr1
    putStrLn $ view expr2
