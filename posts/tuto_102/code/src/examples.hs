{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

import Control.Monad.Logger
import Data.Maybe (catMaybes)
import Data.Text (pack)
import Streaming -- ( MonadIO(..), Of(..), Stream, liftIO)
import Streaming.Prelude qualified as S
import Text.Read (readMaybe)
import TextShow ( TextShow(showt) )

-------------------------------------------------------------------------------
-- donnees
-------------------------------------------------------------------------------

myDataStr' :: [String]
myDataStr' = [ "13", "foo", "42", "bar", "37" ]

myDataStr :: Monad m => Stream (Of String) m ()
myDataStr = S.each [ "13", "foo", "42", "bar", "37" ]

-------------------------------------------------------------------------------
-- mapping
-------------------------------------------------------------------------------

myRead' :: [String] -> [Integer]
myRead' = catMaybes . map readMaybe

myRead :: Monad m => Stream (Of String) m r -> Stream (Of Integer) m r
myRead = S.catMaybes . S.map readMaybe

-------------------------------------------------------------------------------
-- reduction
-------------------------------------------------------------------------------

mySum' :: [Integer] -> Integer
mySum' = sum

mySum :: Monad m => Stream (Of Integer) m r -> m (Of Integer r)
mySum = S.sum

-------------------------------------------------------------------------------
-- reduction
-------------------------------------------------------------------------------

myLengthAndSum' :: [Integer] -> (Int, Integer)
myLengthAndSum' xs = (length xs, mySum' xs)

myLengthAndSum :: Monad m => Stream (Of Integer) m r -> m (Of Int (Of Integer r))
myLengthAndSum = S.length . mySum . S.copy

-------------------------------------------------------------------------------
-- app
-------------------------------------------------------------------------------

app' :: (MonadIO m, MonadLogger m) => [Integer] -> m ()
app' stream = do
  let (s, l) = myLengthAndSum' stream
  liftIO $ putStrLn $ "length=" <> show l <> " sum=" <> show s
  ($logInfo) $ "length=" <> showt l <> " sum=" <> showt s

app :: (MonadIO m, MonadLogger m) => Stream (Of Integer) m r -> m ()
-- app :: Stream (Of Integer) (LoggingT IO) r -> LoggingT IO ()
app stream = do
  (l :> s :> _) <- myLengthAndSum stream
  liftIO $ putStrLn $ "length=" <> show l <> " sum=" <> show s
  ($logInfo) $ "length=" <> showt l <> " sum=" <> showt s

app1 :: Stream (Of Integer) IO r -> IO ()
app1 stream = do
  (l :> s :> _) <- myLengthAndSum stream
  liftIO $ putStrLn $ "length=" <> show l <> " sum=" <> show s

app2 :: Stream (Of Integer) (LoggingT IO) r -> LoggingT IO ()
app2 stream = do
  (l :> s :> _) <- myLengthAndSum stream
  liftIO $ putStrLn $ "length=" <> show l <> " sum=" <> show s
  ($logInfo) $ "length=" <> showt l <> " sum=" <> showt s

-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

_main :: IO ()
_main = do
  print myDataStr'
  print $ myRead' myDataStr'
  print $ mySum' (myRead' myDataStr')
  print $ myLengthAndSum' (myRead' myDataStr')
  runStdoutLoggingT (app' $ myRead' myDataStr')
  runStdoutLoggingT (app' [1 .. 1_000_000])

main :: IO ()
main = do
  -- S.print myDataStr
  -- S.print $ myRead myDataStr
  -- mySum (myRead myDataStr) >>= print
  -- myLengthAndSum (myRead myDataStr) >>= print
  myLengthAndSum (S.each [1 .. 1_000_000]) >>= print
  -- runStdoutLoggingT (app $ myRead myDataStr)
  -- runStdoutLoggingT (app $ S.each [1 .. 1_000_000])

