#!/bin/sh

if [ $# -ne 1 ] ; then
    echo "usage: $0 <app>"
    exit
fi

APP=$1

cabal run ${APP} -- +RTS -hy -l-agu
eventlog2html ${APP}.eventlog

