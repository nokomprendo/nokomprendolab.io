{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

import Apecs
import Apecs.Gloss
import Control.Lens
import Control.Monad
import Control.Monad.State.Strict
import Linear
import System.Exit
import System.Random

-------------------------------------------------------------------------------
-- params
-------------------------------------------------------------------------------

winWidth, winHeight :: Int
winWidth = 400
winHeight = 300

winWidthF, winHeightF, xMin, xMax, yMin, yMax, ballRadius :: Float
winWidthF = fromIntegral winWidth
winHeightF = fromIntegral winHeight
xMin =  ballRadius - 0.5 * winWidthF
xMax = -ballRadius + 0.5 * winWidthF
yMin =  ballRadius - 0.5 * winHeightF
yMax = -ballRadius + 0.5 * winHeightF
ballRadius = 20

-------------------------------------------------------------------------------
-- types
-------------------------------------------------------------------------------

data Ball = Ball
instance Component Ball where type Storage Ball = Map Ball

newtype Position = Position { _unPos :: V2 Float }
instance Component Position where type Storage Position = Map Position

newtype Velocity = Velocity { _unVel :: V2 Float }
instance Component Velocity where type Storage Velocity = Map Velocity

makeWorld "World" [''Ball, ''Position, ''Velocity, ''Camera]

makeLenses ''Position
makeLenses ''Velocity

-------------------------------------------------------------------------------
-- application
-------------------------------------------------------------------------------

main :: IO ()
main = do
  let window = InWindow "AnimApecs5" (winWidth, winHeight) (0, 0)
      bgcolor = makeColor 0.4 0.6 1.0 1.0
      fps = 30 
  world <- initWorld 
  runWith world $ do
    initialize 
    play window bgcolor fps hDraw hEvent hTime

randomBall :: System World (Ball, Position, Velocity)
randomBall = do
  p <- randomRIO (V2 xMin yMin, V2 xMax yMax)
  v <- randomRIO (V2 (-500) (-500), V2 500 500)
  return (Ball, Position p, Velocity v)

initialize :: System World ()
initialize = do
  ball0 <- randomBall
  newEntity_ ball0
  -- ball1 <- randomBall
  -- newEntity_ ball1

hDraw :: System World Picture
hDraw = do
  ballPicture <- foldDraw $ \(Ball, Position (V2 x y), Velocity _) -> 
    translate x y  (circleSolid ballRadius)
  return $ Pictures
      [ ballPicture
      , rectangleWire winWidthF winHeightF ]

hEvent :: Event -> System World ()
hEvent (EventKey (SpecialKey KeyEnter) Up _ _) = 
  cmapM $ \(Ball, Position _, Velocity _) -> randomBall
hEvent (EventKey (SpecialKey KeyEsc) Up   _ _) = liftIO exitSuccess
hEvent _ = return ()

hTime :: Float -> System World ()
hTime deltaTime =
  cmap $ execState (updateMotion deltaTime >> updateBounces)

updateMotion :: Float -> State (Ball, Position, Velocity) ()
updateMotion deltaTime = do
  v <- use $ _3 . unVel
  _2 . unPos += v ^* deltaTime

updateBounces :: State (Ball, Position, Velocity) ()
updateBounces = do
  (V2 x y) <- use $ _2 . unPos
  when (xMin >= x) $ do
    _2 . unPos . _x .= 2*xMin - x
    _3 . unVel . _x %= negate
  when (xMax <= x) $ do
    _2 . unPos . _x .= 2*xMax - x
    _3 . unVel . _x %= negate
  when (yMin >= y) $ do
    _2 . unPos . _y .= 2*yMin - y
    _3 . unVel . _y %= negate
  when (yMax <= y) $ do
    _2 . unPos . _y .= 2*yMax - y
    _3 . unVel . _y %= negate

