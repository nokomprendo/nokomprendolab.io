{-# LANGUAGE TemplateHaskell #-}

import Control.Lens
import Control.Monad.State.Strict
import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game
import Linear.V2
import Linear.Vector
import System.Exit
import System.Random

-------------------------------------------------------------------------------
-- params
-------------------------------------------------------------------------------

winWidth, winHeight :: Int
winWidth = 400
winHeight = 300

winWidthF, winHeightF, xMin, xMax, yMin, yMax, ballRadius :: Float
winWidthF = fromIntegral winWidth
winHeightF = fromIntegral winHeight
xMin =  ballRadius - 0.5 * winWidthF
xMax = -ballRadius + 0.5 * winWidthF
yMin =  ballRadius - 0.5 * winHeightF
yMax = -ballRadius + 0.5 * winHeightF
ballRadius = 20

-------------------------------------------------------------------------------
-- types
-------------------------------------------------------------------------------

data Ball = Ball 
  { _pos :: V2 Float
  , _vel :: V2 Float
  }

newtype Model = Model
  { _ball :: Ball
  }

makeLenses ''Ball
makeLenses ''Model

-------------------------------------------------------------------------------
-- application
-------------------------------------------------------------------------------

main :: IO ()
main = do
  let window = InWindow "AnimGloss" (winWidth, winHeight) (0, 0)
      bgcolor = makeColor 0.4 0.6 1.0 1.0
      fps = 30 
  model <- initialize
  playIO window bgcolor fps model hDisplay hEvent hTime

randomBall :: IO Ball
randomBall = do
  p <- randomRIO (V2 xMin yMin, V2 xMax yMax)
  v <- randomRIO (V2 (-500) (-500), V2 500 500)
  return (Ball p v)

initialize :: IO Model
initialize = do
  ball0 <- randomBall
  return (Model ball0)

hDisplay :: Model -> IO Picture
hDisplay model = 
  let (V2 x y) = model ^. ball . pos
  in return $ Pictures
      [ translate x y (circleSolid ballRadius)
      , rectangleWire winWidthF winHeightF ]

hEvent :: Event -> Model -> IO Model
hEvent (EventKey (SpecialKey KeyEnter) Up _ _) model = do
  newBall <- randomBall
  return $ model & ball .~ newBall
hEvent (EventKey (SpecialKey KeyEsc) Up _ _) _ = exitSuccess
hEvent _ model = return model

hTime :: Float -> Model -> IO Model
hTime deltaTime model =
  return $ model & ball %~ execState (updateMotion deltaTime >> updateBounces)

updateMotion :: Float -> State Ball ()
updateMotion deltaTime = do
  v <- use vel
  pos += v ^* deltaTime

updateBounces :: State Ball ()
updateBounces = do
  (V2 x y) <- use pos
  when (xMin >= x) $ do
    pos . _x .= 2*xMin - x
    vel . _x %= negate
  when (xMax <= x) $ do
    pos . _x .= 2*xMax - x
    vel . _x %= negate
  when (yMin >= y) $ do
    pos . _y .= 2*yMin - y
    vel . _y %= negate
  when (yMax <= y) $ do
    pos . _y .= 2*yMax - y
    vel . _y %= negate

