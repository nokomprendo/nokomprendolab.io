{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

import Apecs
import Apecs.Gloss
import Control.Lens hiding (set)
import Control.Monad
import Control.Monad.State.Strict
import Linear
import System.Exit
import System.Random

-------------------------------------------------------------------------------
-- params
-------------------------------------------------------------------------------

winWidth, winHeight :: Int
winWidth = 400
winHeight = 300

winWidthF, winHeightF, xMin, xMax, yMin, yMax, ballRadius :: Float
winWidthF = fromIntegral winWidth
winHeightF = fromIntegral winHeight
xMin =  ballRadius - 0.5 * winWidthF
xMax = -ballRadius + 0.5 * winWidthF
yMin =  ballRadius - 0.5 * winHeightF
yMax = -ballRadius + 0.5 * winHeightF
ballRadius = 20

-------------------------------------------------------------------------------
-- types
-------------------------------------------------------------------------------

data Ball = Ball 
  { _pos :: V2 Float
  , _vel :: V2 Float
  }

makeLenses ''Ball

instance Component Ball where type Storage Ball = Map Ball

instance Random Ball where
  randomR (Ball al bl, Ball ah bh) = runState $
    Ball <$> state (randomR (al, ah)) 
         <*> state (randomR (bl, bh))

  random = randomR ( Ball (V2 xMin yMin) (V2 (-500) (-500))
                   , Ball (V2 xMax yMax) (V2   500    500))

makeWorld "World" [''Ball, ''Camera]

-------------------------------------------------------------------------------
-- application
-------------------------------------------------------------------------------

main :: IO ()
main = do
  let window = InWindow "AnimApecs4" (winWidth, winHeight) (0, 0)
      bgcolor = makeColor 0.4 0.6 1.0 1.0
      fps = 30 
  world <- initWorld 
  runWith world $ do
    initialize 
    play window bgcolor fps hDraw hEvent hTime

initialize :: System World ()
initialize = do
  ball0 <- randomIO
  newEntity_ (ball0 :: Ball)

hDraw :: System World Picture
hDraw = do
  ballPicture <- foldDraw $ \(Ball (V2 x y) _) -> 
    translate x y  (circleSolid ballRadius)
  return $ Pictures
      [ ballPicture
      , rectangleWire winWidthF winHeightF ]

hEvent :: Event -> System World ()
hEvent (EventKey (SpecialKey KeyEnter) Up _ _) = 
  cmapM $ \(_ :: Ball) -> liftIO (randomIO :: IO Ball)
hEvent (EventKey (SpecialKey KeyEsc) Up   _ _) = liftIO exitSuccess
hEvent _ = return ()

hTime :: Float -> System World ()
hTime deltaTime =
  cmap $ execState (updateMotion deltaTime >> updateBounces)

updateMotion :: Float -> State Ball ()
updateMotion deltaTime = do
  v <- use vel
  pos += v ^* deltaTime

updateBounces :: State Ball ()
updateBounces = do
  (V2 x y) <- use pos
  when (xMin >= x) $ do
    pos . _x .= 2*xMin - x
    vel . _x %= negate
  when (xMax <= x) $ do
    pos . _x .= 2*xMax - x
    vel . _x %= negate
  when (yMin >= y) $ do
    pos . _y .= 2*yMin - y
    vel . _y %= negate
  when (yMax <= y) $ do
    pos . _y .= 2*yMax - y
    vel . _y %= negate

