{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell       #-}

import Apecs.Physics
import Apecs.Physics.Gloss
import Control.Monad
import System.Exit
import System.Random

-------------------------------------------------------------------------------
-- params
-------------------------------------------------------------------------------

winWidth, winHeight :: Int
winWidth = 400
winHeight = 300

winWidthD, winHeightD, xMin, xMax, yMin, yMax, ballRadius :: Double
winWidthD = fromIntegral winWidth
winHeightD = fromIntegral winHeight
xMin =  ballRadius - 0.5 * winWidthD
xMax = -ballRadius + 0.5 * winWidthD
yMin =  ballRadius - 0.5 * winHeightD
yMax = -ballRadius + 0.5 * winHeightD
ballRadius = 20

-------------------------------------------------------------------------------
-- types
-------------------------------------------------------------------------------

data Ball = Ball 
  { _pos :: V2 Double
  , _vel :: V2 Double
  }

instance Random Ball where
  randomR (Ball al bl, Ball ah bh) gen0 = 
    let (pos, gen1) = randomR (al, ah) gen0
        (vel, gen2) = randomR (bl, bh) gen1
    in (Ball pos vel, gen2)

  random = randomR ( Ball (V2 xMin yMin) (V2 (-500) (-500))
                   , Ball (V2 xMax yMax) (V2   500    500))

makeWorld "World" [''Physics, ''Camera]

-------------------------------------------------------------------------------
-- application
-------------------------------------------------------------------------------

main :: IO ()
main = do
  let window = InWindow "AnimApecs3" (winWidth, winHeight) (0, 0)
      bgcolor = black
      fps = 30 
  world <- initWorld 
  runWith world $ do
    ball <- initialize 
    play window bgcolor fps hDraw (hEvent ball) hTime

initialize :: System World Entity
initialize = do
  lineBody <- newEntity StaticBody
  let sides = toEdges $ cRectangle (V2 winWidthD winHeightD)
  forM_ sides $ \side ->
    newEntity (Shape lineBody side, Elasticity 1)

  Ball ballPos ballVel <- liftIO randomIO
  ball <- newEntity (DynamicBody, Position ballPos, Velocity ballVel)
  newEntity_ (Shape ball (cCircle ballRadius), Density 1, Elasticity 1)
  return ball

hDraw :: System World Picture
hDraw = foldDrawM drawBody

hEvent :: Entity -> Event -> System World ()
hEvent ball (EventKey (SpecialKey KeyEnter) Up _ _) = do
  Ball ballPos ballVel <- liftIO randomIO
  set ball (Position ballPos, Velocity ballVel)
hEvent _ (EventKey (SpecialKey KeyEsc) Up   _ _) = liftIO exitSuccess
hEvent _ _ = return ()

hTime :: Float -> System World ()
hTime = stepPhysics . realToFrac

