{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell       #-}

import Apecs.Physics
import Apecs.Physics.Gloss
import Control.Monad
import System.Random

-------------------------------------------------------------------------------
-- params
-------------------------------------------------------------------------------

winWidth, winHeight :: Int
winWidth = 400
winHeight = 300

winWidthD, winHeightD, xMin, xMax, yMin, yMax, ballRadius :: Double
winWidthD = fromIntegral winWidth
winHeightD = fromIntegral winHeight
xMin =  ballRadius - 0.5 * winWidthD
xMax = -ballRadius + 0.5 * winWidthD
yMin =  ballRadius - 0.5 * winHeightD
yMax = -ballRadius + 0.5 * winHeightD
ballRadius = 20

-------------------------------------------------------------------------------
-- types
-------------------------------------------------------------------------------

makeWorld "World" [''Physics, ''Camera]

-------------------------------------------------------------------------------
-- application
-------------------------------------------------------------------------------

main :: IO ()
main = do
  let disp = InWindow "AnimApecs1" (winWidth, winHeight) (0, 0)
  world <- initWorld
  runWith world $ do
    initialize
    simulate disp

initialize :: System World ()
initialize = do
  lineBody <- newEntity StaticBody
  let sides = toEdges $ cRectangle (V2 winWidthD winHeightD)
  forM_ sides $ \side ->
    newEntity (Shape lineBody side, Elasticity 1)

  ballPos <- liftIO $ randomRIO (V2 xMin yMin, V2 xMax yMax)
  ballVel <- liftIO $ randomRIO (V2 (-500) (-500), V2 500 500)
  ball <- newEntity (DynamicBody, Position ballPos, Velocity ballVel)
  newEntity_ (Shape ball (cCircle ballRadius), Density 1, Elasticity 1)
