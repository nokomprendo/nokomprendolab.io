{-# LANGUAGE StrictData #-}

import Control.Lens
import Control.Monad.State.Strict
import qualified Data.Vector as V
import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game (playIO)
import Graphics.Gloss.Interface.IO.Interact
import Linear.Metric
import Linear.V2
import System.Exit
import System.Random

import Animation
import Params
import Scene

-------------------------------------------------------------------------------
-- helpers
-------------------------------------------------------------------------------

checkBalls :: V.Vector Ball -> V.Vector Ball
checkBalls theObjects = 
    let fAcc acc b = if any (overlap b) acc then acc else V.cons b acc
        overlap b1 b2 = let d2 = qd (_pos b1) (_pos b2)
                            r = _rad b1 + _rad b2
                        in d2 < (radiusMargin + r)**2
    in V.foldl' fAcc V.empty theObjects

mkBall :: IO Ball
mkBall = do
    radius <- randomRIO radiusMinMax 
    let m = pi * radius * radius

    posX <- randomRIO (-winWidth05+radius+radiusMargin, winWidth05-radius-radiusMargin) 
    posY <- randomRIO (-winHeight05+radius+radiusMargin, winHeight05-radius-radiusMargin) 

    velTheta <- randomRIO (0, pi) 
    velNorm <- randomRIO velocityMinMax 
    let velX = velNorm * cos velTheta
    let velY = velNorm * sin velTheta

    colR <- randomRIO (0.3, 1.0) 
    colG <- randomRIO (0.3, 1.0) 
    colB <- randomRIO (0.3, 1.0) 
    let c = (colR, colG, colB)

    return $ Ball c radius m (V2 posX posY) (V2 velX velY)

initialize :: IO Scene
initialize = do
    theBalls <- checkBalls <$> V.replicateM nSamples mkBall 
    putStrLn $ "generated " ++ show (V.length theBalls) ++ " balls"
    let theWalls = V.fromList [ WallLeft, WallRight, WallTop, WallBottom ]
    return $ Scene theBalls theWalls

main :: IO ()
main = do
    let width = 5 + round winWidthF
        height = 5 + round winHeightF
        window = InWindow "boules2d-gloss" (width, height) (0, 0)
        bgcolor = black
        fps = 30
    scene <- initialize 
    playIO window bgcolor fps scene hDraw hEvent hTime

hEvent :: Event -> Scene -> IO Scene
hEvent (EventKey (SpecialKey KeyEsc) Up _ _) _ = exitSuccess
hEvent (EventKey (SpecialKey KeyEnter) Up _ _) _ = initialize
hEvent _ scene = return scene

hTime :: Float -> Scene -> IO Scene
hTime dt = return . execState (animateScene (realToFrac dt) )

hDraw :: Scene -> IO Picture
hDraw scene = 
    let bs = V.map drawBall (scene ^. balls)
        ws = V.map drawWall (scene ^. walls)
    in return $ Pictures $ V.toList (bs <> ws)

drawBall :: Ball -> Picture
drawBall (Ball (cr,cg,cb) r _ (V2 x y) _) = 
    Color (makeColor cr cg cb 1) $
        translate (realToFrac x) (realToFrac y) $
            circleSolid (realToFrac r)

drawWall :: Wall -> Picture
drawWall WallLeft = 
    Color white $ Line [(-winWidth05F, -winHeight05F), (-winWidth05F, winHeight05F)]
drawWall WallRight = 
    Color white $ Line [(winWidth05F, -winHeight05F), (winWidth05F, winHeight05F)]
drawWall WallTop = 
    Color white $ Line [(-winWidth05F, winHeight05F), (winWidth05F, winHeight05F)]
drawWall WallBottom = 
    Color white $ Line [(-winWidth05F, -winHeight05F), (winWidth05F, -winHeight05F)]

