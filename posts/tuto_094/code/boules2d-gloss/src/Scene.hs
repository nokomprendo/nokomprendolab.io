{-# LANGUAGE StrictData #-}
{-# LANGUAGE TemplateHaskell #-}

module Scene where

import Control.Lens 
import qualified Data.Vector as V
import Linear.V2

-------------------------------------------------------------------------------
-- types
-------------------------------------------------------------------------------

data Ball = Ball
    { _col :: (Float, Float, Float)
    , _rad :: Double
    , _mass :: Double
    , _pos :: V2 Double
    , _vel :: V2 Double
    }
makeLenses ''Ball

data Wall 
    = WallLeft
    | WallRight
    | WallTop
    | WallBottom

data Scene = Scene
    { _balls :: V.Vector Ball
    , _walls :: V.Vector Wall
    }
makeLenses ''Scene
