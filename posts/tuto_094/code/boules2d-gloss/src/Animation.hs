{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE StrictData #-}

module Animation where

import Control.Lens
import Control.Monad.State.Strict
import qualified Data.Vector as V
import Linear.Affine
import Linear.Matrix
import Linear.V2
import Linear.V4
import Linear.Vector

import Params
import Scene

-------------------------------------------------------------------------------
-- animation
-------------------------------------------------------------------------------

animateScene :: Double -> State Scene ()
animateScene dt = when (dt > 0) $ do
    bs <- use balls
    ws <- use walls
    let hitsBall = computeHit dt bs bs 
        hitsWall = computeHit dt ws bs
        (moves, dti) = case (hitsBall, hitsWall) of
            (Nothing,             Nothing)           -> ([], dt)
            (Nothing,             Just (_, mw2, tw)) -> ([mw2], tw)
            (Just (mb1, mb2, tb), Nothing)           -> ([mb1, mb2], tb)
            (Just (mb1, mb2, tb), Just (_, mw2, tw)) -> 
                if tb < tw then ([mb1, mb2], tb) else ([mw2], tw)
    moveBalls dti
    unless (null moves) $ do
        balls %= (V.// moves)
        animateScene (dt-dti)

-------------------------------------------------------------------------------
-- helpers
-------------------------------------------------------------------------------

moveBalls :: Double -> State Scene ()
moveBalls dt = balls %= V.map (moveBall dt)

moveBall :: Double -> Ball -> Ball
moveBall dt b = b & pos +~ dt *^ _vel b

computeHit :: 
    TryHit a b => 
    Double -> V.Vector a -> V.Vector b -> Maybe ((Int, a), (Int, b), Double)
computeHit dt os1 os2 = 
    V.ifoldl' f1 Nothing os1
    where
        f1 acc i1 o1 = V.ifoldl' (f2 i1 o1) acc os2
        f2 i1 o1 acc i2 o2 = case (acc, tryHit dt o1 o2) of
            (_, Nothing) -> acc
            (Nothing, Just (Hit tHit o1' o2')) ->
                Just ((i1, o1'), (i2, o2'), tHit)
            (Just (_, _, tAcc), Just (Hit tHit o1' o2')) -> 
                if tAcc < tHit then acc else Just ((i1, o1'), (i2, o2'), tHit)
         


-------------------------------------------------------------------------------
-- hit
-------------------------------------------------------------------------------

data Hit a b = Hit
    { _hitTime :: Double    -- step time until the collision
    , _object1 :: a         -- object 1 after collision
    , _object2 :: b         -- object 2 after collision
    }

class TryHit a b where
    tryHit :: Double -> a -> b -> Maybe (Hit a b)

instance TryHit Ball Ball where
    tryHit t b1 b2 = 
        let (Ball _ r1 m1 p1@(V2 p1x p1y) v1@(V2 v1x v1y)) = b1
            (Ball _ r2 m2 p2@(V2 p2x p2y) v2@(V2 v2x v2y)) = b2
            -- detect collision
            dvx = v1x-v2x
            dvy = v1y-v2y
            dpx = p1x-p2x
            dpy = p1y-p2y
            dmin = r1+r2
            a = dvx*dvx + dvy*dvy
            b = dpx*dvx + dpy*dvy
            c = dpx*dpx + dpy*dpy - dmin*dmin
            delta = b*b - a*c
            rd = sqrt delta
            ti = min ((-b-rd)/a) ((-b+rd)/a)
            -- compute bounces
            d1 = p1 .+^ ti *^ v1
            d2 = p2 .+^ ti *^ v2
            nx = d2^._x - d1^._x
            ny = d2^._y - d1^._y
            tx = ny
            ty = -nx
            matA = V4 (V4 m1 0 m2 0)
                    (V4 0 m1 0 m2)
                    (V4 tx ty 0 0)
                    (V4 nx ny (-nx) (-ny))
            vecB = V4 (m1*v1x + m2*v2x)
                    (m1*v1y + m2*v2y)
                    (v1x*tx + v1y*ty)
                    (-elasticity*((v1x-v2x)*nx + (v1y-v2y)*ny))
            (V4 v1x' v1y' v2x' v2y') = luSolveFinite matA vecB
            b1' = b1 { _pos = d1, _vel = V2 v1x' v1y' }
            b2' = b2 { _pos = d2, _vel = V2 v2x' v2y' }
        in if (abs a > 0) && delta>0 && ti>=0 && ti<=t
            then Just (Hit ti b1' b2')
            else Nothing

instance TryHit Wall Ball where
    tryHit t WallLeft b@(Ball _ r _ p v) = 
        let ti = (-winWidth05 + r - p^._x) / v^._x
            o1 = moveBall ti b & vel . _x %~ negate
        in if ti>0 && ti<t then Just (Hit ti WallLeft o1) else Nothing
    tryHit t WallRight b@(Ball _ r _ p v) = 
        let ti = (winWidth05 - r - p^._x) / v^._x 
            o1 = moveBall ti b & vel . _x %~ negate
        in if ti>0 && ti<t then Just (Hit ti WallRight o1) else Nothing
    tryHit t WallTop b@(Ball _ r _ p v) = 
        let ti = (winHeight05 - r - p^._y) / v^._y
            o1 = moveBall ti b & vel . _y %~ negate
        in if ti>0 && ti<t then Just (Hit ti WallTop o1) else Nothing
    tryHit t WallBottom b@(Ball _ r _ p v) = 
        let ti = (-winHeight05 + r - p^._y) / v^._y
            o1 = moveBall ti b & vel . _y %~ negate
        in if ti>0 && ti<t then Just (Hit ti WallBottom o1) else Nothing

