let
  channel = <nixpkgs>;
  pkgs = import channel {};
  drv = pkgs.haskellPackages.callCabal2nix "examples" ./. {};

in
  if pkgs.lib.inNixShell then drv.env else drv

