{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeApplications #-}

import Apecs
import Apecs.Gloss
import Control.Lens
import Control.Monad
import Data.List
import Linear.Metric
import Linear.V2
import System.Exit
import System.Random

import Animation
import Params
import Scene

-------------------------------------------------------------------------------
-- helpers
-------------------------------------------------------------------------------

checkBalls :: [Ball'] -> [Ball']
checkBalls balls = 
    let fAcc acc b = if any (overlap b) acc then acc else b : acc
        overlap b1 b2 = 
            let d2 = qd (b1 ^. _2 . unPos) (b2 ^. _2 . unPos) 
                r = b1 ^. _1 . rad + b2 ^. _1 . rad
            in d2 < (radiusMargin + r)**2
    in foldl' fAcc [] balls

mkBall :: System World Ball'
mkBall = do
    radius <- randomRIO radiusMinMax 
    let m = pi * radius * radius

    posX <- randomRIO 
        (-winWidth05+radius+radiusMargin, winWidth05-radius-radiusMargin) 
    posY <- randomRIO 
        (-winHeight05+radius+radiusMargin, winHeight05-radius-radiusMargin) 

    velTheta <- randomRIO (0, pi) 
    velNorm <- randomRIO velocityMinMax 
    let velX = velNorm * cos velTheta
    let velY = velNorm * sin velTheta

    colR <- randomRIO (0.3, 1.0) 
    colG <- randomRIO (0.3, 1.0) 
    colB <- randomRIO (0.3, 1.0)

    return 
        ( Ball (colR, colG, colB) radius m
        , Position (V2 posX posY)
        , Velocity (V2 velX velY)
        )

initializeBalls :: System World ()
initializeBalls = do
    balls <- checkBalls <$> replicateM nSamples mkBall
    forM_ balls $ \ball -> newEntity_ ball
    liftIO $ putStrLn $ "generated " ++ show (length balls) ++ " balls"

initializeWalls :: System World ()
initializeWalls = do
    newEntity_ WallLeft
    newEntity_ WallBottom
    newEntity_ WallRight
    newEntity_ WallTop

-------------------------------------------------------------------------------
-- application
-------------------------------------------------------------------------------

main :: IO ()
main = do
    let width = 5 + round winWidthF
        height = 5 + round winHeightF
        window = InWindow "boules2d-apecs" (width, height) (0, 0)
        bgcolor = black
        fps = 30
    world <- initWorld 
    runWith world $ do
        initializeWalls
        initializeBalls
        play window bgcolor fps hDraw hEvent hTime

hEvent :: Event -> System World ()
hEvent (EventKey (SpecialKey KeyEsc) Up _ _) = liftIO exitSuccess
hEvent (EventKey (SpecialKey KeyEnter) Up _ _) = do
    cmapM_ $ \(Ball {}, Position _, Velocity _, ety) ->
        destroy ety (Proxy @Ball')
    initializeBalls
hEvent _ = return ()

hTime :: Float -> System World ()
hTime dt = animateScene (realToFrac dt)

hDraw :: System World Picture
hDraw = do
    balls <- foldDraw drawBall
    walls <- foldDraw drawWall
    return $ Pictures [ balls, walls ]

drawBall :: (Ball, Position) -> Picture
drawBall (Ball (cr, cb, cg) r _, Position (V2 x y)) =
    Color (makeColor cr cg cb 1) $
        translate (realToFrac x) (realToFrac y) $
            circleSolid (realToFrac r)

drawWall :: Wall -> Picture
drawWall WallLeft = Color white $ 
    Line [(-winWidth05F, -winHeight05F), (-winWidth05F, winHeight05F)]
drawWall WallRight = Color white $ 
    Line [(winWidth05F, -winHeight05F), (winWidth05F, winHeight05F)]
drawWall WallTop = Color white $ 
    Line [(-winWidth05F, winHeight05F), (winWidth05F, winHeight05F)]
drawWall WallBottom = Color white $ 
    Line [(-winWidth05F, -winHeight05F), (winWidth05F, -winHeight05F)]

