{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Scene where

import Apecs
import Apecs.Gloss
import Control.Lens 
import Linear.V2

-------------------------------------------------------------------------------
-- types
-------------------------------------------------------------------------------

newtype Position = Position { _unPos :: V2 Double }
instance Component Position where type Storage Position = Map Position
makeLenses ''Position

newtype Velocity = Velocity { _unVel :: V2 Double }
instance Component Velocity where type Storage Velocity = Map Velocity
makeLenses ''Velocity

data Ball = Ball
    { _col :: (Float, Float, Float)
    , _rad :: Double
    , _mass :: Double
    }
instance Component Ball where type Storage Ball = Map Ball
makeLenses ''Ball

data Wall 
    = WallLeft
    | WallRight
    | WallTop
    | WallBottom
instance Component Wall where type Storage Wall = Map Wall

makeWorld "World" [''Position, ''Velocity, ''Ball, ''Wall, ''Camera]

type Ball' = (Ball, Position, Velocity)

