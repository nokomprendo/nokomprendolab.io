
let

  pkgs = import <nixpkgs> {};

  ghc = pkgs.haskellPackages;
  # ghc = pkgs.haskell.packages.ghc943;

in ghc.developPackage {
  root = ./.;

  withHoogle = false;

  modifier = drv:
    # pkgs.haskell.lib.dontHaddock (
      pkgs.haskell.lib.addBuildTools drv (with ghc; [
        cabal-install
        eventlog2html
    ]);
    #]));
}
