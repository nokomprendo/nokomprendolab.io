{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}

import Control.Monad.Reader
import Data.Text qualified as T
import Data.Text.Lazy.IO qualified as T
import Lucid

data Env = Env
  { _title :: T.Text
  , _url :: T.Text
  }

class Monad m => MonadLog m where
  logMsg :: String -> m ()

instance MonadIO m => MonadLog (ReaderT e m) where
  logMsg = liftIO . putStrLn

htmlPage :: (MonadReader Env m, MonadLog m) => HtmlT m ()
htmlPage = do
  title <- asks (toHtml . _title)
  url <- asks (toHtml . _url)
  doctypehtml_ $ do
    head_ $ do
      lift $ logMsg "inside header"
      title_ title
    body_ $ do
      lift $ logMsg "inside body"
      h1_ title
      p_ url

main :: IO ()
main = do
  let env = Env "Playground" "localhost"
  html <- runReaderT (renderTextT htmlPage) env
  T.putStrLn html

