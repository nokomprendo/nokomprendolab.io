{-# LANGUAGE OverloadedStrings #-}

import Codec.Compression.GZip qualified as GZip
import Control.Applicative ((<|>))
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Resource (runResourceT)
import Data.Attoparsec.ByteString.Char8 qualified as A
import Data.Attoparsec.ByteString.Lazy qualified as LA
import Data.ByteString qualified as BS
import Data.ByteString.Char8 qualified as BC
import Data.ByteString.Lazy qualified as LBS
import Data.Functor (($>))
import Data.List (sort)
import Graphics.Rendering.Chart.Easy ((.=), def, layout_title, line, plot)
import Graphics.Rendering.Chart.Backend.Cairo (toFile)
import System.Environment (getArgs)

-------------------------------------------------------------------------------
-- types
-------------------------------------------------------------------------------

data Co2Data = Co2Data
  { _country  :: BS.ByteString
  , _year     :: Integer
  , _co2      :: Integer
  } deriving (Show)

-------------------------------------------------------------------------------
-- parsers
-------------------------------------------------------------------------------

fieldP :: A.Parser BS.ByteString
fieldP = A.takeWhile (/= ',') <* A.char ','

skipFieldP :: A.Parser ()
skipFieldP = A.skipWhile (/= ',') <* A.char ','

skipRowP :: A.Parser ()
skipRowP = A.skipWhile (/= '\n') <* A.char '\n'

intFieldP :: A.Parser Integer
intFieldP = (A.decimal <|> pure 0) <* skipFieldP

rowP :: A.Parser Co2Data
rowP = Co2Data 
  <$> fieldP 
  <*> intFieldP <* A.count 5 skipFieldP 
  <*> intFieldP <* skipRowP

{-
fileP :: BS.ByteString -> A.Parser [Co2Data]
fileP c = filter ((==c) . _country) <$> (headerP *> bodyP)
  where
    headerP = skipRowP 
    bodyP = A.manyTill rowP A.endOfInput
-}

fileP :: BS.ByteString -> A.Parser [Co2Data]
fileP c = headerP *> bodyP
  where
    headerP = skipRowP 
    bodyP =   A.endOfInput $> []
          <|> (do r <- rowP
                  if c == _country r then (r :) <$> bodyP else bodyP)

-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

plotCo2 :: String -> FilePath -> [(Integer, Integer)] -> IO ()
plotCo2 country filename xys = toFile def filename $ do
  layout_title .= "CO2 emission in Mt (by co2-base)"
  plot (line country [ xys ])

main :: IO ()
main = do
  let infile = "data/owid-co2-data.csv.gz"
      outfile = "out-co2-stream.png"
  args <- getArgs
  case args of
    [country] -> runResourceT $ do
      let countryC = BC.pack country
      resRead <- liftIO (GZip.decompress <$> LBS.readFile infile)
      let resParse = LA.parseOnly (fileP countryC) resRead
      case resParse of
        Left err -> liftIO $ putStrLn $ "parse error: " <> err
        Right co2data -> 
          let xys = [ (y, co2) | (Co2Data _ y co2) <- co2data, co2 > 0 ] 
          in liftIO $ plotCo2 country outfile (sort xys)
    _ -> putStrLn "usage: <country>"

