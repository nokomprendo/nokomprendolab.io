{-# LANGUAGE OverloadedStrings #-}

import Control.Applicative
import Control.Monad.Trans.Resource
import Data.Attoparsec.ByteString.Char8 as A
import Data.Attoparsec.ByteString.Streaming
import Data.ByteString (ByteString)
import Data.Function ((&))
import Data.Functor (void)
import Streaming.ByteString qualified as Q
import Streaming.Prelude qualified as S

-------------------------------------------------------------------------------
-- parsers
-------------------------------------------------------------------------------

fieldP :: Parser ByteString
fieldP = A.takeWhile (\c -> c /= ',' && c /= '\r' && c /= '\n')

skipRowP :: Parser ()
skipRowP = skipWhile (/= '\n')

rowP :: Parser ByteString
rowP = fieldP <* skipRowP <* (endOfLine <|> endOfInput)

-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

infile :: FilePath
infile = "data/test.csv"

main :: IO ()
main = runResourceT 
    $ Q.readFile infile
    & parsed rowP 
    & void
    & S.drop 1
    & S.group
    & S.mapped S.head
    & S.catMaybes
    & S.map (<> "\n")
    & Q.fromChunks
    & Q.stdout

