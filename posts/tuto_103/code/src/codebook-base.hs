{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

import Control.Applicative
import Control.Monad (forM_)
import Control.Monad.IO.Class
import Control.Monad.Trans.Resource
import Data.Attoparsec.ByteString.Char8 as A
import Data.ByteString qualified as B
import Data.ByteString (ByteString)
import Data.Text (Text)
import Lucid
import Text.RawString.QQ
import TextShow

-------------------------------------------------------------------------------
-- types
-------------------------------------------------------------------------------

data ColumnData = ColumnData
  { _name         :: ByteString
  , _description  :: ByteString
  , _source       :: ByteString
  }

-------------------------------------------------------------------------------
-- parsers
-------------------------------------------------------------------------------

quotedP :: Parser ByteString
quotedP = char '"' *> A.takeWhile (/='"') <* char '"'

fieldP :: Parser ByteString
fieldP 
  =   quotedP
  <|> A.takeWhile (\c -> c /= ',' && c /= '\r' && c /= '\n')

rowP :: Parser ColumnData
rowP = ColumnData 
  <$> fieldP <* char ','
  <*> fieldP <* char ','
  <*> fieldP <* (endOfLine <|> endOfInput)

fileP :: Parser [ColumnData]
fileP = manyTill rowP endOfInput

-------------------------------------------------------------------------------
-- view
-------------------------------------------------------------------------------

myTitle :: Html ()
myTitle = "OWID, CO2 codebook (base)"

myCss :: Text
myCss = 
  [r| 
    table {
      border-collapse: collapse;
    }
    tr:nth-child(even) {
      background-color: lightgrey;
    }
    td {
      border: 1px solid black;
    }
  |]

htmlPage :: [ColumnData] -> Html ()
htmlPage cols = doctypehtml_ $ do
  head_ $ do
    meta_ [charset_ "utf-8"]
    meta_ [ name_ "viewport"
          , content_ "width=device-width,initial-scale=1,shrink-to-fit=no"]
    style_ myCss
    title_ myTitle
  body_ $ do
    h1_ myTitle
    table_  $ do
      tr_ $ do
        th_ "num"
        th_ "column"
        th_ "description"
        th_ "source"
      forM_ (zip cols [1::Int ..]) $ \(col,i) -> do
        tr_ [style_ "border-bottom: 1 px solid black"] $ do
          td_ $ toHtml $ showt i
          td_ $ toHtml $ _name col
          td_ $ toHtml $ _description col
          td_ $ toHtml $ _source col

-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

infile, outfile :: FilePath
infile = "data/owid-co2-codebook.csv"
outfile = "out-codebook-base.html"

main :: IO ()
main = runResourceT $ do
  res <- liftIO $ B.readFile infile 
  case parseOnly fileP res of
    Left err -> liftIO $ putStrLn err
    Right (_:xs) -> liftIO $ renderToFile outfile $ htmlPage xs
    _ -> liftIO $ putStrLn "no data"

