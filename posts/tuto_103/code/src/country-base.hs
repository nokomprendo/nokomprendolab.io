{-# LANGUAGE OverloadedStrings #-}

import Control.Applicative
import Control.Monad.IO.Class
import Control.Monad.Trans.Resource
import Data.Attoparsec.ByteString.Char8 as A
import Data.ByteString qualified as B
import Data.ByteString (ByteString)
import Data.Set qualified as Set

-------------------------------------------------------------------------------
-- parsers
-------------------------------------------------------------------------------

fieldP :: Parser ByteString
fieldP = A.takeWhile (\c -> c /= ',' && c /= '\r' && c /= '\n')

skipRowP :: Parser ()
skipRowP = skipWhile (/= '\n')

rowP :: Parser ByteString
rowP = fieldP <* skipRowP <* (endOfLine <|> endOfInput)

fileP :: Parser (Set.Set ByteString)
fileP = headerP *> bodyP
  where
    headerP = skipRowP 
    bodyP 
      =   endOfInput *> pure Set.empty
      <|> Set.insert <$> rowP <*> bodyP

-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

infile :: FilePath
infile = "data/test.csv"

main :: IO ()
main = runResourceT $ do
  res <- liftIO $ B.readFile infile 
  case parseOnly fileP res of
    Left err -> liftIO $ putStrLn $ "parse error: " <> err
    Right d -> liftIO $ mapM_ (B.putStr . (<> "\n")) d

