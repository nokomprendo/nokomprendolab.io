{-# LANGUAGE OverloadedStrings #-}

-- import Data.Text qualified as T
import Data.Text.Lazy.IO qualified as T
import Lucid

htmlPage :: Monad m => HtmlT m ()
htmlPage = do
  let title = "myhtml2"
  doctypehtml_ $ do
    head_ $ do
      title_ title
    body_ $ do
      h1_ title

main :: IO ()
main = do
  html <- renderTextT htmlPage
  T.putStrLn html

-- TODO runResourcet $ ... each ... renderBST ... S.print


