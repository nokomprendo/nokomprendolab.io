{-# LANGUAGE OverloadedStrings #-}

import Control.Applicative ((<|>))
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Resource (runResourceT)
import Data.Attoparsec.ByteString.Char8 qualified as A
import Data.Attoparsec.ByteString.Streaming (parsed)
import Data.ByteString qualified as BS
import Data.ByteString.Char8 qualified as BC
import Data.Function ((&))
import Data.List (sort)
import Graphics.Rendering.Chart.Easy ((.=), def, layout_title, line, plot)
import Graphics.Rendering.Chart.Backend.Cairo (toFile)
import Streaming.ByteString qualified as Q
import Streaming.Prelude qualified as S
import Streaming.Zip (gunzip)
import System.Environment (getArgs)

-------------------------------------------------------------------------------
-- types
-------------------------------------------------------------------------------

data Co2Data = Co2Data
  { _country  :: BS.ByteString
  , _year     :: Integer
  , _co2      :: Integer
  } deriving (Show)

-------------------------------------------------------------------------------
-- parsers
-------------------------------------------------------------------------------

fieldP :: A.Parser BS.ByteString
fieldP = A.takeWhile (/= ',') <* A.char ','

skipFieldP :: A.Parser ()
skipFieldP = A.skipWhile (/= ',') <* A.char ','

skipRowP :: A.Parser ()
skipRowP = A.skipWhile (/= '\n') <* A.char '\n'

intFieldP :: A.Parser Integer
intFieldP = (A.decimal <|> pure 0) <* skipFieldP

rowP :: A.Parser Co2Data
rowP = Co2Data 
  <$> fieldP 
  <*> intFieldP <* A.count 5 skipFieldP 
  <*> intFieldP <* skipRowP

-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

plotCo2 :: String -> FilePath -> [(Integer, Integer)] -> IO ()
plotCo2 country filename xys = toFile def filename $ do
  layout_title .= "CO2 emission in Mt (by co2-stream)"
  plot (line country [ xys ])

main :: IO ()
main = do
  let infile = "data/owid-co2-data.csv.gz"
      outfile = "out-co2-stream.png"
  args <- getArgs
  case args of
    [country] -> runResourceT $ do
      let countryC = BC.pack country
      xys <- Q.readFile infile
        & gunzip
        & parsed rowP 
        & S.drop 1
        & S.filter (\(Co2Data c _ co2) -> c == countryC && co2 > 0)
        & S.map (\(Co2Data _ y co2) -> (y, co2))
        & S.toList_
      liftIO $ plotCo2 country outfile (sort xys)
    _ -> putStrLn "usage: <country>"

