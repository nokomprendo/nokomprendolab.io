{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

import Control.Applicative
import Control.Monad (forM_)
import Control.Monad.Trans.Resource
import Data.Attoparsec.ByteString.Char8 as A
import Data.Attoparsec.ByteString.Streaming
import Data.ByteString (ByteString)
import Data.Function ((&))
import Data.Functor (void)
import Data.Text (Text)
import Lucid
import Streaming.ByteString qualified as Q
import Streaming.Prelude qualified as S
import Text.RawString.QQ
import TextShow

-------------------------------------------------------------------------------
-- types
-------------------------------------------------------------------------------

data ColumnData = ColumnData
  { _name         :: ByteString
  , _description  :: ByteString
  , _source       :: ByteString
  } deriving Show

-------------------------------------------------------------------------------
-- parsers
-------------------------------------------------------------------------------

quotedP :: Parser ByteString
quotedP = char '"' *> A.takeWhile (/='"') <* char '"'

fieldP :: Parser ByteString
fieldP 
  =   quotedP
  <|> A.takeWhile (\c -> c /= ',' && c /= '\r' && c /= '\n')

rowP :: Parser ColumnData
rowP = ColumnData 
  <$> fieldP <* char ','
  <*> fieldP <* char ','
  <*> fieldP <* (endOfLine <|> endOfInput)

-------------------------------------------------------------------------------
-- view
-------------------------------------------------------------------------------

myTitle :: Html ()
myTitle = "OWID, CO2 codebook (stream)"

myCss :: Text
myCss = 
  [r| 
    table {
      border-collapse: collapse;
    }
    tr:nth-child(even) {
      background-color: lightgrey;
    }
    td {
      border: 1px solid black;
    }
  |]

htmlPage :: [ColumnData] -> Html ()
htmlPage cols = doctypehtml_ $ do
  head_ $ do
    meta_ [charset_ "utf-8"]
    meta_ [ name_ "viewport"
          , content_ "width=device-width,initial-scale=1,shrink-to-fit=no"]
    style_ myCss
    title_ myTitle
  body_ $ do
    h1_ myTitle
    table_  $ do
      tr_ $ do
        th_ "num"
        th_ "column"
        th_ "description"
        th_ "source"
      forM_ (zip cols [1::Int ..]) $ \(col,i) -> do
        tr_ [style_ "border-bottom: 1 px solid black"] $ do
          td_ $ toHtml $ showt i
          td_ $ toHtml $ _name col
          td_ $ toHtml $ _description col
          td_ $ toHtml $ _source col

-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

infile, outfile :: FilePath
infile = "data/owid-co2-codebook.csv"
outfile = "out-codebook-stream.html"

main :: IO ()
main = do
  res <- runResourceT 
    $ Q.readFile infile
    & parsed rowP 
    & void
    & S.drop 1
    & S.toList_
  case res of
    [] -> putStrLn "no data"
    xs -> renderToFile outfile (htmlPage xs)

