#!/bin/sh

FILE0="test0.csv"
FILE="test.csv"
NB_ITER=40

rm -f $FILE0 $FILE

cp owid-co2-data.csv.gz $FILE0.gz
gzip -d $FILE0.gz 

for i in `seq $NB_ITER` ; do
  cat $FILE0 >> $FILE
done

