{-# LANGUAGE DeriveGeneric #-}

module Film where

import Data.Aeson
import Data.Text
import GHC.Generics

data Film = Film
    { _title :: Text
    , _director :: Text
    , _year :: Maybe Int
    } deriving (Generic, Show)

instance ToJSON Film

