{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Routes where

import Data.Text
import Servant
import Servant.HTML.Lucid

import Film

type DirFilmsRoute = "dirFilms" :> Capture "director" Text :> Get '[JSON] [Film]

type AllFilmsRoute = "allFilms" :> Get '[HTML,JSON] [Film]

