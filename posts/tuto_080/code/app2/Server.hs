{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}

module Server where

import Control.Monad
import Data.Text (Text)
import Lucid
import Network.Wai.Handler.Warp (run)
import Servant 

import Film
import Routes

type ServerApi
    =    DirFilmsRoute 
    :<|> AllFilmsRoute

handleServerApi :: Server ServerApi
handleServerApi 
    =    handleDirFilmsRoute 
    :<|> pure myFilms

handleDirFilmsRoute :: Text -> Handler [Film]
handleDirFilmsRoute dir = pure $ filter ((==dir) . _director) myFilms

myFilms :: [Film]
myFilms =
    [ Film "First Blood" "Kotcheff" (Just 1982)
    , Film "Paths of Glory" "Kubrick" (Just 1957)
    , Film "Full Metal Jacket" "Kubrick" Nothing
    ]

serverApp :: Application
serverApp = serve (Proxy @ServerApi) handleServerApi

main :: IO ()
main = do
    let port = 3000
    putStrLn $ "listening on port: " ++ show port ++ "..."
    run port serverApp

instance ToHtml [Film] where
    toHtmlRaw = toHtml

    toHtml films = doctypehtml_ $ do
        body_ $ do
            h2_ "films"
            ul_ $ forM_ films $ \film -> li_ $ toHtml $ show film
            h2_ "routes"
            ul_ $ do
                li_ $ a_ [href_ (dirFilmsUrl "Kotcheff")] "dirFilmsUrl Kotcheff"
                li_ $ a_ [href_ (dirFilmsUrl "Kubrick")] "dirFilmsUrl Kubrick"

dirFilmsUrl :: Text -> Text
dirFilmsUrl dir = toUrlPiece $ safeLink (Proxy @ServerApi) (Proxy @DirFilmsRoute) dir

