{-# LANGUAGE TypeApplications #-}

module Client where

import Data.Text
import Data.Proxy (Proxy(..))
import Network.HTTP.Client (defaultManagerSettings, newManager)
import Servant.Client 

import Routes

queryLog :: Double -> ClientM (Maybe Double)
queryLog = client (Proxy @LogRoute)

queryAdd :: Maybe Double -> Maybe Double -> ClientM Double
queryAdd = client (Proxy @AddRoute)

queryLogAdd :: ClientM Double
queryLogAdd = do
    mx <- queryLog 0
    queryAdd mx (Just 42)

main :: IO ()
main = do
    myManager <- newManager defaultManagerSettings
    let myClient = mkClientEnv myManager (BaseUrl Http "localhost" 3000 "")
    runClientM (queryLog 1) myClient >>= print
    runClientM (queryAdd Nothing (Just 42)) myClient >>= print
    runClientM queryLogAdd myClient >>= print

