{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Routes where

import Servant

type LogRoute 
    = "log" 
    :> Capture "x" Double 
    :> Get '[JSON] (Maybe Double)

type AddRoute 
    = "add" 
    :> QueryParam' '[] "x" Double 
    :> QueryParam' '[] "y" Double 
    :> Get '[JSON] Double

