{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}

module Server where

import Control.Monad
import Data.Maybe
import Data.Text (Text)
import Lucid
import Network.Wai.Handler.Warp (run)
import Servant 
import Servant.HTML.Lucid

import Routes

type ServerApi
    =    LogRoute
    :<|> AddRoute

handleServerApi :: Server ServerApi
handleServerApi
    =    handleLogRoute
    :<|> handleAddRoute

handleLogRoute :: Double -> Handler (Maybe Double)
handleLogRoute x
    | x <= 0 = pure Nothing
    | otherwise = pure $ Just $ log x

handleAddRoute :: Maybe Double -> Maybe Double -> Handler Double
handleAddRoute mx my =
    let x = fromMaybe 0 mx
        y = fromMaybe 0 my
    in pure $ x+y

serverApp :: Application
serverApp = serve (Proxy @ServerApi) handleServerApi

main :: IO ()
main = do
    let port = 3000
    putStrLn $ "listening on port: " ++ show port ++ "..."
    run port serverApp

