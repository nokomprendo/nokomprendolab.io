set out 'out-plot-copy.png'
set terminal png size 500,400
set style data boxplot
set style fill solid 0.5 border -1
set style boxplot nooutliers 
set xtics nomirror
set ytics nomirror
set yrange [0:]
set title 'Impact of Copy vs Reference, on the execution time'
set xtics ('Mc copy' 1, 'Mc ref' 2, 'Mcts copy' 3, 'Mcts ref' 4)
plot 'out-30-mc-200-100-10.csv' using (1):6 notitle lc rgb 'purple', \
     'out-3-mc-200-100-10.csv' using (2):6 notitle lc rgb 'purple', \
     'out-30-mcts-200-100-10.csv' using (3):6 notitle lc rgb 'orange', \
     'out-3-mcts-200-100-10.csv' using (4):6 notitle lc rgb 'orange'

