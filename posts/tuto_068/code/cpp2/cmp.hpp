#include "Bot.hpp"

#include <chrono>

std::tuple<double, double, double, double> run(Bot & botR, Bot & botY, int nGames) {

    auto t0 = std::chrono::steady_clock::now();

    int nWinR = 0;
    int nWinY = 0;
    int nTie = 0;

    Game game;
    for (int n=0; n<nGames; n++) {
        playoutBots(game, botR, botY);
        switch (game.getStatus()) {
            case Status::WinR: nWinR++; break;
            case Status::WinY: nWinY++; break;
            case Status::Tie: nTie++; break;
            default: abort();
        }
        game.newGame();
    }

    const double nGamesD = nGames;
    const double winR = nWinR / nGamesD;
    const double winY = nWinY / nGamesD;
    const double tie = nTie / nGamesD;

    auto t1 = std::chrono::steady_clock::now();
    const double dt = std::chrono::duration<double>(t1 - t0).count();

    return { winR, winY, tie, dt };
}

