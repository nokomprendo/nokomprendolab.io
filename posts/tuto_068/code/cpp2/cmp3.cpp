#include "cmp.hpp"

#include <iostream>

int main(int argc, const char ** argv) {

    if (argc != 5) {
        std::cout << "cmp3: run Mc/Mcts player against Random" << std::endl;
        std::cout << "usage: <mc/mcts> <nsims> <ngames> <nrepeat>" << std::endl;
        exit(-1);
    }
    const std::string algo = argv[1];
    const int nsims = atoi(argv[2]);
    const int ngames = atoi(argv[3]);
    const int nrepeat = atoi(argv[4]);

    std::unique_ptr<Bot> botR = std::make_unique<BotRandom>();
    std::unique_ptr<Bot> botY;

    if (algo == "mc")
        botY = std::make_unique<BotMc>(nsims);
    else if (algo == "mcts")
        botY = std::make_unique<BotMcts>(nsims);
    else {
        std::cout << "unknown algo" << std::endl;
        exit(-1);
    }

    std::cout << "winR winY tie ry ryt dt nGames\n";

    for (int n=0; n<nrepeat; n++) {
        auto [ winR, winY, tie, dt ] = run(*botR, *botY, ngames);
        std::cout << winR << ' ' << winY << ' ' << tie << ' ' 
            << winR+winY << ' ' << winR+winY+tie << ' ' 
            << dt << ' ' << ngames << '\n';
    }

    return 0;
}

