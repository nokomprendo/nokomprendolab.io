#include "Game.hpp"

#include <algorithm>
#include <functional>
#include <limits>
#include <memory>
#include <random>

const double KUCT = 0.5;   // TODO tune kuct

using random_t = std::mt19937;

class Bot {
    public:
        virtual ~Bot() = default;
        // virtual int genmove(const Game & game) = 0;
        virtual int genmove(Game game) = 0;
};

int random(random_t & rng, int n);
Status playoutRandom(Game & game, random_t & rng);
void playoutBots(Game & game, Bot & botR, Bot & botY);
double computeScore(Status s, Player p);

//////////////////////////////////////////////////////////////////////
// BotMcts
//////////////////////////////////////////////////////////////////////

class BotRandom : public Bot {
    private:
        random_t _rng;

    public:
        BotRandom();
        // int genmove(const Game & game) override;
        int genmove(Game game) override;
};

//////////////////////////////////////////////////////////////////////
// BotMcts
//////////////////////////////////////////////////////////////////////

class BotMc : public Bot {
    private:
        random_t _rng;
        const int _nSimsPerMove;

    public:
        BotMc(int nSimsPerMove);
        // int genmove(const Game & game) override;
        int genmove(Game game) override;

    protected:
        double evalMove(const Game & game, int k);
};

//////////////////////////////////////////////////////////////////////
// BotMcts
//////////////////////////////////////////////////////////////////////

class BotMcts : public Bot {
    private:
        random_t _rng;
        const int _nIters;

    public:
        BotMcts(int nIters);
        // int genmove(const Game & game) override;
        int genmove(Game game) override;
};

struct Node {
    double _reward;
    int _nSims;
    Game _game;
    int _nMoves;
    Player _player;  // before move
    Node * _parent;
    std::vector<std::unique_ptr<Node>> _children;
    Node(const Node &) = delete;
    Node(const Game & game);
    Node(const Game & game, Node * parent, int k0);
};

double ucb1(double cReward, double cNsims, int pNsims);
Node * selectUcb(const Node * n);
Node * selectAndExpand(Node * root);
Status simulate(const Node * node, random_t & rng);
void backpropagate(Node * node, Status status);
int bestNode(const Node & root);

