#include "cmp.hpp"

#include <iostream>

int main() {

    BotMc botR(128);
    BotMcts botY(512);

    const int nGames = 100;
    const int nRepeat = 10;

    std::cout << "winR winY tie ry ryt dt nGames\n";

    for (int n=0; n<nRepeat; n++) {
        auto [ winR, winY, tie, dt ] = run(botR, botY, nGames);
        std::cout << winR << ' ' << winY << ' ' << tie << ' ' 
            << winR+winY << ' ' << winR+winY+tie << ' ' 
            << dt << ' ' << nGames << '\n';
    }

    return 0;
}

