set out 'out-plot-options.png'
set terminal png size 400,400
set style data boxplot
set style fill solid 0.5 border -1
set style boxplot outliers pointtype 7
set xtics nomirror
set ytics nomirror
set yrange [0:]
set title 'Impact of the compiler options on the execution time'
set xtics ('no optim' 1, 'default' 2, 'full' 3)
plot 'out-cmp1a.csv' using (1):6 notitle, \
     'out-cmp1b.csv' using (2):6 notitle, \
     'out-cmp1c.csv' using (3):6 notitle

