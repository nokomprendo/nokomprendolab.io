#include "Bot.hpp"

//////////////////////////////////////////////////////////////////////
// helpers
//////////////////////////////////////////////////////////////////////

int random(random_t & rng, int n) {
    std::uniform_int_distribution<int> dist(0, n-1);
    return dist(rng);
}

Status playoutRandom(Game & game, random_t & rng) {
    while (game.isRunning()) {
        const int k = random(rng, game.getMoves().size());
        game.playK(k);
    }
    return game.getStatus();
}

void playoutBots(Game & game, Bot & botR, Bot & botY) {
    while (game.isRunning()) {
        Bot & bot = game.getCurrentPlayer() == Player::R ? botR : botY;
        const int k = bot.genmove(game);
        game.playK(k);
    }
}

double computeScore(Status s, Player p) {
    if ((s == Status::WinR and p == Player::R) or (s == Status::WinY and p == Player::Y))
        return 1.0;
    if (s == Status::Tie)
        return 0.5;
    return 0.0;
}

//////////////////////////////////////////////////////////////////////
// BotRandom
//////////////////////////////////////////////////////////////////////

BotRandom::BotRandom() : _rng(std::random_device{}()) {}

int BotRandom::genmove(const Game & game) {
// int BotRandom::genmove(Game game) {
    const int nMoves = game.getMoves().size();
    return random(_rng, nMoves);
}

//////////////////////////////////////////////////////////////////////
// BotMc
//////////////////////////////////////////////////////////////////////

BotMc::BotMc(int nSimsPerMove) : 
    _rng(std::random_device{}()), 
    _nSimsPerMove(nSimsPerMove) 
{}

int BotMc::genmove(const Game & game) {
// int BotMc::genmove(Game game) {
    const int nMoves = game.getMoves().size();
    int bestK = 0;
    double bestScore = 0.0;
    for (int k=0; k<nMoves; k++) {
        const double score = evalMove(game, k);
        if (score > bestScore) {
            bestScore = score;
            bestK = k;
        }
    }
    return bestK;
}

double BotMc::evalMove(const Game & game, int k) {
    const Player player = game.getCurrentPlayer();
    Game g1(game);
    g1.playK(k);
    double score = 0.0;
    for (int n=0; n<_nSimsPerMove; n++) {
        Game g(g1);
        Status status = playoutRandom(g, _rng);
        score += computeScore(status, player);
    }
    return score;
}

//////////////////////////////////////////////////////////////////////
// BotMcts
//////////////////////////////////////////////////////////////////////

BotMcts::BotMcts(int nIters) : 
    _rng(std::random_device{}()), 
    _nIters(nIters) 
{}

int BotMcts::genmove(const Game & game) {
// int BotMcts::genmove(Game game) {
    Node root(game);
    for (int i=0; i<_nIters; i++) {
        Node * node = selectAndExpand(&root);
        Status status = simulate(node, _rng);
        backpropagate(node, status);
    }
    return bestNode(root);
}

Node::Node(const Game & game) :
    _reward(0), _nSims(0), _game(game), 
    _nMoves(_game.getMoves().size()), 
    _player(game.getCurrentPlayer()), _parent(nullptr) 
{
    _children.reserve(_nMoves);
}

Node::Node(const Game & game, Node * parent, int k0) :
    _reward(0), _nSims(0), _game(game), _parent(parent) 
{
    _player = _game.getCurrentPlayer();
    _game.playK(k0);
    _nMoves = int(_game.getMoves().size());
    _children.reserve(_nMoves);
}

double ucb1(double cReward, double cNsims, int pNsims) {
    assert (cNsims > 0);
    const double exploitation = cReward / cNsims;
    const double exploration = std::sqrt(std::log(1 + pNsims) / cNsims);
    return exploitation + KUCT*exploration;
}

Node * selectUcb(const Node * n) {
    int bestI = -1;
    double bestScore = -1.0;
    for (int i=0; i<n->_nMoves; i++) {
        const auto & c = n->_children[i];
        const double s = ucb1(c->_reward, c->_nSims, n->_nSims);
        if (s > bestScore) {
            bestScore = s;
            bestI = i;
        }
    }
    assert(bestI > -1);
    return n->_children[bestI].get();
}

Node * selectAndExpand(Node * root) {
    Node * n = root;
    while (true) {
        // return node if game terminated
        if (not n->_game.isRunning())
            return n;
        // expand if new child found
        const int k = n->_children.size();
        if (k < n->_nMoves) {
            n->_children.push_back(std::make_unique<Node>(n->_game, n, k));
            return n->_children.back().get();
        }
        // select child node using UCB
        n = selectUcb(n);
    }
}

Status simulate(const Node * node, random_t & rng) {
    Game g(node->_game);
    return playoutRandom(g, rng);
}

void backpropagate(Node * node, Status status) {
    Node * n = node;
    while (n) {
        n->_reward += computeScore(status, n->_player);
        n->_nSims += 1;
        n = n->_parent;
    }
}

int bestNode(const Node & root) {
    const auto & cs = root._children;
    auto cmp = [](const std::unique_ptr<Node> & n1, const std::unique_ptr<Node> & n2)
        { return n1->_nSims < n2->_nSims; };
    auto iter = std::max_element(cs.begin(), cs.end(), cmp);
    return std::distance(cs.begin(), iter);
}

