#include "cmp.hpp"

#include <fstream>
#include <iostream>
#include <memory>

void test1(const std::string & name, int nGames,
        std::function<std::unique_ptr<Bot>(int)> mkBot,
        std::unique_ptr<Bot> & botY,
        std::vector<int> values) {

    std::cout << name << std::endl;
    std::ofstream ofs("out-test1-" + name + ".csv");
    ofs << "winR winY tie ry ryt dt nGames value\n";
    for (int value : values) {
        auto botR = mkBot(value);
        auto [ winR, winY, tie, dt ] = run(*botR, *botY, nGames);
        ofs << winR << ' ' << winY << ' ' << tie << ' ' 
            << winR+winY << ' ' << winR+winY+tie << ' ' 
            << dt << ' ' << nGames << ' ' << value << '\n';
    }
}

void test2(const std::string & name, int nGames,
        std::function<std::unique_ptr<Bot>(int)> mkBotR,
        std::function<std::unique_ptr<Bot>(int)> mkBotY,
        std::vector<std::tuple<int, int>> valuesRY) {

    std::cout << name << std::endl;
    std::ofstream ofs("out-test2-" + name + ".csv");
    ofs << "winR winY tie ry ryt dt nGames valueR valueY\n";
    for (auto [valueR, valueY] : valuesRY) {
        auto botR = mkBotR(valueR);
        auto botY = mkBotY(valueY);
        auto [ winR, winY, tie, dt ] = run(*botR, *botY, nGames);
        ofs << winR << ' ' << winY << ' ' << tie << ' ' 
            << winR+winY << ' ' << winR+winY+tie << ' ' 
            << dt << ' ' << nGames << ' ' << valueR << ' ' << valueY << '\n';
    }
}

int main() {
    
    auto mkBotMc = [](int v){ return std::make_unique<BotMc>(v);};
    auto mkBotMcts = [](int v){ return std::make_unique<BotMcts>(v);};

    std::unique_ptr<Bot> botRandom = std::make_unique<BotRandom>();
    std::unique_ptr<Bot> botMc128 = std::make_unique<BotMc>(128);
    std::unique_ptr<Bot> botMcts512 = std::make_unique<BotMcts>(512);

    const int nGames = 300;

    test1("McX-Random", nGames, mkBotMc, botRandom, 
            {8, 16, 32, 64, 128});
    test1("MctsX-Random", nGames, mkBotMcts, botRandom, 
            {8*6, 16*6, 32*6, 64*6, 128*6});
    
    test2("McX-MctsX", nGames, mkBotMc, mkBotMcts,
            {{8,8*6}, {16,16*6}, {32,32*6}, {64,64*6}, {128,128*6}});

    return 0;
}

