set out 'out-plot-algos-status.png'
set terminal png size 500,300

set style fill solid 1.5 border rgb 'black'
set key outside right center invert reverse Left

set xrange [8:128]
set title 'Mcts vs Monte-Carlo'
plot 'out-test2-McX-MctsX.csv' using 8:1 title 'MC' with filledcurve x1 lc rgb 'purple', \
    '' using 8:1:4 title 'Mcts' with filledcurves lc rgb 'orange', \
    '' using 8:4:5 title 'Tie' with filledcurve lc rgb 'light-blue', \
    '' using 8:1:4 notitle with lines lw 1.5 lc rgb 'black', \
    '' using 8:4:5 notitle with lines lw 1.5 lc rgb 'black'

