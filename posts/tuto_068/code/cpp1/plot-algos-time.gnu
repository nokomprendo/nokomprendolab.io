set out 'out-plot-algos-time.png'
set terminal png size 500,300

set key outside right center reverse Left invert
set grid 

set xrange [8:128]
set title 'Computation time'
plot 'out-test1-McX-Random.csv' using 8:6 title 'Mc' with lines lw 2 lc 'purple', \
     'out-test1-MctsX-Random.csv' using ($8/6):6 title 'Mcts' with lines lw 2 lc 'orange'

