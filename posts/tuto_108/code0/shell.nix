
let
 pkgs = import <nixpkgs> {};

 # ocamlPackages = pkgs.ocaml-ng.ocamlPackages;
 # ocamlPackages = pkgs.ocaml-ng.ocamlPackages_latest;
 ocamlPackages = pkgs.ocaml-ng.ocamlPackages_5_1;

in pkgs.mkShell {
  nativeBuildInputs = with ocamlPackages; [
    ocaml
    ocaml-lsp
    ocamlformat
    utop
  ];

  buildInputs = with ocamlPackages; [
  ];

}

