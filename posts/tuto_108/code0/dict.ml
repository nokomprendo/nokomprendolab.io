
(* définir une interface *)

module type Dict = sig
  type key
  type value
  type t
  val new_dict : t
  val insert_dict : key -> value -> t -> t
  val lookup_dict : key -> t -> value option
end 

(* implémenter une interface par un foncteur *)

module type KeyType = sig
  type t
  val compare : t -> t -> int
end

module type ValueType = sig
  type t
end

module MakeTree (K : KeyType) (V : ValueType) = struct

  type key = K.t

  type value = V.t

  type t =
    | Node of key * value * t * t
    | Leaf

  let new_dict = Leaf

  let rec insert_dict k v t =
    match t with
    | Leaf -> Node (k, v, Leaf, Leaf)
    | Node (ki, vi, left, right) -> 
        let c = K.compare k ki in
        if c < 0 then
          Node (ki, vi, insert_dict k v left, right)
        else if c > 0 then
          Node (ki, vi, left, insert_dict k v right)
        else
          Node (k, v, left, right)

  let rec lookup_dict k t =
    match t with
    | Leaf -> None
    | Node (ki, vi, left, right) ->
        let c = K.compare k ki in
        if c < 0 then
          lookup_dict k left
        else if c > 0 then
          lookup_dict k right
        else
          Some vi
end  

(* autre implémentation *)

module MakeList(K : KeyType) (V : ValueType) = struct

  type key = K.t

  type value = V.t

  type t = (key * value) list

  let new_dict = []

  let rec insert_dict k v = function
    | [] -> [(k, v)]
    | (ki, vi) :: xs -> 
        if K.compare k ki = 0 then
          (k, v) :: xs
        else
          (ki, vi) :: insert_dict k v xs

  let rec lookup_dict k = function
    | [] -> None
    | (ki, vi) :: xs -> 
        if K.compare k ki = 0 then
          Some vi
        else
          lookup_dict k xs
end  

(* instancier un foncteur *)

module IntKey = struct
  type t = int
  let compare = Stdlib.compare 
end

module IntBoolTree = MakeTree(IntKey)(Bool)

let dict1 = IntBoolTree.(new_dict
  |> insert_dict 2 true
  |> insert_dict 1 false
  |> insert_dict 5 true
  |> insert_dict 3 false)

module IntBoolList = MakeList(IntKey)(Bool)

let dict2 = IntBoolList.(new_dict
  |> insert_dict 2 true
  |> insert_dict 1 false
  |> insert_dict 5 true
  |> insert_dict 3 false)

module LowstrKey = struct
  type t = string 
  let compare s1 s2 = 
    Stdlib.compare (String.lowercase_ascii s1) (String.lowercase_ascii s2)
end

module LowstrIntTree = MakeTree(LowstrKey)(Int)

let dict3 = LowstrIntTree.(new_dict
  |> insert_dict "fooBar" 42
  |> insert_dict "FOO" 13
  |> insert_dict "bAR" 37)

module LowstrIntList = MakeList(LowstrKey)(Int)

let dict4 = LowstrIntList.(new_dict
  |> insert_dict "fooBar" 42
  |> insert_dict "FOO" 13
  |> insert_dict "bAR" 37)

(* réutiliser une interface *)

module type ShowKV = sig
  type k
  type v
  val show_k : k -> string
  val show_v : v -> string
end

module Elems 
  (D : Dict) 
  (S : ShowKV with type k = D.key and type v = D.value) = struct

  let elems ks t =
    let elem_k k = 
      let k_str = S.show_k k in
      let v_str =
        match D.lookup_dict k t with
        | None -> "not found"
        | Some v -> S.show_v v
      in
        Printf.printf "%s -> %s\n" k_str v_str
    in
      List.iter elem_k ks
end

module ShowIntBool = struct
  type k = int
  type v = bool
  let show_k = string_of_int
  let show_v = string_of_bool
end

module ElemsIntBoolTree = Elems(IntBoolTree)(ShowIntBool) ;;
module ElemsIntBoolList = Elems(IntBoolList)(ShowIntBool) ;;

ElemsIntBoolTree.elems [3; 4] dict1 ;;
ElemsIntBoolList.elems [3; 4] dict2 ;;

module ShowLowstrInt = struct
  type k = LowstrKey.t
  type v = int
  let show_k = String.lowercase_ascii
  let show_v = string_of_int
end

module ElemsLowstrIntTree = Elems(LowstrIntTree)(ShowLowstrInt) ;;
module ElemsLowstrIntList = Elems(LowstrIntList)(ShowLowstrInt) ;;

ElemsLowstrIntTree.elems ["fOO"; "Blou"] dict3 ;;
ElemsLowstrIntList.elems ["fOO"; "Blou"] dict4 ;;

