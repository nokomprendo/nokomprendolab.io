(* main.ml *)

open Mylib

(* listIntBool *)

(*
module IntKey = struct
  type t = int
  let compare = Stdlib.compare
end
module ListIntBool = Mylist.Make(IntKey)
*)

module ListInt = Mylist.Make(Int)

let listIntBool = ListInt.(new_dict
  |> insert_dict 1 true
  |> insert_dict 2 false
  |> insert_dict 3 true)

module ElemsListIntBool = Myelems.Make(ListInt)(Myshow.MyshowInt)(Myshow.MyshowBool)


(* treeIntBool *)

module TreeInt = Mytree.Make(Int)

module ElemsTreeIntBool = Myelems.Make(TreeInt)(Myshow.MyshowInt)(Myshow.MyshowBool)

let treeIntBool = TreeInt.(new_dict
  |> insert_dict 1 true
  |> insert_dict 2 false
  |> insert_dict 3 true)



(* listStringInt *)

module ListString = Mylist.Make(String)

let listStringInt = ListString.(new_dict
  |> insert_dict "foo" 13
  |> insert_dict "Bar" 37)

module ElemsListStringInt = Myelems.Make(ListString)(Myshow.MyshowString)(Myshow.MyshowInt)


(* listLowstrInt *)

module ListLowstr = Mylist.Make(Mylowstr)

let listLowstrInt = ListLowstr.(new_dict
  (* |> insert_dict (Mylowstr.Lowstr "foobar") 42 *)
  |> insert_dict (Mylowstr.mkLowstr "foo") 13
  |> insert_dict (Mylowstr.mkLowstr "Bar") 37)

module ElemsListLowstrInt = Myelems.Make(ListLowstr)(Mylowstr)(Myshow.MyshowInt)


(* main *)

let () =
  ElemsListIntBool.elems [1; 2; 4] listIntBool ;
  ElemsTreeIntBool.elems [1; 2; 4] treeIntBool ;
  ElemsListStringInt.elems ["foo"; "Foo"] listStringInt ;
  ElemsListLowstrInt.elems [Mylowstr.mkLowstr "foo"; Mylowstr.mkLowstr "Foo"] listLowstrInt


