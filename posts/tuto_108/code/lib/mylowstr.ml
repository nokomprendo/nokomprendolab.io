(* mylowstr.ml *)

type t = Lowstr of string

let mkLowstr str = Lowstr (String.lowercase_ascii str)

let compare (Lowstr s1) (Lowstr s2) = compare s1 s2

let myshow (Lowstr str) = str

