(* mydict.ml *)

module type S = sig
  type key
  type 'a t
  val new_dict : 'a t
  val insert_dict : key -> 'a -> 'a t -> 'a t
  val lookup_dict : key -> 'a t -> 'a option
end 

module type KeyType = sig
  type t
  val compare : t -> t -> int
end

