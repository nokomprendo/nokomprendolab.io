(* myshow.ml *)

module type S = sig
  type t
  val myshow : t -> string
end

module MyshowInt = struct
  type t = int
  let myshow = string_of_int
end

module MyshowBool = struct
  type t = bool
  let myshow = string_of_bool
end

module MyshowString = struct
  type t = string
  let myshow = Fun.id
end

