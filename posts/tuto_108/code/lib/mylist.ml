(* mylist.ml *)

module Make(K : Mydict.KeyType) = struct

  type key = K.t

  type 'a t = (key * 'a) list

  let new_dict = []

  let rec insert_dict k v = function
    | [] -> [(k,v)]
    | (ki,vi) :: kvs -> 
      if K.compare k ki = 0 
      then (k,v) :: kvs
      else (ki,vi) :: insert_dict k v kvs

  let lookup_dict = List.assoc_opt

end  

