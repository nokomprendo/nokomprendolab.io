(* mytree.ml *)

module Make (K : Mydict.KeyType) = struct

  type key = K.t

  type 'a t =
    | Node of key * 'a * 'a t * 'a t
    | Leaf

  let new_dict = Leaf

  let rec insert_dict k v t =
    match t with
    | Leaf -> Node (k, v, Leaf, Leaf)
    | Node (ki, vi, left, right) -> 
        let c = K.compare k ki in
        if c < 0 then
          Node (ki, vi, insert_dict k v left, right)
        else if c > 0 then
          Node (ki, vi, left, insert_dict k v right)
        else
          Node (k, v, left, right)

  let rec lookup_dict k t =
    match t with
    | Leaf -> None
    | Node (ki, vi, left, right) ->
        let c = K.compare k ki in
        if c < 0 then
          lookup_dict k left
        else if c > 0 then
          lookup_dict k right
        else
          Some vi
end  

