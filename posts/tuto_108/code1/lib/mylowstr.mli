(* mylowstr.mli *)

type t 

(* type lowstr = Lowstr of string *)

val mkLowstr : string -> t 

val compare : t -> t -> int 

val myshow : t -> string 

