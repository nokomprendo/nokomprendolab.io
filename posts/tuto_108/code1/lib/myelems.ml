(* myelems.ml *)

module Make 
  (D : Mydict.S) 
  (SK : Myshow.S with type t = D.key)
  (SV : Myshow.S with type t = D.value) = struct

  let elems ks t =
    let elem_k k = 
      let k_str = SK.myshow k in
      let v_str =
        match D.lookup_dict k t with
        | None -> "not found"
        | Some v -> SV.myshow v
      in
        Printf.printf "%s -> %s\n" k_str v_str
    in
      List.iter elem_k ks
end

