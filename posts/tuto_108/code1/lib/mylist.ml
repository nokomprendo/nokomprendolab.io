(* mylist.ml *)

module Make(K : Mydict.KeyType) (V : Mydict.ValueType) = struct

  type key = K.t

  type value = V.t

  type t = (key * value) list

  let new_dict = []

  let rec insert_dict k v = function
    | [] -> [(k, v)]
    | (ki, vi) :: xs -> 
        if K.compare k ki = 0 then
          (k, v) :: xs
        else
          (ki, vi) :: insert_dict k v xs

  let rec lookup_dict k = function
    | [] -> None
    | (ki, vi) :: xs -> 
        if K.compare k ki = 0 then
          Some vi
        else
          lookup_dict k xs
end  

