(* mydict.ml *)

module type S = sig
  type key
  type value
  type t
  val new_dict : t
  val insert_dict : key -> value -> t -> t
  val lookup_dict : key -> t -> value option
end 

module type KeyType = sig
  type t
  val compare : t -> t -> int
end

module type ValueType = sig
  type t
end
