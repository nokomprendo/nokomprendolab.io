(* main.ml *)

open Mylib


(* treeIntBool *)

module TreeIntBool = Mytree.Make(Int)(Bool)

module ElemsTreeIntBool = Myelems.Make(TreeIntBool)(Myshow.MyshowInt)(Myshow.MyshowBool)

let treeIntBool = TreeIntBool.(new_dict
  |> insert_dict 1 true
  |> insert_dict 2 false
  |> insert_dict 3 true)


(* listIntBool *)

(*
module IntKey = struct
  type t = int
  let compare = Stdlib.compare
end
module ListIntBool = Mylist.Make(IntKey)
*)

module ListIntBool = Mylist.Make(Int)(Bool)

module ElemsListIntBool = Myelems.Make(ListIntBool)(Myshow.MyshowInt)(Myshow.MyshowBool)

let listIntBool = ListIntBool.(new_dict
  |> insert_dict 1 true
  |> insert_dict 2 false
  |> insert_dict 3 true)


(* listStringInt *)

module ListStringInt = Mylist.Make(String)(Int)

module ElemsListStringInt = Myelems.Make(ListStringInt)(Myshow.MyshowString)(Myshow.MyshowInt)

let listStringInt = ListStringInt.(new_dict
  |> insert_dict "foo" 13
  |> insert_dict "Bar" 37)


(* listLowstrInt *)

module ListLowstrInt = Mylist.Make(Mylowstr)(Int)

module ElemsListLowstrInt = Myelems.Make(ListLowstrInt)(Mylowstr)(Myshow.MyshowInt)

let listLowstrInt = ListLowstrInt.(new_dict
  (* |> insert_dict (Mylowstr.Lowstr "foobar") 42 *)
  |> insert_dict (Mylowstr.mkLowstr "foo") 13
  |> insert_dict (Mylowstr.mkLowstr "Bar") 37)


(* main *)

let () =
  ElemsTreeIntBool.elems [1; 2; 4] treeIntBool ;
  ElemsListIntBool.elems [1; 2; 4] listIntBool ;
  ElemsListStringInt.elems ["foo"; "Foo"] listStringInt ;
  ElemsListLowstrInt.elems [Mylowstr.mkLowstr "foo"; Mylowstr.mkLowstr "Foo"] listLowstrInt

