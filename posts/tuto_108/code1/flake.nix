
# https://discuss.ocaml.org/t/ocaml-and-nixos/10835

{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
  };

  outputs = { self, nixpkgs }:
    let
      systems = [ "x86_64-linux" ];
      createDevShell = system:
        let
          pkgs = import nixpkgs { inherit system; };
          ocaml = pkgs.ocaml-ng.ocamlPackages_5_1;
        in
        pkgs.mkShell {
          buildInputs = [
            ocaml.dune_3
            ocaml.ocaml
            ocaml.ocamlformat
            ocaml.findlib
            ocaml.ocaml-lsp
            ocaml.utop
          ];
        };
    in
    {
      devShell = nixpkgs.lib.genAttrs systems createDevShell;
    };
}

