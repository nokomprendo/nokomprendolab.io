#include "cmp.hpp"

#include <iostream>

int main() {

    BotZero botR;
    BotZero botY;

    /*
    BotRandom botR;
    BotMc botY(4);
    */

    const int nGames = 100;
    std::cout << "winR winY tie ry ryt dt nGames\n";
    auto [ winR, winY, tie, dt ] = run(botR, botY, nGames);
    std::cout << winR << ' ' << winY << ' ' << tie << ' ' 
        << winR+winY << ' ' << winR+winY+tie << ' ' 
        << dt << ' ' << nGames << '\n';

    return 0;
}

