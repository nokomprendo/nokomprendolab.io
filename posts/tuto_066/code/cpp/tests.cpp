#include <gtest/gtest.h>

#include "Bot.hpp"

//////////////////////////////////////////////////
// BotZero
//////////////////////////////////////////////////

TEST( BotZero, genmove_1 ) {
    Game game;
    BotZero botR;
    BotZero botY;

    const int N = 1000;
    int nbR = 0;
    int nbY = 0;
    int nbT = 0;

    for (int i=0; i<N; i++) {
        playoutBots(game, botR, botY);
        switch (game.getStatus()) {
            case Status::WinR: nbR++; break;
            case Status::WinY: nbY++; break;
            case Status::Tie: nbT++; break;
            default: FAIL();
        }
        game.newGame();
    }

    ASSERT_NEAR(0.5, nbR/double(N), 0.01);
    ASSERT_NEAR(0.5, nbY/double(N), 0.01);
    ASSERT_NEAR(0.0, nbT/double(N), 0.01);
}

//////////////////////////////////////////////////
// BotRandom
//////////////////////////////////////////////////

TEST( BotRandom, random_1 ) {
    const int N = 100;
    random_t rng(std::random_device{}());
    for (int i=0; i<N; i++) {
        const int x = random(rng, 10);
        ASSERT_TRUE(x >= 0);
        ASSERT_TRUE(x < 10);
    }
}

TEST( BotRandom, random_2 ) {
    const int N = 10000;
    random_t rng(std::random_device{}());
    int sum = 0;
    for (int i=0; i<N; i++) {
        sum += random(rng, 11);
    }
    const double avg = sum / double(N);
    ASSERT_NEAR(5, avg, 0.1);
}

TEST( BotRandom, random_3 ) {
    const int N = 10000;
    random_t rng(std::random_device{}());
    std::vector<int> hist(10, 0);
    for (int i=0; i<N; i++) {
        const int x = random(rng, 10);
        hist[x]++;
    }
    for (int h : hist) {
        const double freq = h / double(N);
        ASSERT_NEAR(0.1, freq, 0.05);
    }
}

TEST( BotRandom, genmove_1 ) {
    Game game;
    BotRandom botR;
    BotRandom botY;

    const int N = 1000;
    int nbR = 0;
    int nbY = 0;
    int nbT = 0;

    for (int i=0; i<N; i++) {
        playoutBots(game, botR, botY);
        switch (game.getStatus()) {
            case Status::WinR: nbR++; break;
            case Status::WinY: nbY++; break;
            case Status::Tie: nbT++; break;
            default: FAIL();
        }
        game.newGame();
    }

    ASSERT_NEAR(0.5, nbR/double(N), 0.05);
    ASSERT_NEAR(0.5, nbY/double(N), 0.05);
    ASSERT_NEAR(0.0, nbT/double(N), 0.05);
}

//////////////////////////////////////////////////
// BotMc
//////////////////////////////////////////////////

TEST( BotMc, computeScore_1 ) {
    // Tie
    ASSERT_DOUBLE_EQ(0.5, computeScore(Status::Tie, Player::R));
    ASSERT_DOUBLE_EQ(0.5, computeScore(Status::Tie, Player::Y));

    // WinR
    ASSERT_DOUBLE_EQ(1.0, computeScore(Status::WinR, Player::R));
    ASSERT_DOUBLE_EQ(0.0, computeScore(Status::WinR, Player::Y));

    // WinY
    ASSERT_DOUBLE_EQ(1.0, computeScore(Status::WinY, Player::Y));
    ASSERT_DOUBLE_EQ(0.0, computeScore(Status::WinY, Player::R));

    // Play
    ASSERT_DOUBLE_EQ(0.0, computeScore(Status::PlayR, Player::Y));
    ASSERT_DOUBLE_EQ(0.0, computeScore(Status::PlayR, Player::R));
    ASSERT_DOUBLE_EQ(0.0, computeScore(Status::PlayY, Player::Y));
    ASSERT_DOUBLE_EQ(0.0, computeScore(Status::PlayY, Player::R));
}

TEST( BotMc, playoutRandom_1 ) {
    random_t rng(std::random_device{}());
    Game game;

    playoutRandom(game, rng);

    const std::vector<int> expectedMoves {};
    ASSERT_EQ(expectedMoves, game.getMoves());
    ASSERT_EQ(false, game.isRunning());
}

TEST( BotMc, playoutRandom_2 ) {
    random_t rng(std::random_device{}());
    Game game;
    const int N = 1000;
    int nbR = 0;
    int nbY = 0;
    int nbT = 0;

    for (int i=0; i<N; i++) {
        Status status = playoutRandom(game, rng);
        switch (status) {
            case Status::WinR: nbR++; break;
            case Status::WinY: nbY++; break;
            case Status::Tie: nbT++; break;
            default: FAIL();
        }
        game.newGame();
    }

    ASSERT_NEAR(0.5, nbR/double(N), 0.05);
    ASSERT_NEAR(0.5, nbY/double(N), 0.05);
    ASSERT_NEAR(0.0, nbT/double(N), 0.05);
}

TEST( BotMc, genmove_1 ) {
    Game game;
    BotMc botR(10);
    BotMc botY(10);

    const int N = 200;
    int nbR = 0;
    int nbY = 0;
    int nbT = 0;

    for (int i=0; i<N; i++) {
        playoutBots(game, botR, botY);
        switch (game.getStatus()) {
            case Status::WinR: nbR++; break;
            case Status::WinY: nbY++; break;
            case Status::Tie: nbT++; break;
            default: FAIL();
        }
        game.newGame();
    }

    ASSERT_NEAR(0.5, nbR/double(N), 0.1);
    ASSERT_NEAR(0.5, nbY/double(N), 0.1);
    ASSERT_NEAR(0.0, nbT/double(N), 0.05);
}

TEST( BotMc, genmove_2 ) {
    Game game;
    BotRandom botR;
    BotMc botY(10);

    const int N = 200;
    int nbR = 0;
    int nbY = 0;
    int nbT = 0;

    for (int i=0; i<N; i++) {
        playoutBots(game, botR, botY);
        switch (game.getStatus()) {
            case Status::WinR: nbR++; break;
            case Status::WinY: nbY++; break;
            case Status::Tie: nbT++; break;
            default: FAIL();
        }
        game.newGame();
    }

    ASSERT_NEAR(0.0, nbR/double(N), 0.1);
    ASSERT_NEAR(1.0, nbY/double(N), 0.1);
    ASSERT_NEAR(0.0, nbT/double(N), 0.1);
}

TEST( BotMc, genmove_3 ) {
    Game game;
    BotMc botR(4);
    BotMc botY(8);

    const int N = 500;
    int nbR = 0;
    int nbY = 0;
    int nbT = 0;

    for (int i=0; i<N; i++) {
        playoutBots(game, botR, botY);
        switch (game.getStatus()) {
            case Status::WinR: nbR++; break;
            case Status::WinY: nbY++; break;
            case Status::Tie: nbT++; break;
            default: FAIL();
        }
        game.newGame();
    }

    ASSERT_NEAR(0.3, nbR/double(N), 0.1);
    ASSERT_NEAR(0.7, nbY/double(N), 0.1);
    ASSERT_NEAR(0.0, nbT/double(N), 0.05);
}

//////////////////////////////////////////////////
// main
//////////////////////////////////////////////////

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

