#include "cmp.hpp"

#include <fstream>
#include <functional>
#include <iostream>

void test1(const std::string & name, int nGames,
        std::function<std::unique_ptr<Bot>(int)> mkBot,
        std::unique_ptr<Bot> & botY,
        std::vector<int> values) {

    std::cout << name << std::endl;
    std::ofstream ofs("out-test1-" + name + ".csv");
    ofs << "winR winY tie ry ryt dt nGames value\n";
    for (int value : values) {
        auto botR = mkBot(value);
        auto [ winR, winY, tie, dt ] = run(*botR, *botY, nGames);
        ofs << winR << ' ' << winY << ' ' << tie << ' ' 
            << winR+winY << ' ' << winR+winY+tie << ' ' 
            << dt << ' ' << nGames << ' ' << value << '\n';
    }
}

int main() {
    auto mkBotMc = [](int v){ return std::make_unique<BotMc>(v);};

    std::unique_ptr<Bot> botRandom = std::make_unique<BotRandom>();
    std::unique_ptr<Bot> botMc32 = std::make_unique<BotMc>(32);

    const int nGames = 300;

    test1("McX-Random", nGames, mkBotMc, botRandom, {1, 2, 4, 8, 16, 32, 64});

    test1("McX-Mc32", nGames, mkBotMc, botMc32, {4, 8, 16, 32, 64});

    return 0;
}

