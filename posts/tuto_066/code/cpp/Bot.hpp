#include "Game.hpp"

#include <random>

//////////////////////////////////////////////////////////////////////
// Bot
//////////////////////////////////////////////////////////////////////

class Bot {
    public:
        virtual ~Bot() = default;
        virtual int genmove(const Game & game) = 0;
};

//////////////////////////////////////////////////////////////////////
// BotZero
//////////////////////////////////////////////////////////////////////

class BotZero : public Bot {
    public:
        int genmove(const Game & game) override;
};

void playoutBots(Game & game, Bot & botR, Bot & botY);

//////////////////////////////////////////////////////////////////////
// BotRandom
//////////////////////////////////////////////////////////////////////

using random_t = std::mt19937;

int random(random_t & rng, int n);

class BotRandom : public Bot {
    private:
        random_t _rng;

    public:
        BotRandom();
        int genmove(const Game & game) override;
};

//////////////////////////////////////////////////////////////////////
// BotMc
//////////////////////////////////////////////////////////////////////

double computeScore(Status s, Player p);

Status playoutRandom(Game & game, random_t & rng);

class BotMc : public Bot {
    private:
        random_t _rng;
        const int _nSimsPerMove;

    public:
        BotMc(int nSimsPerMove);
        int genmove(const Game & game) override;

    protected:
        double evalMove(const Game & game, int k);
};

