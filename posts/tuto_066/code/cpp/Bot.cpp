#include "Bot.hpp"

//////////////////////////////////////////////////////////////////////
// BotZero
//////////////////////////////////////////////////////////////////////

int BotZero::genmove(const Game &) {
    return 0;
}

void playoutBots(Game & game, Bot & botR, Bot & botY) {
    while (game.isRunning()) {
        Bot & bot = game.getCurrentPlayer() == Player::R ? botR : botY;
        const int k = bot.genmove(game);
        game.playK(k);
    }
}

//////////////////////////////////////////////////////////////////////
// BotRandom
//////////////////////////////////////////////////////////////////////

int random(random_t & rng, int n) {
    std::uniform_int_distribution<int> dist(0, n-1);
    return dist(rng);
}

BotRandom::BotRandom() : _rng(std::random_device{}()) {}

int BotRandom::genmove(const Game & game) {
    const int nMoves = game.getMoves().size();
    return random(_rng, nMoves);
}

//////////////////////////////////////////////////////////////////////
// BotMc
//////////////////////////////////////////////////////////////////////

double computeScore(Status s, Player p) {
    if ((s == Status::WinR and p == Player::R) or (s == Status::WinY and p == Player::Y))
        return 1.0;
    if (s == Status::Tie)
        return 0.5;
    return 0.0;
}

Status playoutRandom(Game & game, random_t & rng) {
    while (game.isRunning()) {
        const int k = random(rng, game.getMoves().size());
        game.playK(k);
    }
    return game.getStatus();
}

BotMc::BotMc(int nSimsPerMove) : 
    _rng(std::random_device{}()), 
    _nSimsPerMove(nSimsPerMove) 
{}

int BotMc::genmove(const Game & game) {
    const int nMoves = game.getMoves().size();
    int bestK = 0;
    double bestScore = 0.0;
    for (int k=0; k<nMoves; k++) {
        const double score = evalMove(game, k);
        if (score > bestScore) {
            bestScore = score;
            bestK = k;
        }
    }
    return bestK;
}

double BotMc::evalMove(const Game & game, int k) {
    const Player player = game.getCurrentPlayer();
    Game g1(game);
    g1.playK(k);
    double score = 0.0;
    for (int n=0; n<_nSimsPerMove; n++) {
        Game g(g1);
        Status status = playoutRandom(g, _rng);
        score += computeScore(status, player);
    }
    return score;
}

