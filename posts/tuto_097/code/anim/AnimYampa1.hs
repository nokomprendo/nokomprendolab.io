{-# LANGUAGE Arrows #-}
{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE TemplateHaskell #-}

import Control.Arrow
import Control.Lens hiding (pre)
import Control.Monad.State.Strict
import FRP.Yampa
import Graphics.Gloss                     
import Graphics.Gloss.Interface.FRP.Yampa
import Graphics.Gloss.Interface.IO.Game qualified as G
import Linear.V2
import Linear.Vector
import System.Random

-------------------------------------------------------------------------------
-- params
-------------------------------------------------------------------------------

winWidth, winHeight :: Int
winWidth = 400
winHeight = 300

winWidthF, winHeightF, xMin, xMax, yMin, yMax, ballRadius :: Float
winWidthF = fromIntegral winWidth
winHeightF = fromIntegral winHeight
xMin =  ballRadius - 0.5 * winWidthF
xMax = -ballRadius + 0.5 * winWidthF
yMin =  ballRadius - 0.5 * winHeightF
yMax = -ballRadius + 0.5 * winHeightF
ballRadius = 20

-------------------------------------------------------------------------------
-- Types
-------------------------------------------------------------------------------

data Ball = Ball 
  { _pos :: V2 Float
  , _vel :: V2 Float
  }

data GameState = GameState 
  { _ball :: Ball
  , _gen :: StdGen
  }

makeLenses ''Ball
makeLenses ''GameState

data GameAction
  = Reset
  | None

type GameInput = Event GameAction

-------------------------------------------------------------------------------
-- Game
-------------------------------------------------------------------------------

drawGame :: GameState -> Picture
drawGame gs = 
  let (V2 x y) = gs ^. ball . pos
  in Pictures
      [ translate x y (circleSolid ballRadius)
      , rectangleWire winWidthF winHeightF ]

randomBall :: StdGen -> (Ball, StdGen)
randomBall g0 =
  let (p, g1) = randomR (V2 xMin yMin, V2 xMax yMax) g0
      (v, g2) = randomR (V2 (-500) (-500), V2 500 500) g1
  in (Ball p v, g2)

initGame :: StdGen -> GameState
initGame g0 = 
  let (b, g1) = randomBall g0
  in GameState b g1

updateMotion :: Float -> State Ball ()
updateMotion deltaTime = do
  v <- use vel
  pos += v ^* deltaTime

updateBounces :: State Ball ()
updateBounces = do
  (V2 x y) <- use pos
  when (xMin >= x) $ do
    pos . _x .= 2*xMin - x
    vel . _x %= negate
  when (xMax <= x) $ do
    pos . _x .= 2*xMax - x
    vel . _x %= negate
  when (yMin >= y) $ do
    pos . _y .= 2*yMin - y
    vel . _y %= negate
  when (yMax <= y) $ do
    pos . _y .= 2*yMax - y
    vel . _y %= negate

updateGame :: Float -> GameState -> GameState
updateGame deltaTime gs = 
  gs  & ball %~ execState (updateMotion deltaTime >> updateBounces)

-------------------------------------------------------------------------------
-- Signal Functions
-------------------------------------------------------------------------------

inputSF :: SF (Event InputEvent) GameInput
inputSF = arr $ \evt ->
  case evt of
    Event (G.EventKey (G.SpecialKey G.KeyEnter) G.Up _ _) -> evt `tag` Reset
    _ -> evt `tag` None

-- loopPre :: c -> SF (a, c) (b, c) -> SF a b
gameSF :: StdGen -> SF GameInput GameState
gameSF g = loopPre (0, initGame g) stepSF

stepSF :: SF (GameInput, (Time, GameState)) (GameState, (Time, GameState))
stepSF = proc (gi, (t0, gs0)) -> do
  t1 <- time -< ()
  case gi of
    Event Reset -> 
      let gs1 = initGame (gs0 ^. gen)
      in returnA -< (gs1, (t1, gs1))
    _ -> 
      let gs1 = updateGame (realToFrac $ t1 - t0) gs0
      in returnA -< (gs1, (t1, gs1))

mainSF :: StdGen -> SF (Event InputEvent) Picture
mainSF g = inputSF >>> gameSF g >>^ drawGame

-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

main :: IO ()
main = do
  g <- newStdGen
  let window = InWindow "AnimYampa1" (winWidth, winHeight) (0, 0)
      bgcolor = makeColor 0.4 0.6 1.0 1.0
      fps = 30
  playYampa window bgcolor fps (mainSF g)

