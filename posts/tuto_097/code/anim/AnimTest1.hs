{-# LANGUAGE Arrows #-}

import Control.Arrow
import FRP.Yampa
import Graphics.Gloss
import Graphics.Gloss.Geometry.Angle
import Graphics.Gloss.Interface.FRP.Yampa

animSF :: SF (Event InputEvent) Picture
animSF = proc _ -> do
    t <- time -< ()
    let angle = 2 * pi * realToFrac t
    returnA -< rotate (radToDeg angle)
                $ translate 100 0
                $ circleSolid 20

main :: IO ()
main = 
    let window = InWindow "AnimTest1" (400, 300) (0, 0)
        bgcolor = makeColor 0.4 0.6 1.0 1.0
        fps = 30
    in playYampa window bgcolor fps animSF


