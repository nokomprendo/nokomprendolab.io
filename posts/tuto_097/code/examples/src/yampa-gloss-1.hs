{-# LANGUAGE Arrows           #-}
{-# LANGUAGE ParallelListComp #-}

import Control.Arrow
import FRP.Yampa                          ( SF, Event, time )
import Graphics.Gloss                     
import Graphics.Gloss.Interface.FRP.Yampa ( InputEvent, playYampa )

main :: IO ()
main = 
  let window = InWindow "YampaDemo" (800, 600) (0, 0)
      bgcolor = makeColor 0.4 0.6 1.0 1.0
      fps = 30
  in playYampa window bgcolor fps anim

anim :: SF (Event InputEvent) Picture
anim = rotatingColor &&& plainWave >>^ uncurry mappend

anim' :: SF (Event InputEvent) Picture
anim' = proc input -> do
  p1 <- rotatingColor -< input
  p2 <- plainWave -< input
  returnA -< p1 `mappend` p2

anim'' :: SF (Event InputEvent) Picture
anim'' = proc input -> do
  (p1, p2) <- (rotatingColor -< input) &&& (plainWave -< input)
  returnA -< p1 `mappend` p2

rotatingColor :: SF (Event InputEvent) Picture
rotatingColor = proc _ -> do
  t <- ftime -< ()
  returnA -< rotate (-180 * t / pi) 
                $ Translate 250 0
                $ Color black
                $ circleSolid 20

plainWave :: SF (Event InputEvent) Picture
plainWave = proc _ -> do
    t <- (*1.25) ^<< ftime -< ()

    let t2      n = sin (t + 2 * pi * fromIntegral n / 45) ** 2
        circleX n = fromIntegral n * 20 - 100
        circleY n = t2 n * 200 - 100

    returnA -< Pictures
      [ Translate (circleX n) (circleY n)
          $ Color c
          $ circleSolid 10
        | n <- [0 .. length colors]
        | c <- colors
      ]

colors :: [Color]
colors = [ red,  orange, yellow, chartreuse, green,   aquamarine
         , cyan, azure,  blue,   violet,     magenta, rose
         ]

ftime :: SF a Float
ftime = time >>^ realToFrac

ftime' :: SF a Float
ftime' = proc _ -> do
  t <- time -< ()
  returnA -< realToFrac t
