cmake_minimum_required( VERSION 2.8 )
project( mydisplay )

find_package( PkgConfig REQUIRED )
pkg_check_modules( MYPKG REQUIRED opencv )
include_directories( ${MYPKG_INCLUDE_DIRS} )

add_executable( mydisplay mydisplay.cpp )
target_link_libraries( mydisplay ${MYPKG_LIBRARIES} )
install( TARGETS mydisplay DESTINATION bin )

