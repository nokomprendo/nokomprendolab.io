#include <opencv2/opencv.hpp>

int main(int argc, char ** argv) {

  if (argc != 2) {
    std::cout << "usage: " << argv[0] << " <filename>\n";
    exit(-1);
  }

  cv::Mat img = cv::imread(argv[1], cv::IMREAD_ANYDEPTH | cv::IMREAD_ANYCOLOR);
  if (not img.data) {
    std::cout << "error: unable to open " << argv[1] << std::endl;
    std::exit(-1);
  }

  cv::imshow("mydisplay", img);
  while (true) {
    int key = cv::waitKey(30) % 0x100;
    if (key == 27) 
      break;
  }

  return 0;
}

