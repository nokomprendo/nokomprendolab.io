import Data.Char (toUpper)
import Data.Function ((&))
import qualified Streamly.Internal.FileSystem.File as File
import qualified Streamly.Prelude as Stream
import qualified Streamly.Data.Unicode.Stream as Stream
import System.Environment (getArgs)

main :: IO ()
main = do
    args <- getArgs
    case args of
        [filename] -> 
              Stream.unfold File.read filename
            & Stream.decodeUtf8
            & Stream.map toUpper
            & Stream.mapM_ putChar
        _ -> putStrLn "usage: <filename>"

