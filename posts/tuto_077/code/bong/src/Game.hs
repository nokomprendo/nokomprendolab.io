module Game where

import Linear.V2

import Collision

-------------------------------------------------------------------------------
-- params
-------------------------------------------------------------------------------

gameXMin, gameXMax, gameYMin, gameYMax :: Float
gameXMin = -300
gameXMax = 300
gameYMin = -200
gameYMax = 200

ballY0, bobY0, bobRadius, ballRadius :: Float
ballY0 = -130
bobY0 = -170
bobRadius = 35
ballRadius = 10

ballVel0 :: V2 Float
ballVel0 = V2 0 300

-------------------------------------------------------------------------------
-- types
-------------------------------------------------------------------------------

data Status = Running | Launching

data Game = Game
    { _ballPos :: V2 Float
    , _ballVel :: V2 Float
    , _bobPos :: V2 Float
    , _status :: Status
    }

-------------------------------------------------------------------------------
-- functions
-------------------------------------------------------------------------------

newGame :: Game
newGame = Game (V2 0 ballY0) (V2 0 0) (V2 0 bobY0) Launching

moveGame :: Float -> Game -> Game
moveGame x g = 
    let (V2 _ bobY) = _bobPos g
        g1 = g  { _bobPos = V2 x bobY }
        g2 = g1 { _ballPos = V2 x ballY0 }
    in case _status g of
        Running -> g1
        Launching -> g2

launchGame :: Game -> Game
launchGame g = 
    let (V2 bobX _) = _bobPos g
    in g { _status = Running
         , _ballPos = V2 bobX ballY0
         , _ballVel = ballVel0 }

animateGame :: Float -> Game -> Game
animateGame dt g = 
    case _status g of
        Running -> 
            let objects =
                    [ Paddle (_bobPos g) bobRadius
                    , WallTop gameYMax
                    , WallLeft gameXMin
                    , WallRight gameXMax
                    ]
                b0 = Ball (_ballPos g) (_ballVel g) ballRadius
                b1 = animateBall dt b0 objects
                (V2 _ b1Y) = _pos b1
            in if b1Y < gameYMin 
                then g { _status = Launching } 
                else g { _ballPos = _pos b1 , _ballVel = _vel b1 }
        _ -> g

