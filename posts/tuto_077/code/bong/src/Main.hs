import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
import Linear.V2

import Game

data Model = Model
    { _bobPicture :: Picture
    , _bobScale :: Float
    , _game :: Game
    }

main :: IO ()
main = do
    bob <- loadBMP "bob.bmp"
    let window = InWindow "Bong" (gameWidth, gameHeight) (0, 0) 
        bgcolor = makeColor 0.4 0.6 1.0 1.0
        fps = 30 
        model = Model bob 1 newGame
    play window bgcolor fps model handleDisplay handleEvent handleTime

gameWidth, gameHeight :: Int
gameWidth = round $ gameXMax - gameXMin
gameHeight = round $ gameYMax - gameYMin

handleDisplay :: Model -> Picture
handleDisplay (Model bobPicture bobScale game) = 
    let (V2 ballX ballY) = _ballPos game
        (V2 bobX bobY) = _bobPos game
    in Pictures
        [ translate bobX bobY $ scale bobScale 1 $ Pictures 
            [bobPicture, color black $ circle bobRadius]
        , translate ballX ballY $ color white $ circleSolid ballRadius
        ]

handleEvent :: Event -> Model -> Model
handleEvent (EventMotion (x, _)) m = 
    let (V2 bx _) = _bobPos $ _game m
        s = if bx > x then 1 else -1
    in m { _bobScale = s, _game = moveGame x (_game m) }
handleEvent (EventKey (MouseButton LeftButton) Up _ _) m = 
    m {_game = launchGame (_game m)}
handleEvent _ m = m

handleTime :: Float -> Model -> Model
handleTime dt m = m { _game = animateGame dt (_game m) }

