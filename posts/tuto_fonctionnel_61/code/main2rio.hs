{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE FlexibleInstances #-}
import RIO
import System.IO (putStrLn)

data User = User
    { userName :: String
    , userAge :: Int
    } deriving (Show)

data Env1 = Env1
    { env1Log :: String -> IO ()
    , env1Users :: [User]
    }

class HasUsers a where
    getUsers :: a -> [User]
instance HasUsers Env1 where
    getUsers = env1Users

readUsers :: (HasUsers e) => RIO e [User]
readUsers = asks getUsers

class HasLog a where
    getLog :: a -> (String -> IO ())
instance HasLog Env1 where
    getLog = env1Log

logMsg :: (HasLog e) => String -> RIO e ()
logMsg msg = do
    env <- ask
    liftIO $ getLog env msg

app1 :: (HasLog e, HasUsers e) => RIO e ()
app1 = do
    logMsg "begin app1"
    users <- readUsers
    logMsg $ show users
    logMsg "end app1"

env1 :: Env1
env1 = Env1
    { env1Log = putStrLn
    , env1Users = [User "Pedro" 13, User "John" 42]
    }



data Env2 = Env2

instance HasLog Env2 where
    getLog _ msg = putStrLn $ "-> " <> msg

app2 :: (HasLog e) => RIO e ()
app2 = do
    logMsg "begin app2"
    logMsg "end app2"



main :: IO ()
main = do
    runRIO env1 app1
    runRIO Env2 app2
    runRIO env1 app2
    -- runRIO env2 app1
