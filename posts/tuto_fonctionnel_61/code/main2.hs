{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
import Control.Monad.Reader

data User = User
    { userName :: String
    , userAge :: Int
    } deriving (Show)

data Env1 = Env1
    { env1Log :: String -> IO ()
    , env1Users :: [User]
    }

class HasUsers a where
    getUsers :: a -> [User]
instance HasUsers Env1 where
    getUsers = env1Users

readUsers :: (HasUsers e, MonadReader e m) => m [User]
readUsers = asks getUsers

class HasLog a where
    getLog :: a -> (String -> IO ())
instance HasLog Env1 where
    getLog = env1Log

logMsg :: (HasLog e, MonadReader e m, MonadIO m) => String -> m ()
logMsg msg = do
    env <- ask
    liftIO $ getLog env msg

app1 :: (HasLog e, HasUsers e, MonadReader e m, MonadIO m) => m ()
app1 = do
    logMsg "begin app1"
    users <- readUsers
    logMsg $ show users
    logMsg "end app1"

env1 :: Env1
env1 = Env1
    { env1Log = putStrLn
    , env1Users = [User "Pedro" 13, User "John" 42]
    }



data Env2 = Env2

instance HasLog Env2 where
    getLog _ msg = putStrLn $ "-> " <> msg

app2 :: (HasLog e, MonadReader e m, MonadIO m) => m ()
app2 = do
    logMsg "begin app2"
    logMsg "end app2"



main :: IO ()
main = do
    runReaderT app1 env1
    runReaderT app2 Env2
    runReaderT app2 env1
    -- runReaderT app1 Env2

