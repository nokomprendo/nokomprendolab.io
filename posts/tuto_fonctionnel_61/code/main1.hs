{-# LANGUAGE FlexibleContexts #-}
import Control.Monad.Reader

data User = User
    { userName :: String
    , userAge :: Int
    } deriving (Show)

data Env1 = Env1
    { env1Log :: String -> IO ()
    , env1Users :: [User]
    }

readUsers1 :: (MonadReader Env1 m) => m [User]
readUsers1 = asks env1Users

logMsg1 :: (MonadReader Env1 m, MonadIO m) => String -> m ()
logMsg1 msg = do
    env <- ask
    liftIO $ env1Log env msg

app1 :: (MonadReader Env1 m, MonadIO m) => m ()
app1 = do
    logMsg1 "begin app1"
    users <- readUsers1
    logMsg1 $ show users
    logMsg1 "end app1"

env1 :: Env1
env1 = Env1
    { env1Log = putStrLn
    , env1Users = [User "Pedro" 13, User "John" 42]
    }



data Env2 = Env2

logMsg2 :: (MonadReader Env2 m, MonadIO m) => String -> m ()
logMsg2 msg =  liftIO $ putStrLn $ "-> " <> msg

app2 :: (MonadReader Env2 m, MonadIO m) => m ()
app2 = do
    logMsg2 "begin app2"
    logMsg2 "end app2"



main :: IO ()
main = do
    runReaderT app1 env1
    runReaderT app2 Env2
    -- runReaderT app2 env1      -- error
    -- runReaderT app1 Env2

