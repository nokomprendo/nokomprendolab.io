{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
import Control.Monad.Reader
import Control.Monad.State.Lazy
import Data.Functor.Identity

data User = User
    { userName :: String
    , userAge :: Int
    } deriving (Show)

data Env1 = Env1
    { env1Log :: String -> IO ()
    , env1Users :: [User]
    }

class HasUsers a where
    getUsers :: a -> [User]
instance HasUsers Env1 where
    getUsers = env1Users

readUsers :: (HasUsers e, MonadReader e m) => m [User]
readUsers = asks getUsers

class HasLog a where
    getLog :: a -> (String -> IO ())
instance HasLog Env1 where
    getLog = env1Log

class Monad m => MonadLog m where
    logMsg :: String -> m ()
instance (HasLog e, MonadIO m) => MonadLog (ReaderT e m) where
    logMsg msg = do
        env <- ask
        liftIO $ getLog env msg

app1 :: (MonadLog m, HasUsers e, MonadReader e m) => m ()
app1 = do
    logMsg "begin app1"
    users <- readUsers
    logMsg $ show users
    logMsg "end app1"

env1 :: Env1
env1 = Env1
    { env1Log = putStrLn
    , env1Users = [User "Pedro" 13, User "John" 42]
    }



app2 :: (MonadLog m) => m ()
app2 = do
    logMsg "begin app2"
    logMsg "end app2"

type EnvMock = State ([String], [User])

instance MonadLog EnvMock where
    logMsg msg = do
        (msgs, users) <- get
        put (msg:msgs, users)


main :: IO ()
main = do
    runReaderT app1 env1

    let (msgs, _) = execState app2 ([]::[String], [User "John" 42]) 
    mapM_ putStrLn msgs

