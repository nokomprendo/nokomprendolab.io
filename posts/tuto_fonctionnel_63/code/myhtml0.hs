
data Term
    = TermH1
    | TermB
    | TermDiv
    deriving Show

data Html 
    = HtmlNode Term [Html]
    | HtmlData String 
    deriving Show

renderTerm :: Term -> String
renderTerm TermH1 = "h1"
renderTerm TermB = "b"
renderTerm TermDiv = "div"

render :: Html -> String
render (HtmlData str) = str
render (HtmlNode t nodes)
    = "<" ++ renderTerm t ++ ">" ++ concatMap render nodes ++ "</" ++ renderTerm t ++ ">"

mypage :: Html
mypage =
    HtmlNode TermDiv 
        [ HtmlNode TermH1 [HtmlData "mypage"]
        , HtmlNode TermDiv
            [ HtmlData "this is mypage "
            , HtmlNode TermB [HtmlData "in haskell"]
            ]
        ]

main :: IO ()
main = do
    print mypage
    putStrLn $ render mypage
    writeFile "out1.html" (render mypage)

