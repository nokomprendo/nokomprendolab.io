{-# LANGUAGE FunctionalDependencies, FlexibleInstances #-}

class Term arg result | result -> arg where
    term :: String -> arg -> result 

newtype Html a = Html { runHtml :: (String, a) }
    deriving (Show)

toHtml :: String -> Html ()
toHtml str = Html (str, ())

render :: Html () -> String
render = fst . runHtml

b_, h1_, div_ :: Term arg result => arg -> result
b_ = term "b"
h1_ = term "h1"
div_ = term "div"

mypage :: Html ()
mypage = div_ $ do
    h1_ $ toHtml "mypage"
    div_ $ do
        toHtml "this is mypage "
        b_ $ toHtml "in haskell"

main :: IO ()
main = do
    print mypage
    putStrLn $ render mypage
    writeFile "out2.html" (render mypage)


instance Functor Html where
    -- fmap :: (a -> b) -> Html a -> Html b
    fmap f (Html xa) = Html $ f <$> xa

instance Applicative Html where
    -- pure :: a -> Html a
    pure a = Html ("", a)
    -- (<*>) :: Html (a -> b) -> Html a -> Html b
    (Html (sx, ax)) <*> (Html (sy, ay)) = Html (sx <> sy, ax ay)

instance Monad Html where
    -- (>>=) :: Html a -> (a -> Html b) -> Html b
    (Html (sx, ax)) >>= f =
        let (Html (sy, ay)) = f ax
        in Html (sx <> sy, ay)

instance Term (Html a) (Html a) where
    term name (Html (x,a)) = Html (y, a)
        where y = "<" <> name <> ">" <> x <> "</" <> name <> ">"

