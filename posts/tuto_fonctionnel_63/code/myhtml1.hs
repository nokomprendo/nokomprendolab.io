
newtype Term = Term { unTerm :: String }
    deriving Show

data Html 
    = HtmlNode Term [Html]
    | HtmlData String 
    deriving Show

termB, termH1, termDiv :: Term
termB = Term "b"
termH1 = Term "h1"
termDiv = Term "div"

render :: Html -> String
render (HtmlData str) = str
render (HtmlNode t nodes)
    = "<" ++ unTerm t ++ ">" ++ concatMap render nodes ++ "</" ++ unTerm t ++ ">"

mypage :: Html
mypage =
    HtmlNode termDiv 
        [ HtmlNode termH1 [HtmlData "mypage"]
        , HtmlNode termDiv
            [ HtmlData "this is mypage "
            , HtmlNode termB [HtmlData "in haskell"]
            ]
        ]

main :: IO ()
main = do
    print mypage
    putStrLn $ render mypage
    writeFile "out1.html" (render mypage)

