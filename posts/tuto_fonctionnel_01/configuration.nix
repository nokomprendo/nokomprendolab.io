{ config, pkgs, ... }: {

  imports = [ ./hardware-configuration.nix ];

  boot.loader.grub = {
    device = "/dev/sda"; 
    enable = true;
    version = 2;
  };

  environment.systemPackages = with pkgs; [
    #firefox htop pavucontrol sudo
    vim
  ];

  #hardware.pulseaudio.enable = true;

  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "fr";
    #consoleKeyMap = "fr-bepo";
    defaultLocale = "fr_FR.UTF-8";
  };

  networking.hostName = "tutovm"; 

  #services = {
  #  dbus.enable = true;
  #  udisks2.enable = true;
  #  xserver = {
  #    desktopManager.xfce.enable = true;
  #    displayManager.lightdm.enable = true;
  #    enable = true;
  #    layout = "fr";
  #    #xkbVariant = "bepo";
  #  };
  #};

  system.stateVersion = "17.09";

  time.timeZone = "Europe/Paris";
}

