
## build image

```
docker build -t nokomprendo/node .
docker login
docker push nokomprendo/node
docker logout
```

## run

```
alias npm="docker run --rm -v `pwd`:/tmp -p3000:3000 -e USER_ID=`id -u` nokomprendo/node npm"
npm install
...
```

