title: Comprendre Go en 5 minutes, en Haskell

D'après leur réputation, Go et Haskell sont des langages de programmation assez opposés : Go privilégie la simplicité, Haskell l'expressivité. Cependant ces deux langages ne sont pas si éloignés que cela. Plus exactement, les concepts de base de Go se retrouvent, de façon assez ressemblante, en Haskell.

Ce journal reprend, en Haskell, les exemples de code en Go de l'article [Comprendre Go en 5 minutes](https://www.jesuisundev.com/comprendre-go-en-5-minutes/).  L'objectif n'est pas de présenter exhaustivement Go ou Haskell, ni de démontrer que l'un est "meilleur" que l'autre mais plutôt de montrer l'équivalent en Haskell de quelques fonctionnalités de Go.

Avertissement : je ne connais rien à Go et ne suis pas un expert en Haskell. Les exemples ci-dessous présentent juste ce que j'en ai compris.

# Hello world

Commençons par le traditionnel "helloworld". En Go, il faut définir un package, importer le module `fmt` et définir une fonction `main`.

```go
package main

import "fmt"

func main() {
    fmt.Println("hello world")
}
```

En Haskell, il faut également définir une fonction `main` mais il n'est pas nécessaire de définir un module ni d'importer de module particulier.

```haskell
main :: IO ()
main = putStrLn "hello world"
```


# Fonctions

En Go, la définition et l'évaluation de fonction ressemble à n'importe quel langage de type "langage C".

```go
package main

import "fmt"

func add(x int, y int) int {    // définit une fonction add
    return x + y
}

func main() {
    var i, j int = 10, 2
    fmt.Println(add(i, j))    // évalue add
}
```

En Haskell, on utilise la forme curryfiée et sans parenthèse d'évaluation.

```haskell
add :: Int -> Int -> Int    -- définit une fonction add
add x y = x + y

main :: IO ()
main = do
    let i = 10
        j = 2
    print (add i j)    -- évalue add
```

# Interfaces

Go permet de définir des types et des interfaces. Par exemple, on peut définir un type `GoDeveloper` implémentant une interface `Developer`.

```go
package main

import "fmt"

type Developer interface {    // définit une interface Developer
    Code() string
}

type GoDeveloper struct {    // définit un type GoDeveloper
}

func (g GoDeveloper ) Code() string {    // implémente Developer pour GoDeveloper
    return "Go code"
}

func main() {
    goDeveloper := GoDeveloper{}
    fmt.Println(goDeveloper.Code())
}
```

Le système de type de Haskell est très évolué mais les classes de types et les types algébriques permettent d'écrire un code équivalent au code Go précédent.

```haskell
class Developer a where    -- définit une "interface" Developer
    code :: a -> String

data GoDeveloper = GoDeveloper    -- définit un type GoDeveloper

instance Developer GoDeveloper where    -- implémente Developer pour GoDeveloper
    code g = "go code"

main :: IO ()
main = do
    let goDeveloper = GoDeveloper
    putStrLn (code goDeveloper)
```

# Processus légers

Enfin, Go propose des processus légers, appelés "go routines" et lancés via le mot-clé `go`. 

```go
package main

import (
    "fmt"
    "time"
)

func say(s string) {
    for i := 0; i < 5; i++ {
        time.Sleep(100 * time.Millisecond)
        fmt.Println(s)
    }
}

func main() {
    go say("world")    // lance un processus léger, en parallèle
    say("hello")
}
```

Haskell possède également des processus légers, lancés via `forkIO`.

```haskell
import Control.Concurrent
import Control.Monad
import System.Clock

say :: String -> IO ()
say s = forM_ [1 .. 5] $ \_ -> do
    threadDelay 100000
    putStrLn s

main :: IO ()
main = do
    forkIO (say "world")    -- lance un processus léger, en parallèle
    say "hello"
```

# Conclusion

Le langage Go permet de définir et d'utiliser des fonctions, des types, des interfaces et des processus légers (appelés "go routines"). Ces fonctionnalités existent également en Haskell et s'utilisent de façon assez ressemblante.

