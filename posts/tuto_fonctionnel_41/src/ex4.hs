import Control.Concurrent
import Control.Monad

say :: String -> IO ()
say s = forM_ [1 .. 5] $ \_ -> do
    threadDelay 100000
    putStrLn s

main :: IO ()
main = do
    forkIO (say "world")
    say "hello"

