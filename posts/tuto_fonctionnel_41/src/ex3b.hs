class Developer a where
    code :: a -> IO ()

data GoDeveloper = GoDeveloper

instance Developer GoDeveloper where
    code _ = putStrLn "go code"

main :: IO ()
main = do
    let goDeveloper = GoDeveloper
    code goDeveloper

