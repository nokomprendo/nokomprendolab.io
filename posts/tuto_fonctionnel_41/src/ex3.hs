class Developer a where
    code :: a -> String

data GoDeveloper = GoDeveloper

instance Developer GoDeveloper where
    code g = "go code"

main :: IO ()
main = do
    let goDeveloper = GoDeveloper
    putStrLn (code goDeveloper)
