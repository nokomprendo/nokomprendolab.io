package main

func main() {
    var add = func(a int) func(b int) int {
        return func(b int) int {
            return a + b
        }
    }

    var x = 10
    var y = 2
    println(add(x)(y))
}

