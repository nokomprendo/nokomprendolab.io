add :: Int -> Int -> Int
add x y = x + y

main :: IO ()
main = do
    let i = 10
        j = 2
    print (add i j)

