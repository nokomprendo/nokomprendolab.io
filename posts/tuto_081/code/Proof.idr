-- idris2 Proof.idr -o Proof && ./build/exec/Proof

-- functions

mylength : List a -> Nat
mylength [] = 0
mylength (x::xs) = 1 + mylength xs

myappend : List a -> List a -> List a
myappend [] ys = ys
myappend (x::xs) ys = x :: (myappend xs ys)

-- main

main : IO ()
main = do
  printLn $ mylength [1..9]
  printLn $ mylength [1..9]
  printLn $ myappend [1..4] [5..9]

-- proofs

prop_1_plus_1 : 1 + 1 = 2
prop_1_plus_1 = Refl

prop_radiohead : 2 + 2 = 5 -> Void
prop_radiohead Refl impossible

prop_length_nil : mylength [] = 0
prop_length_nil = Refl

prop_length_cons : (x : a) -> (xs : List a) -> mylength (x::xs) = 1 + mylength xs
prop_length_cons x xs = Refl

prop_length_append : (xs, ys : List a) -> mylength (myappend xs ys) = mylength xs + mylength ys
prop_length_append [] ys = Refl
prop_length_append (x::xs) ys = cong S (prop_length_append xs ys)

