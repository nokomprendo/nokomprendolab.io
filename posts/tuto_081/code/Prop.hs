import MyModule hiding (main)

import Test.Hspec
import Test.Hspec.QuickCheck

main :: IO ()
main = hspec $ 

    describe "myappend" $ do

        prop "[] ys = ys" $ \ys -> 
            myappend [] ys `shouldBe` (ys::[Int])

        prop "xs [] = xs" $ \xs -> 
            myappend xs [] `shouldBe` (xs::[Int])

        prop "length (append xs ys) = length xs + length ys" $ \(xs, ys) -> 
            mylength (myappend xs ys) `shouldBe` mylength xs + mylength (ys::[Int])

