let

  pkgs = import <nixpkgs> {};

  ghc = pkgs.haskellPackages.ghcWithPackages (ps: with ps; [
    hspec
    QuickCheck
  ]);

in pkgs.stdenv.mkDerivation {
  name = "haskell-env";
  buildInputs = [
    ghc 
    pkgs.idris2
  ];
  shellHook = "eval $(egrep ^export ${ghc}/bin/ghc)";
}

