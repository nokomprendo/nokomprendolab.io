module MyModule where

mylength :: [a] -> Int
mylength [] = 0
mylength (x:xs) = 1 + mylength xs

myappend :: [a] -> [a] -> [a]
myappend [] ys = ys
myappend (x:xs) ys = x : (myappend xs ys)

main :: IO ()
main = do
    print $ mylength [1..9]
    print $ myappend [1..4] [5..9]

