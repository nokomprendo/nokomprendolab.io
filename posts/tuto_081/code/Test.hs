import MyModule hiding (main)

import Test.Hspec

main :: IO ()
main = hspec $ do

    describe "mylength" $ do
        it "[]" $ mylength [] `shouldBe` 0
        it "[1..9]" $ mylength [1..9] `shouldBe` 9

    describe "myappend" $ do
        it "[] [5..9]" $ myappend [] [5..9] `shouldBe` [5..9]
        it "[1..4] []" $ myappend [1..4] [] `shouldBe` [1..4]
        it "[1..4] [5..9]" $ myappend [1..4] [5..9] `shouldBe` [1..9]

