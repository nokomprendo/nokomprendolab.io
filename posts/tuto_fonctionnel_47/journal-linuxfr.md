---
title: Les rollbacks avec NixOS, ou comment casser son système
---

Une distribution Linux "rolling release" permet de faire évoluer progressivement son système. Ceci permet d'avoir des logiciels plus à jour et surtout d'éviter les montées de version majeure. 

Cependant, le rolling release augmente le risque de "mise-à-jour qui casse tout". Une solution classique à ce risque est de lire les news de mise-à-jour, faire un snapshot avec Timeshift, lancer la mise-à-jour après avoir tiré une carte chance et faire un rollback vers la case départ sans toucher vingt milles francs en cas de problème. Ou sinon, utiliser NixOS...

Voir aussi : [video youtube](https://youtu.be/o0clsnkTxTY) - [video peertube](https://peertube.fr/videos/watch/36bc6106-4342-468a-942a-98b9c71b4272) - [article de blog](https://nokomprendo.gitlab.io/posts/tuto_fonctionnel_47/2020-11-23-fr-README.html)

# Les canaux de NixOS

NixOS utilise un système de canaux de paquets. Ceci permet de choisir entre une approche rolling release (canal `nixos-unstable`) ou une approche par version (`nixos-20.09`, `nixos-20.03`...). Il est même possible de changer de canal, d'utiliser plusieurs canaux en même temps, de spécifier des canaux par utilisateur, de personnaliser des canaux...

On peut lister les canaux systèmes avec la commande suivante :

```
# nix-channel --list
nixos https://nixos.org/channels/nixos-unstable
```

On peut également mettre à jour le système, avec les dernières versions de paquets disponibles sur le canal :

```
# nixos-rebuild switch --upgrade
[...]
```

Il peut arriver que des paquets soient cassés, que des incompatibilités apparaissent, ou tout simplement qu'on n'ait fait une erreur dans le fichier de configuration. NixOS propose différents outils pour gérer les mises-à-jour et résoudre les éventuels problèmes.

# Les générations systèmes

Lors d'une mise-à-jour système (`nixos-rebuild`), NixOS crée une nouvelle "génération" de la configuration et ajoute une entrée dans l'historique des générations systèmes.

Par exemple, on peut afficher l'historique des générations :

```
# nix-env -p /nix/var/nix/profiles/system --list-generations 
  11   2020-11-22 22:27:47   (current)
```

Puis modifier la configuration et mettre à jour le système :

```
# nixos-rebuild edit
[...]

# nixos-rebuild switch 
building Nix...
building the system configuration...
these derivations will be built:
[...]
```

L'historique indiquera alors une nouvelle génération :

```
# nix-env -p /nix/var/nix/profiles/system --list-generations 
  11   2020-11-22 22:27:47   
  12   2020-11-22 23:34:26   (current)
```

Il est alors possible de faire un `nixos-rebuild switch --rollback` pour revenir sur la configuration précédente. 

En fait, les générations sont également accessibles depuis le bootloader. On peut donc rebooter la machine et sélectionner la génération à utiliser, depuis l'amorceur.

![](images/tuto47-generations-small.png)

# Nettoyer les générations systèmes

Avec NixOS, tout paquet ou configuration est stocké sous forme de dossier dans `/nix/store/`. L'environnement courant est simplement un ensemble de liens symboliques vers des éléments du store. Lorsqu'on "désinstalle" un paquet Nix, seuls les liens symboliques sont supprimés; les éléments du store sont conservés.

Ainsi, pour nettoyer complètement les générations inutilisées, il faut nettoyer le store, supprimer les générations et mettre à jour les entrées du bootloader : 

```
# nix-collect-garbage -d
# nix-env -p /nix/var/nix/profiles/system --delete-generations 
# nixos-rebuild switch
```

# Les vérifications automatiques

Si NixOS permet de revenir facilement à des configurations précédentes, il effectue même des vérifications pour éviter de générer des configurations invalides.

Par exemple, soit la configuration suivante du serveur graphique : 

```nix
  services = {
    xserver = {
      enable = true;
      layout = "fr";
      displayManager.lightdm.enable = true;
      ...
```

Nix vérifie que les options spécifiées existent bien. Par exemple, si on remplace `layout` par `layoute`,

```nix
      layoute = "fr";
```

alors on obtient l'erreur suivante :

```
# nixos-rebuild switch 
building the system configuration...
error: The option `services.xserver.layoute' does not exist. Definition values:
- In `/etc/nixos/configuration.nix': "fr"
(use '--show-trace' to show detailed location information)
```

Nix vérifie également les valeurs de certaines options. Par exemple, si on remplace le layout `fr` par `fre`,

```nix
      layout = "fre";
```

on obtient l'erreur :

```
# nixos-rebuild switch 
building Nix...
building the system configuration...
[...]

The value `fre' for keyboard layout is invalid.

Please check the definition in `services.xserver.layout'.

Detailed XKB compiler errors:
[...]
```

Enfin, Nix vérifie certaines assertions, notamment des dépendances entre options. Par exemple, si on désactive le serveur graphique (tout en laissant activer le gestionnaire de connection `lightdm`).

```nix
      enable = false;
```

on obtient l'erreur :

```
# nixos-rebuild switch 
building Nix...
building the system configuration...
error: 
Failed assertions:
- LightDM requires services.xserver.enable to be true

(use '--show-trace' to show detailed location information)
```

# Conclusion

NixOS est basé sur un système d'environnements immuables, reproductibles et composables, ce qui permet, assez naturellement, de naviguer dans l'historique des mises-à-jour. De plus, comme ce système est déclaratif, Nix peut effectuer certaines vérifications et donc éviter de générer des configurations invalides.

Tout ceci permet de simplifier les mises-à-jour du système, sans avoir à utiliser un système de snapshots supplémentaire ni de prendre des précautions particulières avant de lancer la commande de mise-à-jour. Cependant, le système de mise-à-jour de NixOS ne se substitue pas non plus à un système de sauvegarde de fichiers.


