-- transparence référentielle

-- fonction pure
f1 :: Int -> Int
f1 x = x*2

-- fonction "monadique"
f2 :: Int -> IO Int
f2 x = do
    putStrLn "On est dans f2 !"
    return (x*2)


main :: IO ()
main = do

    let v1 = f1 21    -- appelle une fonction pure (avec transp. ref.)
    print v1

    v2 <- f2 21       -- appelle une fonction monadique (sans transp. ref.)
    print v2

