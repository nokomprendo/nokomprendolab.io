-- totalité d'une fonction

{-# Language NoImplicitPrelude #-}
{-# Language OverloadedStrings #-}

import Relude

-- fonction partiel
myhead1 :: [a] -> a
myhead1 (x : _) = x
myhead1 _ = error "empty list"

-- fonction totale, via un Maybe
myhead2 :: [a] -> Maybe a
myhead2 (x : _) = Just x
myhead2 _ = Nothing

-- fonction totale, via NonEmpty
myhead3 :: NonEmpty a -> a
myhead3 (x :| _) = x


main :: IO ()
main = do

    print $ myhead1 ([1,2] :: [Int])
    -- print $ myhead1 ([] :: [Int])            -- erreur à l'exécution

    print $ myhead2 ([1,2] :: [Int])            -- pas d'erreur, mais résultat
    print $ myhead2 ([] :: [Int])               -- encapsulé dans un Maybe

    case myhead2 ([1,2] :: [Int]) of
        Nothing -> putStrLn "liste vide"
        Just v -> print v

    print $ myhead3 (1:|[2] :: NonEmpty Int)
    -- print $ myhead3 ([] :: NonEmpty Int)     -- erreur à la compilation

