#include <iostream>

int f1(int x) {
    return x*2;
}

int f2(int x) {
    std::cout << "On est dans f2 !\n";    // effet de bord
    return x*2;
}

int main() {
    std::cout << f1(21) << std::endl;
    // équivalent à : std::cout << 42 << std::endl;

    std::cout << f2(21) << std::endl;
    // différent de : std::cout << 42 << std::endl;
    // (car f2 fait un affichage supplémentaire)

    return 0;
}

