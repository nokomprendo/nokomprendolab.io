set terminal png size 400,400
set style data boxplot
set style fill solid 0.5 border -1
set style boxplot nooutliers
set xtics nomirror
set ytics nomirror
set yrange [0:]
set grid

set out 'out-time.png'
set title 'Computation time'
set xtics ('C++' 1, 'Haskell' 2)
plot 'out-cpp.csv' using (1):6 notitle, 'out-hs.csv' using (2):6 notitle

