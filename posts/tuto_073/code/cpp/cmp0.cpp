#include "cmp.hpp"

#include <iostream>

int main() {
    const int nGames = 100;
    const int nRepeats = 10;
    BotMc botR(500);
    BotMcts botY(1000);
    std::cout << "winR winY tie ry ryt dt nGames\n";
    for (int i=0; i<nRepeats; i++) {
        auto [ winR, winY, tie, dt ] = run(botR, botY, nGames);
        std::cout << winR << ' ' << winY << ' ' << tie << ' ' 
            << winR+winY << ' ' << winR+winY+tie << ' ' 
            << dt << ' ' << nGames << '\n';
    }
    return 0;
}

