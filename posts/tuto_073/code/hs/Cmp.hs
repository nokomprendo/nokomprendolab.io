{-# Language FlexibleContexts, FlexibleInstances, MultiParamTypeClasses #-}

module Cmp (run) where

import Bot
import Game

import Control.Monad.ST

{-# INLINE run #-}
run :: (Bot s b1, Bot s b2)
    => b1 -> b2 -> Int -> ST s (Double, Double, Double)
run botR botY nGames = 
    let aux 0 r y t _g0 = 
            let nGamesD = fromIntegral nGames
                rD = fromIntegral r
                yD = fromIntegral y
                tD = fromIntegral t
            in return (rD/nGamesD, yD/nGamesD, tD/nGamesD)
        aux n r y t g0 = do
            g1 <- nextGame g0
            s1 <- playoutBots botR botY g1
            case s1 of
                WinR -> aux (n-1) (r+1) y t g1
                WinY -> aux (n-1) r (y+1) t g1
                Tie  -> aux (n-1) r y (t+1) g1
                _ -> error "game not terminated"
    in mkGame PlayerR >>= aux nGames (0::Int) (0::Int) (0::Int)

