let

  rev = "d6fe7f78a8739d825ba1176d2d3ecf596368f7a5"; # 2021-05-16
  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz") {};

  ghc = pkgs.haskellPackages.ghcWithPackages (ps: with ps; [

    hspec
    massiv
    mwc-random
    timeit
    vector

  ]);

in pkgs.stdenv.mkDerivation {
  name = "haskell-env";
  buildInputs = [
    pkgs.evince
    ghc 
    pkgs.llvm_9
  ];
  shellHook = "eval $(egrep ^export ${ghc}/bin/ghc)";
}

