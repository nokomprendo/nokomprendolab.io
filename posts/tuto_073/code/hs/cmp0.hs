import Bot
import Cmp

import Control.Monad
import Control.Monad.ST
import System.Random.MWC
import System.TimeIt

main :: IO ()
main = do
    let nGames = 100
        nRepeats = 10
    botR <- BotMc 500 <$> createSystemRandom
    botY <- BotMcts 1000 <$> createSystemRandom
    putStrLn "winR WinY tie ry ryt dt nGames"
    forM_ [1 .. nRepeats::Int] $ \_i -> do
        (dt, (r, y, t)) <- timeItT $ stToIO (run botR botY nGames)
        putStrLn $ unwords (map show [r, y, t, r+y, r+y+t, dt] ++ [show nGames])


