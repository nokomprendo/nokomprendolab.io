-- TODO verifier

import Control.Monad.Cont

data Tree a
  = Node a (Tree a) (Tree a)
  | Empty

data SearchFun r m a = SearchFun
  { _success :: a -> ContT r m a
  , _failure :: ContT r m a
  }

searchCont :: Monad m => (a -> Bool) -> Tree a -> SearchFun r m a -> ContT r m a
searchCont _p Empty fs = _failure fs
searchCont p (Node x l r) fs = 
  if p x
  then _success fs x
  else searchCont p l fs { _failure = searchCont p r fs }

searchIO :: (Eq a, Show a) => a -> Tree a -> IO ()
searchIO x t = 
  let success n = ContT $ const $ putStrLn $ "found " <> show n
      failure = ContT $ const $ putStrLn "not found"
  in runContT (searchCont (==x) t $ SearchFun success failure) (const $ return ())

searchPure :: (Eq a) => a -> Tree a -> Maybe a
searchPure x t = 
  let success x = ContT $ const $ Just x
      failure = ContT $ const Nothing
  in runContT (searchCont (==x) t $ SearchFun success failure) Just

main :: IO ()
main = do
  let t1 = Node (13::Integer)
            (Node 12 Empty Empty)
            (Node 42 
              (Node 37 Empty Empty)
              Empty)

  searchIO 37 t1 
  searchIO 36 t1

  print $ searchPure 37 t1 
  print $ searchPure 36 t1 

