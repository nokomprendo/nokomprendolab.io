import Control.Monad.Cont

-------------------------------------------------------------------------------
-- pure
-------------------------------------------------------------------------------

add :: Int -> Int -> Int
add x y = x+y

mul2 :: Int -> Int
mul2 x = x*2

mul2add1 :: Int -> Int
mul2add1 = add 1 . mul2

-------------------------------------------------------------------------------
-- CPS
-------------------------------------------------------------------------------

addCps :: Int -> Int -> (Int -> r) -> r
addCps x y k = k (x+y)

mul2Cps :: Int -> (Int -> r) -> r
mul2Cps x k = k (x*2)

mul2add1Cps :: Int -> (Int -> r) ->r
mul2add1Cps x k = mul2Cps x (\y -> addCps 1 y k)

mul2add1' :: Int -> Int 
mul2add1' x = mul2Cps x (\y -> addCps 1 y id)

-------------------------------------------------------------------------------
-- Cont
-------------------------------------------------------------------------------

addCont :: Int -> Int -> Cont r Int
addCont x y = return (x+y)

mul2Cont :: Int -> Cont r Int
mul2Cont = cont . mul2Cps

mul2add1Cont :: Int -> Cont r Int
mul2add1Cont x = mul2Cont x >>= addCont 1

mul2add1Cont' :: Int -> Cont r Int
mul2add1Cont' = mul2Cont >=> addCont 1

-------------------------------------------------------------------------------
-- ContT
-------------------------------------------------------------------------------

addContT :: Int -> Int -> ContT r m Int
addContT x y = return (x+y)

mul2ContT :: Int -> ContT r m Int
mul2ContT = ContT . mul2Cps

mul2add1ContT :: Int -> ContT r m Int
mul2add1ContT = mul2ContT >=> addContT 1

mul2add1ContTIO :: MonadIO m => Int -> ContT r m Int
mul2add1ContTIO x = do
  y <- mul2ContT x 
  z <- addContT 1 y
  liftIO $ putStrLn $ "mul2add1ContTIO: " <> show z
  return z

-------------------------------------------------------------------------------
-- MonadCont
-------------------------------------------------------------------------------

addC :: Monad m => Int -> Int -> m Int
addC x y = return (x+y)

mul2C :: Monad m => Int -> m Int
mul2C x = runContT (ContT $ mul2Cps x) return

mul2add1C :: Monad m => Int -> m Int
mul2add1C = mul2C >=> addC 1

mul2add1CIO :: MonadIO m => Int ->  m Int
mul2add1CIO x = do
  y <- mul2C x 
  z <- addC 1 y
  liftIO $ putStrLn $ "mul2add1CIO: " <> show z
  return z

-- >>> runContT (mul2add1CIO 3) print
-- mul2add1CIO: 7
-- 7
--

-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

main :: IO ()
main = do

  print $ add 1 $ mul2 3
  print $ mul2add1 3

  print $ mul2add1' 3
  mul2Cps 3 (\r -> addCps 1 r print)
  mul2add1Cps 3 print

  runCont (mul2Cont 3 >>= addCont 1) print
  runCont (mul2add1Cont 3) print
  runCont (mul2add1Cont' 3) print

  runContT (mul2ContT 3 >>= addContT 1) print
  runContT (mul2add1ContT 3) print

  runContT (mul2add1ContTIO 3) print

  runContT (mul2add1C 3) print
  runContT (mul2add1CIO 3) print

