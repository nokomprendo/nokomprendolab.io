import Control.Monad.Cont
import System.Environment (getArgs)

-------------------------------------------------------------------------------
-- non-tail-recursive
-------------------------------------------------------------------------------

factRec :: Integer -> Integer
factRec 0 = 1
factRec n = n * factRec (n-1)

-------------------------------------------------------------------------------
-- tail-recursive
-------------------------------------------------------------------------------

factTailRec :: Integer -> Integer
factTailRec n = go n 1
  where
    go :: Integer -> Integer -> Integer
    go 0 acc = acc
    go k acc = go (k-1) (k*acc)

-------------------------------------------------------------------------------
-- cps
-------------------------------------------------------------------------------

factCps :: Integer -> (Integer -> r) -> r
factCps 0 k = k 1
factCps n k = factCps (n-1) (\x -> k (n*x))

-------------------------------------------------------------------------------
-- cont 
-------------------------------------------------------------------------------

factCont :: Integer -> Cont r Integer
factCont n0 = cont $ go n0
  where
    go :: Integer -> (Integer -> r) -> r
    go 0 k = k 1
    go n k = go (n-1) (\x -> k (n*x))

_factCont :: Integer -> Cont r Integer
_factCont 0 = return 1
_factCont n = (*n) <$> factCont (n-1)

__factCont :: Integer -> Cont r Integer
__factCont = cont . factCps

___factCont :: Integer -> Cont r Integer
___factCont 0 = return 1
___factCont n = runCont (factCont (n-1)) (\x -> return (n*x))

-------------------------------------------------------------------------------
-- main 
-------------------------------------------------------------------------------

main :: IO ()
main = do
  args <- getArgs
  case args of
    ["factRec", nStr] -> print $ factRec $ read nStr
    ["factTailRec", nStr] -> print $ factTailRec $ read nStr
    ["factCps", nStr] -> factCps (read nStr) print
    ["factCont", nStr] -> runCont (factCont (read nStr)) print
    _ -> putStrLn "usage: <factRec|factTailRec|factCps|factCont> <n>"

