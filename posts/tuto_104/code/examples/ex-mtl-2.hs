-- https://hackage.haskell.org/package/mtl-2.3.1/docs/Control-Monad-Cont.html

import Control.Monad.Cont

-- Returns a string depending on the length of the name parameter.
-- If the provided string is empty, returns an error.
-- Otherwise, returns a welcome message.
whatsYourName :: String -> String
whatsYourName name =
  (`runCont` id) $ do                      -- 1
    callCC $ \exit -> do       -- 2
      validateName name exit               -- 3
      return $ "Welcome, " ++ name ++ "!"  -- 4

validateName :: (Applicative f) => String -> (String -> f ()) -> f ()
validateName name exit = do
  when (null name) (exit "You forgot to tell me your name!")

main :: IO ()
main = do
  putStrLn (whatsYourName "toto")
  putStrLn (whatsYourName "")

