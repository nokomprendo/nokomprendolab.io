
mapCps :: (a -> b) -> [a] -> ([b] -> r) -> r
mapCps _ [] k = k []
mapCps f (x:xs) k = mapCps f xs (\ys -> k (f x : ys))

main :: IO ()
main = do
  mapCps (*2) [12, 37::Integer] print

