-- TODO verifier

import Control.Monad.Cont

data Tree a
  = Node a (Tree a) (Tree a)
  | Empty

data SearchFun a b = SearchFun
  { _success :: a -> b
  , _failure :: b
  }

searchCont :: Monad m => (a -> Bool) -> Tree a -> SearchFun a (m b) -> ContT b m b
searchCont _p Empty fs = ContT $ const $ _failure fs
searchCont p (Node x l r) fs = 
  if p x
  then ContT $ const $ _success fs x
  else searchCont p l fs { _failure = runContT (searchCont p r fs) return }

searchIO :: (Eq a, Show a) => a -> Tree a -> IO ()
searchIO x t = 
  let success n = putStrLn $ "found " <> show n
      failure = putStrLn "not found"
  in runContT (searchCont (==x) t $ SearchFun success failure) return

searchPure :: (Eq a) => a -> Tree a -> Maybe a
searchPure x t = runContT (searchCont (==x) t $ SearchFun Just Nothing) return

main :: IO ()
main = do
  let t1 = Node (13::Integer)
            (Node 12 Empty Empty)
            (Node 42 
              (Node 37 Empty Empty)
              Empty)

  searchIO 37 t1 
  searchIO 36 t1

  print $ searchPure 37 t1 
  print $ searchPure 36 t1 

