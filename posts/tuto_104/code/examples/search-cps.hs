
data Tree a
  = Node a (Tree a) (Tree a)
  | Empty

searchCps :: 
  (a -> Bool) -> 
  Tree a -> 
  (a -> r) -> 
  (() -> r) -> 
  r
searchCps _p Empty _sc fc = fc ()
searchCps p (Node x l r) sc fc = 
  if p x
  then sc x
  else searchCps p l sc (\() -> searchCps p r sc fc)

searchIO :: (Eq a) => a -> Tree a -> IO ()
searchIO x t = searchCps (==x) t (const $ putStrLn "found") (const $ putStrLn "not found")

main :: IO ()
main = do
  let t1 = Node (13::Integer)
            (Node 12 Empty Empty)
            (Node 42 
              (Node 37 Empty Empty)
              Empty)

  searchCps (==37) t1 print (const $ putStrLn "not found")
  searchCps (==36) t1 print (const $ putStrLn "not found")

  searchIO 37 t1 
  searchIO 36 t1 

