import Control.Monad.Cont
import System.IO

askString :: (String -> ContT () IO String) -> ContT () IO String
askString next = do
  liftIO $ putStrLn "Please enter a string"
  s <- liftIO getLine
  next s

reportResult :: String -> IO ()
reportResult s = do
  putStrLn ("You entered: " ++ s)

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  runContT (callCC askString) reportResult


