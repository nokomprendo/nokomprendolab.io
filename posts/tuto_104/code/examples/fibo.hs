import Control.Monad.Cont
import System.Environment (getArgs)

-------------------------------------------------------------------------------
-- non-tail-recursive
-------------------------------------------------------------------------------

fiboRec :: Integer -> Integer
fiboRec 0 = 0
fiboRec 1 = 1
fiboRec n = fiboRec (n-1) + fiboRec (n-2)

-------------------------------------------------------------------------------
-- tail-recursive
-------------------------------------------------------------------------------

fiboTailRec :: Integer -> Integer
fiboTailRec n = go n 0 1
  where
    go :: Integer -> Integer -> Integer -> Integer
    go 0 fnm2 _fnm1 = fnm2
    go k fnm2 fnm1 = go (k-1) fnm1 (fnm1+fnm2)

-------------------------------------------------------------------------------
-- cps
-------------------------------------------------------------------------------

fiboCps :: Integer -> (Integer -> r) -> r
fiboCps n0 f0 = go n0 (f0 . fst)
  where
    go :: Integer -> ((Integer, Integer) -> r) -> r
    go 0 k = k (0,1)
    go n k = go (n-1) (\(fnm2, fnm1) -> k (fnm1, fnm1+fnm2))

-------------------------------------------------------------------------------
-- cont
-------------------------------------------------------------------------------

fiboCont1 :: Integer -> Cont r Integer
fiboCont1 = cont . fiboCps

fiboCont2 :: Integer -> Cont r Integer
fiboCont2 n0 = fst <$> cont (go n0)
  where
    go :: Integer -> ((Integer, Integer) -> r) -> r
    go 0 k = k (0,1)
    go n k = go (n-1) (\(fnm2, fnm1) -> k (fnm1, fnm1+fnm2))

fiboCont3 :: Integer -> Cont r Integer
fiboCont3 n0 = fst <$> go n0
  where
    go :: Integer -> Cont r (Integer, Integer)
    go 0 = return (0,1)
    go n = (\(fnm2, fnm1) -> (fnm1, fnm1+fnm2)) <$> go (n-1) 

-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

main :: IO ()
main = do
  args <- getArgs
  case args of
    ["fiboRec", nStr] -> print $ fiboRec $ read nStr
    ["fiboTailRec", nStr] -> print $ fiboTailRec $ read nStr
    ["fiboCps", nStr] -> fiboCps (read nStr) print
    ["fiboCont1", nStr] -> runCont (fiboCont1 (read nStr)) print
    ["fiboCont2", nStr] -> runCont (fiboCont2 (read nStr)) print
    ["fiboCont3", nStr] -> runCont (fiboCont3 (read nStr)) print
    _ -> putStrLn "usage: <fiboRec|fiboTailRec|fiboCps|fiboCont1|fiboCont2|fiboCont3> <n>"


