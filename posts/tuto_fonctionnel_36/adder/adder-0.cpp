#include <functional>
#include <iostream>
#include <numeric>
#include <vector>

using namespace std;

struct Adder {
    int _result;
    Adder(const vector<int> & values) {
        _result = accumulate(values.begin(), values.end(), 0, plus<int>());
    }
};

int main() {
    Adder a1({1});
    cout << a1._result << endl;
    Adder a3({1,2,3});
    cout << a3._result << endl;
    return 0;
}

