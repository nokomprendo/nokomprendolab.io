#include <functional>
#include <iostream>
#include <numeric>
#include <vector>

using namespace std;

template <int ... XS>
struct Adder {
    static const int _result = (0 + ... + XS);
};

int main() {
    Adder<1> a1;
    cout << a1._result << endl;
    Adder<1,2,3> a3;
    cout << a3._result << endl;
    cout << Adder<1,2,3>::_result << endl;
    return 0;
}

