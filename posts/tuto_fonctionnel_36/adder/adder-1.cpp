#include <functional>
#include <iostream>
#include <numeric>
#include <vector>

using namespace std;

template<int ... XS> struct Adder;

template<int X> 
struct Adder<X> {
    static const int _result = X;
};

template<int X, int ... XS> 
struct Adder<X, XS ...> {
    static const int _result = X + Adder<XS...>::_result;
};

int main() {
    Adder<1> a1;
    cout << a1._result << endl;
    Adder<1,2,3> a3;
    cout << a3._result << endl;
    cout << Adder<1,2,3>::_result << endl;
    return 0;
}

