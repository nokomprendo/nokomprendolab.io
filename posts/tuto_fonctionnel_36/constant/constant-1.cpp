#include <iostream>

using namespace std;

template <int V>
struct Constant {
    Constant() = default;
    int getValue() const { return V; } 
};

int main() {

    Constant<1> c1;
    cout << c1.getValue() << endl;

    Constant<2> c2;
    cout << c2.getValue() << endl;

    return 0;
}


