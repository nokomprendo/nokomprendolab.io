#include <iostream>

using namespace std;

struct Constant {
    const int _value;
    Constant(int value) : _value(value) {}
    int getValue() const { return _value; } 
};

int main() {

    Constant c1(1);
    cout << c1.getValue() << endl;

    Constant c2(2);
    cout << c2.getValue() << endl;

    return 0;
}

