#include <algorithm>
#include <cassert>
#include <functional>
#include <numeric>
#include <vector>

using namespace std;

struct Tensor {

    vector<int> _dims;
    vector<double> _data;

    explicit Tensor(const vector<int> & dims) : _dims(dims) {
        int n = accumulate(_dims.begin(), _dims.end(), 1, multiplies<int>());
        _data.resize(n);
    }

    void fill(const double & v) {
        std::fill(_data.begin(), _data.end(), v);
    }

    int ind(const vector<int> & inds) const {
        assert(inds.size() == _dims.size());
        assert(inds[0]<_dims[0]);
        int k = inds[0];
        for (unsigned i=1; i<_dims.size(); i++) {
            assert(inds[i]<_dims[i]);
            k = k * _dims[i] + inds[i];
        }
        return k;
    }

    double & operator()(const vector<int> & is) {
        return _data[ind(is)];
    }

    const Tensor & operator+=(const Tensor & t) {
        assert(_data.size() == t._data.size());
        for (unsigned i=0; i<t._data.size(); i++)
            _data[i] +=  t._data[i];
        return *this;
    }

};

Tensor operator*(double k, const Tensor & t1) {
    Tensor t2(t1._dims);
    for (unsigned i=0; i<t1._data.size(); i++)
        t2._data[i] = k * t1._data[i];
    return t2;
}

#include <iostream>

int main() {

    Tensor t1({2,3,4});
    t1.fill(7.0);
    Tensor t2({2,3,4});
    t2.fill(3.0);
    Tensor t3 = 2.0 * t1;

    t1 += t2;
    cout << t1({1, 2, 3}) << endl;
    cout << t2({1, 2, 3}) << endl;
    cout << t3({1, 2, 3}) << endl;

    t1({1, 2, 3}) = 0.0;
    cout << t1({1, 2, 3}) << endl;

    Tensor t4({2});
    t4.fill(22.0);
    Tensor t5({2});
    t5.fill(20.0);
    t4 += t5; 
    Tensor t6 = 2.0 * t4;
    cout << t6({1}) << endl;

    // t1 += t4;    // erreur détectée à l'exécution

    return 0;
}

