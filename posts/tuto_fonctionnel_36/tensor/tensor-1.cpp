#include <array>

using namespace std;

template<size_t ... XS> struct Tensor;

template<size_t X> 
struct Tensor<X> {

    using Type = array<double, X>;
    Type _data;

    double & operator[](unsigned i) {
        return _data[i];
    }

    const Tensor<X> & operator+=(const Tensor<X> & t) {
        _data += t._data;
        return *this;
    }

    void fill(const double & v) {
        _data.fill(v);
    }
};

template<size_t X, size_t ... XS> 
struct Tensor<X, XS...> {

    using NestedType = Tensor<XS...>;
    using Type = array<NestedType, X>;
    Type _data;

    NestedType & operator[](unsigned i) {
        return _data[i];
    }

    const Tensor<X, XS...> & operator+=(const Tensor<X, XS...> & t) {
        _data += t._data;
        return *this;
    }

    void fill(const double & v) {
        for (auto & t : _data)
            t.fill(v);
    }
}; 

template <typename T, size_t X>
const array<T, X> & operator+=(array<T, X> & t1, const array<T, X> & t2) {
    for (unsigned i=0; i<X; i++)
        t1[i] += t2[i];
    return t1;
}

template<size_t X, size_t ... XS> 
const Tensor<X, XS...> operator*(double k, const Tensor<X, XS...> & t1) {
    Tensor<X, XS...> t2;
    t2._data = k * t1._data;
    return t2;
}

template <typename T, size_t X>
const array<T, X> operator*(double k, const array<T, X> & t1) {
    array<T, X> t2;
    for (unsigned i=0; i<X; i++)
        t2[i] = k * t1[i];
    return t2;
}

#include <iostream>

int main() {

    Tensor<2,3,4> t1;
    t1.fill(7.0);
    Tensor<2,3,4> t2;
    t2.fill(3.0);
    Tensor<2,3,4> t3 = 2.0 * t1;

    t1 += t2;
    cout << t1[1][2][3] << endl;
    cout << t2[1][2][3] << endl;
    cout << t3[1][2][3] << endl;

    t1[1][2][3] = 0.0;
    cout << t1[1][2][3] << endl;

    Tensor<2> t4;
    t4.fill(22.0);
    Tensor<2> t5;
    t5.fill(20.0);
    t4 += t5; 
    Tensor<2> t6 = 2.0 * t4;
    cout << t6[1] << endl;

    // t1 += t4;    // erreur détectée à la compilation

    return 0;
}

