#include <algorithm>
#include <array>
#include <cassert>

using namespace std;

template <int ... XS>
struct Tensor {

    static const int _K = (1 * ... * XS);
    array<double, _K> _data;
    static const int _D = sizeof...(XS);
    static constexpr array<int, _D> _dims {XS...};

    static_assert(_D > 0);

    void fill(const double & v) {
        _data.fill(v);
    }

    int ind(const array<int, _D> & inds) const {
        assert(inds[0]<_dims[0]);
        int k = inds[0];
        for (int i=1; i<_D; i++) {
            assert(inds[i]<_dims[i]);
            k = k*_dims[i] + inds[i];
        }
        return k;
    }

    double & operator()(const array<int, _D> & inds) {
        return _data[ind(inds)];
    }

    const Tensor<XS...> & operator+=(const Tensor<XS...> & t) {
        transform(_data.begin(), _data.end(), t._data.begin(),
                _data.begin(), plus<double>());
        return *this;
    }
};

template <int ... XS>
const Tensor<XS...> operator*(double k, const Tensor<XS...> & t1) {
    Tensor<XS...> t2;
    for (int i=0; i<t1._K; i++)
        t2._data[i] = k * t1._data[i];
    return t2;
}

#include <iostream>

int main() {

    Tensor<2,3,4> t1;
    t1.fill(7.0);
    Tensor<2,3,4> t2;
    t2.fill(3.0);
    Tensor<2,3,4> t3 = 2.0 * t1;

    t1 += t2;
    cout << t1({1,2,3}) << endl;
    cout << t2({1,2,3}) << endl;
    cout << t3({1,2,3}) << endl;

    t1({1,2,3}) = 0.0;
    cout << t1({1,2,3}) << endl;

    Tensor<2> t4;
    t4.fill(22.0);
    Tensor<2> t5;
    t5.fill(20.0);
    t4 += t5; 
    Tensor<2> t6 = 2.0 * t4;
    cout << t6({1}) << endl;

    // t1 += t4;    // erreur détectée à la compilation

    return 0;
}

