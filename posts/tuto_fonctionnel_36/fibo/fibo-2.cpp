#include <iostream>
using namespace std;

template <int N> int fibo() {
  return fibo<N-1>() + fibo<N-2>();
}

template<> int fibo<0>() {
    return 0;
}

template<> int fibo<1>() {
    return 1;
};

int main() {
    cout << fibo<0>() << endl;
    cout << fibo<1>() << endl;
    cout << fibo<2>() << endl;
    cout << fibo<3>() << endl;
    cout << fibo<42>() << endl;
    return 0;
}

