#include <iostream>
using namespace std;

template <int N> struct Fibo {
  static const int value = Fibo<N-1>::value + Fibo<N-2>::value;
};

template<> struct Fibo<0> {
    static const int value = 0;
};

template<> struct Fibo<1> {
    static const int value = 1;
};

int main() {
    cout << Fibo<0>::value << endl;
    cout << Fibo<1>::value << endl;
    cout << Fibo<2>::value << endl;
    cout << Fibo<3>::value << endl;
    cout << Fibo<42>::value << endl;
    return 0;
}

