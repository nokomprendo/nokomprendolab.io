#include <cassert>
#include <iostream>
using namespace std;

int fibo(int n) {
    assert(n >= 0);
    return n <= 1 ? n : fibo(n-1) + fibo(n-2);
}

int main() {
    cout << fibo(0) << endl;
    cout << fibo(1) << endl;
    cout << fibo(2) << endl;
    cout << fibo(3) << endl;
    cout << fibo(42) << endl;
    return 0;
}

