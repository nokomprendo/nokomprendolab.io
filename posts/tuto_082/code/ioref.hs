{-# LANGUAGE OverloadedStrings #-}

import Control.Monad (replicateM_)
import Control.Monad.IO.Class (liftIO)
import Data.IORef
import Lucid
import Web.Scotty

data Model = Model
    { _counter1 :: Int
    , _counter2 :: Int
    } deriving (Show)


incrementCounter1 :: Model -> Model
incrementCounter1 (Model c1 c2) = Model (1+c1) c2

incrementCounter1Ref :: IORef Model -> IO ()
incrementCounter1Ref modelRef = 
    atomicModifyIORef' modelRef $ \m -> (incrementCounter1 m, ())


updateCounters :: Model -> Model
updateCounters (Model c1 c2) = Model (c1 `mod` 10) (c1 `div` 10 + c2)

updateCountersRef :: IORef Model -> IO ()
updateCountersRef modelRef =
    atomicModifyIORef' modelRef $ \m -> (updateCounters m, ())


incrementAndUpdateRef :: IORef Model -> IO ()
incrementAndUpdateRef modelRef =
    atomicModifyIORef' modelRef $ \m -> (updateCounters (incrementCounter1 m), ())


test :: IO ()
test = do
    modelRef <- newIORef (Model 0 0)

    incrementCounter1Ref modelRef
    updateCountersRef modelRef

    incrementAndUpdateRef modelRef

    replicateM_ 2 $ incrementAndUpdateRef modelRef

    readIORef modelRef >>= print


main :: IO ()
main = do

    modelRef <- newIORef (Model 0 0)

    scotty 3000 $ do

        get "/" $ do
            Model c1 c2 <- liftIO $ readIORef modelRef
            html $ renderText $ do
                div_ $ toHtml $ show c1 <> " " <> show c2
                div_ $ form_ [action_ "/inc", method_ "post"] $ 
                    input_ [type_ "submit", value_ "increment"]

        post "/inc" $ do
            liftIO $ incrementAndUpdateRef modelRef
            redirect "/"

