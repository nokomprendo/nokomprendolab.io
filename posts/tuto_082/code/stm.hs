{-# LANGUAGE OverloadedStrings #-}

import Control.Concurrent.STM
import Control.Monad (replicateM_)
import Control.Monad.IO.Class (liftIO)
import Lucid
import Web.Scotty

data Model = Model
    { _counter1 :: Int
    , _counter2 :: Int
    } deriving (Show)


incrementCounter1 :: TVar Model -> STM ()
incrementCounter1 modelVar =
    modifyTVar modelVar $ \(Model c1 c2) -> Model (1+c1) c2

updateCounters :: TVar Model -> STM ()
updateCounters modelVar = do
    (Model c1 c2) <- readTVar modelVar
    writeTVar modelVar $ Model (c1 `mod` 10) (c1 `div` 10 + c2)

incrementAndUpdate :: TVar Model -> STM ()
incrementAndUpdate modelVar = do
    incrementCounter1 modelVar
    updateCounters modelVar


test :: IO ()
test = do
    modelVar <- newTVarIO (Model 0 0)

    model2 <- atomically $ do

                incrementCounter1 modelVar
                updateCounters modelVar

                incrementAndUpdate modelVar

                replicateM_ 2 $ incrementAndUpdate modelVar

                readTVar modelVar

    print model2


main :: IO ()
main = do

    modelVar <- newTVarIO (Model 0 0)

    scotty 3000 $ do

        get "/" $ do
            Model c1 c2 <- liftIO $ readTVarIO modelVar
            html $ renderText $ do
                div_ $ toHtml $ show c1 <> " " <> show c2
                div_ $ form_ [action_ "/inc", method_ "post"] $ 
                    input_ [type_ "submit", value_ "increment"]

        post "/inc" $ do
            liftIO $ atomically $ incrementAndUpdate modelVar
            redirect "/"

