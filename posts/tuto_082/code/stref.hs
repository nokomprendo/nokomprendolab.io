
import Control.Monad (replicateM_)
import Control.Monad.ST
import Data.STRef

data Model = Model
    { _counter1 :: Int
    , _counter2 :: Int
    } deriving (Show)

incrementCounter1 :: STRef s Model -> ST s ()
incrementCounter1 modelRef =
    modifySTRef' modelRef $ \(Model c1 c2) -> Model (1+c1) c2

updateCounters :: STRef s Model -> ST s ()
updateCounters modelRef =
    modifySTRef' modelRef $ 
        \(Model c1 c2) -> Model (c1 `mod` 10) (c1 `div` 10 + c2)

incrementAndUpdate :: STRef s Model -> ST s ()
incrementAndUpdate modelRef = do
    incrementCounter1 modelRef
    updateCounters modelRef

test :: IO ()
test = do
    let res = runST $ do
                modelRef <- newSTRef (Model 0 0)

                incrementCounter1 modelRef
                updateCounters modelRef

                incrementAndUpdate modelRef

                replicateM_ 2 $ incrementAndUpdate modelRef

                readSTRef modelRef

    print res

main :: IO ()
main = test

