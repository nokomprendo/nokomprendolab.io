{-# LANGUAGE OverloadedStrings #-}

import Control.Concurrent.MVar
import Control.Monad (replicateM_)
import Control.Monad.IO.Class (liftIO)
import Lucid
import Web.Scotty

data Model = Model
    { _counter1 :: Int
    , _counter2 :: Int
    } deriving (Show)


incrementCounter1 :: Model -> Model
incrementCounter1 (Model c1 c2) = Model (1+c1) c2

incrementCounter1Var :: MVar Model -> IO ()
incrementCounter1Var modelVar = 
    modifyMVar_ modelVar (return . incrementCounter1)


updateCounters :: Model -> Model
updateCounters (Model c1 c2) = Model (c1 `mod` 10) (c1 `div` 10 + c2)

updateCountersVar :: MVar Model -> IO ()
updateCountersVar modelVar =
    modifyMVar_ modelVar (return . updateCounters)


incrementAndUpdateVar :: MVar Model -> IO ()
incrementAndUpdateVar modelVar =
    modifyMVar_ modelVar (return . updateCounters . incrementCounter1)


test :: IO ()
test = do
    modelVar <- newMVar (Model 0 0)

    incrementCounter1Var modelVar
    updateCountersVar modelVar

    incrementAndUpdateVar modelVar

    replicateM_ 2 $ incrementAndUpdateVar modelVar

    model2 <- readMVar modelVar

    print model2


main :: IO ()
main = do

    modelVar <- newMVar (Model 0 0)

    scotty 3000 $ do

        get "/" $ do
            Model c1 c2 <- liftIO $ readMVar modelVar
            html $ renderText $ do
                div_ $ toHtml $ show c1 <> " " <> show c2
                div_ $ form_ [action_ "/inc", method_ "post"] $ 
                    input_ [type_ "submit", value_ "increment"]

        post "/inc" $ do
            liftIO $ incrementAndUpdateVar modelVar
            redirect "/"

