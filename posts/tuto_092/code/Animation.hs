{-# LANGUAGE TemplateHaskell #-}

import Control.Lens
import Control.Monad.State.Strict
import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
import Linear.V2
import Linear.Vector
import System.Random

-------------------------------------------------------------------------------
-- params
-------------------------------------------------------------------------------

winWidth, winHeight :: Int
winWidth = 400
winHeight = 300

winWidthF, winHeightF, xMin, xMax, yMin, yMax, ballRadius :: Float
winWidthF = fromIntegral winWidth
winHeightF = fromIntegral winHeight
xMin =  ballRadius - 0.5 * winWidthF
xMax = -ballRadius + 0.5 * winWidthF
yMin =  ballRadius - 0.5 * winHeightF
yMax = -ballRadius + 0.5 * winHeightF
ballRadius = 20

-------------------------------------------------------------------------------
-- types
-------------------------------------------------------------------------------

data Ball = Ball 
  { _pos :: V2 Float
  , _vel :: V2 Float
  }

newtype Model = Model
  { _ball :: Ball
  }

instance Random Ball where
  randomR (Ball al bl, Ball ah bh) = runState $
    Ball <$> state (randomR (al, ah)) 
         <*> state (randomR (bl, bh))

  random = randomR ( Ball (V2 xMin yMin) (V2 (-500) (-500))
                   , Ball (V2 xMax yMax) (V2   500    500))

-------------------------------------------------------------------------------
-- application
-------------------------------------------------------------------------------

main :: IO ()
main = do
  let ball = Ball (V2 0 0) (V2 20 200)
      model = Model ball
      window = InWindow "Animation" (winWidth, winHeight) (0, 0)
      bgcolor = makeColor 0.4 0.6 1.0 1.0
      fps = 30 
  play window bgcolor fps model handleDisplay handleEvent handleTime

handleDisplay :: Model -> Picture
handleDisplay model = 
  let (V2 x y) = _pos $ _ball model
  in Pictures
      [ translate x y (circleSolid ballRadius)
      , rectangleWire winWidthF winHeightF ]

handleEvent :: Event -> Model -> Model
-- handleEvent (EventKey (SpecialKey KeyEnter) Up _ _) model =
handleEvent _ model = model

handleTime :: Float -> Model -> Model
handleTime deltaTime model = 
  let ball1 = updateMotion deltaTime $ _ball model
      ball2 = updateBounces ball1
  in model { _ball = ball2 }

updateMotion :: Float -> Ball -> Ball
updateMotion deltaTime (Ball pos vel) = Ball (pos + vel ^* deltaTime) vel

updateBounces :: Ball -> Ball
updateBounces ball0 = ball4
  where
    (V2 px py) = _pos ball0
    (V2 vx vy) = _vel ball0
    ball1 = if xMin >= px
                then Ball (V2 (2*xMin - px) py) (V2 (-vx) vy)
                else ball0
    ball2 = if xMax <= px
                then Ball (V2 (2*xMax - px) py) (V2 (-vx) vy)
                else ball1
    ball3 = if yMin >= py 
                then Ball (V2 px (2*yMin - py)) (V2 vx (-vy))
                else ball2
    ball4 = if yMax <= py 
                then Ball (V2 px (2*yMax - py)) (V2 vx (-vy))
                else ball3

