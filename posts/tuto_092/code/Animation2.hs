{-# LANGUAGE TemplateHaskell #-}

import Control.Lens
import Control.Monad.State.Strict
import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
import Linear.V2
import Linear.Vector
import System.Random

-------------------------------------------------------------------------------
-- params
-------------------------------------------------------------------------------

winWidth, winHeight :: Int
winWidth = 400
winHeight = 300

winWidthF, winHeightF, xMin, xMax, yMin, yMax, ballRadius :: Float
winWidthF = fromIntegral winWidth
winHeightF = fromIntegral winHeight
xMin =  ballRadius - 0.5 * winWidthF
xMax = -ballRadius + 0.5 * winWidthF
yMin =  ballRadius - 0.5 * winHeightF
yMax = -ballRadius + 0.5 * winHeightF
ballRadius = 20

-------------------------------------------------------------------------------
-- types
-------------------------------------------------------------------------------

data Ball = Ball 
  { _pos :: V2 Float
  , _vel :: V2 Float
  }
makeLenses ''Ball

data Model = Model
  { _ball :: Ball
  , _nextBalls :: [Ball]
  }
makeLenses ''Model

instance Random Ball where
  randomR (Ball al bl, Ball ah bh) = runState $
    Ball <$> state (randomR (al, ah)) 
         <*> state (randomR (bl, bh))

  random = randomR ( Ball (V2 xMin yMin) (V2 (-500) (-500))
                   , Ball (V2 xMax yMax) (V2   500    500))

-------------------------------------------------------------------------------
-- application
-------------------------------------------------------------------------------

main :: IO ()
main = do
  (ball0 : balls) <- randoms <$> getStdGen
  let model = Model ball0 balls
      window = InWindow "Animation2" (winWidth, winHeight) (0, 0)
      bgcolor = makeColor 0.4 0.6 1.0 1.0
      fps = 30 
  play window bgcolor fps model handleDisplay handleEvent handleTime

handleDisplay :: Model -> Picture
handleDisplay model = 
  let (V2 x y) = model ^. ball . pos
  in Pictures
      [ translate x y (circleSolid ballRadius)
      , rectangleWire winWidthF winHeightF ]

handleEvent :: Event -> Model -> Model
handleEvent (EventKey (SpecialKey KeyEnter) Up _ _) model = 
  model & ball .~ head (model ^. nextBalls)
        & nextBalls %~ tail  
handleEvent _ model = model

handleTime :: Float -> Model -> Model
handleTime deltaTime model = 
  model & ball %~ updateMotion deltaTime
        & ball %~ updateBounces

updateMotion :: Float -> Ball -> Ball
updateMotion deltaTime ball0 = ball0 & pos +~ ball0 ^. vel ^* deltaTime

updateBounces :: Ball -> Ball
updateBounces ball0 = ball4
  where
    (V2 x y) = ball0 ^. pos
    ball1 = if xMin >= x
                then ball0 & pos . _x .~ 2*xMin - x
                           & vel . _x %~ negate
                else ball0
    ball2 = if xMax <= x
                then ball1 & pos . _x .~ 2*xMax - x
                           & vel . _x %~ negate
                else ball1
    ball3 = if yMin >= y 
                then ball2 & pos . _y .~ 2*yMin - y
                           & vel . _y %~ negate
                else ball2
    ball4 = if yMax <= y 
                then ball3 & pos . _y .~ 2*yMax - y
                           & vel . _y %~ negate
                else ball3

