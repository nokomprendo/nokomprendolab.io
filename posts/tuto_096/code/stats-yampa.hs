
{-# LANGUAGE Arrows #-}

import FRP.Yampa


sumSF :: Num a => SF a a
sumSF = sscan (+) 0

lengthSF :: Num b => SF a b
lengthSF = const 1 ^>> sumSF

-- maxSF :: (Ord a, Bounded a) => SF a a
-- maxSF = sscan max minBound


meanSF1 :: Fractional a => SF a a
meanSF1 = proc input -> do
    s <- sumSF -< input
    n <- lengthSF -< input
    --ou: n <- sumSF -< 1
    returnA -< (s / n)

meanSF2 :: Fractional a => SF a a
meanSF2 = sumSF &&& lengthSF >>^ uncurry (/)


main :: IO ()
main = do
    
    let xs = deltaEncode 0 [2, 3, 1::Int]
    print $ embed lengthSF xs
    print $ embed sumSF xs
    -- print $ embed maxSF xs

    let ys = deltaEncode 0 [2, 3, 1::Double]
    print $ embed meanSF1 ys
    print $ embed meanSF2 ys

