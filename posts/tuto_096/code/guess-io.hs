
import Control.Monad
import Text.Read (readMaybe)
import System.Random

maxTries, minGuess, maxGuess :: Int
maxTries = 7
minGuess = 0
maxGuess = 100

data GameStatus 
    = TooLow
    | TooHigh 
    | Win 
    | Lose 
    deriving (Eq, Show)

data GameState = GameState
    { _target :: Int
    , _tries :: Int
    }

mkGameState :: Int -> GameState
mkGameState target = GameState target 0

play :: Int -> GameState -> (GameStatus, GameState)
play x (GameState t a)
    | x == t        = (Win, GameState t a)
    | a >= maxTries = (Lose, GameState t a)
    | x < t         = (TooLow, GameState t (a+1))
    | otherwise     = (TooHigh, GameState t (a+1))

runGuess :: GameState -> IO ()
runGuess gs0 = do
    putStrLn ("\nnumber (" ++ show (maxTries - _tries gs0) ++ " lives)?")
    xm <- readMaybe <$> getLine
    case xm of
        Nothing -> runGuess gs0
        Just x -> do
            let (result, gs1) = play x gs0
            print result
            when (result /= Win && result /= Lose) (runGuess gs1)

main :: IO ()
main = do
    target <- randomRIO (minGuess, maxGuess)
    runGuess (mkGameState target)

