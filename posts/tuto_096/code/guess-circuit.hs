{-# LANGUAGE Arrows #-}

import Control.Arrow
import System.Random
import Text.Read

import Circuit

maxTries, minGuess, maxGuess :: Int
maxTries = 7
minGuess = 0
maxGuess = 100

data GameStatus 
    = TooLow
    | TooHigh 
    | Win 
    | Lose 
    deriving (Eq, Show)

delayedEcho :: a -> Circuit a a
delayedEcho acc = accum acc (\a b -> (b,a))

total :: Num a => Circuit a a
total = accum_ 0 (+)

guess :: Int -> Circuit String (Bool, [String]) 
guess target = proc input -> do
    let mNb = readMaybe input 
    tries <- updateTries -< (target, mNb)
    result <- updateResults -< (target, mNb, tries)
    end <- delayedEcho True -< result /= Just Win && result /= Just Lose
    let promptNb = "\nnumber (" ++ show (maxTries - tries) ++ " lives)?"
    let prompt = case result of
            Nothing     -> [promptNb]
            Just Lose   -> [show Lose]
            Just Win    -> [show Win]
            Just r      -> [show r, promptNb]
    returnA -< (end, prompt)

updateResults :: Circuit (Int, Maybe Int, Int) (Maybe GameStatus)
updateResults = proc (target, mNb, tries) -> do
    case mNb of
        Nothing -> returnA -< Nothing
        Just nb -> 
            let res | nb == target      = Win
                    | tries >= maxTries = Lose
                    | nb < target       = TooLow
                    | otherwise         = TooHigh
            in returnA -< Just res

updateTries :: Circuit (Int, Maybe Int) Int
updateTries = proc (target, mNb) -> do
    total -< case mNb of
        Nothing -> 0
        Just nb -> if nb == target then 0 else 1

main :: IO ()
main = do
    target <- randomRIO (minGuess, maxGuess)
    input <- getContents
    putStrLn 
        $ unlines
        $ concatMap snd 
        $ takeWhile fst
        $ runCircuit (guess target)
        $ ("":)
        $ lines input

