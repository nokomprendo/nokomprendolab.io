
-- https://en.wikibooks.org/wiki/Haskell/Arrow_tutorial

{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TupleSections #-}

module Circuit where

import Control.Arrow
import Control.Category qualified as Cat

-------------------------------------------------------------------------------
-- type
-------------------------------------------------------------------------------

newtype Circuit a b = Circuit (a -> (Circuit a b, b))
-- newtype Circuit a b = Circuit { unC :: a -> (Circuit a b, b) }


instance Cat.Category Circuit where

  id :: Circuit a a
  id = Circuit (Cat.id,)

  (.) :: Circuit b c -> Circuit a b -> Circuit a c
  Circuit f2 . Circuit f1 = Circuit $ \a ->
    let (cir1, b) = f1 a
        (cir2, c) = f2 b
    in (cir2 Cat.. cir1, c)


instance Arrow Circuit where

  arr :: (a -> b) -> Circuit a b
  arr f = Circuit $ \a -> (arr f, f a)

  first :: Circuit a b -> Circuit (a, c) (b, c)
  first (Circuit f) = Circuit $ \(a, c) ->
    let (cir, b) = f a
    in (first cir, (b, c))


instance ArrowChoice Circuit where

  left :: Circuit a b -> Circuit (Either a c) (Either b c)
  left cir@(Circuit f) = Circuit $ \case
    Left a -> let (cir', c) = f a
              in  (left cir', Left c)
    Right a -> (left cir, Right a)


runCircuit :: Circuit a b -> [a] -> [b]
runCircuit _ [] = []
runCircuit (Circuit f) (a:as) =
  let (cir, b) = f a
  in b : runCircuit cir as
-- runCircuit f = snd . mapAccumL unC f

-------------------------------------------------------------------------------
-- helpers
-------------------------------------------------------------------------------

-- Accumulator that outputs a value determined by the supplied function.
accum :: acc -> (a -> acc -> (b, acc)) -> Circuit a b
accum acc f = Circuit $ \a ->
  let (b, acc') = f a acc
      cir = accum acc' f
  in (cir, b)

-- Accumulator that outputs the accumulator value.
accum_ :: b -> (a -> b -> b) -> Circuit a b
accum_ acc f = accum acc (\a b -> let b' = f a b in (b', b'))

