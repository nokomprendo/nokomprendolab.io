{-# LANGUAGE Arrows #-}
import Control.Arrow
import Control.Monad
import Data.Maybe
import FRP.Yampa
import System.Random
import Text.Read

maxTries, minGuess, maxGuess :: Int
maxTries = 7
minGuess = 0
maxGuess = 100

data GameStatus 
    = TooLow
    | TooHigh 
    | Win 
    | Lose 
    deriving (Eq, Show)

-------------------------------------------------------------------------------
-- sample
-------------------------------------------------------------------------------

newtype Sample = Sample { _input :: Maybe Int }

initialSample :: IO Sample
initialSample = return (Sample Nothing)

nextSample :: Bool -> IO (DTime, Maybe Sample)
nextSample _ = do
    input <- readMaybe <$> getLine
    return (0, Just (Sample input))

outputResult :: Bool -> (Int, Maybe GameStatus) -> IO Bool
outputResult _ (tries, mRes) = do
    forM_ mRes print
    if mRes == Just Win || mRes == Just Lose
        then return True
        else do
            putStrLn $ "\nnumber (" ++ show (maxTries - tries) ++ " lives)?"
            return False

-------------------------------------------------------------------------------
-- guess
-------------------------------------------------------------------------------

guess :: Int -> SF Sample (Int, Maybe GameStatus)
guess target = proc (Sample input) -> do
    let countInput x = if isNothing x then 0 else 1
    totalTries <- sscan (+) 0 -< countInput input
    let result = updateResult target totalTries <$> input
    returnA -< (totalTries, result)

updateResult :: Int -> Int -> Int -> GameStatus
updateResult target tries nb
    | nb == target      = Win
    | tries >= maxTries = Lose
    | nb < target       = TooLow
    | otherwise         = TooHigh

-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

main :: IO ()
main = do
    target <- randomRIO (minGuess, maxGuess)
    reactimate initialSample nextSample outputResult (guess target)

-- reactimate :: IO a -> (Bool -> IO (DTime, Maybe a)) -> (Bool -> b -> IO Bool) -> SF a b -> IO ()

-- https://hackage.haskell.org/package/Yampa-0.14.2/docs/FRP-Yampa.html
-- https://github.com/ivanperez-keera/Yampa/

