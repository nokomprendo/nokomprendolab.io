
{-# LANGUAGE Arrows #-}

import Control.Arrow

import Circuit


sumC :: Num a => Circuit a a
sumC = accum_ 0 (+)

lengthC :: Num b => Circuit a b
lengthC = arr (const 1) >>> sumC
-- ou: lengthC = const 1 ^>> sumC

-- maxC :: (Ord a, Bounded a) => Circuit a a
-- maxC = accum_ minBound max


meanC1 :: Fractional a => Circuit a a
meanC1 = proc input -> do
    s <- sumC -< input
    n <- lengthC -< input
    -- ou: n <- sumC -< 1
    returnA -< (s / n)

meanC2 :: Fractional a => Circuit a a
meanC2 = sumC &&& lengthC >>^ uncurry (/)


main :: IO ()
main = do
    let xs = [2, 3, 1::Int]
    print $ runCircuit lengthC xs
    print $ runCircuit sumC xs
    -- print $ runCircuit maxC xs

    let ys = [2, 3, 1::Double]
    print $ runCircuit meanC1 ys
    print $ runCircuit meanC2 ys

