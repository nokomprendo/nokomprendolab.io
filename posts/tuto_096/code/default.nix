
let

  Yampa-src = fetchTarball {
    url = https://github.com/ivanperez-keera/Yampa/archive/refs/tags/v0.14.2.tar.gz;
    sha256 = "sha256:1rzaavdwl0f2qjhda660xm9hw70p22a2iygazbkf6b3bryzb0rsc";
  };

  yampa-gloss-src = fetchTarball {
    url = https://github.com/ivanperez-keera/yampa-gloss/archive/refs/tags/v0.2.1.tar.gz;
    sha256 = "sha256:1wrcm9da0dk54lyhnqd0iri98pihxdsj4k50dfhxiiiwa6apfsyb";
  };

  config = {
    allowBroken = true;
    packageOverrides = pkgz: rec {
      haskellPackages = pkgz.haskellPackages.override {
        overrides = hpNew: hpOld: with pkgs.haskell.lib; {

          Yampa = dontCheck (doJailbreak (
              hpNew.callCabal2nix "Yampa" "${Yampa-src}/yampa" {}
            ));

          yampa-gloss = dontCheck (doJailbreak (
              hpNew.callCabal2nix "yampa-gloss" yampa-gloss-src {}
            ));

        };
      };
    };
  };

  pkgs = import <nixpkgs> { inherit config; };

  ghc = pkgs.haskellPackages;
  # ghc = pkgs.haskell.packages.ghc943;

in ghc.developPackage {
  root = ./.;

  withHoogle = false;

  modifier = drv:
    # pkgs.haskell.lib.dontHaddock (
      pkgs.haskell.lib.addBuildTools drv (with ghc; [
        cabal-install
    ]);
    #]));
}

