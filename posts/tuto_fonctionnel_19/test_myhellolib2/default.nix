with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "test_myhellolib2";
  src = ./.;
  buildInputs = [ myhellolib ];

  buildPhase = "g++ -o test_myhellolib2 test_myhellolib.cpp -l myhellolib";

  installPhase = ''
      mkdir -p $out/bin
      cp test_myhellolib2 $out/bin/
  '';
}

