#include <iostream>
#include <Eigen/Dense>

int main() {
  auto m = Eigen::MatrixXd::Random(2, 3);
  std::cout << m << std::endl;

  std::cout << EIGEN_WORLD_VERSION << '.'
   << EIGEN_MAJOR_VERSION << '.' 
   << EIGEN_MINOR_VERSION << std::endl;
}

