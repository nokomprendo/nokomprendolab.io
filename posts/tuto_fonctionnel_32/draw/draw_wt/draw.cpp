#include <Wt/WApplication.h>
#include <Wt/WContainerWidget.h>
#include <Wt/WEnvironment.h>
#include <Wt/WPaintedWidget.h>
#include <Wt/WPainter.h>
#include <Wt/WPushButton.h>
#include <Wt/WSlider.h>
#include <Wt/WTemplate.h>
#include <Wt/WText.h>
using namespace std;
using namespace Wt;

// a canvas with a pen for drawing pathes 
class Canvas : public WPaintedWidget {
    protected:
        WPen _pen;           // current pen (color + width)
        WPainterPath _path;  // current path

    public:
        Canvas(int penWidth, int width, int height) : WPaintedWidget() {
            resize(width, height);
            _pen.setWidth(penWidth);
            _pen.setCapStyle(PenCapStyle::Round);
            _pen.setJoinStyle(PenJoinStyle::Round);
        }

        // set the width of the pen
        void setWidth(int w) {
            _pen.setWidth(w);
        }

        // set the color of the pen
        // updateFunc defines how to update color (for example, modify the red channel)
        void setColor(function<void(WColor & c)> updateFunc) {
            WColor color = _pen.color();
            updateFunc(color);
            _pen.setColor(color);
        }

        // clear canvas
        void clear() {
            _path = WPainterPath();
            update();
        }
};

// canvas for previewing the brush
class CanvasBrush : public Canvas {
    public:
        CanvasBrush(int penWidth) : Canvas(penWidth, 50, 50) {}

    private:
        // paint event: draw a point in the middle of the canvas
        void paintEvent(WPaintDevice * paintDevice) override {
            WPainter painter(paintDevice);
            painter.setPen(_pen);
            _path.moveTo(25, 25);
            _path.lineTo(25, 25);
            painter.strokePath(_path, _pen);
        }
};

// canvas for drawing
class CanvasDraw : public Canvas {
    public:
        CanvasDraw(int penWidth, int width, int height) : Canvas(penWidth, width, height) {
            WCssDecorationStyle deco;
            deco.setBorder(WBorder(BorderStyle::Solid));
            setDecorationStyle(deco);
            // connect event handlers
            mouseDragged().connect(this, &CanvasDraw::mouseDrag);
            mouseWentDown().connect(this, &CanvasDraw::mouseDown);
        }

    private:
        void paintEvent(WPaintDevice * paintDevice) override {
            // draw current path
            WPainter painter(paintDevice);
            painter.setPen(_pen);
            painter.drawPath(_path);
        }

        void mouseDown(const WMouseEvent & e) {
            // start path from current position 
            Coordinates c = e.widget();
            _path = WPainterPath(WPointF(c.x, c.y));
        }

        void mouseDrag(const WMouseEvent & e) {
            // add current position into the path
            Coordinates c = e.widget();
            _path.lineTo(c.x, c.y);
            update(PaintFlag::Update);
        }
};

// slider that prints its value and calls and updating function when moved
class Slider : public WContainerWidget {
    private:
        WSlider * _slider;
        WText * _text;

    public:
        Slider(const string & title, int vmin, int vmax, int vinit) {
            addWidget(make_unique<WText>(title));
            _slider = addWidget(make_unique<WSlider>());
            _text = addWidget(make_unique<WText>(to_string(vinit)));
            _slider->setRange(vmin, vmax);
            _slider->setValue(vinit);
        }

        void setUpdateFunc(function<void(int)> updateFunc) {
            _slider->sliderMoved().connect([=](int v) {
                // call updating function
                updateFunc(v);
                // update slider text
                _text->setText(to_string(v));
            });
        }
};

// main template of the application
const string APP_TEMPLATE = R"(
    <h1>Draw</h1>
    <p> ${slider_r} </p>
    <p> ${slider_g} </p>
    <p> ${slider_b} </p>
    <p> ${slider_width} </p>
    <p> ${canvas_brush} </p>
    <p> ${canvas_draw} </p>
    <p> ${button_clear} </p>
)";

struct App : WApplication {

    App(const WEnvironment & env) : WApplication(env) {
        auto tmpl = root()->addWidget(make_unique<WTemplate>(APP_TEMPLATE));
        auto canvasBrush = tmpl->bindWidget("canvas_brush", make_unique<CanvasBrush>(10));
        auto canvasDraw = tmpl->bindWidget("canvas_draw", make_unique<CanvasDraw>(10, 640, 480));

        auto clearButton = tmpl->bindWidget("button_clear", make_unique<WPushButton>("Clear"));
        clearButton->clicked().connect(canvasDraw, &CanvasDraw::clear);

        // add a slider for the red color channel of the pen
        auto sliderR = tmpl->bindWidget("slider_r", make_unique<Slider>("R", 0, 255, 0));
        // set an updating function that assigns the value v to the red channel
        sliderR->setUpdateFunc([=](int v) {
            canvasDraw->setColor([v](WColor & c) { c.setRgb(v, c.green(), c.blue()); });
            canvasBrush->setColor([v](WColor & c) { c.setRgb(v, c.green(), c.blue()); });
            canvasBrush->update();
        });

        auto sliderG = tmpl->bindWidget("slider_g", make_unique<Slider>("G", 0, 255, 0));
        sliderG->setUpdateFunc([=](int v) {
            canvasDraw->setColor([v](WColor & c) { c.setRgb(c.red(), v, c.blue()); });
            canvasBrush->setColor([v](WColor & c) { c.setRgb(c.red(), v, c.blue()); });
            canvasBrush->update();
        });

        auto sliderB = tmpl->bindWidget("slider_b", make_unique<Slider>("B", 0, 255, 0));
        sliderB->setUpdateFunc([=](int v) {
            canvasDraw->setColor([v](WColor & c) { c.setRgb(c.red(), c.green(), v); });
            canvasBrush->setColor([v](WColor & c) { c.setRgb(c.red(), c.green(), v); });
            canvasBrush->update();
        });

        auto sliderWidth = tmpl->bindWidget("slider_width", make_unique<Slider>("Width", 2, 50, 10));
        sliderWidth->setUpdateFunc([=](int v) {
            canvasDraw->setWidth(v);
            canvasBrush->setWidth(v);
            canvasBrush->update();
        });
    }
};

// start the application
int main(int argc, char **argv) {
    auto mkApp = [](const WEnvironment& env) { return make_unique<App>(env); };
    return WRun(argc, argv, mkApp);
}

