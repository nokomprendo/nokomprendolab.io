with import <nixpkgs> {};

emscriptenStdenv.mkDerivation {
  name = "draw_sdl";
  src = ./.;
  buildInputs = [
    gnumake
    emscripten
  ];
}

