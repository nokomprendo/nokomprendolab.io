{-# LANGUAGE OverloadedStrings #-}
import Control.Monad (when)
import Data.Map (singleton)
import JavaScript.Web.Canvas
import Miso
import Miso.String hiding (singleton)

main :: IO ()
main = startApp App 
    { initialAction = UpdateBrushOp
    , update = updateModel 
    , view = viewModel
    , model  = Model 0 0 0 10 (0, 0) False
    , subs   = [ mouseSub UpdateDrawOp ]
    , events = defaultEvents
    , mountPoint = Nothing }

data Model = Model
    { modelR :: Int                -- brush color
    , modelG :: Int
    , modelB :: Int
    , modelRadius :: Int           -- brush radius
    , modelXy :: (Double, Double)  -- last position inside drawing canvas
    , modelDrawing :: Bool         -- drawing state
    } deriving (Eq)

data Action
    = NoOp
    | ClearDrawOp              -- request clearing canvas_draw
    | UpdateBrushOp            -- update canvas_brush
    | UpdateDrawOp (Int, Int)  -- update canvas_draw
    | UpdateDrawingOp Bool     -- update drawing state
    | UpdateFormOp (Model -> MisoString -> Model) MisoString  -- update a range element
    | UpdateXyOp (Double, Double)  -- update last position

-- View

-- create a range element
mkRange :: MisoString -> Int -> Int -> Int -> (MisoString -> Action) -> View Action
mkRange title vmin vmax v op = p_ [] 
    [ text title
    , input_ 
        [ type_ "range"
        , onInput op
        , min_ (toMisoString vmin)
        , max_ (toMisoString vmax)
        , value_ (toMisoString v) ]
    , text (toMisoString v) ]

-- create view
viewModel :: Model -> View Action
viewModel (Model r g b radius _ _) = span_ []
    [ h1_ [] [ text "draw_miso" ]
    , mkRange "R" 0 255 r (UpdateFormOp (\ m v -> m { modelR = fromMisoString v }))
    , mkRange "G" 0 255 g (UpdateFormOp (\ m v -> m { modelG = fromMisoString v }))
    , mkRange "B" 0 255 b (UpdateFormOp (\ m v -> m { modelB = fromMisoString v }))
    , mkRange "radius" 2 50 radius (UpdateFormOp (\ m v -> m { modelRadius = fromMisoString v }))
    , p_ [] [ canvas_ [ id_ "canvas_brush" , width_ "100" , height_ "100" ] [] ]
    , p_ [] [ canvas_ [ id_ "canvas_draw" , width_ "600" , height_ "300"
                      , style_  (singleton "border" "1px solid black")
                      , onMouseDown (UpdateDrawingOp True), onMouseUp (UpdateDrawingOp False) ] [] ]
    , p_ [] [ button_ [ onClick ClearDrawOp ] [ text "Clear" ] ] ]

-- bind javascript foreign functions for accessing canvas

foreign import javascript unsafe "$r = document.getElementById($1).getContext('2d');"
    jsCtx :: MisoString -> IO Context

foreign import javascript unsafe "$r = canvas_draw.getBoundingClientRect().left;"
    jsRectLeft :: IO Int

foreign import javascript unsafe "$r = canvas_draw.getBoundingClientRect().top;"
    jsRectTop :: IO Int

foreign import javascript unsafe "$r = canvas_draw.clientWidth;"
    jsWidth :: IO Int

foreign import javascript unsafe "$r = canvas_draw.clientHeight;"
    jsHeight :: IO Int

-- Update

updateModel :: Action -> Model -> Effect Action Model

updateModel UpdateBrushOp m@(Model r g b radius _ _) = m <# do
    ctx <- jsCtx "canvas_brush"
    clearRect 0 0 100 100 ctx
    lineWidth 1 ctx
    strokeStyle r g b 255 ctx
    fillStyle r g b 255 ctx
    beginPath ctx
    arc 50 50 (fromIntegral radius) 0 (2*pi) True ctx
    fill ctx
    stroke ctx
    pure NoOp

updateModel ClearDrawOp m = m <# do
    ctx <- jsCtx "canvas_draw"
    w <- jsWidth
    h <- jsHeight
    clearRect 0 0 (fromIntegral w) (fromIntegral h) ctx
    pure NoOp

updateModel (UpdateDrawOp (x1, y1)) m@(Model r g b radius (x0, y0) drawing) = m <# do
    ctx <- jsCtx "canvas_draw"
    left <- jsRectLeft
    top <- jsRectTop
    let x1' = fromIntegral $ x1 - left
    let y1' = fromIntegral $ y1 - top
    when drawing $ do
        lineWidth 0 ctx
        strokeStyle r g b 255 ctx
        fillStyle r g b 255 ctx
        lineWidth (fromIntegral $ 2*radius) ctx
        lineCap LineCapRound ctx
        beginPath ctx
        moveTo x0 y0 ctx
        lineTo x1' y1' ctx
        stroke ctx
    pure $ UpdateXyOp (x1', y1')  -- request to update drawing position

updateModel (UpdateXyOp xy) m = noEff m { modelXy = xy }  -- update the field modelXy
updateModel (UpdateFormOp mFunc value) m = mFunc m value <# pure UpdateBrushOp
updateModel (UpdateDrawingOp drawing) m = noEff m { modelDrawing = drawing }
updateModel NoOp m = noEff m

