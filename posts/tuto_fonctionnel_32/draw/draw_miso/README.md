
- shell:

```
nix-shell
cabal new-build
ln -sf $(find dist-newstyle -name all.js) .
firefox index.html

cabal new-build
```

- build:

```
nix-build
firefox result/index.html
```

