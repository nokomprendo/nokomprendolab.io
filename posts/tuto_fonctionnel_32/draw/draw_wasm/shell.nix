with import <nixpkgs> {};

emscriptenStdenv.mkDerivation {
  name = "draw";
  src = ./.;
  nativeBuildInputs = [
    gnumake
    emscripten
  ];
}

