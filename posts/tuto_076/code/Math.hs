module Math where


mul2Num x = x*2

mul2Int :: Int -> Int
mul2Int x = x*2


class Expr repr where
    val :: (Num a, Show a) => a -> repr a
    add :: (Num a) => repr a -> repr a -> repr a
    mul :: (Num a) => repr a -> repr a -> repr a

newtype Eval a = Eval { unEval :: a }

instance Expr Eval where
    val x = Eval x
    add x y = Eval $ unEval x + unEval y
    mul x y = Eval $ unEval x * unEval y

expr1 :: Expr repr => repr Int
expr1 = mul (val 21) (add (val 1) (val 1)) 

