
// g++ cps1a.cpp && ./a.out

#include <iostream>

int add(int x, int y) {
  return x+y;
}

int isNull(int x) {
  return x==0;
}

std::string fmtBool(bool x) {
  return x ? "true" : "false";
}

int main() {
  auto r1 =
    fmtBool(
      isNull(
        add(1, 2)
      )
    );
  std::cout << r1 << std::endl;

  return 0;
}



