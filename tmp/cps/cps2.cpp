
// g++ cps2.cpp && ./a.out

#include <functional>
#include <iostream>

///////////////////////////////////////////////////////////////////////////////

template <typename T> 
using BoolCps = std::function<T(T, T)>;

template <typename T>
BoolCps<T> trueP = [](T const & x, T const & _y) { return x; };

template <typename T>
BoolCps<T> falseP = [](T const & _x, T const & y) { return y; };

template <typename T>
T matchBool(BoolCps<T> p, T x, T y) { return p(x, y); }

///////////////////////////////////////////////////////////////////////////////

using VoidFunc = std::function<void()>;

using BoolCps_ = std::function<VoidFunc(VoidFunc,VoidFunc)>;

BoolCps_ trueP_ = [](VoidFunc f1, VoidFunc _f2) { return f1; };

BoolCps_ falseP_ = [](VoidFunc _f1, VoidFunc f2) { return f2; };

void matchBool_(BoolCps_ p, VoidFunc x, VoidFunc y) { p(x, y)(); }

///////////////////////////////////////////////////////////////////////////////

int main() {

  auto x1 = trueP<std::string>;
  auto r1 = matchBool<std::string>(x1, 
    "vrai", 
    "faux");
  std::cout << r1 << std::endl;

  auto x2 = falseP_;
  matchBool_(x2, 
    [](){ std::cout << "#t" << std::endl; },
    [](){ std::cout << "#f" << std::endl; });

  return 0;
}


