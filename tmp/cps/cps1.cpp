
// g++ cps1.cpp && ./a.out

#include <functional>
#include <iostream>

template <typename T>
T addCps(int x, int y, std::function<T(int)> k) {
  return k(x+y);
}

template <typename T>
T isNullCps(int x, std::function<T(bool)> k) {
  return k(x==0);
}

template <typename T>
T fmtBool(bool x, std::function<T(std::string)> k) {
  return k(x ? "true" : "false");
}

int main() {

  auto r1 = 
    addCps<std::string>( 1, 2, [] (int x) {
      return isNullCps<std::string>( x, [] (bool x) {
        return fmtBool<std::string>( x, [] (std::string const& x) {
          return x;
        });
      });
    });
  std::cout << r1 << std::endl;

  return 0;
}


