{-# LANGUAGE ExistentialQuantification, RankNTypes #-}

newtype Pair a b = Pair { runPair :: forall c. (a -> b -> c) -> c }

mkPair :: a -> b -> Pair a b
mkPair a b = Pair $ \f -> f a b

fstPair :: Pair a b -> a
fstPair p = runPair p (\x y -> x) 

sndPair :: Pair a b -> b
sndPair p = runPair p (\x y -> y)

p1 :: Pair Integer String
p1 = mkPair 42 "foobar"

main :: IO ()
main = do
  print $ fstPair p1
  print $ sndPair p1

