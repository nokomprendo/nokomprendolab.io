
data ShowBox = forall s. Show s => SB s

instance Show ShowBox where
  show (SB x) = show x

heteroList :: [ShowBox]
heteroList = [SB (), SB (42::Integer), SB True]

main :: IO ()
main = do
  print heteroList


