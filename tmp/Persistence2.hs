{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE NoImplicitPrelude #-}

import Control.Monad.Except (MonadError, catchError, throwError)
import Data.List ((!!))
import Relude

data User = User
    { id'  :: Int
    , name :: Text
    } deriving Show

class (Monad m) => MonadUser m where
    getUserById  :: Int -> m User
    listUsers    :: m [User]

class (Monad m) => MonadDB m where
    type Transact m :: Type -> Type
    runTransaction :: Transact m a -> m a

doThings :: (MonadUser m, MonadIO m) => m (User, [User])
doThings = do
    u <- getUserById 1
    putTextLn "got one user"
    us <- listUsers
    pure (u, us)

handler :: forall n .
            ( MonadDB n
            , MonadIO (Transact n)
            , MonadUser (Transact n))
        => n String
handler = do
    r <- runTransaction doThings
    pure $ show r

newtype DBT m a = DBT { unDBT :: ReaderT [User] m a }
    deriving newtype (Functor, Applicative, Monad, MonadTrans, MonadIO)

runDBT :: MonadIO m
        => [User] -> DBT m a -> m a
runDBT us DBT{..} = do
    putTextLn "Running transaction"
    x <- (`runReaderT` us) unDBT
    putTextLn "Transaction over"
    pure x

newtype App a = App { unApp :: ReaderT [User] (ExceptT String IO) a }
    deriving newtype (Functor, Applicative, Monad, MonadReader [User], MonadIO, MonadError String)

instance (Monad m, MonadError String m) => MonadUser (DBT m) where
    getUserById ix = do
        list <- DBT ask
        if length list > ix
        then pure $ list !! ix
        else lift $ throwError $ "no user at " <> show ix

    listUsers = DBT ask

instance MonadDB App where
    type Transact App = DBT App
    runTransaction action = do
        p <- ask
        runDBT p action

newtype Test a = Test { unTest :: IO a }
    deriving newtype (Functor, Applicative, Monad, MonadIO)

instance MonadError String Test where
    throwError e = error (show e)
    catchError (Test t) f = Test . catchError t $ (unTest . f . show)

instance MonadUser Test where
    getUserById n = pure $ User n "toto"
    listUsers = pure [User 0 "toto"]

instance MonadDB Test where
    type Transact Test = Test
    runTransaction = id

main :: IO ()
main = do
    let users = [User 0 "Abott", User 1 "Costello"]

    putTextLn "with app"
    res <- runExceptT $ (`runReaderT` users) $ unApp handler
    case res of
        Left e  -> error (show e)
        Right s -> putStrLn s

    putTextLn "\nwith test"
    unTest handler >>= putStrLn

