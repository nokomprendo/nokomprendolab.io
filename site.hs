{-# LANGUAGE OverloadedStrings #-}

import Data.Monoid (mappend)
import Hakyll
import Text.Pandoc.Options

config :: Configuration
config = defaultConfiguration { destinationDirectory = "public" }


pandocMathCompiler =
    let mathExtensions    = extensionsFromList [
                              Ext_tex_math_dollars, 
                              Ext_tex_math_double_backslash, 
                              Ext_latex_macros ]
        defaultExtensions = writerExtensions defaultHakyllWriterOptions
        newExtensions     = defaultExtensions <> mathExtensions
        writerOptions     = defaultHakyllWriterOptions {
                              writerExtensions = newExtensions,
                              writerHTMLMathMethod = MathJax "" }
    in pandocCompilerWith defaultHakyllReaderOptions writerOptions


feedConfig :: FeedConfiguration
feedConfig = FeedConfiguration
    { feedTitle       = "Tuto fonctionnel"
    , feedDescription = "Des tutos sur la programmation fonctionnelle en général : Haskell, NixOS..."
    , feedAuthorName  = "Nokomprendo"
    , feedAuthorEmail = "nokomprendo@gmail.com"
    , feedRoot        = "https://nokomprendo.gitlab.io"
    }


postCtx :: Context String
postCtx = dateField "date" "%Y-%m-%d" `mappend` defaultContext

main :: IO ()
main = hakyllWith config $ do

  match "css/*" $ do
    route idRoute
    compile compressCssCompiler

  match "posts/*/images/*/*" $ do
    route idRoute
    compile copyFileCompiler

  match "posts/*/images/*" $ do
    route idRoute
    compile copyFileCompiler

  match "posts/*/data/*" $ do
    route idRoute
    compile copyFileCompiler

  match "images/*" $ do
    route idRoute
    compile copyFileCompiler

  match "templates/*" $ compile templateBodyCompiler

  match "posts/*/*-README.md" $ do
    route $ setExtension "html"
    compile $ pandocMathCompiler 
      >>= loadAndApplyTemplate "templates/default.html" postCtx
      >>= relativizeUrls

  match "posts/*/*.md" $ do
    route $ setExtension "html"
    compile $ pandocMathCompiler
      >>= loadAndApplyTemplate "templates/default.html" postCtx
      >>= saveSnapshot "content"
      >>= relativizeUrls

  match "index.html" $ do
    route idRoute
    compile $ do
      posts <- recentFirst =<< loadAll "posts/*/*-README*"
      let indexCtx =
            listField "posts" postCtx (return posts) `mappend`
            constField "title" "Accueil" `mappend`
            defaultContext
      getResourceBody
        >>= applyAsTemplate indexCtx
        >>= loadAndApplyTemplate "templates/default.html" indexCtx
        >>= relativizeUrls

  create ["atom.xml"] $ do
    route idRoute
    compile (feedCompiler renderAtom)

  create ["rss.xml"] $ do
    route idRoute
    compile (feedCompiler renderRss)

feedCompiler renderer = do
    let feedCtx = postCtx `mappend` bodyField "description"
    posts <- recentFirst =<< loadAll "posts/*/*-README.md"
    -- posts <- fmap (take 10) . recentFirst =<< loadAll "posts/*/*-README.md"
    renderer feedConfig feedCtx posts

