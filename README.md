
![](images/logo_tuto_small.png)

# tuto fonctionnel
 
## tuto sur la programmation fonctionnelle au sens large

- langage de programmation (Haskell...)
- gestionnaires de paquets (Nix, Guix...)
- distribution linux (NixOS...)
- ...


## ressources

- [les videos sur peertube](https://peertube.fr/video-channels/tuto_fonctionnel/videos)
- [les videos sur youtube](https://www.youtube.com/channel/UCqxytIRrg8LRl31QuXDXf2A/videos)
- [les articles](https://nokomprendo.gitlab.io)
- [le dépôt git](https://gitlab.com/nokomprendo/nokomprendo.gitlab.io)


## génération du blog

```
nix-shell --run "cabal run site watch"
```

## ffmpeg

```
mylist.txt
file '2021-03-22 13-12-10.mkv'
file '2021-03-22 13-18-38.mkv'

ffmpeg -f concat -safe 0 -i mylist.txt -c copy input.mkv

ffmpeg -i input.mp4 -af "volumedetect" -vn -sn -dn -f null /dev/null
ffmpeg -i input.mp4 -map_channel 0.1.0 -af "volume=6dB" -c:v copy output.mp4

ffmpeg -i input.mkv -i input.wav -c:v copy -c:a aac -map 0:v:0 -map 1:a:0 output.mkv

ffmpeg -ss 00:01 -i in.mp4 -vframes 1 out.png
```

## latex

- [detexify](http://detexify.kirelabs.org/classify.html)



